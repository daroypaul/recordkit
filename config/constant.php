<?php

return [
    'apps_name' => 'RecordKit Modular System - Beta',
    'apps_setup' => 'root',//root/public
    'default_layout' => 'miniside',
    'access_denied_msg' => 'You do not have the permission for this action. Please contact your system administrator',
    'copyright' => '&copy; '.date('Y').' RecordKit by Paul Daroy. All Rights Received',
    'copyright_link' => '&copy; '.date('Y').' <a style="color:#ffffff !important;" href="http://recordkit.co/">RecordKit</a> by Paul Daroy. All Rights Received'
];