<?php

use Illuminate\Database\Seeder;
use App\utilities\Currencies;

class Currency extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            ["currency_code" => "AFN","currency_name" => "Afghanistan Afghani","currency_symbol" => "\u060b"],
            ["currency_code" => "ALL","currency_name" => "Albania Lek","currency_symbol" => "Lek"],
            ["currency_code" => "DZD","currency_name" => "Algeria Dinar","currency_symbol" => "\u062f.\u062c"],
            ["currency_code" => "AOA","currency_name" => "Angola Kwanza","currency_symbol" => "Kz"],
            ["currency_code" => "ARS","currency_name" => "Argentina Peso","currency_symbol" => "$"],
            ["currency_code" => "AMD","currency_name" => "Armenia Dram","currency_symbol" => "\u058f"],
            ["currency_code" => "AWG","currency_name" => "Aruba Guilder","currency_symbol" => "\u0192"],
            ["currency_code" => "AUD","currency_name" => "Australia Dollar","currency_symbol" => "$"],
            ["currency_code" => "AZN","currency_name" => "Azerbaijan New Manat","currency_symbol" => "\u043c\u0430\u043d"],
            ["currency_code" => "BSD","currency_name" => "Bahamas Dollar","currency_symbol" => "$"],
            ["currency_code" => "BHD","currency_name" => "Bahrain Dinar","currency_symbol" => ".\u062f.\u0628"],
            ["currency_code" => "BDT","currency_name" => "Bangladesh Taka","currency_symbol" => "\u09f3"],
            ["currency_code" => "BBD","currency_name" => "Barbadian Dollar","currency_symbol" => "$"],
            ["currency_code" => "BYR","currency_name" => "Belarus Ruble","currency_symbol" => "Br"],
            ["currency_code" => "BZD","currency_name" => "Belize Dollar","currency_symbol" => "BZ$"],
            ["currency_code" => "BMD","currency_name" => "Bermuda Dollar","currency_symbol" => "$"],
            ["currency_code" => "BTN","currency_name" => "Bhutan Ngultrum","currency_symbol" => "Nu."],
            ["currency_code" => "BOB","currency_name" => "Bolivia Boliviano","currency_symbol" => "Bs."],
            ["currency_code" => "BAM","currency_name" => "Bosnia","currency_symbol" => ""],
            ["currency_code" => "BWP","currency_name" => "Botswana Pula","currency_symbol" => "P"],
            ["currency_code" => "BRL","currency_name" => "Brazil Real","currency_symbol" => "R$"],
            ["currency_code" => "BND","currency_name" => "Brunei Darussalam Dollar","currency_symbol" => "$"],
            ["currency_code" => "BGN","currency_name" => "Bulgaria Lev","currency_symbol" => "\u043b\u0432"],
            ["currency_code" => "BIF","currency_name" => "Burundi Franc","currency_symbol" => "FBu"],
            ["currency_code" => "KHR","currency_name" => "Cambodia Riel","currency_symbol" => "\u17db"],
            ["currency_code" => "CAD","currency_name" => "Canada Dollar","currency_symbol" => "$"],
            ["currency_code" => "CVE","currency_name" => "Cape Verde Escudo","currency_symbol" => "CVE"],
            ["currency_code" => "KYD","currency_name" => "Cayman Islands Dollar","currency_symbol" => "\u062f.\u0645."],
            ["currency_code" => "CLP","currency_name" => "Chile Peso","currency_symbol" => "$"],
            ["currency_code" => "CNY","currency_name" => "China Yuan Renminbi","currency_symbol" => "\u00a5"],
            ["currency_code" => "COP","currency_name" => "Colombia Peso","currency_symbol" => "$"],
            ["currency_code" => "KMF","currency_name" => "Comoros Franc","currency_symbol" => "CF"],
            ["currency_code" => "XOF","currency_name" => "Communaut\u00e9 Financi\u00e8re Africaine","currency_symbol" => "XOF"],
            ["currency_code" => "XAF","currency_name" => "Communaut\u00e9 Financi\u00e8re Africaine","currency_symbol" => "XAF"],
            ["currency_code" => "XPF","currency_name" => "Comptoirs Fran\u00e7ais du Pacifique","currency_symbol" => "XPF"],
            ["currency_code" => "CDF","currency_name" => "Congo\/Kinshasa Franc","currency_symbol" => "FC"],
            ["currency_code" => "CRC","currency_name" => "Costa Rica Colon","currency_symbol" => "\u20a1"],
            ["currency_code" => "HRK","currency_name" => "Croatia Kuna","currency_symbol" => "kn"],
            ["currency_code" => "CUC","currency_name" => "Cuba Convertible Peso","currency_symbol" => "CUC$"],
            ["currency_code" => "CUP","currency_name" => "Cuba Peso","currency_symbol" => "$"],
            ["currency_code" => "CZK","currency_name" => "Czech Republic Koruna","currency_symbol" => "K\u010d"],
            ["currency_code" => "DKK","currency_name" => "Denmark Krone","currency_symbol" => "kr"],
            ["currency_code" => "DJF","currency_name" => "Djibouti Franc","currency_symbol" => "Fdj"],
            ["currency_code" => "DOP","currency_name" => "Dominican Republic Peso","currency_symbol" => "RD$"],
            ["currency_code" => "XCD","currency_name" => "East Caribbean Dollar","currency_symbol" => "$"],
            ["currency_code" => "EGP","currency_name" => "Egypt Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "SVC","currency_name" => "El Salvador Colon","currency_symbol" => "$"],
            ["currency_code" => "ERN","currency_name" => "Eritrea Nakfa","currency_symbol" => "Nfk"],
            ["currency_code" => "EEK","currency_name" => "Estonia Kroon","currency_symbol" => "kr"],
            ["currency_code" => "ETB","currency_name" => "Ethiopia Birr","currency_symbol" => "Br"],
            ["currency_code" => "EUR","currency_name" => "Euro Member Countries","currency_symbol" => "\u20ac"],
            ["currency_code" => "FKP","currency_name" => "Falkland Islands (Malvinas) Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "FJD","currency_name" => "Fiji Dollar","currency_symbol" => "$"],
            ["currency_code" => "GMD","currency_name" => "Gambia Dalasi","currency_symbol" => "D"],
            ["currency_code" => "GEL","currency_name" => "Georgia Lari","currency_symbol" => "\u10da"],
            ["currency_code" => "GHS","currency_name" => "Ghana Cedi","currency_symbol" => "\u20b5"],
            ["currency_code" => "GIP","currency_name" => "Gibraltar Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "GTQ","currency_name" => "Guatemala Quetzal","currency_symbol" => "Q"],
            ["currency_code" => "GGP","currency_name" => "Guernsey Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "GNF","currency_name" => "Guinea Franc","currency_symbol" => "Fr"],
            ["currency_code" => "GYD","currency_name" => "Guyana Dollar","currency_symbol" => "$"],
            ["currency_code" => "HTG","currency_name" => "Haiti Gourde","currency_symbol" => "G"],
            ["currency_code" => "HNL","currency_name" => "Honduras Lempira","currency_symbol" => "L"],
            ["currency_code" => "HKD","currency_name" => "Hong Kong Dollar","currency_symbol" => "$"],
            ["currency_code" => "HUF","currency_name" => "Hungary Forint","currency_symbol" => "Ft"],
            ["currency_code" => "ISK","currency_name" => "Iceland Krona","currency_symbol" => "kr"],
            ["currency_code" => "INR","currency_name" => "India Rupee","currency_symbol" => "\u20b9"],
            ["currency_code" => "IDR","currency_name" => "Indonesia Rupiah","currency_symbol" => "Rp"],
            ["currency_code" => "IRR","currency_name" => "Iran Rial","currency_symbol" => "\ufdfc"],
            ["currency_code" => "IQD","currency_name" => "Iraq Dinar","currency_symbol" => "\u062f.\u0639"],
            ["currency_code" => "IMP","currency_name" => "Isle of Man Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "ILS","currency_name" => "Israel Shekel","currency_symbol" => "\u20aa"],
            ["currency_code" => "JMD","currency_name" => "Jamaica Dollar","currency_symbol" => "J$"],
            ["currency_code" => "JPY","currency_name" => "Japan Yen","currency_symbol" => "\u00a5"],
            ["currency_code" => "JEP","currency_name" => "Jersey Pound","currency_symbol" => "\u00a3"],
            ["currency_code" => "JOD","currency_name" => "Jordan Dinar","currency_symbol" => "\u062f.\u0627"],
            ["currency_code" => "KZT","currency_name" => "Kazakhstan Tenge","currency_symbol" => "\u20b8"],
            ["currency_code" => "KES","currency_name" => "Kenya Shilling","currency_symbol" => "KSh"],
            ["currency_code" => "KPW","currency_name" => "Korea (North) Won","currency_symbol" => "\u20a9"],
            ["currency_code" => "KRW","currency_name" => "Korea (South) Won","currency_symbol" => "\u20a9"],
            ["currency_code" => "KWD","currency_name" => "Kuwait Dinar","currency_symbol" => "\u062f.\u0643"],
            ["currency_code" => "KGS","currency_name" => "Kyrgyzstan Som","currency_symbol" => "\u043b\u0432"],
            ["currency_code" => "LAK","currency_name" => "Laos Kip","currency_symbol" => "\u20ad"],
            ["currency_code" => "PHP","currency_name" => "Philippine Peso","currency_symbol" => ""]
        ];

        foreach ($currencies as $currency) {
            Currencies::insert($currency);
        }
    }
}
