<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Utilities\GetMenus as Menus;
use App\Utilities\GetOptions as Options;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
        $this->call(Currency::class);
        $this->call(TimeZone::class);
        //Model::guard();

        //Default Options
        /*$options = [
            [
                "account_id" => 1,
                "option_name" => "apps_cat_type",
                "option_value" => "asset"
            ],
            //[
            //    "account_id" => 1,
            //    "option_name" => "apps_cat_type",
            //    "option_value" => "article"
            //],

            [
                "account_id" => 1,
                "option_name" => "tag_prefix",
                "option_value" => "FA"
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_suffix",
                "option_value" => ""
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_with_padding",
                "option_value" => 1
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_padding_digits",
                "option_value" => 7
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_with_dashed",
                "option_value" => 1
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_with_year",
                "option_value" => 0
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_year_strlen",
                "option_value" => 2
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_insert_year",
                "option_value" => 'AFT_PREF'
            ],
            [
                "account_id" => 1,
                "option_name" => "tag_last_counter",
                "option_value" => 1
            ],
        ];


        Options::insert($options);*/

        //Default Menus
        $menus = [
            [
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "dashboard",
                "menu_label"=> "Dashboard",
                "menu_url"=> "dashboard",
                "menu_icon"=> "mdi mdi-gauge",
                "privilege_key"=> '["exempt"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            /*  Reports Menu */
            [
                //"menu_id"=> 2,
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "reports-main",
                "menu_label"=> "Reports",
                "menu_url"=> "#",
                "menu_icon"=> "mdi mdi-chart-line",
                "privilege_key"=> '["exempt"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 3,
                "account_id"=> 1,
                "parent_menu"=> 2,
                "menu_name"=> "reports-activity-logs",
                "menu_label"=> "Activity Log Reports",
                "menu_url"=> "reports/activity-logs",
                "menu_icon"=> "mdi mdi-timetable m-r-10",
                "privilege_key"=> '["all","report_activitylog"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            /*  Tools Menu */
            [
                //"menu_id"=> 4,
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "tools-main",
                "menu_label"=> "Tools",
                "menu_url"=> "#",
                "menu_icon"=> "mdi mdi-wrench",
                "privilege_key"=> '["exempt"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            /*  Packages Menu */
            [
                //"menu_id"=> 5,
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "package-manager-main",
                "menu_label"=> "Packages",
                "menu_url"=> "#",
                "menu_icon"=> "mdi mdi-package",
                "privilege_key"=> '["all","package_manager"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 6,
                "account_id"=> 1,
                "parent_menu"=> 5,
                "menu_name"=> "package-manager-installed",
                "menu_label"=> "Installed",
                "menu_url"=> "packages/installed",
                "menu_icon"=> "mdi mdi-package-down m-r-10",
                "privilege_key"=> '["exempt"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 7,
                "account_id"=> 1,
                "parent_menu"=> 5,
                "menu_name"=> "package-manager-add",
                "menu_label"=> "Add New",
                "menu_url"=> "packages/add",
                "menu_icon"=> "mdi mdi-plus-circle m-r-10",
                "privilege_key"=> '["exempt"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            /*  Ssystem Profile Menu */
            [
                //"menu_id"=> 8,
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "system-profile-main",
                "menu_label"=> "System Profiles",
                "menu_url"=> "#",
                "menu_icon"=> "mdi mdi-dns",
                "privilege_key"=> '["all","admin_user_view","admin_usergroup_view","admin_site_view","admin_location_view","admin_cat_view","admin_employee_view","admin_dept_view","admin_supplier_view","admin_km_article_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 9,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-users",
                "menu_label"=> "Users",
                "menu_url"=> "admin/users",
                "menu_icon"=> "mdi mdi-account-key m-r-10",
                "privilege_key"=> '["all","admin_user_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 10,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-user-group",
                "menu_label"=> "User Group",
                "menu_url"=> "admin/user-groups",
                "menu_icon"=> "mdi mdi-account-settings-variant m-r-10",
                "privilege_key"=> '["all","admin_usergroup_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 11,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-employee",
                "menu_label"=> "Employees",
                "menu_url"=> "admin/employees",
                "menu_icon"=> "mdi mdi-account-multiple m-r-10",
                "privilege_key"=> '["all","admin_employee_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 12,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-site",
                "menu_label"=> "Sites",
                "menu_url"=> "admin/site",
                "menu_icon"=> "far fa-building m-r-10",
                "privilege_key"=> '["all","admin_site_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 13,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-location",
                "menu_label"=> "Locations",
                "menu_url"=> "admin/location",
                "menu_icon"=> "mdi mdi-map-marker m-r-10",
                "privilege_key"=> '["all","admin_location_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 14,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-category",
                "menu_label"=> "Categories",
                "menu_url"=> "admin/categories",
                "menu_icon"=> "mdi mdi-bookmark m-r-10",
                "privilege_key"=> '["all","admin_cat_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 15,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-department",
                "menu_label"=> "Departments",
                "menu_url"=> "admin/departments",
                "menu_icon"=> "mdi mdi-source-branch m-r-10",
                "privilege_key"=> '["all","admin_dept_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 16,
                "account_id"=> 1,
                "parent_menu"=> 8,
                "menu_name"=> "system-profile-supplier",
                "menu_label"=> "Suppliers",
                "menu_url"=> "admin/suppliers",
                "menu_icon"=> "mdi mdi-basket m-r-10",
                "privilege_key"=> '["all","admin_supplier_view"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            /* Settings Menu */
            [
                //"menu_id"=> 17,
                "account_id"=> 1,
                "parent_menu"=> 0,
                "menu_name"=> "settings-main",
                "menu_label"=> "Settings",
                "menu_url"=> "#",
                "menu_icon"=> "mdi mdi-settings",
                "privilege_key"=> '["all","setting_datafields","setting_system","setting_company_info"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 18,
                "account_id"=> 1,
                "parent_menu"=> 17,
                "menu_name"=> "settings-company",
                "menu_label"=> "Company Information",
                "menu_url"=> "settings/company-info",
                "menu_icon"=> "mdi mdi-information-outline m-r-10",
                "privilege_key"=> '["all","setting_company_info"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 19,
                "account_id"=> 1,
                "parent_menu"=> 17,
                "menu_name"=> "settings-custom-fields",
                "menu_label"=> "Custom Data Fields",
                "menu_url"=> "settings/data-fields",
                "menu_icon"=> "mdi mdi-table-edit m-r-10",
                "privilege_key"=> '["all","setting_datafields"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ],
            [
                //"menu_id"=> 20,
                "account_id"=> 1,
                "parent_menu"=> 17,
                "menu_name"=> "settings-application",
                "menu_label"=> "Application Settings",
                "menu_url"=> "settings/application",
                "menu_icon"=> "mdi mdi-application m-r-10",
                "privilege_key"=> '["all","setting_system"]',
                "menu_order"=> null,
                "is_disable"=> 0
            ]
        ];

        Menus::insert($menus);
    }
}
