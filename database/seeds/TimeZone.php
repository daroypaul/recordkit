<?php

use Illuminate\Database\Seeder;
use App\utilities\TimeZones;

class TimeZone extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timezones = [
            ["timezone_offset" => "-12:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Baker Island Time"],
            ["timezone_offset" => "-11:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Niue Island, Samoa Island"],
            ["timezone_offset" => "-10:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Hawaii"],
            ["timezone_offset" => "-9:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Alaska"],
            ["timezone_offset" => "-8:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Pacific Time (North America)"],
            ["timezone_offset" => "-7:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Mountain Time (North America)"],
            ["timezone_offset" => "-6:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Central Time (North America, Easter Island,Gal\u00e1pagos)"],
            ["timezone_offset" => "-5:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Eastern Time (Acre,Colombia,Cuba,Ecuador,Peru)"],
            ["timezone_offset" => "-4:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Atlantic Time (Bolivia,Chile,Venezuelan)"],
            ["timezone_offset" => "-3:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Argentina, Brasilia,French Guiana,Uruguay"],
            ["timezone_offset" => "-2:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Fernando de Noronha, South Georgia and the South Sandwich Islands"],
            ["timezone_offset" => "-1:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Cape Verde, Greenland , Portugal"],
            ["timezone_offset" => "0:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Ireland"],
            ["timezone_offset" => "+1:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "European Central Time"],
            ["timezone_offset" => "+2:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Eastern European Time,  (Arabic) Egypt Standard Time"],
            ["timezone_offset" => "+3:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Eastern African Time"],
            ["timezone_offset" => "+3:30","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Middle East Time"],
            ["timezone_offset" => "+4:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Near East Time"],
            ["timezone_offset" => "+4:30","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Afghanistan"],
            ["timezone_offset" => "+5:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Pakistan Lahore Time"],
            ["timezone_offset" => "+5:30","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "India Standard Time"],
            ["timezone_offset" => "+5:45","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Nepal"],
            ["timezone_offset" => "+6:00","timezone_hours" => "+6","timezone_minutes" => "0","timezone_name" => "Bangladesh Standard Time"],
            ["timezone_offset" => "+6:30","timezone_hours" => "+6","timezone_minutes" => "30","timezone_name" => "Myanmar"],
            ["timezone_offset" => "+7:00","timezone_hours" => "+7","timezone_minutes" => "","timezone_name" => "Vietnam Standard Time"],
            ["timezone_offset" => "+8:00","timezone_hours" => "+8","timezone_minutes" => "0","timezone_name" => "Taipei, Singapore"],
            ["timezone_offset" => "+9:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => " \tJapan Standard Time"],
            ["timezone_offset" => "+9:30","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Australia Central Time"],
            ["timezone_offset" => "+10:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Australia Eastern Time"],
            ["timezone_offset" => "+11:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => "Solomon Standard Time"],
            ["timezone_offset" => "+12:00","timezone_hours" => "","timezone_minutes" => "","timezone_name" => " \tNew Zealand Standard Time"]
        ];

        foreach ($timezones as $timezone) {
            TimeZones::insert($timezone);
        }
    }
}
