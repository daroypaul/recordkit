<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUserGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_user_group', function (Blueprint $table) {
            $table->increments('user_group_id');
            $table->integer('account_id')->nullable();
            $table->string('group_name', 100)->nullable();
            $table->longText('group_privileges')->nullable();
            $table->string('group_remarks', 1000)->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('is_system_built')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_user_group');
    }
}
