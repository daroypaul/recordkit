<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_activity_logs', function (Blueprint $table) {
            $table->increments('activity_id');
            $table->integer('account_id')->nullable();
            $table->string('action_type', 50)->nullable();
            $table->integer('action_ref_id')->nullable();
            $table->string('activity_desc', 1000)->nullable();
            $table->dateTime('activity_date')->nullable();
            $table->integer('action_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_activity_logs');
    }
}
