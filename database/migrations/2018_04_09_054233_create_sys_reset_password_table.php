<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysResetPasswordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_reset_password', function (Blueprint $table) {
            $table->increments('request_id');
            $table->integer('user_id')->nullable();
            $table->string('username', 100)->nullable();
            $table->string('token', 255)->nullable();
            $table->dateTime('date_requested')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_reset_password');
    }
}
