<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysSysSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_suppliers', function (Blueprint $table) {
            $table->increments('supplier_id');
            $table->integer('account_id')->nullable();
            $table->string('supplier_name', 255)->nullable();
            $table->string('supplier_address', 500)->nullable();
            $table->string('supplier_remarks', 1000)->nullable();
            $table->integer('is_provider')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('record_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_suppliers');
    }
}
