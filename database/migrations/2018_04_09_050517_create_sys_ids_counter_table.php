<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysIdsCounterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('sys_ids_counter', function (Blueprint $table) {
            $table->increments('counter_id');
            $table->integer('account_id')->nullable();
            $table->string('counter_type', 50)->nullable();
            $table->integer('counter_value')->nullable();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_ids_counter');
    }
}
