<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('user_group_id')->nullable();
            $table->string('user_login', 100)->nullable();
            $table->string('user_pass', 500)->nullable();
            $table->string('user_fullname', 100)->nullable();
            $table->string('user_email', 100)->nullable();
            $table->string('user_avatar', 255)->nullable();
            //$table->string('display_name', 100)->nullable();
            $table->string('user_remarks', 1000)->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->integer('record_stat')->nullable();
            $table->integer('is_deleted')->nullable();
            $table->integer('is_system_built')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_users');
    }
}
