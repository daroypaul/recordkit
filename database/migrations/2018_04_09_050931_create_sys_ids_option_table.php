<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysIdsOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('sys_ids_option', function (Blueprint $table) {
            $table->increments('option_id');
            $table->integer('account_id')->nullable();
            $table->string('ids_type', 50)->nullable();
            $table->string('ids_prefix', 100)->nullable();
            $table->string('ids_suffix', 100)->nullable();
            $table->integer('ids_with_padding')->nullable();
            $table->integer('ids_padding_digits')->nullable();
            $table->integer('ids_with_dashed')->nullable();
            $table->integer('ids_with_year')->nullable();
            $table->integer('ids_string')->nullable();
            $table->string('ids_insert_year', 50)->nullable();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_ids_option');
    }
}
