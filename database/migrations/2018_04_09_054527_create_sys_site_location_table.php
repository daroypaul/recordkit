<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysSiteLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_site_location', function (Blueprint $table) {
            $table->increments('location_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->string('location_name', 255)->nullable();
            $table->string('location_remarks', 1000)->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('record_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_site_location');
    }
}
