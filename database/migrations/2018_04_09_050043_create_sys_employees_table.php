<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_employees', function (Blueprint $table) {
            $table->increments('employee_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('emp_code', 150)->nullable();
            $table->string('emp_fullname', 255)->nullable();
            //$table->string('emp_last_name', 255)->nullable();
            //$table->string('emp_first_name', 255)->nullable();
            //$table->string('emp_middle_name', 255)->nullable();
            $table->string('emp_gender', 10)->nullable();
            $table->string('emp_address', 1000)->nullable();
            $table->string('emp_contact', 100)->nullable();
            $table->string('emp_email', 100)->nullable();
            $table->string('emp_remarks', 1000)->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('record_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_employees');
    }
}
