<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_notifications', function (Blueprint $table) {
            $table->increments('notification_id');
            $table->integer('account_id')->nullable();
            $table->string('notif_content', 1000)->nullable();
            $table->dateTime('notif_date')->nullable();
            $table->string('notif_type', 50)->nullable();
            $table->string('notif_importancy', 50)->nullable();
            $table->integer('to_all')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_notifications');
    }
}
