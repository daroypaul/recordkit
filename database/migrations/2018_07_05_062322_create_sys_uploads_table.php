<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_uploads', function (Blueprint $table) {
            $table->increments('upload_id');
            $table->integer('account_id')->nullable();
            $table->integer('owner_id')->nullable();  
            $table->string('owner', 50)->nullable();
            $table->string('upload_filename', 255)->nullable();
            $table->string('upload_desc', 1000)->nullable();
            $table->string('upload_extension', 20)->nullable();
            $table->string('upload_type', 100)->nullable();
            $table->string('upload_size', 50)->nullable();
            $table->string('upload_use', 100)->nullable();
            $table->dateTime('date_uploaded')->nullable();
            $table->integer('uploaded_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_uploads');
    }
}
