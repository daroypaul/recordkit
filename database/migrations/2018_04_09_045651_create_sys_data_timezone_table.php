<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysDataTimezoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_data_timezone', function (Blueprint $table) {
            $table->increments('timezone_id');
            $table->string('timezone_offset', 50)->nullable();
            $table->string('timezone_hours', 10)->nullable();
            $table->string('timezone_minutes', 10)->nullable();
            $table->string('timezone_name', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_data_timezone');
    }
}
