<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_menus', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->integer('account_id')->nullable();
            $table->integer('parent_menu')->nullable();  
            $table->string('menu_name', 100)->nullable();
            $table->string('menu_label', 100)->nullable();
            $table->string('menu_url', 500)->nullable();
            $table->string('menu_icon', 100)->nullable();
            $table->string('privilege_key', 500)->nullable();
            $table->integer('menu_order')->nullable(); 
            $table->integer('is_disable')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_menus');
    }
}
