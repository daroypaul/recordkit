<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_company_info', function (Blueprint $table) {
            $table->increments('info_id');
            $table->integer('account_id')->nullable();
            $table->string('comp_name', 255)->nullable();
            $table->string('comp_address', 500)->nullable();
            $table->string('comp_email', 100)->nullable();
            $table->string('comp_logo_desktop', 500)->nullable();
            $table->string('comp_logo_mobile', 500)->nullable();
            $table->string('comp_website', 100)->nullable();
            $table->string('apps_name', 100)->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_company_info');
    }
}
