<?php

/*
|--------------------------------------------------------------------------
| Weventory - Asset Management System
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */


    //Route::redirect('/', 'auth/login', 301);

    Route::group(['middleware' => 'auth'], function () {
        #Dashboard Routes
        //Route::get('dashboard', 'DashboardController@index');
        //Route::post('dashboard/widget/fetch/data', 'DashboardController@widgets_feth_data');

        Route::get('branding/{filename}', function ($filename)
        {

            $module_folder_path = '';

            $path = storage_path('app/public/' . $module_folder_path . $filename);

            if (!File::exists($path)) {
                abort(404);
            }
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;
        });

        Route::get('uploaded/file/{filename}', function ($filename)
        {

            $module_folder_path = '';

            $path = storage_path('app/public/' . $module_folder_path . $filename);

            if (!File::exists($path)) {
                abort(404);
            }
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;
        });
        
        Route::get('assets/{type}/{filename}', function ($type, $filename)
        {
            $package_dir = array_map('strtolower', array_filter(glob('app/packages/*'), 'is_dir'));
            $module_dir = array_map('strtolower', array_filter(glob('app/modules/*'), 'is_dir'));
            $file_routes = [];

            //Scan Packages File Routes
            foreach(packages::scan_packages() as $package_dir){
                $package_detail_basepath = base_path($package_dir . '/package.json');

                if(file_exists($package_detail_basepath)) {
                    $get_detail_data = file_get_contents($package_detail_basepath);
                    $detail_data = json_decode($get_detail_data, true);
                    
                    //Check if Package installed && activated
                    if($detail_data['active']!=0&&$detail_data['installed']!=0){
                        $file_basepath = base_path($package_dir.'/file_routes.json');
                        
                        if (file_exists($file_basepath)) {
                            $get_data = file_get_contents($file_basepath);
                            $json_data = json_decode($get_data, true);
    
                            foreach($json_data as $key => $value){
                                $file_routes[$key] = $value;
                            }
                        }
                    }
                }
            }
            
            //Scan Packages File Routes
            foreach($module_dir as $module){
                $file_basepath = base_path($module.'/file_routes.json');
                
                if (File::exists($file_basepath)) {
                    $get_data = file_get_contents($file_basepath);
                    $json_data = json_decode($get_data, true);
    
                    foreach($json_data as $key => $value){
                        $file_routes[$key] = $value;
                    }
                }
            }

            if(!isset($file_routes[$filename])){
                abort(404);
            }

            $path = base_path($file_routes[$filename]);

            if (!File::exists($path)) {
                abort(404);
            }
        
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;
        });
    });

    #Image File Routes
    Route::get('img/{filename}', function ($filename)
    {
        $path = public_path('/img/'.$filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    
    Route::get('img/avatars/{filename}', function ($filename)
    {
        $path = public_path('/img/avatars/'.$filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });

    #Test Email Blade Route Only
    Route::get('/email', 'EmailSender@send');