<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Notification</title>
	
<link rel="stylesheet" type="text/css" href="stylesheets/email.css" />

<style>
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }


a {
    color: #2BA6CB;
}

p.callout {
    padding: 15px;
    background-color: #ECF8FF;
    margin-bottom: 0px;
    background-color: #ebebeb;
}

table.footer-wrap {
    width: 100%;
    clear: both !important;
}

table.footer-wrap p.footer-link a{
    text-decoration: none;
    color: #333333;
    font-size: 12px;
}

.container {
    display: block !important;
    max-width: 600px !important;
    margin: 0 auto !important;
    clear: both !important;
}

.content {
    padding: 15px;
    max-width: 600px;
    margin: 0 auto;
    display: block;
}

.content table {
    width: 100%;
}

p {
    margin-bottom: 0px;
    font-weight: normal;
    font-size: 13px;
    line-height: 1.6;
}

table.bordered{
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 5px;
    font-size: 0.8em;
    color: #333333;
    text-align: left;
}
table.bordered th , table.bordered td{
    border-bottom: 1px solid;
    border-color: #999999;
    padding: 5px;
}

table.bordered th{
    border-bottom: 2px solid;
    font-size: 1.1em;
    border-color: #999999;
    text-align: left;
}

table.details{
    border-collapse: collapse;
    width: auto;
    font-size: 0.9em;
    color: #333333;
    text-align: left;
    
    margin-bottom: 15px;
}

table.details th, table.details td{
    padding: 5px 10px 0px 0;
    text-align: left;
}

table.details th{
    font-weight: 600;
    text-align: left;
}

a.link-button{
    border:1px solid;
    text-decoration: none;
    padding: 7px 12px;
    background: #398bf7;
    border: 1px solid #398bf7;
    color:#ffffff;
    border-radius: .25rem;
    clear:both;
    margin: 0 auto;
}

/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}
	
	table.social div[class="column"] {
		width:auto!important;
	}

}
</style>

</head>
 
<body bgcolor="#FFFFFF" style="-webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; width: 100%!important; height: 100%;">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#2a3e52" style="width: 100%;">
	<tr>
		<td></td>
		<td class="header container" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">
				
				<div class="content" style="padding:15px; max-width:600px;margin:0 auto; display:block; ">
                    <table bgcolor="#2a3e52" style="width: 100%;">
                        <tr>
                            <td style="">
                                <a href="{{(isset($company_info->comp_website)) ? $company_info->comp_website : 'http://weventory.com'}}"><img src="{{(isset($company_info->comp_logo_desktop))? url('/branding').'/'.$company_info->comp_logo_desktop : custom_asset('img/weventory_default.png')}}" height="50" alt="Branding Logo"/></a>
                            </td>
                            <td style="padding-left:0px;" align="right"><h6 class="collapse color-white" style="margin:0; font-weight:900; font-size: 14px; text-transform: uppercase; color:#ffffff;">{{(isset($company_info->apps_name)) ? $company_info->apps_name : 'Asset Tracking System'}}</h6></td>
                        </tr>
                    </table>
				</div>
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap" style="width: 100%;">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;">

			<div class="content" style="padding: 25px 15px 15px; max-width: 600px; margin: 0 auto; display: block;">
			<table class="width: 100%;">
                @if(isset($type)&&$type=='reset_pass')
                <tr>
					<td>
						<h3 style="font-weight: 500; font-size: 24px; margin-bottom:20px;">Hi, {{(isset($fullname)) ? $fullname : ''}}</h3>
						<div class="msg" style="margin-bottom:0x; border-bottom:1px solid #999999;">
                            <p style="margin-bottom:30px;">We’ve received a request to reset your password.</p>

                            <div style="text-align: center;">
                                <a href="{{(isset($link)) ? $link : ''}}" class="link-button">Reset Password</a>
                            </div>

                            <br style="clear:both;"/>
                            <p>or copy this link: </p>
                            <p style="color: #398bf7; margin-bottom:20px; word-break: break-all;">{{(isset($link)) ? $link : ''}}</p>
                            
                            <p style="margin-bottom:10px;">If you didn’t request this, you can ignore this email or let us know. Your password won’t be changed</p>
                        </div>
                        <p></p>
					</td>
                </tr>
                @else
				<tr>
					<td>
						<h3 style="font-weight: 500; font-size: 24px; margin-bottom:20px;">Hi, {{$requestor}}</h3>
						<div class="msg" style="margin-bottom:20px;">

                            {!!$bodymessage!!}
                        </div>
                        <div>
                            <p>Thanks, <br/>IT Support Team</p>
                            <p style="margin-bottom: 20px;"></p>
                        </div>
						<p class="callout">
							<small>Note: This is system generated notification. Please do not reply. Contact your system administrator.</small>
						</p>
					</td>
                </tr>
                @endif
			</table>
			</div><!-- /content -->
									
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
				<!-- content -->
				<div class="content" style="padding-top:5px;">
				<table>
                <tr>
					<td align="center">
						<p style="margin-bottom:0px;">
                            <small>Powered By: RecordKits Application Suite<sup>&reg;</sup></small>
                            <div></div>
                            <a href="http://recuda.com/"><img src="{{custom_asset('img/recuda.png')}}" alt="Apps Logo" width="100"></a>
						</p>
					</td>
				</tr>
				<!--<tr>
					<td align="center">
						<p class="footer-link">
							<a href="#">Terms</a> |
							<a href="#">Privacy</a> |
							<a href="#"><unsubscribe>Unsubscribe</unsubscribe></a>
						</p>
					</td>
				</tr>-->
			</table>
				</div><!-- /content -->
				
		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>