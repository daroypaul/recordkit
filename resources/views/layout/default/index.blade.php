<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    @yield('meta')
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ custom_asset('template2/assets/images/favicon.png') }}">
    <title>@yield('module_title') | {{Config::get('constant.apps_name')}}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ custom_asset('js/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/style.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/colors/'.$system_options->option_skins.'.css') }}" id="theme" rel="stylesheet">

    <link href="{{ custom_asset('js/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/jquery-asColorPicker-master/css/asColorPicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ custom_asset('js/plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ custom_asset('js/plugins/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <!--<link href="{{ custom_asset('js/plugins/js-datatables/css/datatables.min.css') }}" rel="stylesheet">-->
    <!--<link href="{{ custom_asset('js/plugins/js-datatables/buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">-->
    <link href="{{ custom_asset('js/plugins/js-datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!--<link href="{{ custom_asset('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">-->
    <!--<link href="{{ custom_asset('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">-->
    <link href="{{ custom_asset('js/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ custom_asset('js/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ custom_asset('css/custom.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader" id="page-loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Initializing...</p>
        </div>
    </div>

    <div class="apps-preloader custom-preloader" id="form-loader" style="display: none;">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label" id="loader-caption">Fetching Record</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

        @include('layout.default.header')

        @include('layout.default.nav')

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">{!!Config::get('constant.copyright')!!}</footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ custom_asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ custom_asset('js/template/default/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ custom_asset('js/template/default/waves.js') }}"></script>
    <script src="{{ custom_asset('js/template/default/sidebarmenu.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ custom_asset('js/template/default/custom.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/moment/moment.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/autonumeric/autoNumeric-min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/toast-master/js/jquery.toast.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <!--<script src="{{ custom_asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ custom_asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>-->
    <script src="{{ custom_asset('js/plugins/js-datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/js/dataTables.bootstrap4.min.js') }}"></script>    
    <script src="{{ custom_asset('js/plugins/js-datatables/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/pdfmake-0.1.32/pdfmake.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/pdfmake-0.1.32/vfs_fonts.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/js-datatables/js/date-dd-MMM-yyyy.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>

    <script src="{{ custom_asset('js/global.js') }}"></script>
    <script src="{{ custom_asset('js/modal.js') }}"></script>
    @yield('js')<!--Custom js loaded per page-->
</body>

</html>