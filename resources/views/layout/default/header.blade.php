<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/dashboard')}}">
                <!-- Logo icon -->
                <div class="mobile-logo hidden-xl-up hidden-lg-up hidden-md-up ">
                    <img src="{{($company_info->comp_logo_mobile)? url('/branding/'.$company_info->comp_logo_mobile) : url('img/icon.png')}}" id="apps-branding-icon" alt="homepage" />
                </div>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="desktop-logo color-white" style="text-color:white !important;">
                  <img src="{{($company_info->comp_logo_desktop)? url('/branding/'.$company_info->comp_logo_desktop) : url('img/apps_white_logo.png')}}" id="apps-branding-logo" alt="Weventory" />
                </span>
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item hidden-sm-down"></li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
                <!--<li class="nav-item hidden-xs-down search-box"> <a class="nav-link hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                    <form class="app-search">
                        <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                </li>-->
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-plus-circle"></i></a>
                    <div class="dropdown-menu dropdown-menu-right animated slideInDown"> 
                        <a class="dropdown-item" href="#"><i class="mdi mdi-barcode" style="width: 30px !important;"></i> Add New Asset</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-login" style="width: 30px !important;"></i> Check-In Asset</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-logout" style="width: 30px !important;"></i> Check Asset</a>
                    </div>
                </li>-->

                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                        <ul>
                            <li>
                                <div class="drop-title">0 Notifications</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>-->

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i></a>
                    <div class="dropdown-menu dropdown-menu-right animated bounceInDown"> 
                        @if(in_array('pkg_fixasset_add', $user_privileges)||in_array('all', $user_privileges))
                        <a class="dropdown-item" href="{{URL::to('/asset/add')}}"><i class="fa fa-barcode m-r-10"></i> New Asset</a> 
                        @endif

                        @if(in_array('pkg_fixasset_inout', $user_privileges)||in_array('all', $user_privileges))
                        <a class="dropdown-item" href="{{URL::to('/asset/checkin')}}"><i class="mdi mdi-login m-r-10"></i> Check-In</a> 
                        <a class="dropdown-item" href="{{URL::to('/asset/checkout')}}"><i class="mdi mdi-logout m-r-10"></i> Check-Out</a> 
                        @endif

                        @if(in_array('pkg_fixasset_lease', $user_privileges)||in_array('all', $user_privileges))
                        <a class="dropdown-item" href="{{URL::to('/asset/lease')}}"><i class="mdi mdi-calendar-clock m-r-10"></i> Lease</a> 
                        <a class="dropdown-item" href="{{URL::to('/asset/return-lease')}}"><i class="mdi mdi-calendar-check m-r-10"></i> Lease Return</a>
                        @endif

                        <a class="dropdown-item" href="{{URL::to('/asset/move')}}"><i class="mdi mdi-directions-fork m-r-10"></i> Move</a> 
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('img/user-icon.png')}}" alt="user" class="profile-pic bg-white" style="padding: 2px;" id="profile-img" /></a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                        <img src="{{url('img/user-icon.png')}}" alt="user" id="profile-img-box">
                                        
                                        <!--<img src="{{(Auth::user()->user_avatar) ? url('/img/'.Auth::user()->user_avatar) : url('img/user-icon.png')}}" alt="user" id="profile-img-box">-->
                                    </div>
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->user_fullname}}</h4>
                                        <p class="text-muted">{{ Auth::user()->user_email}}</p>
                                        <a href="{{URL::to('/user/profile')}}" class="btn btn-rounded btn-danger btn-sm"><i class="ti-settings"></i> Profile Settings</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <!--<li><a href="/account/profile"><i class="ti-user"></i> My Profile</a></li>-->
                            <!--<li><a href="/account/settings"><i class="ti-settings"></i> Account Setting</a></li>-->
                            <!--<li role="separator" class="divider"></li>-->
                            <li><a href="{{URL::to('/logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        