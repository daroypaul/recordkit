<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">PERSONAL</li>

                {!!$nav_menu_sidemini!!}

                <!--<li> <a class="waves-effect waves-dark" href="{{URL::to('/dashboard')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a></li>


                @if( in_array('pkg_fixasset_view', $user_privileges) || in_array('pkg_fixasset_inout', $user_privileges) || in_array('pkg_fixasset_lease', $user_privileges) || in_array('all', $user_privileges))
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-barcode"></i>
                        <span class="hide-menu">Asset</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @if(in_array('pkg_fixasset_view', $user_privileges)||in_array('all', $user_privileges))
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="ti-list m-r-5"></i> Asset List</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{URL::to('/asset/list/all')}}"><i class="ti-control-record m-r-5"></i> All Asset List</a></li>
                                <li><a href="{{URL::to('/asset/list/leasable')}}"><i class="ti-control-record m-r-5"></i> Leasable List</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(in_array('pkg_fixasset_add', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/asset/add')}}"><i class="mdi mdi-plus-circle m-r-5"></i> Add New Asset</a></li>
                        @endif
                        @if(in_array('pkg_fixasset_inout', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/asset/checkin')}}"><i class="mdi mdi-login m-r-5"></i> Check-In</a></li>
                        <li><a href="{{URL::to('/asset/checkout')}}"><i class="mdi mdi-logout m-r-5"></i> Check-Out</a></li>
                        @endif
                        @if(in_array('pkg_fixasset_lease', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/asset/lease')}}"><i class="mdi mdi-calendar-clock m-r-5"></i> Leasing</a></li>
                        <li><a href="{{URL::to('/asset/return-lease')}}"><i class="mdi mdi-calendar-check m-r-5"></i> Return Leased</a></li>
                        @endif
                        <li><a href="{{URL::to('/asset/move')}}"><i class="mdi mdi-directions-fork m-r-5"></i> Move</a></li>


                        @if(isset($menu_list->asset))
                            @foreach($menu_list->asset as $menu)
                                <?php $feature_name = $menu->enable_feature_name;?>
                                @if(isset($all_options->$feature_name)&&$all_options->$feature_name[0]->value)
                                <li><a href="{{$menu->url}}"><i class="{{$menu->icon}} m-r-5"></i> {{$menu->caption}}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </li>
                @endif

                @if( in_array('pkg_km_view', $user_privileges) || in_array('pkg_km_list', $user_privileges) || in_array('pkg_km_add', $user_privileges) || in_array('all', $user_privileges))                
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-information-outline"></i>
                        <span class="hide-menu">Knowledge Base</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @if(in_array('pkg_km_view', $user_privileges)||in_array('all', $user_privileges))
                            <li><a href="{{URL::to('/kb')}}"><i class="mdi mdi-gauge m-r-5"></i> Home</a></li>
                        @endif
                        @if(in_array('pkg_km_list', $user_privileges)||in_array('all', $user_privileges))
                            <li><a href="{{URL::to('/kb/article/all')}}"><i class="mdi mdi-file-document m-r-5"></i> All Articles</a></li>
                        @endif
                        @if(in_array('pkg_km_add', $user_privileges)||in_array('all', $user_privileges))
                            <li><a href="{{URL::to('/kb/article/add')}}"><i class="mdi mdi-plus-circle m-r-5"></i> Add New Article</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if( in_array('pkg_fixasset_report_asset', $user_privileges) || in_array('pkg_fixasset_report_checkout', $user_privileges) || in_array('pkg_fixasset_report_lease', $user_privileges) || in_array('pkg_fixasset_report_status', $user_privileges) || in_array('report_activitylog', $user_privileges) || in_array('all', $user_privileges))
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-chart-line"></i>
                        <span class="hide-menu">Reports</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @if(in_array('pkg_fixasset_report_asset', $user_privileges)||in_array('all', $user_privileges))
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-barcode m-r-5"></i> Asset Reports</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{URL::to('/reports/asset/bymodel')}}"><i class="ti-arrow-right m-r-5"></i> By Model</a></li>
                                <li><a href="{{URL::to('/reports/asset/bycategory')}}"><i class="ti-arrow-right m-r-5"></i> By Category</a></li>
                                <li><a href="{{URL::to('/reports/asset/bysitelocation')}}"><i class="ti-arrow-right m-r-5"></i> By Site/Location</a></li>
                                <li><a href="{{URL::to('/reports/asset/byassignee')}}"><i class="ti-arrow-right m-r-5"></i> By Assignee</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(in_array('pkg_fixasset_report_checkout', $user_privileges)||in_array('all', $user_privileges))
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-logout m-r-5"></i> Check-Out Reports</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{URL::to('/reports/checkout/bydate')}}"><i class="ti-arrow-right m-r-5"></i> By Date</a></li>
                                <li><a href="{{URL::to('/reports/checkout/bysitelocation')}}"><i class="ti-arrow-right m-r-5"></i> By Site/Location</a></li>
                                <li><a href="{{URL::to('/reports/checkout/byrequestor')}}"><i class="ti-arrow-right m-r-5"></i> By Requestor</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(in_array('pkg_fixasset_report_lease', $user_privileges)||in_array('all', $user_privileges))
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-calendar-clock m-r-5"></i> Asset Lease Reports</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{URL::to('/reports/leasable-asset/available')}}"><i class="ti-arrow-right m-r-5"></i> Available Assets</a></li>
                                <li><a href="{{URL::to('/reports/leasable-asset/leased')}}"><i class="ti-arrow-right m-r-5"></i> Leased Assets</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(in_array('pkg_fixasset_report_status', $user_privileges)||in_array('all', $user_privileges))
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-check-circle m-r-5"></i> Status Reports</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{URL::to('/reports/status/available')}}"><i class="ti-arrow-right m-r-5"></i> Available Assets</a></li>
                                <li><a href="{{URL::to('/reports/status/donated')}}"><i class="ti-arrow-right m-r-5"></i> Donated Assets</a></li>
                                <li><a href="{{URL::to('/reports/status/damage')}}"><i class="ti-arrow-right m-r-5"></i> Damage Assets</a></li>
                                <li><a href="{{URL::to('/reports/status/disposed')}}"><i class="ti-arrow-right m-r-5"></i> Disposed Assets</a></li>
                                <li><a href="{{URL::to('/reports/status/lost')}}"><i class="ti-arrow-right m-r-5"></i> Lost Assets</a></li>
                                <li><a href="{{URL::to('/reports/status/sold')}}"><i class="ti-arrow-right m-r-5"></i> Sold Assets</a></li>
                            </ul>
                        </li>
                        @endif
                        @if(in_array('report_activitylog', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/reports/activity-logs')}}"><i class="mdi mdi-timetable m-r-5"></i> Activity Log Reports</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                @if( in_array('admin_user_view', $user_privileges) || in_array('admin_usergroup_view', $user_privileges) || in_array('admin_site_view', $user_privileges) || in_array('admin_location_view', $user_privileges) || in_array('admin_cat_view', $user_privileges) || in_array('admin_employee_view', $user_privileges) || in_array('admin_dept_view', $user_privileges) || in_array('admin_supplier_view', $user_privileges) || in_array('admin_km_article_view', $user_privileges) || in_array('all', $user_privileges))
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-dns"></i>
                        <span class="hide-menu">System Profiles</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @if(in_array('admin_user_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/users')}}"><i class="mdi mdi-account-key m-r-5"></i> Users</a></li>
                        @endif
                        @if(in_array('admin_usergroup_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/user-groups')}}"><i class="mdi mdi-account-settings-variant m-r-5"></i> User Group</a></li>
                        @endif
                        @if(in_array('admin_employee_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/employees')}}"><i class="mdi mdi-account-multiple m-r-5"></i> Employees</a></li>
                        @endif
                        @if(in_array('admin_site_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/site')}}"><i class="fa fa-building-o m-r-5"></i> Sites</a></li>
                        @endif
                        @if(in_array('admin_location_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/location')}}"><i class="mdi mdi-map-marker m-r-5"></i> Locations</a></li>
                        @endif
                        @if(in_array('admin_cat_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/categories')}}"><i class="mdi mdi-bookmark m-r-5"></i> Categories</a></li>
                        @endif
                        
                        @if(in_array('admin_dept_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/departments')}}"><i class="mdi mdi-source-branch m-r-5"></i> Departments</a></li>
                        @endif
                        @if(in_array('admin_supplier_view', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/admin/suppliers')}}"><i class="mdi mdi-basket m-r-5"></i> Suppliers</a></li>
                        @endif
                    </ul>
                </li>
                @endif

                

                @if(request()->getHttpHost()!='uatinout.worldvision.org.ph'&&request()->getHttpHost()!='inout.worldvision.org.ph')
                    @if( in_array('pkg_tag_printing', $user_privileges) || in_array('all', $user_privileges))
                    <li> 
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-wrench"></i>
                            <span class="hide-menu">Tools</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{URL::to('/tools/label-printing')}}"><i class="mdi mdi-printer m-r-5"></i> Tag Label Printing</a></li>

                            @if(isset($menu_list->tools))
                            @foreach($menu_list->tools as $menu)
                                <?php $feature_name = $menu->enable_feature_name;?>
                                @if(isset($all_options->$feature_name)&&$all_options->$feature_name[0]->value)
                                <li><a href="{{$menu->url}}"><i class="{{$menu->icon}} m-r-5"></i> {{$menu->caption}}</a></li>
                                @endif
                            @endforeach
                            @endif
                        </ul>
                    </li>
                    @endif
                @endif

                @if( in_array('pkg_fixasset_setting_assettag', $user_privileges) || in_array('setting_datafields', $user_privileges) || in_array('setting_system', $user_privileges) || in_array('setting_company_info', $user_privileges) || in_array('all', $user_privileges))
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Settings</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        @if(in_array('setting_company_info', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/settings/company-info')}}"><i class="mdi mdi-information-outline m-r-5"></i> Company Information</a></li>
                        @endif
                        @if(in_array('pkg_fixasset_setting_assettag', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/settings/tag')}}"><i class="mdi mdi-tag m-r-5"></i> Asset Tag</a></li>
                        @endif
                        @if(in_array('setting_datafields', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/settings/data-fields')}}"><i class="mdi mdi-table-edit m-r-5"></i> Custom Data Fields</a></li>
                        @endif
                        @if(in_array('setting_system', $user_privileges)||in_array('all', $user_privileges))
                        <li><a href="{{URL::to('/settings/application')}}"><i class="mdi mdi-application m-r-5"></i> Application Settings</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                -->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->