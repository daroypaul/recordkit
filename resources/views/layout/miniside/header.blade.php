<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{url('/dashboard')}}">
                <!-- Logo icon -->
                <b class="mobile-logo">
                    <img src="{{($company_info->comp_logo_mobile)? url('/branding/'.$company_info->comp_logo_mobile) : url('img/icon.png')}}" id="apps-branding-icon" alt="Icon" />
                </b>

                <span class="desktop-logo">
                    <img src="{{($company_info->comp_logo_desktop)? url('/branding/'.$company_info->comp_logo_desktop) : url('img/apps_logo.png')}}" id="apps-branding-logo" alt="Logo" />
                </span>
            </a>
        </div>

        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item hidden-sm-down"></li>
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('img/user-icon.png')}}" alt="user" class="profile-pic bg-white" style="padding: 2px;" id="profile-img" /></a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box" style="padding-bottom:5px !important;">
                                    <!--<div class="u-img m-b-10" style="width:60px;">
                                        <img src="{{url('img/user-icon.png')}}" alt="user" id="profile-img-box">
                                        
                                        <!--<img src="{{(Auth::user()->user_avatar) ? url('/img/'.Auth::user()->user_avatar) : url('img/user-icon.png')}}" alt="user" id="profile-img-box">--
                                    </div>-->
                                    <div class="u-text">
                                        <h4 style="font-size: 1.2em;">{{Auth::user()->user_fullname}}</h4>
                                        <p class="text-muted m-b-0" style="margin-bottom:0px !important;">{{ Auth::user()->user_email}}</p>
                                        <!--<a href="{{URL::to('/user/profile')}}" class="btn btn-rounded btn-danger btn-sm"><i class="ti-settings"></i> Profile Settings</a>-->
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{URL::to('/user/profile')}}"><i class="ti-settings m-r-10"></i> Settings</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{URL::to('/logout')}}"><i class="fa fa-power-off m-r-10"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>