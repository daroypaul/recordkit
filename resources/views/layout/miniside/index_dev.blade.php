<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    @yield('meta')
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ custom_asset('template2/assets/images/favicon.png') }}">
    <title>@yield('module_title') | {{Config::get('constant.apps_name')}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ custom_asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('template/minisidebar/css/style.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('template/minisidebar/css/colors/'.$system_options->option_skins.'.css') }}" id="theme" rel="stylesheet">
    <link href="{{ custom_asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/jquery-asColorPicker-master/css/asColorPicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ custom_asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ custom_asset('plugins/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <!--<link href="{{ custom_asset('plugins/js-datatables/css/datatables.min.css') }}" rel="stylesheet">-->
    <!--<link href="{{ custom_asset('plugins/js-datatables/buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">-->
    <link href="{{ custom_asset('plugins/js-datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!--<link href="{{ custom_asset('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">-->
    <!--<link href="{{ custom_asset('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">-->
    <link href="{{ custom_asset('plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ custom_asset('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ custom_asset('css/custom.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body class="fix-header card-no-border fix-sidebar minisidebar">
    <div class="preloader">
        <div class="loader" id="page-loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Initializing...</p>
        </div>
    </div>

    <div class="apps-preloader custom-preloader" id="form-loader" style="display: none;">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label" id="loader-caption">Fetching Record</p>
        </div>
    </div>

    <div id="main-wrapper">

        @include('layout.miniside.header')

        @include('layout.miniside.nav')

        <div class="page-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
            
            <footer class="footer">{!!Config::get('constant.copyright')!!}</footer>
            
        </div>
        
    </div>
    
    <script src="{{ custom_asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ custom_asset('template/minisidebar/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ custom_asset('template/minisidebar/js/waves.js') }}"></script>
    <script src="{{ custom_asset('template/minisidebar/js/sidebarmenu.js') }}"></script>
    <script src="{{ custom_asset('template/minisidebar/js/custom.js') }}"></script>
    <script src="{{ custom_asset('plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="{{ custom_asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ custom_asset('plugins/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/jquery-asColorPicker-master/libs/jquery-asColor.js') }}"></script>
    <script src="{{ custom_asset('plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js') }}"></script>
    <script src="{{ custom_asset('plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ custom_asset('plugins/autonumeric/autoNumeric-min.js') }}"></script>
    <script src="{{ custom_asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/toast-master/js/jquery.toast.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <!--<script src="{{ custom_asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ custom_asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>-->
    <script src="{{ custom_asset('plugins/js-datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/js/dataTables.bootstrap4.min.js') }}"></script>    
    <script src="{{ custom_asset('plugins/js-datatables/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/pdfmake-0.1.32/pdfmake.min.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/pdfmake-0.1.32/vfs_fonts.js') }}"></script>
    <script src="{{ custom_asset('plugins/js-datatables/js/date-dd-MMM-yyyy.js') }}"></script>
    <script src="{{ custom_asset('plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/global.js') }}"></script>
    <script src="{{ custom_asset('js/modal.js') }}"></script>
    @yield('js')
</body>

</html>