/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){

    $('div#article-content').each(function(){
        var content = $(this).text().replace(/\n/g, ' ').replace(/\s\s+/g, ' ');
        var id = $(this).data('id');
        var link = $apps_base_url + '/kb/article/'+id;

        var a = jQuery.trim(content).substring(0, 200).split(" ").slice(0, -1).join(" ") + '. ... <a href="'+link+'">continue</a>';

        $(this).html(a);
    });
});