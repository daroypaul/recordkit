/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    
    $('#select-status').select2({
        search : false,
    });

    if($('textarea#mymce').length){
        tinymce.init({
            selector: "textarea#mymce",
            theme: "modern",
            height: 500,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

        });
    }

    $('select#select-category').select2({
        placeholder : 'Select Cateory',
        ajax: {
            url: $apps_base_url+'/admin/categories/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_with_keywords',
                    apps : 'article',
                    keywords: params.term,
                }

                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.cat_name,
                            id: item.cat_id
                        }
                    })
                };
            }
        }
    });

    $('button#add-category').click(function(){
        $.admin_category_form('','add','Add New Category','dropdown','article');//id='', ref='', title='Add New Asset Category', append_type='table', name='', remarks=''
    });

    /*$('input#input_title').on('input',function(e){
        var title = $(this).val().trim();
        title = title.toLowerCase();
        title = title.replace(/[^a-z0-9\s]/gi, ' ');
        title = title.replace("-", " ");
        title = title.replace(/\s+/g, "-");

        if(title){
            $('#permalink').show();
            $('#generated-permalink').text($apps_base_url + '/kb'+ '/' + title);
        }else{
            $('#permalink').hide();
            $('#generated-permalink').text($apps_base_url);
        }
    });*/

    
    
    $('#form').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Processing...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);
        if($('#_id').val()!=0||$('#_id').val()){
            form_data.append('id', $('#_id').val()); 
        }

        
        if($('#_type').val()=='add'||$('#_type').val()=='edit'){
            form_data.append('ref', $('#_type').val());
        }

        $.ajax({
            type:'POST',
            url : window.location,
            //dataType: "json",
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                var new_link = $apps_base_url + '/kb/article/'+data.id;

                $('#form-loader').toggle();
                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                    if(data.stat=='success'){
                        if($('#_type').val()=='add'){
                            $('#form')[0].reset();  
                            $('#permalink').hide();
                            var new_link = $apps_base_url + '/kb/article/'+data.id;
                           window.location.replace(new_link);
                        }
                    }

                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    /*
    |--------------------
    | Article Display
    |--------------------
    */
    //Hit like
    $('#btn-like-this').click(function(){
        var form_data = new FormData();
        
        if($('#_id').val()!=0||$('#_id').val()){
            form_data.append('id', $('#_id').val()); 
            form_data.append('action', 'like'); 
            var id = $('#_id').val();

            if($(this).hasClass('btn-info')){
                //$(this).removeClass('btn-info active');
                //$(this).addClass('btn-default');
            }else{
                //$(this).removeClass('btn-default');
                //$(this).addClass('btn-info active');
            }

            $.ajax({
                type:'POST',
                url : window.location,
                dataType: "json",
                data:  {
                    'id':id,
                    'action':'like'
                },
                //processData: false, // Don't process the files
                //contentType: false, // Set content type to false as jQuery will tell the server its a query string
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    if(data.stat=='success'){
                            $('#total-likes').text(data.total_likes);

                        if(data.return=='like'){
                            $('#btn-like-this').removeClass();
                            $('#btn-like-this').addClass('btn btn-info active float-right');
                            //$('#btn-like-this').attr('title','You liked this post');
                            //$('#btn-like-this').attr('data-original-title','You liked this post');
                            // /
                        }else{
                            $('#btn-like-this').removeClass();
                            $('#btn-like-this').addClass('btn btn-default float-right');
                            //$('#btn-like-this').attr('title','Like this post');
                            //$('#btn-like-this').attr('data-original-title','Like this post');
                        }
                    }else{
                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                    }
                },
                error :function (data) {
                    //$('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        }
    });

    //Post comment
    $('#comment-form').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Posting comment...');
        $('#form-notification').html('');

        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').remove();
        var max_length = 1000;
        var form_data = new FormData($(this)[0]);
        form_data.append('action', 'comment');

        if($('#_id').val()!=0||$('#_id').val()){
            form_data.append('id', $('#_id').val()); 
        }

        if($('textarea#comment').val().trim()==''){
            $('#comment-form [name="comment"]').parents('.form-group').addClass('has-danger');
            $('#comment-form [name="comment"]').parents('.form-group').append('<small class="form-control-feedback">Comment required</small>');
            $('#form-loader').toggle();
        }else if($('textarea#comment').val().trim().length<1){
            $('#comment-form [name="comment"]').parents('.form-group').addClass('has-danger');
            $('#comment-form [name="comment"]').parents('.form-group').append('<small class="form-control-feedback">Comment must be at least '+max_length+' characters</small>');
            $('#form-loader').toggle();
        }else if($('textarea#comment').val().trim().length>max_length){
            $('#comment-form [name="comment"]').parents('.form-group').addClass('has-danger');
            $('#comment-form [name="comment"]').parents('.form-group').append('<small class="form-control-feedback">Comment may not be greater than '+max_length+' characters</small>');
            $('#form-loader').toggle();
        }else{
            $.ajax({
                type:'POST',
                url : window.location,
                //dataType: "json",
                data:  form_data,
                dataType: "json",
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {

                    $('#form-loader').toggle();
                    $('.form-group').removeClass('has-danger');
                    $('.form-control-feedback').remove();
                    $.each(data.val_error,function(i,v){
                    $('[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                    $('[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                    });

                    if(data.stat){
                        if(data.stat=='success'){
                            var comment_total = $('#comments .comment-row').length;
                            $('#total-comment-1, #total-comment-2').text(comment_total + 1);

                            var comment = '<div class="d-flex flex-row comment-row">'+
                                        '<div class="p-2"><span class="round bg-white"><img src="'+data.avatar+'" alt="User" width="50"></span></div>'+
                                        '<div class="comment-text w-100">'+
                                            '<small class="text-muted float-right">'+data.date+'</small>'+
                                            '<h5>'+data.commentor+'</h5>'+
                                            '<p class="m-b-5">'+$('textarea#comment').val().trim()+'</p>'+
                                            '<div class="comment-footer">'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                            $('div#comments').append(comment);
                            $('#comment-form')[0].reset();
                        }

                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        }
    });

    /*
    |--------------------
    | Article List
    |--------------------
    */

    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "order": [[ 0, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [9] }
            ],
            /*"dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-2"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-2"<"#bulk-action-toolbars">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<a type="button" href="'+$apps_base_url + '/kb/article/add'+'" class="btn btn-success m-r-10 m-b-10" data-toggle="tooltips" title="Add New" id="btn-add-record"><i class="fas fa-plus"></i> Add New</a>';
                var bulk_action =  
                                '<div class="form-group m-b-0"><div class="input-group">'+
                                    '<select class="form-control custom-select" id="bulk-action-option">'+
                                        '<option value="">- Bulk Action -</option>'+
                                        '<option value="Delete">Delete</option>'+
                                    '</select>'+            
                                    '<span class="input-group-btn">'+
                                    '<button id="btn-bulk-action" class="btn btn-default" type="button" tabindex="-1">Apply</button>'+
                                    '</span>'+
                                '</div></div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);
            }*/
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');
                var form_data = new FormData();
                form_data.append('id', id); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/categories/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            $.admin_category_form(data.list.cat_id,'edit','Update Category','table', 'kms', data.list.cat_name, data.list.cat_remarks);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or category might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }
        });

        $('input#select-all').click(function(){
            var c = this.checked;
    
            $('input.selected-item').each(function(){
                var id = $(this).parents('tr').data('id');
    
                $(this).prop('checked', c);
    
                if(c){
                    if(id!=0||id){
                        $selected_asset.push(id);
                    }
                }else{
                    $selected_asset = $.grep($selected_asset, function (value) {
                        return value != id;
                    });
                }
            });
        });


        $('#btn-bulk-action').click(function(){
            var opt = $('#bulk-action-option').val();
            $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
            $('#bulk-action-option').removeClass('form-control-danger');
    
            if($selected_asset.length==0){
                $.notification('Error','Select a category','top-right','error','');
            }else if(opt==''){
                $('#bulk-action-option').parents('.form-group').addClass('has-danger');
                $('#bulk-action-option').addClass('form-control-danger');
                $.notification('Error','Select an action option','top-right','error','');
            }else if(opt=='Delete'){
                if($selected_asset.length!=0){
                    
                    var url = $apps_base_url+'/admin/categories/delete';
                    var settings = {
                        title : "Deleting Record(s)",
                        text:"Your about to delete "+$selected_asset.length+" category record(s). Are you want to continue?",
                        confirmButtonText:"Delete",
                        showCancelButton:true,
                        type : 'warning',
                        confirmButtonColor: "#DD6B55",
                        //closeOnConfirm: false
                    }
                    
                    swal(settings).then(result => {
                        var form_data = new FormData();
                        $.each($selected_asset, function(i, v){
                            form_data.append('id[]', v);
                        });
                        swal.close();
                        
                        if(result.value){
                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                data: form_data,
                                processData: false,
                                contentType: false,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $eid = [];
                                    $sid = [];
                                    $('.table-danger').removeClass('table-danger');
                                    $('input#select-all').prop('checked', false);
                                    $('input.selected-item:checked').prop('checked', false);
                                    $('#bulk-action-option').val('');
                                    $selected_asset = [];
                                    $status_updated = 1;

                                    if(data.stat){
                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }else{
                                        if(data.sid.length!=0){
                                            $.notification('Success','Selected category has been successfully deleted.','top-right','success','');
        
                                            $.each(data.sid,function(i,v){
                                                $record_table.row('tr[data-id='+v+']').remove().draw();
                                            });
                                        }
        
                                        if(data.eid.length!=0){
                                            $.notification('Error Process','Selected category can not be deleted. Invalid reference ID or category might already deleted. See highlighten row','top-right','error','');
                                            
                                            $.each(data.eid,function(i,v){
                                                $("tr[data-id='"+v+"']").addClass('table-danger');
                                            });
                                            
                                            $eid = data.eid;
                                        }
                                    }
                                },
                                error :function (data) {
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });
                        }
                    });
                }
            }else{
                if(opt!='Delete'&&opt!='Email'){
                    $status_updated = 0;
                    $.asset_status_modal($selected_asset, opt + ' Asset', opt);
                }
            }
        });
        

    $('#btn-add-record').click(function(){
        $.admin_category_form('','add','Add New Category','table','kms');
    });

    $('#btn-delete-article').click(function(){
        var id = $('#_id').val();

        if(id){
            delete_article(id, 'form');
        }
    });


    $('#record-table').on('click',"button#btn-actions", function(){
        var action = $(this).data('action');
        var id = $(this).parents('tr').data('id');


        if(id){
            delete_article(id, 'list');
        }
    });
});


function delete_article(id, origin='list') {
    var url = $apps_base_url+'/kb/article/all';

                    var settings = {
                        title : "Deleting Article",
                        text:"Your about to delete this article. Are you want to continue?",
                        confirmButtonText:"Delete",
                        showCancelButton:true,
                        type : 'warning',
                        confirmButtonColor: "#DD6B55",
                        //closeOnConfirm: false
                    }
                    
                    swal(settings).then(result => {
                        var form_data = new FormData();
                        form_data.append('id', id);
                        form_data.append('origin', origin);
                        swal.close();
                        
                        if(result.value){
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Deleting record...');

                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                data: form_data,
                                processData: false,
                                contentType: false,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {

                                    $('#form-loader').toggle();

                                    $eid = [];
                                    $sid = [];
                                    $('.table-danger').removeClass('table-danger');
                                    $('input#select-all').prop('checked', false);
                                    $('input.selected-item:checked').prop('checked', false);
                                    $('#bulk-action-option').val('');
                                    $selected_asset = [];
                                    $status_updated = 1;

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(origin=='list'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }else if(origin=='form'){
                                                var url = $apps_base_url+'/kb/article/all?m='+data.msg_key;

                                                window.location.replace(url);
                                            }
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                },
                                error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });
                        }
                    });
}

function title() {
    var title = $(this).val().trim();
        //title = title.trim();
        title = title.toLowerCase();
        title = title.replace(/[^a-z0-9\s]/gi, '');
        title = title.replace("-", " ");
        title = title.replace(/\s+/g, "-");

        if(title){
            $('#permalink').show();
            $('#generated-permalink').text($apps_base_url + '/kb'+ '/' + title);
        }else{
            $('#permalink').hide();
            $('#generated-permalink').text($apps_base_url);
        }
}