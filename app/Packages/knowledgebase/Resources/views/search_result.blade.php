@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row">
                    <!--<div class="col-md-9 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <p>Welcome, {{ Auth::user()->user_fullname}}</p>
                    </div>
                    <div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                    <div class="col-md-1">

                    </div>
                    
                    @include('knowledgebase::search_bar')

                    <div class="col-md-1">

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    @if(Session::get('denied_access_notify'))
                        <div class="col-12">
                            <div class="alert alert-danger">Access Denied: You do not have the permission to view the page. Please contact your system administration.</div>
                        </div>
                    @endif
                </div>

                @if($result)
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
                            @if($type=='category')
                                <div class="card-body p-30 p-b-10">
                                    <h2 class="card-title">Category: {{ucwords($category)}} <span class="badge badge-success">{{$total_post}}</span></h2>
                                    <hr/>

                                    @if($result)
                                    <ul class="search-listing m-b-20">
                                        @foreach($result as $article)
                                            <li>
                                                <h3><i class="fa fa-file-text-o m-r-10"></i> <a href="{{url('/kb/article/'.$article->post_id)}}">{{$article->post_title}}</a></h3>
                                                <div id="article-content" class="m-t-20" data-id="{{$article->post_id}}">{!!$article->post_content!!}</div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @else
                                        <h1 class="text-info font-bold m-b-20" style="font-size: 3rem;">We're sorry</h1>
                                        <h3>
                                                The document you have requested is not available. You may have entered an incorrect URL or login with authorized credentials to view the page. <br/>Try going back to <a href="{{url('/kb')}}">Home</a> or use Knowledge base search above to find what you need.
                                        </h3>
                                    @endif
                                    
                                    {!! ($result) ? $result->appends(['c' => $category])->render() : '' !!}
                                </div><!--/card-body-->
                            @elseif($type=='tag')
                                <div class="card-body p-30 p-b-10">
                                    <h2 class="card-title">Tag: {{ucwords($tag)}} <span class="badge badge-success">{{$total_post}}</span></h2>
                                    <hr/>

                                    @if($result)
                                    <ul class="search-listing m-b-20">
                                        @foreach($result as $article)
                                            <li>
                                                <h3><i class="fa fa-file-text-o m-r-10"></i> <a href="{{url('/kb/article/'.$article->post_id)}}">{{$article->post_title}}</a></h3>
                                                <div id="article-content" class="m-t-20" data-id="{{$article->post_id}}">{!!$article->post_content!!}</div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @else
                                        <h1 class="text-info font-bold m-b-20" style="font-size: 3rem;">We're sorry</h1>
                                        <h3>
                                                The document you have requested is not available. You may have entered an incorrect URL or login with authorized credentials to view the page. <br/>Try going back to <a href="{{url('/kb')}}">Home</a> or use Knowledge base search above to find what you need.
                                        </h3>
                                    @endif
                                    
                                    {!! ($result) ? $result->appends(['t' => $tag])->render() : '' !!}
                                </div><!--/card-body-->
                                @elseif($type=='search')
                                <div class="card-body p-30 p-b-10">
                                    <h4 class="card-title"><div class="badge badge-success">{{$total_post}}</div> Search result for: {{$search}}</h4>
                                    <hr/>

                                    @if($result)
                                    <ul class="search-listing m-b-20">
                                        @foreach($result as $article)
                                            <li>
                                                <h3><i class="fa fa-file-text-o m-r-10"></i> <a href="{{url('/kb/article/'.$article->post_id)}}">{{$article->post_title}}</a></h3>
                                                <div id="article-content" class="m-t-20" data-id="{{$article->post_id}}">{!!$article->post_content!!}</div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @else
                                        <h1 class="text-info font-bold m-b-20" style="font-size: 3rem;">We're sorry</h1>
                                        <h3>
                                                The document you have requested is not available. You may have entered an incorrect URL or login with authorized credentials to view the page. <br/>Try going back to <a href="/kb">Home</a> or use Knowledge base search above to find what you need.
                                        </h3>
                                    @endif
                                    
                                    <!--{{ ($result) ? $result->links() : '' }}-->
                                    {!! ($result) ? $result->appends(['q' => $search])->render() : '' !!}
                                </div><!--/card-body-->
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-4">
                            @include('knowledgebase::right_sidebar')
                    </div>
                </div>
                @else
                    <div class="row m-t-20">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body p-30">
                                    <h1 class="text-info font-bold m-b-20" style="font-size: 3rem;">We're sorry</h1>
                                    <h3>
                                            The document you have requested is not available. You may have entered an incorrect URL or login with authorized credentials to view the page. <br/>Try going back to <a href="/kb">Home</a> or use Knowledge base search above to find what you need.
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                @endif
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css')}}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/other-pages.css')}}" rel="stylesheet">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('js')
    <script src="{{ url('assets/js/EhOeSv6li0prc931XMuK.js') }}"></script>
@endsection