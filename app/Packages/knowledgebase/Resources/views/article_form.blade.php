@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/kb')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/kb/article/all')}}">Article</a></li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    @if(Session::get('denied_access_notify'))
                        <div class="col-12">
                            <div class="alert alert-danger">Access Denied: You do not have the permission to view the page. Please contact your system administration.</div>
                        </div>
                    @endif
                </div>

                <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12" id="permalink" style="display:none">
                                            <div class="form-group m-b-10">
                                                <label class="form-control-label">Permalink:</label>
                                                <code id="generated-permalink">{{url('/').'/kb/title'}}</code>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label">Title <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                <input class="form-control" type="text" placeholder="Article Title" id="input_title" name="title" value="{{isset($article) ? $article->post_title : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group m-b-5">
                                                    <textarea id="mymce" name="content">{{isset($article) ? $article->post_content : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-lg-4">
                            <div class="col-12 p-0">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-category" data-original-title="Add New Category"><i class="fas fa-plus"></i></button>
                                                    <label>Category <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <?php $cat_id = isset($article) ? $article->cat_id : '0';?>
                                                    <select class="form-control" name="category" id="select-category">
                                                            <option value="">- Select Category -</option>
                                                            <!--@if($category_list)
                                                                @foreach($category_list as $category)
                                                                    @if($cat_id==$category->cat_id)
                                                                        <option value="{{$category->cat_id}}" selected>{{ucwords($category->cat_name)}}</option>
                                                                    @else
                                                                        <option value="{{$category->cat_id}}">{{ucwords($category->cat_name)}}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endif-->
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                        <label>Status</label>
                                                        <select class="form-control" id="select-status" name="status">
                                                            <?php
                                                                $stats = ['published', 'draft'];

                                                                foreach($stats as $stat){
                                                                    if(isset($article)&&$article->post_stat==$stat){
                                                                        echo '<option value="'.$stat.'" selected>'.ucwords($stat).'</option>';
                                                                    }else{
                                                                        echo '<option value="'.$stat.'">'.ucwords($stat).'</option>';
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                </div>
                                            </div>       
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Tags</label><br/>
                                                    <select multiple data-role="tagsinput" name="tags[]" placeholder="Tags">
                                                        @if(isset($tags))
                                                            @foreach($tags as $tag)
                                                                <option value="{{ucwords($tag->tag_name)}}">{{$tag->tag_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>   
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                        <input id="featured-article" class="filled-in" name="is_featured" value="1" type="checkbox" {{(isset($article)&&$article->is_featured==1) ? 'checked' : ''}}>
                                                        <label for="featured-article">Featured Article</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-action">
                                <button type="submit" class="btn btn-success m-r-10"> <i class="fa fa-check"></i> Save</button>

                                @if(isset($article))
                                    <button type="button" class="btn btn-danger" id="btn-delete-article"> <i class="ti-trash m-r-5"></i> Delete</button>
                                @endif
                            </div>
                        </div>
                    </div><!--/row-->
                </form>
                <input type="hidden" id="_id" value="{{isset($article) ? $article->post_id : ''}}" />
                <input type="hidden" id="_type" value="{{isset($type) ? $type : ''}}" />
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css')}}" rel="stylesheet">

    <link href="{{ custom_asset('js/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/html5-editor/bootstrap-wysihtml5.css')}}" rel="stylesheet" />
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>

    <script src="{{ url('assets/js/xOVB3l9z0p4iUMcZ2ua7.js') }}"></script>
@endsection