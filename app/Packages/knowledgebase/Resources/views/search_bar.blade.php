<div class="col-md-10" style="padding: 20px 10px 40px 10px;">
        @if(request()->getHttpHost()=='uatinout.worldvision.org.ph')
            <h2 class="text-center">IT Support Knowledge Base Management</h2>
        @else
            <h2 class="text-center">Knowledge Base Management</h2>
        @endif
    <form id="search-km-form" type="get" name="q" action="{{url('/kb/search')}}">
        <div class="form-group">
            <div class="input-group mb-3">
                <input class="form-control form-control-lg" placeholder="Search the Knowledge Base" type="text" id="input-search-km" name="q">
                <span class="input-group-append">
                    <button class="btn btn-default btn-lg" type="submit" id="btn-search-km"><i class="mdi mdi-magnify"></i></button>
                </span>
            </div>
        </div>
    </form>
</div>