<div class="col-12 p-0">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Featured Articles</h4><hr/>
            @if(isset($featured_article)&&count($featured_article)!=0)
            <ul class="list-icons">
                @foreach($featured_article as $article)
                    <li><i class="fa fa-file-text-o"></i> <a href="{{url('/kb/article').'/'.$article->post_id}}">{{$article->post_title}}</a></li>
                @endforeach   
            </ul>
            @else
            <p>No Featured Articles</p>
            @endif
        </div>
    </div>
</div>

<div class="col-12 p-0">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Latest Articles</h4><hr/>

            @if(isset($latest_article)&&count($latest_article)!=0)
            <ul class="list-icons">
                @foreach($latest_article as $article)
                    <li><i class="fa fa-file-text-o"></i> <a href="{{url('/kb/article').'/'.$article->post_id}}">{{$article->post_title}}</a></li>
                @endforeach   
            </ul>
            @else
                <p>No Latest Articles</p>
            @endif
        </div>
    </div>
</div>

<div class="col-12 p-0">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tags</h4><hr/>
            @if(isset($cloud_tags)&&count($cloud_tags)!=0)
                @foreach($cloud_tags as $tag)
                    <a href="{{url('/kb/tag?t='.$tag->tag_name)}}" class="badge badge-info">{{ucwords($tag->tag_name)}}</a>
                @endforeach
            @else
                <p>No Tags Data</p>
            @endif
            
        </div>
    </div>
</div>