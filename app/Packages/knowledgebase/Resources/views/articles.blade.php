@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row">
                    <div class="col-md-1">

                    </div>
                    
                    @include('knowledgebase::search_bar')

                    <div class="col-md-1">

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    @if(Session::get('denied_access_notify'))
                        <div class="col-12">
                            <div class="alert alert-danger">Access Denied: You do not have the permission to view the page. Please contact your system administration.</div>
                        </div>
                    @endif
                </div>
                
                @if($article)
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body p-20">
                                <nav class="breadcrumb p-10" style="font-size:12px;">
                                    <a class="breadcrumb-item" href="{{url('/kb')}}"><i class="fa fa-home"></i></a>
                                    <a class="breadcrumb-item" href="{{url('/kb/category?c=')}}{{isset($article->category) ? $article->category->cat_name : ''}}">{{isset($article->category) ? $article->category->cat_name : ''}}</a>
                                    <span class="breadcrumb-item active">{{isset($article->post_title)?$article->post_title:''}}</span>
                                </nav>
                                <h2><i class="fa fa-file-text-o m-r-10"></i>{{isset($article->post_title)?$article->post_title:''}}</h2>
                                <hr class="m-b-5"/>
                                <div class="row">
                                    <div class="col-md-3"><i class="ti-calendar"></i> <small>{{isset($article->date_recorded) ? date('d-M-Y', strtotime($article->date_recorded. $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes')) : ''}}</small></div>

                                    <div class="col-md-3"><i class="ti-user text-success"></i> <small>{{isset($article->recorder) ? $article->recorder->user_fullname : 'Undefined'}}</small></div>

                                    <div class="col-md-3"><i class="ti-bookmark text-danger"></i> <a href="{{url('/kb/category?c=')}}{{($article->category) ? $article->category->cat_name : ''}}"><small>{{isset($article->category) ? $article->category->cat_name : 'Undefined'}}</small></a></div>

                                    <div class="col-md-1" title="Comments"><i class="ti-comments text-primary"></i> <small id="total-comment-1">{{isset($comments)?count($comments):'0'}}</small></div>

                                    <div class="col-md-1" title="Views"><i class="mdi mdi-eye-outline text-warning"></i><small>{{isset($article)?$article->total_view:'0'}}</small></div>

                                    <div class="col-md-1" title="Likes"><i class="mdi mdi-thumb-up-outline text-info m-r-5"></i> <small id="total-likes">{{$likes}}</small></div>
                                </div>

                                <hr class="m-t-5 m-b-10"/>

                                <?php
                                $is_liked = 'btn-default';
                                //$tooltip = 'Like this post';
                                if($liked){
                                    if($liked->like_by==Auth::id()){
                                        $is_liked = 'btn-info active';
                                        //$tooltip = 'You liked this post';
                                    }
                                }
                                ?>
                                <button id="btn-like-this" type="button" class="btn {{$is_liked}} float-right" data-toggle="tooltip" title=""><i class="mdi mdi-thumb-up-outline"></i></button>

                                @if(count($tags))
                                <div class="m-b-30">
                                        Tags: 
                                        @if(count($tags))
                                            @foreach($tags as $tag)
                                                <!--<span class="badge badge-info m-b-5">{{ucwords($tag->tag_name)}}</span>-->
                                                <a href="{{url('/kb/tag?t=')}}{{$tag->tag_name}}" class="badge badge-success">{{ucwords($tag->tag_name)}}</a>
                                            @endforeach
                                        @else 
                                            (None)
                                        @endif
                                </div>
                                @endif
                                
                                <div class="m-t-30">
                                    {!!isset($article) ? $article->post_content : ''!!}
                                </div>
                                <br/>

                                <!--<div class="text-center">
                                    <h4>This article is helpfull?</h4> <button id="btn-like-this" type="button" class="btn btn-default" data-toggle="tooltip" title="Yes"><i class="mdi mdi-thumb-up-outline"></i></button>
                                </div>
                                <br/>-->
                                <h3>Comments (<span id="total-comment-2">{{count($comments)}}</span>)</h3>
                                <hr/>
                                
                                <div class="comment-widgets" id="comments">
                                        @foreach($comments as $comment)
                                            @if($comment->commentor)
                                            <div class="d-flex flex-row comment-row">
                                                <div class="p-2"><span class="round bg-white"><img src="{{($comment->commentor->user_avatar) ? $comment->commentor->user_avatar : custom_asset('/').'img/avatars/avatar-1.png'}}" alt="User" width="50"></span></div>
                                                <div class="comment-text w-100">
                                                    <small class="text-muted float-right">{{date('d-M-Y h:iA', strtotime($comment->date_recorded. $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</small> 
                                                    <h5>{{($comment->commentor) ? $comment->commentor->user_fullname : ''}}</h5>
                                                    <p class="m-b-5">{{$comment->comment_content}}</p>
                                                    <div class="comment-footer">
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                </div>

                                <form action="#" class="form-materials" method="post" id="comment-form" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="form-group m-b-10">
                                                <textarea class="form-control" placeholder="Your comment here..." name="comment" id="comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12" >
                                                <button type="submit" class="btn btn-info float-right btn-sm"> <i class="fa fa-check"></i> Post</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <a href="{{url()->current().'/edit'}}" class="btn btn-sm btn-warning m-r-10"> <i class="ti-pencil m-r-5"></i> Edit</a>
                    </div>

                    
                    <div class="col-lg-4">

                        @include('knowledgebase::right_sidebar')
                    </div>
                </div>
                @else
                    <div class="row m-t-20">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body p-30">
                                    <h1 class="text-info font-bold m-b-20" style="font-size: 3rem;">We're sorry</h1>
                                    <h3>
                                            The document you have requested is not available. You may have entered an incorrect URL or login with authorized credentials to view the page. <br/>Try going back to <a href="{{url('/kb')}}">Home</a> or use Knowledge base search above to find what you need.
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                @endif

                <input type="hidden" id="_id" value="{{isset($article) ? $article->post_id : ''}}" />
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css')}}" rel="stylesheet">
@endsection

@section('module_title')
Article: {{$form_title}}
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('js')
<script src="{{ url('assets/js/xOVB3l9z0p4iUMcZ2ua7.js') }}"></script>
@endsection