@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row">
                    <!--<div class="col-md-9 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <p>Welcome, {{ Auth::user()->user_fullname}}</p>
                    </div>
                    <div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                    <div class="col-md-1">

                    </div>
                    
                    @include('knowledgebase::search_bar')

                    <div class="col-md-1">

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    @if(Session::get('denied_access_notify'))
                        <div class="col-12">
                            <div class="alert alert-danger">Access Denied: You do not have the permission to view the page. Please contact your system administration.</div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body p-30">
                                <div class="row">
                                    @if(count($categories)!=0)
                                    @foreach($categories as $category)
                                        <div class="col-lg-6">
                                            <h2 class="card-title"><a href="{{url()->full().'/category?c='.$category->cat_name}}">{{$category->cat_name}}</a> <span style="font-weight:lighter !important; font-size:80%; color:#999;"></span></h2>

                                            @if(count($articles)!=0)
                                                <ul class="list-icons m-l-20">
                                                    @foreach($articles as $article)
                                                        @if($article->cat_id==$category->cat_id)
                                                            <li><i class="fa fa-file-text-o"></i> <a href="{{url()->full().'/article/'.$article->post_id}}">{{$article->post_title}}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <p>No article in this category</p> 
                                            @endif
                                        </div>
                                    @endforeach
                                    @else
                                        <div class="col-lg-12">
                                            <div class="alert alert-info">No Article Data</div>

                                            <center>
                                            <a href="{{url('/admin/categories')}}" class="btn btn-inverse">Add Article Category</a>
                                            <a href="{{url('/kb/article/add')}}" class="btn btn-inverse">Add New Article</a>
                                            </center>
                                        </div>
                                    @endif
                                    
                                    <!--<div class="col-lg-6 m-b-20">
                                        <h2 class="card-title"><a href="{{url()->full().'/category/troubleshooting'}}">How-To</a> <span style="font-weight:lighter !important; font-size:80%; color:#999;">(2)</span></h2>
                                        <ul class="list-icons m-l-20">
                                            <li><i class="fa fa-file-text-o"></i> <a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-file-text-o"></i> Consectetur adipiscing elit</li>
                                            <li><i class="fa fa-file-text-o"></i> Integer molestie lorem at massa </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">
                                        <h2 class="card-title"><a href="{{url()->full().'/category/troubleshooting'}}">Web Application</a> <span style="font-weight:lighter !important; font-size:80%; color:#999;">(2)</span></h2>
                                        <ul class="list-icons m-l-20">
                                            <li><i class="fa fa-file-text-o"></i> <a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-file-text-o"></i> Consectetur adipiscing elit</li>
                                            <li><i class="fa fa-file-text-o"></i> Integer molestie lorem at massa </li>
                                        </ul>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-lg-4">
                            @include('knowledgebase::right_sidebar')
                    </div>
                </div>
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css')}}" rel="stylesheet">
@endsection

@section('module_title')
{{$form_title}}
@endsection