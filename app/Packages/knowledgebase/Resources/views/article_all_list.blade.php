@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}} <a class="btn btn-success m-l-10" data-action="edit" href="{{url('/kb/article/add')}}"><i class="fas fa-plus m-r-5"></i> Add New</a></h3>
                        
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/kb')}}">Home</a></li>
                            <li class="breadcrumb-item active">Article</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(isset($_GET['m'])&&session()->has($_GET['m']))
                                    <div class="alert alert-success">{{session()->pull($_GET['m'])}} <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>
                                @endif
                                

                                <div class="table-responsive"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <!--<th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>-->
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Categories</th>
                                                <th>Tags</th>
                                                <th title="Comments"><i class="font-bold ti-comments"></i></th>
                                                <th title="Likes"><i class="font-bold ti-thumb-up"></i></th>
                                                <th title="Views"><i class="font-bold ti-eye"></i></th>
                                                <th>Date</th>
                                                <th>Last Modefied</th>
                                                <th width="60">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($article_list as $article)
                                                <tr data-id="{{$article->post_id}}">
                                                    <!--<td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$article->cat_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$article->cat_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>-->
                                                    <td>
                                                        <a href="{{url('/kb/article/'.$article->post_id)}}" style="color: #67757c;">{{$article->post_title}}</a> 
                                                        {!!($article->post_stat=='draft')?'	
                                                        <b> —</b> <span class="badge badge-info">Draft</span>':''!!}
                                                    </td>
                                                    <td>{!!($article->recorder) ? $article->recorder->user_fullname:'---';!!}</td>
                                                    <td>{!!($article->category) ? $article->category->cat_name : '<code>Undefined</code>'!!}</td>
                                                    <td><small>
                                                        <?php
                                                            $a = '';
                                                            foreach($tags as $tag){
                                                                if($article->post_id==$tag->post_id){
                                                                    $a .= $tag->tag_name . ', '; 
                                                                }
                                                            }

                                                            echo ($a) ? rtrim($a,", ") : '---';
                                                        ?></small>
                                                    </td>
                                                    <td>
                                                        @foreach($comment_like as $comments)
                                                            @if($comments['post_id']==$article->post_id)
                                                                {{$comments['comments']}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                            @foreach($comment_like as $likes)
                                                            @if($likes['post_id']==$article->post_id)
                                                                {{$likes['likes']}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>{{$article->total_view}}</td>
                                                    <td>{{date('Y/m/d', strtotime($article->date_recorded . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</td>
                                                    <td title="{{date('Y/m/d h:i:s A', strtotime($article->date_updated . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}">
                                                        <span style="border-bottom: 1px dotted #000 !important; cursor: pointer;">{{date('Y/m/d', strtotime($article->date_updated . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</span>
                                                    </td>
                                                    <td width="60">
                                                        <a class="btn btn-xs btn-warning" data-action="edit" href="{{url('/kb/article/'.$article->post_id.'/edit')}}">Edit</a>
                                                        <button type="button" class="btn btn-xs btn-danger" data-action="delete" id="btn-actions">Delete</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    <script src="{{ url('assets/js/xOVB3l9z0p4iUMcZ2ua7.js') }}"></script>
@endsection