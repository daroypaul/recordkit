<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSysKmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sys_km_posts');
        Schema::dropIfExists('sys_km_post_comments');
        Schema::dropIfExists('sys_km_post_likes');
        Schema::dropIfExists('sys_km_post_tags');
        Schema::dropIfExists('sys_km_tags');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
