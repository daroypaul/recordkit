<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysKmPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_km_posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->integer('account_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->string('post_title', 255)->nullable();
            $table->string('post_name', 255)->nullable();
            $table->longText('post_content')->nullable();
            //$table->string('post_tags', 255)->nullable();
            $table->string('post_permalinks', 500)->nullable();
            $table->integer('total_view')->nullable();
            $table->longText('privacy_type')->nullable();
            $table->longText('collab_type')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('is_featured')->nullable();
            $table->string('post_stat', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_km_posts');
    }
}
