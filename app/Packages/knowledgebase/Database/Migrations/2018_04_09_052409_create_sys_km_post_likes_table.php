<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysKmPostLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_km_post_likes', function (Blueprint $table) {
            $table->increments('like_id');
            $table->integer('account_id')->nullable();
            $table->integer('post_id')->nullable();
            $table->integer('like_by')->nullable();
            $table->dateTime('date_liked')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_km_post_likes');
    }
}
