<?php

namespace App\Packages\Knowledgebase\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'sys_km_posts';
    protected $primaryKey = 'post_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function category()
    {
        return $this->hasOne('App\modules\adminprofile\models\Categories','cat_id','cat_id');
    }
}
