<?php

namespace App\Packages\Knowledgebase\Models;

use Illuminate\Database\Eloquent\Model;

class PostTags extends Model
{
    protected $table = 'sys_km_post_tags';
    //protected $primaryKey = 'post_id';
    public $timestamps = false;

    public function tags()
    {
        return $this->hasMany('App\Packages\Knowledgebase\Models\Tags','tag_id','tag_id');
    }

    public function posts()
    {
        return $this->hasMay('App\Packages\Knowledgebase\Models\Posts','post_id','post_by');
    }
}
