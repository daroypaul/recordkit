<?php

namespace App\Packages\Knowledgebase\Models;

use Illuminate\Database\Eloquent\Model;

class PostComments extends Model
{
    protected $table = 'sys_km_post_comments';
    protected $primaryKey = 'comment_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function commentor()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','comment_by');
    }
}
