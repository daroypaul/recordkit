<?php

namespace App\Packages\Knowledgebase\Models;

use Illuminate\Database\Eloquent\Model;

class PostLikes extends Model
{
    protected $table = 'sys_km_post_likes';
    protected $primaryKey = 'like_id';
    public $timestamps = false;
}
