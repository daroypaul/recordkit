<?php

namespace App\Packages\Knowledgebase\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'sys_km_tags';
    protected $primaryKey = 'tag_id';
    public $timestamps = false;
}
