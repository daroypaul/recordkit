<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Knowledgebase\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\modules\adminprofile\models\Categories;
use App\Packages\Knowledgebase\Models\Posts;
use App\Packages\Knowledgebase\Models\PostTags;
use App\Packages\Knowledgebase\Models\PostLikes;
use App\Packages\Knowledgebase\Models\PostComments;
use App\Packages\Knowledgebase\Models\Tags;
use ActivityLogs;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class Articles extends Controller
{
    public function index($articles){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        $account_id = Auth::user()->account_id;

        $post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->where('post_name', $articles)->first();

        $total_likes = PostLikes::where('account_id',  $account_id)->where('post_id',  $post->post_id)->count();
        
        $data = [
                "form_title"=> $articles,
                "article"=>$post,
                "likes"=>$total_likes,
        ];

        //return $total_likes;
        return view('knowledgebase::articles')->with($data);
    }

    public function all_list(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        
        if(!Helpers::get_user_privilege('pkg_km_list')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $total_likes = [];
        $total_comments = [];
        $tags = [];
        $comments = [];
        $comment_like = [];

        $post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->where('post_stat', '<>', 'delete')->get();

        if($post){
            $tags = DB::table('sys_km_post_tags')
                        ->join('sys_km_tags', 'sys_km_tags.tag_id', '=', 'sys_km_post_tags.tag_id')
                        ->select('sys_km_post_tags.id', 'sys_km_post_tags.post_id', 'sys_km_tags.tag_id', 'sys_km_tags.tag_name')
                        ->where('sys_km_tags.account_id', $account_id)
                        ->orderBy('sys_km_tags.tag_name', 'ASC')
                        ->get();
            

            foreach($post as $a){
                $b = [];
                $post_likes = PostLikes::where('account_id',  $account_id)->where('post_id',  $a->post_id)->count();   

                $post_comments = PostComments::where('account_id',  $account_id)->where('post_id',  $a->post_id)->count();   

                $b['post_id'] = $a->post_id;
                $b['likes'] = $post_likes;
                $b['comments'] = $post_comments;

                $comment_like[] = $b;
            }       
        }

        $data = [
            "form_title"=> 'All Articles',
            "article_list"=>$post,
            "tags"=>$tags,
            "comment_like"=>$comment_like,
        ];

        return view('knowledgebase::article_all_list')->with($data);
    }

    public function view($id){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_km_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::id();
        $total_likes = 0;
        $tags = [];
        $comments = [];
        $liked = '';

        $post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->where('post_id', $id)->first();

        if($post){
            $total_likes = PostLikes::where('account_id',  $account_id)->where('post_id',  $post->post_id)->count();

            $query = "SELECT
                            sys_km_post_tags.id, sys_km_post_tags.post_id, sys_km_tags.tag_id, sys_km_tags.tag_name 
                            FROM `sys_km_post_tags` 
                            INNER JOIN sys_km_tags 
                                ON sys_km_tags.tag_id = sys_km_post_tags.tag_id 
                            WHERE sys_km_post_tags.post_id = '".$id."' ORDER BY sys_km_tags.tag_name ASC";
            $tags =  DB::select($query);

            $comments = PostComments::where('account_id',  $account_id)->where('post_id',  $post->post_id)->orderBy('date_recorded', 'asc')->get();

            $liked = PostLikes::where('account_id',  $account_id)->where('post_id',  $post->post_id)->where('like_by', $user_id)->first();

            //Add value on views
            //$add_views = Posts::where('account_id',  $account_id)->where('post_id', $id)->increment('total_view', 1);
            $increment_views = Posts::find($post->post_id);
            $increment_views->total_view += 1;
            $increment_views->timestamps = false;
            $increment_views->save();
        }
        
        //return $post;
        $data = [
                "form_title"=> ($post) ? $post->post_title : 'Articles',
                "tags"=> $tags,
                "comments"=> $comments,
                "article"=>$post,
                "likes"=>$total_likes,
                "liked"=>$liked 
        ];

        return view('knowledgebase::articles')->with($data);

        //return $post;
    }

    public function add(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        if(!Helpers::get_user_privilege('pkg_km_add')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $categories = Categories::where('account_id',  $account_id)->where('apps_type', 'article')->orderBy('cat_name', 'asc')->get();

        $data = [
                "form_title"=> 'New Article',
                "category_list"=> $categories,
                "type"=>"add",
        ];

        
        return view('knowledgebase::article_form')->with($data);

        

        //return $check_tags;
    }

    public function edit($id){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        if(!Helpers::get_user_privilege('pkg_km_edit')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $categories = [];
        $tags = [];

        $post = Posts::with(['category'])->where('account_id',  $account_id)->where('post_id', $id)->first();
        $categories = Categories::where('account_id',  $account_id)->where('apps_type', 'article')->orderBy('cat_name', 'asc')->get();

        if($post){

            $query = "SELECT
                            sys_km_post_tags.id, sys_km_post_tags.post_id, sys_km_tags.tag_id, sys_km_tags.tag_name 
                            FROM `sys_km_post_tags` 
                            INNER JOIN sys_km_tags 
                                ON sys_km_tags.tag_id = sys_km_post_tags.tag_id 
                            WHERE sys_km_post_tags.post_id = '".$id."'";
            $tags =  DB::select($query);

            

            //
        }

        $data = [
                "form_title"=> 'Update Article',
                "article"=> $post,
                "category_list"=> $categories,
                "tags"=>$tags,
                "type"=>"edit",
        ];

        //return $tags;


        return view('knowledgebase::article_form')->with($data);
        
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_fullname = Auth::user()->user_fullname;
        $response = [];

        $rules = [
            'title'=>"required|min:2|max:150|regex:/^[a-zA-Z0-9\s'-:,.&\/]+$/u",
            'content'=>'required',
            'category'=>'required',
        ];

        /*if($request->remarks){
            $rules['remarks'] = "min:2|max:750|regex:/(^[A-Za-z0-9 ,-._();']+$)+/";
        }*/

        $messages = [
            'title.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $val_msg = $validator->errors();

        if($request->ref=='add'&&!Helpers::get_user_privilege('pkg_km_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='edit'&&!Helpers::get_user_privilege('pkg_km_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{
            $title = $request->title;
            $title = preg_replace('/[^a-z0-9\s]/i', ' ', $title);
            $title = str_replace('-', ' ', $title);
            $title = strtolower(preg_replace("/\s+/", '-', $title));
            $permalink = url('/') . '/kb/'.$title;

            if($request->ref=='add'){

                $count = Posts::where('account_id', $account_id)->where('post_name', $title)->where('post_stat', 'published')->count();
                
                if($count!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Article';
                    $response['stat_msg'] = 'Article already exists';
                }else{
                    $add_article = new Posts;
                    $add_article->account_id = $account_id;
                    $add_article->cat_id = $request->category;
                    $add_article->post_title = ucfirst($request->title);
                    $add_article->post_content = $request->content;
                    $add_article->post_name = $title;
                    $add_article->post_permalinks = $permalink;
                    //Carbon::now('UTC')->toDateTimeString()
                    $add_article->total_view = 0;
                    $add_article->post_stat = ($request->status) ? $request->status : 'draft';
                    $add_article->is_featured = ($request->is_featured)?1:0;
                    $add_article->recorded_by = $user_id;
                    $add_article->updated_by = $user_id;
                    $add_article->save();
                    
                    if($add_article){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New article has been added';
                        $response['id'] = $add_article->post_id;

                        //Add Logs
                        /*$new_logs = new ActivityLogs;
                        $new_logs->account_id = $account_id;
                        $new_logs->action_type = "km-article-add";
                        $new_logs->action_ref_id = $add_article->post_id;
                        $new_logs->activity_desc = $user_fullname . ' created article "'.ucfirst($request->title).'"';
                        $new_logs->activity_date = Carbon::now('UTC')->toDateTimeString();
                        $new_logs->action_by = $user_id;
                        $new_logs->save();*/
                        $desc = 'created an article "'.ucfirst($request->title).'"';
                        Helpers::add_activity_logs(['km-article-add',$add_article->post_id,$desc]);

                        $article_id = $add_article->post_id;
                        if(count($request->tags)!=0){
                            foreach($request->tags as $tag){
                                $check_tags = Tags::where('account_id', $account_id)->where('tag_name', $tag)->count();
        
                                if($check_tags){
                                    $tag_id = Tags::where('account_id',  $account_id)->where('tag_name', $tag)->first()->tag_id;
                                    
                                    //Add Post Tags (Relationship)
                                    $add_post_tags = new PostTags;
                                    $add_post_tags->post_id = $article_id;
                                    $add_post_tags->tag_id = $tag_id;
                                    $add_post_tags->save();
                                }else{
                                    $b[] = 'Add tag';
        
                                    //Add Tags
                                    $add_tags = new Tags;
                                    $add_tags->account_id = $account_id;
                                    $add_tags->tag_name = $tag;
                                    $add_tags->save();
                                    $tag_id = $add_tags->tag_id;
                                    
                                    //Add Post Tags (Relationship)
                                    $add_post_tags = new PostTags;
                                    $add_post_tags->post_id = $article_id;
                                    $add_post_tags->tag_id = $tag_id;
                                    $add_post_tags->save();
                                }
                            }
                        }
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add article. Error occured during process.';
                    }
                }

            }else if($request->ref=='edit'){
                $id = $request->id;
                $check_record = Posts::where('account_id', $account_id)->where('post_id', $id)->count();

                if($check_record==0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or article might already deleted.';
                }else{
                    $check1 = Posts::where('account_id', $account_id)->where('post_id', $id)->where('post_title', $request->title)->count();
                    //If title is updated
                    $check2 = Posts::where('account_id', $account_id)->where('post_title', $request->title)->count();
                    $update = '';

                    if($check1!=0){
                        $update = $this->query_update($id, $request, $title, $permalink);
                        $response['a'] = 'proceed 1';
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Article';
                        $response['stat_msg'] = 'Article already exists.';
                    }else{
                        $update = $this->query_update($id, $request, $title, $permalink);
                        $response['a'] = 'proceed 2';
                    }

                    if($update){
                        if($update=='success'){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Article has been successfully updated';
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update. Invalid reference ID or article might already deleted.';
                        }
                    }

                }
            }
        }

        return $response;
    }

    #For comment and likes
    public function store_alt(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $timezone = Options::apps('timezone');
        $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
        
        if($request->action=='like'){
            $post_id = $request->id;
            $like = '';
            
            $check_record = Posts::where('account_id', $account_id)->where('post_id', $post_id)->count();

            if($check_record){
                $check_like = PostLikes::where('account_id', $account_id)->where('post_id', $post_id)->where('like_by', $user_id)->first();
                

                if($check_like){
                    $like_id = $check_like->like_id;
                    $like = PostLikes::find($like_id)->delete();

                    if($like){
                        $count_likes = PostLikes::where('account_id', $account_id)->where('post_id', $post_id)->count();
                        //$count_likes--;
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['total_likes'] = ($count_likes>0) ? $count_likes : 0;
                        $response['a'] = $count_likes;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to unlike this post. Error occured during process';
                    }
                    $response['return'] = 'unlike';
                }else{
                    //Add like
                    $like = new PostLikes;
                    $like->account_id = $account_id;
                    $like->post_id = $post_id;
                    $like->like_by = $user_id;
                    $like->date_liked = Carbon::now('UTC')->toDateTimeString();
                    $like->save();

                    if($like){
                        $count_likes = PostLikes::where('account_id', $account_id)->where('post_id', $post_id)->count();
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['total_likes'] = $count_likes;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to like this post. Error occured during process';
                    }
                    $response['return'] = 'like';
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to like this page. Invalid reference ID or article might already deleted.';    
            }
           
        }
        else if($request->action=='comment'){
            $rules = [
                'comment'=>"required|min:1|max:1000",
            ];
    
            $messages = [
                'title.regex' => 'The :attribute format is invalid.',
            ];
    
            $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
            $val_msg = $validator->errors();
    
            if($validator->fails()) {
                $response['val_error']= $val_msg;
            }else{
                $check_record = Posts::where('account_id', $account_id)->where('post_id', $request->id)->count();

                if($check_record!=0){
                    $post_comment = new PostComments;
                    $post_comment->account_id = $account_id;
                    $post_comment->post_id = $request->id;
                    $post_comment->comment_by = $user_id;
                    $post_comment->comment_content = $request->comment;
                    $post_comment->save();

                    if($post_comment){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Comment has been posted successfully';
                        $response['commentor'] = Auth::user()->user_fullname;
                        $response['avatar'] = (Auth::user()->user_avatar) ? Auth::user()->user_avatar : custom_asset('img/avatars/avatar-1');
                        $response['date'] = date('d-M-Y h:iA', strtotime($post_comment->date_recorded .  $interval));

                        //Add Logs
                        $post_detail = Posts::find($request->id);
                        $desc = 'commented "'.$request->comment.'" on article "'.$post_detail->post_title.'"';
                        Helpers::add_activity_logs(['km-article-post-comment',$request->id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to post comment. Error occured during process';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to post comment. Invalid reference ID or article might already deleted.';
                }
            }
        }

        return $response;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        if(!Helpers::get_user_privilege('pkg_km_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $check_record = Posts::where('account_id', $account_id)->where('post_id', $request->id)->count();

            if($check_record!=0){
                $post_detail = Posts::find($request->id);
                $check_record = Posts::find($request->id)->delete();

                if($check_record!=0){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Article has been successfully deleted successfully';
                    $response['a'] = $request->origin;

                    if($request->origin=='form'){
                        $msg_key = str_random(15);
                        session()->put($msg_key, "Article has been successfully deleted successfully");
                        $response['msg_key'] = $msg_key;
                    }

                    $delete_tags = PostTags::where('post_id', $request->id)->delete();
                    $delete_comments = PostComments::where('post_id', $request->id)->delete();
                    $delete_likes = PostLikes::where('post_id', $request->id)->delete();

                    $desc = 'deleted an article "'.$post_detail->post_title.'"';
                    Helpers::add_activity_logs(['km-article-delete',0,$desc]);
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to delete. Invalid reference ID or article might already deleted.';
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to delete. Invalid reference ID or article might already deleted.';
            }
        }

        return $response;
    }

    function query_update($id, Request $request, $title, $permalink){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_fullname = Auth::user()->user_fullname;

        $update_article = Posts::find($id);
        $update_article->account_id = $account_id;
        $update_article->cat_id = $request->category;
        $update_article->post_title = ucfirst($request->title);
        $update_article->post_content = $request->content;
        $update_article->post_name = $title;
        $update_article->post_permalinks = $permalink;
        $update_article->post_stat = ($request->status) ? $request->status : 'draft';
        $update_article->is_featured = ($request->is_featured)?1:0;
        $update_article->recorded_by = $user_id;
        $update_article->updated_by = $user_id;
        $update_article->save();

        /*
        |----------------------------------------
        | Tags
        |--------------------------------------
        */
        $query = "SELECT
            sys_km_post_tags.id, sys_km_post_tags.post_id, sys_km_tags.tag_id, sys_km_tags.tag_name 
            FROM `sys_km_post_tags` 
            INNER JOIN sys_km_tags 
                ON sys_km_tags.tag_id = sys_km_post_tags.tag_id 
            WHERE sys_km_post_tags.post_id = '".$id."'";
        $post_tags =  DB::select($query);

        $for_delete_posttag_id = [];
        $for_delete_tag_id = [];
        $for_delete_tag = [];
        $existing_post_tag = [];
        foreach($post_tags as $a){
            if(count($request->tags)){
                if(!in_array($a->tag_name, array_map('strtolower', $request->tags))){
                    $for_delete_posttag_id[] = $a->id;
                    $for_delete_tag_id[] = $a->tag_id;
                    $for_delete_tag[] = strtolower($a->tag_name);
                }else{
                    $existing_post_tag[] = strtolower($a->tag_name);
                }
            }else{
                $for_delete_posttag_id[] = $a->id;
            }
        }

        if(count($request->tags)!=0){
            foreach(array_map('strtolower', $request->tags) as $tag){
                if(!in_array($tag, $existing_post_tag)){
                    $check_tags = Tags::where('account_id', $account_id)->where('tag_name', $tag)->count();

                    if($check_tags){
                        $tag_id = Tags::where('account_id',  $account_id)->where('tag_name', $tag)->first()->tag_id;
                        
                        //Add Post Tags (Relationship)
                        $add_post_tags = new PostTags;
                        $add_post_tags->post_id = $id;
                        $add_post_tags->tag_id = $tag_id;
                        $add_post_tags->save();
                    }else{
                        //Add Tags
                        $add_tags = new Tags;
                        $add_tags->account_id = $account_id;
                        $add_tags->tag_name = $tag;
                        $add_tags->save();
                        $tag_id = $add_tags->tag_id;
                        
                        //Add Post Tags (Relationship)
                        $add_post_tags = new PostTags;
                        $add_post_tags->post_id = $id;
                        $add_post_tags->tag_id = $tag_id;
                        $add_post_tags->save();
                    }
                }
            }
        }

        //Delete the Post tag relation link
        foreach($for_delete_posttag_id as $post_tag_id){
            $delete = PostTags::find($post_tag_id)->delete();
        }
        /*
        |----------------------------------------
        | End of Post Tags
        |--------------------------------------
        */

        //Add Logs
        $desc = 'updated an article "'.ucfirst($request->title).'"';
        Helpers::add_activity_logs(['km-article-add',$id,$desc]);

        if($update_article){
            $stat = 'success';
        }else{
            $stat = 'error';
        }

        return $stat;
    }
}
