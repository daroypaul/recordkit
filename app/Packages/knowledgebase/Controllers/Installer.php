<?php

namespace App\Packages\Knowledgebase\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Utilities\Migrations as Migrations;
use App\modules\adminprofile\models\Categories;
use Artisan;
use Menus;
use Packages;
use Options;

class Installer
{

    public static function install($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Menu Data ###
        $get_primary_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->orderBy('menu_order')->get();
        $get_report_menu = Menus::where('account_id', $account_id)->where('menu_name', 'reports-main')->first()->menu_id;
        $menu_order = [];

        foreach($get_primary_menu as $key => $menu){
            $menu_order[] = $menu['menu_id'];
        }

        $index = array_search($get_report_menu, $menu_order);
        $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert before Reports Menu
        
        $child_menus = [
                            [
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-home",
                                "menu_label"=> "Home",
                                "menu_url"=> "kb",
                                "menu_icon"=> "mdi mdi-gauge m-r-10",
                                "privilege_key"=> '["all","pkg_km_view"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ],
                            [
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-article-list",
                                "menu_label"=> "All Articles",
                                "menu_url"=> "kb/article/all",
                                "menu_icon"=> "mdi mdi-file-document m-r-10",
                                "privilege_key"=> '["all","pkg_km_list"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ],
                            [
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-article-add",
                                "menu_label"=> "Add New Article",
                                "menu_url"=> "kb/article/add",
                                "menu_icon"=> "mdi mdi-plus-circle m-r-10",
                                "privilege_key"=> '["all","pkg_km_add"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ]
                      ];

        $order_num = 1;
        $menu_stat = [];
        foreach($menu_order as $item){
            if($item=='new'){//Add Menu
                //Prevent Duplicaction
                $check_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->first();

                if(!$check_menu){
                    // Add Parent Menu
                    $menu = new Menus;
                    $menu->account_id = $account_id;
                    $menu->parent_menu = 0;
                    $menu->menu_name = 'package-km-main';
                    $menu->menu_label = 'Knowledge Base';
                    $menu->menu_url = '#';
                    $menu->menu_icon = 'mdi mdi-information-outline';
                    $menu->privilege_key = '["all","pkg_km_view","pkg_km_list","pkg_km_add","pkg_km_edit"]';
                    $menu->menu_order = $order_num;
                    $menu->is_disable = 0;
                    $menu->save();

                    $parent_menu_id = $menu->menu_id;
                }else{
                    $menu = Menus::find($check_menu->menu_id);
                    $menu->is_disable = 0;
                    $menu->save();

                    $parent_menu_id = $check_menu->menu_id;
                }

                $menu_stat[] = $menu;

                // Add Children Menu
                foreach($child_menus as $cm){
                    //Prevent Duplicaction
                    $check_child_menu = Menus::where('account_id', $account_id)->where('menu_name', $cm['menu_name'])->first();

                    if(!$check_child_menu){
                        $menu = new Menus;
                        $menu->account_id = $account_id;
                        $menu->parent_menu = $parent_menu_id;
                        $menu->menu_name = $cm['menu_name'];
                        $menu->menu_label = $cm['menu_label'];
                        $menu->menu_url = $cm['menu_url'];
                        $menu->menu_icon = $cm['menu_icon'];
                        $menu->privilege_key = $cm['privilege_key'];
                        $menu->is_disable = 0;
                        $menu->save();
                    }else{
                        $menu = Menus::find($check_child_menu->menu_id);
                        $menu->parent_menu = $parent_menu_id;
                        $menu->is_disable = 0;
                        $menu->save();
                    }

                    $menu_stat[] = $menu;
                }
                //End of For Loop
            }else{//Update Menu Ordering
                $menu = Menus::find($item);
                $menu->menu_order = $order_num;
                $menu->save();
                $menu_stat[] = $menu;
            }

            $order_num++;
        }
        ### End of Menu Data Code ###

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }
        ######################

        ### Option for Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'article';
            $add_option->save();
        }
        ################################

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
        $package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

        if($output||$menu_stat||$add_option||$package_activate||$package_installed||$category_stat){
            return true;
        }else{
            return false;
        }
    }

    public static function uninstall($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations/drop';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Delete Migration Data ###
        $migrated_names = ['2018_07_21_041613_drop_sys_km_table','2018_04_09_051635_create_sys_km_posts_table','2018_04_09_052135_create_sys_km_post_comments_table','2018_04_09_052409_create_sys_km_post_likes_table','2018_04_09_052607_create_sys_km_post_tags_table','2018_04_09_052753_create_sys_km_tags_table'];

        $delete_migration = [];
        foreach($migrated_names as $migration){
            $check_migration = Migrations::where('migration', $migration)->first();
            
            if($check_migration){
                $id = $check_migration->id;
                
                //Delete Migration Data
                $delete_migration = Migrations::find($id)->delete();
            }
        }

        ### Delete Menu Data ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->count();
        $delete_menu = [];

        if($has_menu){
            $get_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->first();
            $menu_id = $get_menu->menu_id;

            ## Delete Parent Menu ##
            $delete_menu = Menus::find($menu_id)->delete();
        }

        $child_menus = [["menu_name"=> "package-km-home"],
                        ["menu_name"=> "package-km-article-list"],
                        ["menu_name"=> "package-km-article-add"]];

        //Delete Child Menu
        foreach($child_menus as $cm){
            $get_menu = Menus::where('account_id', $account_id)->where('menu_name', $cm['menu_name'])->first();

            if($get_menu){
                $menu_id = $get_menu->menu_id;
                $delete_menu = Menus::find($menu_id)->delete();
            }
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }
        #####################

        ### Delete Category List Option ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->count();
        $delete_option = false;

        if($has_option){
            $get_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->first();
            $option_id = $get_option->option_id;
            
            ## Delete Option ##
            $delete_option = Options::find($option_id)->delete();
        }

        ### Delete Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;

        $arr_index = array();
        foreach ($json_data as $key => $value) {
            if($key=='xOVB3l9z0p4iUMcZ2ua7.js'){
                $arr_index[] = $key;
            }else if($key=='EhOeSv6li0prc931XMuK.js'){
                $arr_index[] = $key;
            }
        }

        if($arr_index){
            // delete json data
            foreach ($arr_index as $i)
            {
                unset($json_data[$i]);
            }

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        $package_uninstalled = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];

        if(count($delete_migration)||$delete_menu||$delete_option||$package_deactive||$package_uninstalled||$category_stat){
            return true;
        }else{
            return false;
        }
    }

    public static function activate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu Data ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->count();
        $menu_stat = [];

        if($has_menu){
            $get_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->first();
            $menu_id = $get_menu->menu_id;

            ## Activate Menu ##
            $menu = Menus::find($menu_id);
            $menu->is_disable = 0;
            $menu->save();
            $menu_stat[] = $menu;
        }
        //else{

            $get_primary_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->orderBy('menu_order')->get();
            $get_report_menu = Menus::where('account_id', $account_id)->where('menu_name', 'reports-main')->first()->menu_id;
            $menu_order = [];
            
            foreach($get_primary_menu as $key => $menu){
                $menu_order[] = $menu['menu_id'];
            }

            $index = array_search($get_report_menu, $menu_order);
            $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert before Reports Menu

            $child_menus = [[
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-home",
                                "menu_label"=> "Home",
                                "menu_url"=> "kb",
                                "menu_icon"=> "mdi mdi-gauge m-r-10",
                                "privilege_key"=> '["all","pkg_km_view","pkg_km_list","pkg_km_add"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ],
                            [
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-article-list",
                                "menu_label"=> "All Articles",
                                "menu_url"=> "kb/article/all",
                                "menu_icon"=> "mdi mdi-file-document m-r-10",
                                "privilege_key"=> '["all","pkg_km_list"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ],
                            [
                                "account_id"=> 1,
                                "parent_menu"=> 12,
                                "menu_name"=> "package-km-article-add",
                                "menu_label"=> "Add New Article",
                                "menu_url"=> "kb/article/add",
                                "menu_icon"=> "mdi mdi-plus-circle m-r-10",
                                "privilege_key"=> '["all","pkg_km_add"]',
                                "menu_order"=> null,
                                "is_disable"=> 0
                            ]];

            $order_num = 1;
            foreach($menu_order as $item){
                if($item=='new'){//Add Menu
                    //Prevent Duplicaction
                    $check_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->first();
    
                    if(!$check_menu){
                        // Add Parent Menu
                        $menu = new Menus;
                        $menu->account_id = $account_id;
                        $menu->parent_menu = 0;
                        $menu->menu_name = 'package-km-main';
                        $menu->menu_label = 'Knowledge Base';
                        $menu->menu_url = '#';
                        $menu->menu_icon = 'mdi mdi-information-outline';
                        $menu->privilege_key = '["all","pkg_km_view","pkg_km_list","pkg_km_add"]';
                        $menu->menu_order = $order_num;
                        $menu->is_disable = 0;
                        $menu->save();
    
                        $parent_menu_id = $menu->menu_id;
                    }else{
                        $menu = Menus::find($check_menu->menu_id);
                        $menu->is_disable = 0;
                        $menu->save();
    
                        $parent_menu_id = $check_menu->menu_id;
                    }
    
                    $menu_stat[] = $menu;

                    // Add Children Menu
                    foreach($child_menus as $cm){
                        //Prevent Duplicaction
                        $check_child_menu = Menus::where('account_id', $account_id)->where('menu_name', $cm['menu_name'])->first();
    
                        if(!$check_child_menu){
                            $menu = new Menus;
                            $menu->account_id = $account_id;
                            $menu->parent_menu = $parent_menu_id;
                            $menu->menu_name = $cm['menu_name'];
                            $menu->menu_label = $cm['menu_label'];
                            $menu->menu_url = $cm['menu_url'];
                            $menu->menu_icon = $cm['menu_icon'];
                            $menu->privilege_key = $cm['privilege_key'];
                            $menu->is_disable = 0;
                            $menu->save();
                        }else{
                            $menu = Menus::find($check_child_menu->menu_id);
                            $menu->parent_menu = $parent_menu_id;
                            $menu->is_disable = 0;
                            $menu->save();
                        }
    
                        $menu_stat[] = $menu;
                    }//End of For loop
                }else{//Update Menu Ordering
                    $menu = Menus::find($item);
                    $menu->menu_order = $order_num;
                    $menu->save();
                    $menu_stat[] = $menu;
                }
    
                $order_num++;
            }//End of For Loop
        //}//End of If Statement
        ### End of Menu Data Code ###

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'article')
                                                ->update(['record_stat' => 0]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }
        ####################

        ### Option for Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'article';
            $add_option->save();
        }
        ################################

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];

        if($menu_stat||$add_option||$activate_package||$category_stat){
            return true;
        }else{
            return false;
        }
    }


    public static function deactivate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Delete Menu Data ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->count();
        $update_menu = [];

        if($has_menu){
            $get_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-km-main')->first();
            $menu_id = $get_menu->menu_id;

            ## Disable Menu ##
            $update_menu = Menus::find($menu_id);
            $update_menu->is_disable = 1;
            $update_menu->save();
        }

        $child_menus = [["menu_name"=> "package-km-home"],
                        ["menu_name"=> "package-km-article-list"],
                        ["menu_name"=> "package-km-article-add"]];

        //Delete Child Menu
        foreach($child_menus as $cm){
            $get_menu = Menus::where('account_id', $account_id)->where('menu_name', $cm['menu_name'])->first();
            $menu_id = $get_menu->menu_id;

            if($get_menu){
                //$update_menu = Menus::find($menu_id)->delete();
                ## Disable Menu ##
                $update_menu = Menus::find($menu_id);
                $update_menu->is_disable = 1;
                $update_menu->save();
            }
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'article')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'article')
                                                ->update(['record_stat' => 1]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }
        ### Delete Category List Option ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->count();
        $delete_option = false;

        if($has_option){
            $get_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'article')->first();
            $option_id = $get_option->option_id;
            
            ## Delete Option ##
            $delete_option = Options::find($option_id)->delete();
        }

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];

        if($update_menu||$delete_option||$package_deactive||$category_stat){
            return true;
        }else{
            return false;           
        }
    }
}
