<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Knowledgebase\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\modules\adminprofile\models\Categories;
//use App\Packages\Knowledgebase\Models\Categories;
use App\Packages\Knowledgebase\Models\Posts;
use App\Packages\Knowledgebase\Models\PostTags;
use App\Packages\Knowledgebase\Models\PostLikes;
use App\Packages\Knowledgebase\Models\PostComments;
use App\Packages\Knowledgebase\Models\Tags;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class SearchResult extends Controller
{
    public function search(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_km_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $total_post = 0;
        $post = [];

        $q = isset($_GET['q']) ? $_GET['q'] : '';

        /*SELECT sys_km_posts.* 
            FROM `sys_km_posts` 
        LEFT JOIN sys_categories
            ON sys_categories.cat_id = sys_km_posts.cat_id
        LEFT JOIN sys_km_post_tags
            ON sys_km_post_tags.post_id = sys_km_posts.post_id
        LEFT JOIN sys_km_tags
            ON sys_km_tags.tag_id = sys_km_post_tags.tag_id
        WHERE 
            sys_km_posts.account_id = 1
            AND sys_km_posts.post_stat = 'published'
        AND 
        (
        sys_km_posts.post_title LIKE '%microsoft%' OR
        sys_km_posts.post_content LIKE '%microsoft%' OR
        sys_km_tags.tag_name LIKE '%microsoft%' OR
        sys_categories.cat_name LIKE '%microsoft%'
        )
        GROUP BY sys_km_posts.post_id*/
        $post = DB::table('sys_km_posts')
                        ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_km_posts.cat_id')
                        ->leftJoin('sys_km_post_tags', 'sys_km_post_tags.post_id', '=', 'sys_km_posts.post_id')
                        ->leftJoin('sys_km_tags', 'sys_km_tags.tag_id', '=', 'sys_km_posts.post_id')
                        ->select('sys_km_posts.*')
                        ->where('sys_km_posts.account_id', $account_id)
                        ->where('sys_km_posts.post_stat', 'published')
                        ->where(function($query) use ($q) {
                            $query
                              ->where('sys_km_posts.post_title', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_km_posts.post_content', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_km_tags.tag_name', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_categories.cat_name', 'LIKE', '%'.$q.'%');
                          })
                        ->groupBy('sys_km_posts.post_id')
                        ->paginate(5);

        $total_post = DB::table('sys_km_posts')
                        ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_km_posts.cat_id')
                        ->leftJoin('sys_km_post_tags', 'sys_km_post_tags.post_id', '=', 'sys_km_posts.post_id')
                        ->leftJoin('sys_km_tags', 'sys_km_tags.tag_id', '=', 'sys_km_posts.post_id')
                        ->select('sys_km_posts.*')
                        ->where('sys_km_posts.account_id', $account_id)
                        ->where('sys_km_posts.post_stat', 'published')
                        ->where(function($query) use ($q) {
                            $query
                              ->where('sys_km_posts.post_title', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_km_posts.post_content', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_km_tags.tag_name', 'LIKE', '%'.$q.'%')
                              ->orWhere('sys_categories.cat_name', 'LIKE', '%'.$q.'%');
                          })
                        ->groupBy('sys_km_posts.post_id')
                        ->paginate(5)
                        ->count();

        $query = "SELECT count(*) as total_result 
                        FROM `sys_km_posts` 
                    LEFT JOIN sys_categories
                        ON sys_categories.cat_id = sys_km_posts.cat_id
                    LEFT JOIN sys_km_post_tags
                        ON sys_km_post_tags.post_id = sys_km_posts.post_id
                    LEFT JOIN sys_km_tags
                        ON sys_km_tags.tag_id = sys_km_post_tags.tag_id
                    WHERE 
                        sys_km_posts.account_id = ".$account_id."
                        AND sys_km_posts.post_stat = 'published'
                    AND 
                    (
                    sys_km_posts.post_title LIKE '%".$q."%' OR
                    sys_km_posts.post_content LIKE '%".$q."%' OR
                    sys_km_tags.tag_name LIKE '%".$q."%' OR
                    sys_categories.cat_name LIKE '%".$q."%'
                    )
                    GROUP BY sys_km_posts.post_id";

        $data = [
            "form_title"=> "Search: ".$q,
            "result"=> $post,
            "type"=> "search",
            "total_post"=>$total_post,
            "search"=>$q
        ];

        //return $total_post;
        //return $total_likes;
        return view('knowledgebase::search_result')->with($data);

        //return $post;
    }

    public function category(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_km_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $total_post = 0;
        $post = [];

        $id = isset($_GET['c']) ? $_GET['c'] : '';
        $category = Categories::where('account_id',  $account_id)->where('apps_type', 'article')->where('cat_name', $id)->first();
        
        if($category){
            $cat_id = $category->cat_id;
        
            $post = Posts::where('account_id',  $account_id)->where('cat_id', $cat_id)->where('post_stat', 'published')->paginate(10);
            $total_post = Posts::where('account_id',  $account_id)->where('cat_id', $cat_id)->where('post_stat', 'published')->count();
        }
        
        $data = [
            "form_title"=> "Category: ".$id,
            "result"=> $post,
            "type"=> "category",
            "category"=> $id,
            "total_post"=>$total_post
        ];

        
        //return $total_likes;
        return view('knowledgebase::search_result')->with($data);

        //return $post;
    }

    public function tag(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_km_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }
        
        $account_id = Auth::user()->account_id;
        $total_post = 0;
        $post = [];

        $id = isset($_GET['t']) ? $_GET['t'] : '';
        $category = Categories::where('account_id',  $account_id)->where('apps_type', 'article')->where('cat_name', $id)->first();
        $tag = Tags::where('account_id',  $account_id)->where('tag_name', $id)->first();
        
        

        if($tag){
            //$cat_id = $category->cat_id;
        
            $query = "SELECT sys_km_posts.* 
                        FROM `sys_km_tags`
                    INNER JOIN sys_km_post_tags 
                        ON sys_km_post_tags.tag_id = sys_km_tags.tag_id
                    INNER JOIN sys_km_posts
                        ON sys_km_posts.post_id = sys_km_post_tags.post_id
                    WHERE sys_km_posts.account_id='".$account_id."' 
                        AND sys_km_tags.tag_name = '".$id."'
                    ORDER BY sys_km_posts.date_recorded desc";
            $post = DB::table('sys_km_tags')
                        ->join('sys_km_post_tags', 'sys_km_post_tags.tag_id', '=', 'sys_km_tags.tag_id')
                        ->join('sys_km_posts', 'sys_km_posts.post_id', '=', 'sys_km_post_tags.post_id')
                        ->select('sys_km_posts.*')
                        ->where('sys_km_posts.account_id', $account_id)
                        ->where('sys_km_tags.tag_name', $id)
                        ->where('sys_km_posts.post_stat', 'published')
                        ->groupBy('sys_km_posts.post_id')
                        ->paginate(10);
            $total_post = DB::table('sys_km_tags')
                        ->join('sys_km_post_tags', 'sys_km_post_tags.tag_id', '=', 'sys_km_tags.tag_id')
                        ->join('sys_km_posts', 'sys_km_posts.post_id', '=', 'sys_km_post_tags.post_id')
                        ->select('sys_km_posts.*')
                        ->where('sys_km_posts.account_id', $account_id)
                        ->where('sys_km_tags.tag_name', $id)
                        ->where('sys_km_posts.post_stat', 'published')
                        ->groupBy('sys_km_posts.post_id')
                        ->count();

            $d = DB::table('sys_km_tags')
                    ->join('sys_km_post_tags', 'sys_km_post_tags.tag_id', '=', 'sys_km_tags.tag_id')
                    ->select('sys_km_tags.*')
                    ->where('sys_km_tags.account_id', $account_id)
                    ->groupBy('sys_km_tags.tag_name')
                    ->get();
            //$post =  DB::select($query)->paginate(2);
            //$total_post = Posts::where('account_id',  $account_id)->where('cat_id', $cat_id)->count();
        }
        
        //return $d;

        $data = [
            "form_title"=> "Tag: ".$id,
            "result"=> $post,
            "type"=> "tag",
            "tag"=> $id,
            "total_post"=>$total_post
        ];

        //return $total_post;
        //return $total_likes;
        return view('knowledgebase::search_result')->with($data);

        //return $post;
    }
}
