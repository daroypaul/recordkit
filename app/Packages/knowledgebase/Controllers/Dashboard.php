<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Knowledgebase\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\modules\adminprofile\models\Categories;
use App\Packages\Knowledgebase\Models\Posts;
use App\Packages\Knowledgebase\Models\PostTags;
use App\Packages\Knowledgebase\Models\PostLikes;
use App\Packages\Knowledgebase\Models\PostComments;
use App\Packages\Knowledgebase\Models\Tags;

use Session;
use Helpers;

class Dashboard extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        
        if(!Helpers::get_user_privilege('pkg_km_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $categories = Categories::where('account_id',  $account_id)->where('apps_type', 'article')->orderBy('cat_name', 'asc')->get();

        $post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->where('post_stat', 'published')->orderBy('date_recorded', 'desc')->orderBy('cat_id', 'asc')->get();

        $data = [
            "form_title"=> "Home",
            "categories"=>$categories,
            "articles"=>$post,
        ];


        //return $post;
        return view('knowledgebase::dashboard')->with($data);
        //$user = Auth::user();

        //return 'Dashboard';
    }
}
