<?php

namespace App\Packages\Knowledgebase\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use View;
use Schema;

class KnowledgeBaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view) {
            if(DB::connection()->getDatabaseName() && Schema::hasTable('sys_km_posts'))
            {
                $id = (Auth::check()) ? Auth::user()->account_id : 0;
                
                $view->with('latest_article', \App\Packages\Knowledgebase\Models\Posts::with(['recorder','updator','category'])->where('account_id',  $id)->where('post_stat', 'published')->orderBy('date_recorded', 'desc')->limit(5)->get());
                $view->with('featured_article', \App\Packages\Knowledgebase\Models\Posts::with(['recorder','updator','category'])->where('account_id',  $id)->where('post_stat', 'published')->where('is_featured', 1)->orderBy('date_recorded', 'desc')->limit(5)->get());
                $view->with('cloud_tags', DB::table('sys_km_tags')
                    ->join('sys_km_post_tags', 'sys_km_post_tags.tag_id', '=', 'sys_km_tags.tag_id')
                    ->select('sys_km_tags.*')
                    ->where('sys_km_tags.account_id', $id)
                    ->groupBy('sys_km_tags.tag_name')
                    ->get());
            }
        });
    }



    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
