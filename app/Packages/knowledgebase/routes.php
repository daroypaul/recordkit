<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'kb', 'namespace' => 'App\Packages\knowledgebase\Controllers'], function()
{
    Route::get('/', 'Dashboard@index');

    #Articles
    //Route::get('/{articles}', 'Articles@index');
    //Route::get('/article', 'Articles@index');
    Route::get('/article/all', 'Articles@all_list');
    Route::post('/article/all', 'Articles@delete');
    Route::get('/article/add', 'Articles@add');
    Route::post('/article/add', 'Articles@store');
    Route::get('/article/{id}/edit', 'Articles@edit');
    Route::post('/article/{id}/edit', 'Articles@store');
    Route::get('/article/{id}', 'Articles@view');
    Route::post('/article/{id}', 'Articles@store_alt');#Comment and Likes
    Route::get('/category/{id}', 'SearchResult@category');
    Route::get('/category', 'SearchResult@category');
    Route::get('/tag/{id}', 'SearchResult@tag');
    Route::get('/tag', 'SearchResult@tag');
    Route::get('/search', 'SearchResult@search');
    //Route::get('/search/{q}', 'SearchResult@search');
});