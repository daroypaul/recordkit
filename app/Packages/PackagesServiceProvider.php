<?php

namespace App\Packages;

use Illuminate\Support\ServiceProvider;
use Packages;
use App;
use File;

class PackagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $packages = packages::scan_folders();

        foreach($packages as $package){
            $json_file_base = __DIR__.'/'.$package.'/package.json';

            //Check if json file is exist
            if(file_exists($json_file_base)) {
                $get_data = file_get_contents($json_file_base);
                $json_data = json_decode($get_data, true);

                $is_package_active = $json_data['active'];

                //Check if Package is active && installed
                if($is_package_active!=0&&$json_data['installed']!=0){
                    // Load the routes for each of the modules
                    if(file_exists(__DIR__.'/'.$package.'/routes.php')) {
                        include __DIR__.'/'.$package.'/routes.php';
                    }

                    // Load the views
                    if(is_dir(__DIR__.'/'.$package.'/resources/views')) {
                        $this->loadViewsFrom(__DIR__.'/'.$package.'/resources/views', $package);
                    }
                }
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        ## Auto Load Package Service Provider ##
        $scan_packages = packages::scan_packages();//Scan All Packages
        $packages_data = Packages::data();//Scan Packages Data

        foreach($scan_packages as $package){
            $explode_dir = explode('/', $package);
            $package_name = end($explode_dir);

            $src = base_path($package . '/include.php');//Get the base path of the package include file

            if(File::exists($src) && isset($packages_data[$package_name]) && $packages_data[$package_name]['installed'] && $packages_data[$package_name]['active'] ){//Check include file is existed
                require_once($src);
            }
        }
    }
}
