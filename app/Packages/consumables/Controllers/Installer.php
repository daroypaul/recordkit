<?php

namespace App\Packages\Consumables\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Utilities\Migrations as Migrations;
use App\modules\adminprofile\models\Categories;
use Artisan;
use Menus;
use Packages;
use Options;

class Installer
{

    public static function install($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Menu ###
        $get_primary_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->orderBy('menu_order')->get();
        $get_asset_menu = Menus::where('account_id', $account_id)->where('menu_name', 'fix-asset-main')->first();
        $get_report_menu = Menus::where('account_id', $account_id)->where('menu_name', 'reports-main')->first();

        $menu_order = [];
        $menu_stat = [];

        foreach($get_primary_menu as $key => $menu){
            $menu_order[] = $menu['menu_id'];
        }

        if($get_asset_menu){
            $asset_menu_id = $get_asset_menu->menu_id;
            $index = array_search($asset_menu_id, $menu_order) + 1;
            $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert after
        }else{
            $report_menu_id = $get_report_menu->menu_id;
            $index = array_search($report_menu_id, $menu_order);
            $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert before          
        }

        $order_num = 1;
        foreach($menu_order as $item){
            if($item=='new'){//Add Menu
                $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-consumables')->first();

                if($has_menu){
                    //$get_menu = Menus::where('menu_name', 'package-consumables')->first();
                    $menu_id = $has_menu->menu_id;
        
                    ## Activating Menu ##
                    $menu = Menus::find($menu_id);
                    $menu->is_disable = 0;
                    $menu->menu_order = $order_num;
                    $menu->save();
                }else{
                    ### Add Menu Item ###
                    $menu = new Menus;
                    $menu->account_id = $account_id;
                    $menu->parent_menu = 0;
                    $menu->menu_name = 'package-consumables';
                    $menu->menu_label = 'Consumables';
                    $menu->menu_url = 'consumables';
                    $menu->menu_icon = 'mdi mdi-recycle';
                    $menu->privilege_key = '["all","pkg_consumable_list"]';
                    $menu->is_disable = 0;
                    $menu->menu_order = $order_num;
                    $menu->save();
                }

                $menu_stat[] = $menu;
            }else{
                $menu = Menus::find($item);
                $menu->menu_order = $order_num;
                $menu->save();
                $menu_stat[] = $menu;
            }

            $order_num++;
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }

        ### Option for Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'consumable';
            $add_option->save();
        }

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
        $package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

        if($menu_stat||$category_stat||$add_option||$package_installed||$package_activate){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function uninstall($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations/drop';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Delete Migration Data ###
        $migrated_names = ['2018_07_21_041613_drop_sys_consumable_table', '2018_05_03_090244_create_sys_consumable_table', '2018_05_20_013719_create_sys_consumable_checkout_table'];

        $delete_migration = [];
        foreach($migrated_names as $migration){
            $check_migration = Migrations::where('migration', $migration)->first();
            
            if($check_migration){
                $id = $check_migration->id;
                
                //Delete Migration Data
                $delete_migration = Migrations::find($id)->delete();
            }
        }

        ### Delete Menu Data ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-consumables')->count();
        $delete_menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-consumables')->first();
            $menu_id = $get_menu->menu_id;

            ## Activating Menu ##
            $delete_menu = Menus::find($menu_id)->delete();
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }

        ### Delete Category List Option ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $delete_option = false;

        if($has_option){
            $get_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->first();
            $option_id = $get_option->option_id;
            
            ## Delete Option ##
            $delete_option = Options::find($option_id)->delete();
        }

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        $package_uninstalled = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];
        
        if(count($delete_migration)||$category_stat||$delete_menu||$delete_option||$package_deactive||$package_uninstalled){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function activate($package)
    {
        $account_id = Auth::user()->account_id;
        

        ### Menu ###
        $get_primary_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->orderBy('menu_order')->get();
        $get_asset_menu = Menus::where('account_id', $account_id)->where('menu_name', 'fix-asset-main')->first();
        $get_report_menu = Menus::where('account_id', $account_id)->where('menu_name', 'reports-main')->first();

        $report_menu_id = $get_report_menu->menu_id;
        $menu_order = [];
        $menu_stat = [];

        foreach($get_primary_menu as $key => $menu){
            $menu_order[] = $menu['menu_id'];
        }

        if($get_asset_menu){
            $asset_menu_id = $get_asset_menu->menu_id;
            $index = array_search($asset_menu_id, $menu_order) + 1;
            $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert after
        }else{
            $index = array_search($report_menu_id, $menu_order);
            $update_ordering = array_splice($menu_order , $index, 0, 'new');//Insert before          
        }

        $order_num = 1;
        foreach($menu_order as $item){
            if($item=='new'){//Add Menu
                $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-consumables')->first();

                if($has_menu){
                    $menu_id = $has_menu->menu_id;
        
                    ## Activating Menu ##
                    $menu = Menus::find($menu_id);
                    $menu->is_disable = 0;
                    $menu->menu_order = $order_num;
                    $menu->save();
                }else{
                    ### Add Menu Item ###
                    $menu = new Menus;
                    $menu->account_id = $account_id;
                    $menu->parent_menu = 0;
                    $menu->menu_name = 'package-consumables';
                    $menu->menu_label = 'Consumables';
                    $menu->menu_url = 'consumables';
                    $menu->menu_icon = 'mdi mdi-recycle';
                    $menu->privilege_key = '["all","pkg_consumable_list"]';
                    $menu->is_disable = 0;
                    $menu->menu_order = $order_num;
                    $menu->save();
                }

                $menu_stat[] = $menu;
            }else{
                $menu = Menus::find($item);
                $menu->menu_order = $order_num;
                $menu->save();
                $menu_stat[] = $menu;
            }

            $order_num++;
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'consumable')
                                                ->update(['record_stat' => 0]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }

        ### Option Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'consumable';
            $add_option->save();
        }

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];

        if($menu_stat||$category_stat||$add_option||$activate_package){
            return true;
        }else{
            return false;
        }



        ### Menu ###
        /*$has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-consumables')->count();
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-consumables')->first();
            $menu_id = $get_menu->menu_id;

            ## Disable Menu ##
            $menu = Menus::find($menu_id);
            $menu->is_disable = 0;
            $menu->save();
        }else{
            $get_menu = Menus::where('menu_name', 'fix-asset-main')->first();
            $parent_menu = $get_menu->menu_id;

            ### Add Menu Item ###
            $menu = new Menus;
            $menu->account_id = $account_id;
            $menu->parent_menu = $parent_menu;
            $menu->menu_name = 'package-consumables';
            $menu->menu_label = 'Consumables';
            $menu->menu_url = 'consumables';
            $menu->menu_icon = 'mdi mdi-recycle';
            $menu->privilege_key = '["all","pkg_consumable_list"]';
            $menu->is_disable = 0;
            $menu->save();
        }

        ### Option Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'consumable';
            $add_option->save();
        }

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];

        if($menu||$add_option||$activate_package){
            return true;
        }else{
            return false;
        }*/
    }


    public static function deactivate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-consumables')->count();
        $update_menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-consumables')->first();
            $parent_menu = $get_menu->menu_id;

            ## Disable Menu ##
            $update_menu = Menus::find($parent_menu);
            $update_menu->is_disable = 1;
            $update_menu->save();
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'consumable')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'consumable')
                                                ->update(['record_stat' => 1]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }

        ### Option Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $delete_option = false;

        if($has_option){
            $get_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->first();
            $option_id = $get_option->option_id;
            
            ## Delete Option ##
            $delete_option = Options::find($option_id)->delete();
        }

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];

        if($update_menu||$category_stat||$delete_option||$activate_package){
            return true;
        }else{
            return false;           
        }
    }
}
