<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Consumables\Models;

use Illuminate\Database\Eloquent\Model;

class ConsumableCheckout extends Model
{
    protected $table = 'sys_consumable_checkout';
    protected $primaryKey = 'checkout_id';
    public $timestamps = false;
}
