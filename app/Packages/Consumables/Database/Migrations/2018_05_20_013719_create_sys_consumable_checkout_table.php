<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConsumableCheckoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_consumable_checkout', function (Blueprint $table) {
            $table->increments('checkout_id');
            $table->integer('account_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('checkout_to')->nullable();
            $table->date('date_checkout')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->integer('recorded_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_checkout');
    }
}
