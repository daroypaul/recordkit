<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConsumableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_consumable', function (Blueprint $table) {
            $table->increments('item_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->string('item_name', 100)->nullable();
            $table->string('item_model', 100)->nullable();
            $table->string('item_no', 100)->nullable();
            $table->string('item_brand', 100)->nullable();
            $table->string('po_number', 100)->nullable();
            $table->date('date_purchased')->nullable();
            $table->string('purchased_cost', 100)->nullable();
            $table->integer('qty_remain')->nullable();
            $table->integer('qty_alert')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('record_stat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable');
    }
}
