<?php

namespace App\Packages\Consumables\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Packages\Consumables\Models\Consumable;
use App\Packages\Consumables\Models\ConsumableCheckout as Checkout;

use Options;
use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;

class ConsumableController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function page()
    {
        if(!Helpers::get_user_privilege('pkg_consumable_list')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $consumable = Consumable::with(['category','site'])->where('account_id', $account_id)->where('record_stat', 0)->get();


        $response = [
            "consumables"=>$consumable,
            "account_id"=>$account_id,
            "form_title"=>"Consumables"
        ];

        return view('consumables::index')->with($response);
    }

    public function index()
    {
        /*//$consumables = Consumable::paginate(5);
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $consumables = Consumable::with(['category','site'])->where('account_id', $account_id)->where('record_stat', 0)->get();

        return ConsumableResource::collection($consumables);
        //return view('consumables::index');*/
        
        //$account_id = Auth::user()->account_id;

        //$consumables = Consumable::with(['category','site'])->where('account_id', $account_id)->where('record_stat', 0)->get();

        //return ConsumableResource::collection($consumables);
        return Auth::guard('api')->user();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('consumables::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $currency = Options::apps('currency')->currency_code;

        $rules = [
            'name'=>'required|min:3|max:100|regex:/(^[A-Za-z0-9 ,-._()]+$)+$/u',
            'model'=>'required|min:3|max:100|regex:/(^[A-Za-z0-9 ,-._()]+$)+$/u',
            'qty'=>'required|min:1|max:10000000|numeric',
            'min_qty'=>'required|min:1|max:10000000|numeric',
            'site'=>'required',
            'category'=>'required',
        ];

        if($request->brand){
            $rules['brand'] = 'max:100|regex:/(^[A-Za-z0-9 ,-._()]+$)+$/u';
        }

        if($request->purchased_date){
            $rules['purchased_date'] = 'date_format:"M d, Y"';
        }

        $messages = [
            'purchased_date.date_format' => 'Valid date format (Jan 01, 2018)',
            'qty.min' => 'Must be at least 1',
            'min_qty.min' => 'Must be at least 1',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation

        if($request->qty<$request->min_qty){
            $validator->getMessageBag()->add('qty', 'Qty must be not lower than min Qty');
        }

        $val_msg = $validator->errors();
        $response = [];
        $response = [];

        if($request->ref=='add'&&!Helpers::get_user_privilege('pkg_consumable_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='edit'&&!Helpers::get_user_privilege('pkg_consumable_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{
            if($request->ref=='add'){
                $purchased_date  = ($request->purchased_date) ? date('Y-m-d', strtotime($request->purchased_date)) : Null;
                $add = new Consumable;
                $add->account_id = $account_id;
                $add->site_id = $request->site;
                $add->cat_id = $request->category;
                $add->item_name = $request->name;
                $add->item_model = $request->model;
                $add->item_brand = $request->brand;
                $add->po_number = $request->order_number;
                $add->date_purchased = $purchased_date;
                $add->purchased_cost = Helpers::converts('currency_to_int', $request->cost);
                $add->qty_remain = $request->qty;
                $add->qty_alert = $request->min_qty;
                $add->recorded_by = $user_id;
                $add->updated_by = $user_id;
                $add->record_stat = 0;
                $add->save();

                if($add){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'New consumable successfully added';

                    $consumable = Consumable::with(['category','site'])->find($add->item_id);


                    $response['id'] = $consumable->item_id;
                    $response['name'] = $consumable->item_name;
                    $response['model'] = $consumable->item_model;
                    $response['brand'] = ($consumable->item_model) ? $consumable->item_model : '---';
                    $response['cost'] = ($consumable->purchased_cost) ? number_format($consumable->purchased_cost, 2) . ' ' . $currency : '---';
                    $response['date_purchased'] = ($consumable->date_purchased&&$consumable->date_purchased!='0000-00-00') ? date('d-M-Y', strtotime($consumable->date_purchased)) : '---';
                    $response['site'] = $consumable->site->site_name;
                    $response['category'] = $consumable->category->cat_name;
                    $response['qty'] = number_format($consumable->qty_remain);
                    $response['qty_limit'] = number_format($consumable->qty_alert);
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to add consumable. Error occured during process.';
                }
            }else if($request->ref=='edit'){
                $id= $request->id;
                $check_record = Consumable::where('account_id', $account_id)->where('item_id', $id)->count();

                if($check_record){
                    //If remarks is updated and name is constant
                    $check1 = Consumable::where('account_id', $account_id)->where('item_id', $id)->where('item_name', $request->name)->count();
                    //If name is updated
                    $check2 = Consumable::where('account_id', $account_id)->where('item_name', $request->name)->count();

                    if($check1!=0){
                        $update = static::update($id, $request);
                        $update_done = true;
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'Consumable already exists.';
                        $update_done = false;
                    }else{
                        $update = static::update($id, $request);
                        $update_done = true;
                    }

                    if($update_done){
                        if($update){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Consumable has been successfully updated';

                            $response['id'] = $update->item_id;
                            $response['name'] = $update->item_name;
                            $response['model'] = $update->item_model;
                            $response['brand'] = ($update->item_model) ? $update->item_model : '---';
                            $response['cost'] = ($update->purchased_cost) ? number_format($update->purchased_cost, 2) . ' ' . $currency : '---';
                            $response['date_purchased'] = ($update->date_purchased&&$update->date_purchased!='0000-00-00') ? date('d-M-Y', strtotime($update->date_purchased)) : '---';
                            $response['site'] = $update->site->site_name;
                            $response['category'] = $update->category->cat_name;
                            $response['qty'] = number_format($update->qty_remain);
                            
                            $response['qty_limit'] = number_format($update->qty_alert);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update. Error occured during process.';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or record might be deleted.';
                }
            }
        }

        return $response;
    }

    public function fetch_data(Request $request)
    {
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $currency = Options::apps('currency')->currency_code;

        if($request->ref=='single'){
            $id = $request->id;
            $consumables = Consumable::with(['site','category'])->where('account_id', $account_id)->where('item_id', $id)->first();
        }else{
            //$site_id = ($request->site!='all')
            if($request->site){
                $site_id = $request->site;
                $get_record = Consumable::with(['site','category'])->where('account_id', $account_id)->where('site_id', $site_id)->get();
            }else{
                $get_record = Consumable::with(['site','category'])->where('account_id', $account_id)->get();
            }
            
            $a = [];
            $consumables = [];
            foreach($get_record as $record){
                $a['id'] = $record->item_id;
                $a['name'] = $record->item_name;
                $a['model'] = $record->item_model;
                $a['brand'] = ($record->item_model) ? $record->item_model : '---';
                $a['cost'] = ($record->purchased_cost) ? number_format($record->purchased_cost, 2) . ' ' . $currency : '---';
                $a['date_purchased'] = ($record->date_purchased&&$record->date_purchased!='0000-00-00') ? date('d-M-Y', strtotime($record->date_purchased)) : '---';
                $a['site'] = $record->site->site_name;
                $a['category'] = $record->category->cat_name;
                $a['qty'] = number_format($record->qty_remain);
                $a['qty_limit'] = number_format($record->qty_alert);

                $consumables[] = $a;
            }
        }

        //return new ConsumableResource($consumables);

        return $consumables;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        //return view('consumables::show');

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $currency = Options::apps('currency')->currency_code;

        $consumables = Consumable::with(['site','category'])->where('account_id', $account_id)->where('item_id', $id)->first();

        return new ConsumableResource($consumables);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('consumables::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public static function update($id, Request $request)
    {
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $purchased_date  = ($request->purchased_date) ? date('Y-m-d', strtotime($request->purchased_date)) : Null;

        $update = Consumable::find($id);
        $update->site_id = $request->site;
        $update->cat_id = $request->category;
        $update->item_name = $request->name;
        $update->item_model = $request->model;
        $update->item_brand = $request->brand;
        $update->po_number = $request->order_number;
        $update->date_purchased = $purchased_date;
        $update->purchased_cost = Helpers::converts('currency_to_int', $request->cost);
        $update->qty_remain = $request->qty;
        $update->qty_alert = $request->min_qty;
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        $del_type = $request->del_type;

        if(!Helpers::get_user_privilege('pkg_consumable_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            if($del_type=='single'){
                $item_id = $request->id;
                
                $check_record = Consumable::where('account_id', $account_id)->where('item_id', $item_id)->count();

                if($check_record){
                    $delete = Consumable::find($item_id)->delete();

                    if($delete){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Consumable has been successfully deleted';
                        $response['id'] = $item_id;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to delete. Error occured during process.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to delete. Invalid reference ID or record might be deleted.';
                }
            }else if($del_type=='multi'){
                $x = [];
                $s = [];
                foreach($request->id as $item_id){
                    //$asset_record = Assets::find($item_id);
                    
                    $check_record = Consumable::where('account_id', $account_id)->where('item_id', $item_id)->count();
                    if($check_record!=0){
                        $delete_record = Consumable::find($item_id)->delete();
    
                        if($delete_record){
                            $s[] = $item_id;
                            //$_log_desc .= $name . ', ';
                        }else{
                            $x[] = $item_id;
                        }
                    }else{
                        $x[] = $item_id;
                    }
                }
    
                $response['eid'] = $x;
                $response['sid'] = $s;
            }
            /**/
        }

        return $response;
    }

    public function checkout(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        
        $item_id = $request->id;
        $rules = [
            'employee'=>'required',
            'checkout_date'=>'required|date_format:"M d, Y"',
            'qty'=>'required|numeric|min:1',
        ];

        $messages = [
            'checkout_date.date_format' => 'Valid date format (Jan 01, 2018)',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation

        if($request->qty<$request->min_qty){
            $validator->getMessageBag()->add('qty', 'Qty must be not lower than min Qty');
        }

        $val_msg = $validator->errors();
        $response = [];

        if(!Helpers::get_user_privilege('pkg_consumable_checkout')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{    
            $check_record = Consumable::where('account_id', $account_id)->where('item_id', $item_id)->count();

            if($check_record){
                $check_stocks = Consumable::find($item_id)->qty_remain;
                
                if($request->qty > $check_stocks){
                    $validator->getMessageBag()->add('qty', 'Insufficient Stock');
                    $val_msg = $validator->errors();
                    $response['val_error']= $val_msg;
                }else{
                    $purchased_date  = ($request->purchased_date) ? date('Y-m-d', strtotime($request->purchased_date)) : Null;
                    $checkout = new Checkout;
                    $checkout->account_id = $account_id;
                    $checkout->item_id = $item_id;
                    $checkout->checkout_to = $request->employee;
                    $checkout->date_checkout = date('Y-m-d', strtotime($request->checkout_date));
                    $checkout->date_recorded = Carbon::now('UTC')->toDateTimeString();
                    $checkout->recorded_by = $user_id;
                    $checkout->save();

                    if($checkout){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Successfully Checked-Out';
                        $decrement = Consumable::find($item_id)->decrement('qty_remain', $request->qty);

                        $record = Consumable::find($item_id);
                        $response['stocks'] = $record->qty_remain;
                        $response['stocks_limit'] = $record->qty_alert;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to checkout. Error occured during process.';
                    }
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to proceed. Invalid reference ID or record might be deleted.';
            }
        }

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
        
    }
}
