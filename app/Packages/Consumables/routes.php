<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'consumables', 'namespace' => 'App\Packages\consumables\Controllers'], function()
{
    Route::get('/', 'ConsumableController@page');
    Route::post('/', 'ConsumableController@store');//Edit & Add
    Route::post('/fetchdata', 'ConsumableController@fetch_data');
    Route::post('/delete', 'ConsumableController@delete');
    Route::post('/checkout', 'ConsumableController@checkout');
    
    
    Route::get('/checkout/a', function() {
        return 'a';
    });
});