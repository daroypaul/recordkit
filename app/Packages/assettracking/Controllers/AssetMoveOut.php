<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetEvents;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\adminprofile\models\Employees;//Get the last number used

use Validator;
use Session;
use Helpers;#Custom Helper Class
use Carbon\Carbon;

class AssetMoveOut extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_transfer')){
            //session()->flash('denied_access_notify', 1);
            //return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
                "sites"=>$sites,
                "form_title"=> "Bulk Transfer Asset"
        ];

        return view('assettracking::asset_move')->with($data);

    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'move_date' => 'required|date_format:"F d, Y"',
            'site' => 'required',
            'location' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $val_msg = $validator->errors();
        
        //Check site and Location if not deleted
        $check_site = Site::where('account_id', $account_id)->where('site_id', $request->site)->where('record_stat',0)->count();
        $check_location = Location::where('account_id', $account_id)->where('location_id', $request->location)->where('record_stat',0)->count();

        if($check_site==0){
            $val_msg->add('site', 'Site already deleted');
            $val_msg->add('location', 'Location already deleted');
        }else{
            if($check_location==0){
                $val_msg->add('location', 'Location already deleted');
            }
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_transfer')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()||count($val_msg)!=0) {
            $response['val_errors']= $val_msg;
        }else if( count(array_filter($request->get('input_asset_id')))==0 ){
            $response['stat']= 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg']= 'Please add atleast one (1) asset';
        }else{
            $i = 0;
            $aa = [];
            $rec = 0;//success storing to database
            foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                $assets = Assets::where('account_id', $account_id)->where('asset_id', $asset_id)->first();

                if(count($assets)!=0){
                    $remarks = $request->get('input_asset_remarks')[$i];
                    //Add Asset Events
                    $new_events = new AssetEvents;
                    $new_events->account_id = $account_id;
                    $new_events->asset_id = $asset_id;
                    $new_events->event_type = 'movement';
                    $new_events->event_date = date('Y-m-d', strtotime($request->get('move_date')));
                    $new_events->event_remarks = $remarks;
                    $new_events->ref_id = 0;
                    $new_events->prev_site_id = $assets->site_id;
                    $new_events->prev_location_id = $assets->location_id;
                    $new_events->new_site_id = $request->get('site');
                    $new_events->new_location_id = $request->get('location');
                    $new_events->requestor_id = 0;
                    $new_events->recorded_by = $user_id;
                    $new_events->record_stat = 1;
                    $new_events->date_recorded = Carbon::now('UTC')->toDateTimeString();
                    $new_events->save();
                    $event_id = $new_events->event_id;

                    if($new_events){
                        $update_asset = Assets::find($asset_id);
                        $update_asset->site_id = $request->get('site');
                        $update_asset->location_id = $request->get('location');
                        $update_asset->updated_by = $user_id;
                        $update_asset->save();

                        if($update_asset){
                            $update_event = AssetEvents::find($event_id);
                            $update_event->record_stat = 0;
                            $update_event->save();


                            $desc = 'relocated asset ['.$assets->asset_tag.'] to new site and location';
                            Helpers::add_activity_logs(['ams-asset-relocate',$asset_id,$desc]);

                            $rec++;
                        }
                    }
                }

                $i++;
            }//End For
            $response['a'] = $rec;
            if($rec!=0){
                $response['stat']= 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg']= 'Asset(s) successfully transferred.';
            }
        }


        return response()->json($response);
    }
}
