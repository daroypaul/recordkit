<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\settings\models\CustomFields;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class ReportLease extends Controller
{

    public function index($type){
        if (!Auth::check())
        {
            $url = url()->full();
            return redirect('auth/login?continue='.$url);
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            
            $url = url()->full();
            return redirect('auth/login?continue='.$url);
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_asset')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';

        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        $data['custom_field_list'] = $custom_fields;
        $data['type'] = $type;

        if($type=='all'){
            $data['form_title'] = 'All Leasable Asset Reports';
        }else if($type=='available'){
            $data['form_title'] = 'Available Leasable Asset Reports';
        }else if($type=='leased'){
            $data['form_title'] = 'Current Leased Asset Reports';
        }

        return view('assettracking::report_lease_new')->with($data);
    }

    public function generate_report_new(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;
        $type =  $request->type;

        //Primary Filter
        $start_date = (isset($request->start_date)) ? date('Y-m-d', strtotime($request->start_date)) : '';
        $end_date = (isset($request->end_date)) ? date('Y-m-d', strtotime($request->end_date)) : '';
        $category = (isset($request->category)) ? $request->category : '';
        $site = (isset($request->site)) ? $request->site : '';
        $location = (isset($request->location)) ? $request->location : '';
        $employee = (isset($request->employee)) ? $request->employee : '';

        $brand = (isset($request->brand)) ? $request->brand : '';
        $cost = (isset($request->cost)) ? $request->cost : '';
        $date_purchased = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $desc = (isset($request->desc)) ? $request->desc : '';
        $model = (isset($request->model)) ? $request->model : '';
        $serial = (isset($request->serial)) ? $request->serial : '';
        $status = (isset($request->status)) ? $request->status : '';
        $tag = (isset($request->tag)) ? $request->tag : '';
        $vendor = (isset($request->vendor)) ? $request->vendor : '';
        $status = (isset($request->status)) ? $request->status : '';
        $is_leasable = (isset($request->is_leasable)) ? $request->is_leasable : '';
        $custom_data_fields = (isset($request->custom_fields)) ? $request->custom_fields : '';
        $leased_remarks = (isset($request->leased_remarks)) ? $request->leased_remarks : '';

        if($type!='available'){
            $asset_list = DB::table('sys_asset');
            $asset_custom_values = DB::table('sys_asset_custom_value')->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id');

            if($type=='all'){
                $asset_list = $asset_list
                    ->leftJoin('sys_asset_lease_items', function($join){
                        $join->on('sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id');
                        $join->where('sys_asset_lease_items.item_stat', '=', 0);
                    })
                    ->leftjoin('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id');

                $asset_custom_values = $asset_custom_values
                    ->leftJoin('sys_asset_lease_items', function($join){
                        $join->on('sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id');
                        $join->where('sys_asset_lease_items.item_stat', '=', 0);
                    })
                    ->leftjoin('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id');
                    
            }else if($type=='leased'){
                $asset_list = $asset_list
                    ->Join('sys_asset_lease_items', function($join){
                        $join->on('sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id');
                        $join->where('sys_asset_lease_items.item_stat', '=', 0);
                    })
                    ->join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id');

                $asset_custom_values = $asset_custom_values
                    ->Join('sys_asset_lease_items', function($join){
                        $join->on('sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id');
                        $join->where('sys_asset_lease_items.item_stat', '=', 0);
                    })
                    ->join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id');
            }

            $asset_list = $asset_list
                            ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                            ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                            ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                            ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                            ->leftJoin('sys_users', 'sys_users.user_id', '=', 'sys_asset_lease_logs.recorded_by')
                            ->leftJoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
                            ->select('sys_asset.*', 'sys_asset_lease_items.item_stat','sys_asset_lease_items.release_remarks','sys_employees.emp_fullname','sys_asset_lease_logs.*','sys_categories.cat_name','sys_site.site_name','sys_site_location.location_name','sys_users.user_fullname')
                            ->where('sys_asset.is_leasable', 1)
                            ->where('sys_asset.account_id', $account_id);
            
            $asset_custom_values = $asset_custom_values
                            ->select('sys_asset_custom_value.*', 'sys_asset.account_id')
                            ->where('sys_asset.is_leasable', 1)
                            ->where('sys_asset.account_id', $account_id);


            
            if($leased_remarks){
                $asset_list = $asset_list->where('sys_asset_lease_items.release_remarks', 'LIKE', '%'.$leased_remarks.'%');
                $asset_custom_values = $asset_custom_values->where('sys_asset_lease_items.release_remarks', 'LIKE', '%'.$leased_remarks.'%');
            }

            if($start_date&&$end_date){
                $asset_list = $asset_list->whereBetween('sys_asset_lease_logs.lease_start', [$start_date, $end_date]);
                $asset_custom_values = $asset_custom_values->whereBetween('sys_asset_lease_logs.lease_start', [$start_date, $end_date]);
            }

            if($employee){
                $asset_list = $asset_list->where('sys_asset_lease_logs.requestor_id', $employee);
                $asset_custom_values = $asset_custom_values->where('sys_asset_lease_logs.requestor_id', $employee);
            }
        }else{
            $asset_list = DB::table('sys_asset')
            ->join(DB::raw("(
                SELECT sys_asset_lease_logs.*, sys_asset_lease_items.asset_id as lease_asset_id,sys_asset_lease_items.item_stat, sys_asset_lease_items.release_remarks  FROM sys_asset_lease_logs 
                INNER JOIN sys_asset_lease_items ON sys_asset_lease_items.lease_id = sys_asset_lease_logs.lease_id 
                WHERE sys_asset_lease_items.item_stat = 0 
                and sys_asset_lease_logs.account_id = ".$account_id."
                GROUP BY sys_asset_lease_items.asset_id
                ) as leased_asset"),function($join){
                    $join->on("leased_asset.lease_asset_id","<>","sys_asset.asset_id");
            })
            ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
            ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
            ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
            ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
            ->leftJoin('sys_users', 'sys_users.user_id', '=', 'leased_asset.recorded_by')
            ->leftJoin('sys_employees', 'sys_employees.employee_id', '=', 'leased_asset.requestor_id')
            ->select('sys_asset.*', 'sys_employees.emp_fullname','leased_asset.*','sys_categories.cat_name','sys_site.site_name','sys_site_location.location_name','sys_users.user_fullname')
            ->where('sys_asset.is_leasable', 1)
            ->where('sys_asset.account_id', $account_id);


            $asset_custom_values = DB::table('sys_asset_custom_value')
                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                    ->join(DB::raw("(
                        SELECT sys_asset_lease_logs.*, sys_asset_lease_items.asset_id as lease_asset_id ,sys_asset_lease_items.item_stat, sys_asset_lease_items.release_remarks  FROM sys_asset_lease_logs 
                        INNER JOIN sys_asset_lease_items ON sys_asset_lease_items.lease_id = sys_asset_lease_logs.lease_id 
                        WHERE sys_asset_lease_items.item_stat = 0 
                        and sys_asset_lease_logs.account_id = ".$account_id."
                        GROUP BY sys_asset_lease_items.asset_id
                        ) as leased_asset"),function($join){
                            $join->on("leased_asset.lease_asset_id","<>","sys_asset.asset_id");
                    })
                    ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id')
                    ->where('sys_asset.is_leasable', 1)
                    ->where('sys_asset.account_id', $account_id);

            if($leased_remarks){
                $asset_list = $asset_list->where('leased_asset.release_remarks', 'LIKE', '%'.$leased_remarks.'%');
                $asset_custom_values = $asset_custom_values->where('leased_asset.release_remarks', 'LIKE', '%'.$leased_remarks.'%');
            }

            if($employee){
                $asset_list = $asset_list->where('leased_asset.requestor_id', $employee);
                $asset_custom_values = $asset_custom_values->where('leased_asset.requestor_id', $employee);
            }
        }



        if($category){
            $asset_list = $asset_list->where('sys_asset.cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }
        if($site){
            $asset_list = $asset_list->where('sys_asset.site_id', $site);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site);
        }
        if($location){
            $asset_list = $asset_list->where('sys_asset.location_id', $location);
            $asset_custom_values = $asset_custom_values->where('sys_asset.location_id', $location);
        }
        if($tag){
            $asset_list = $asset_list->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
        }
        if($desc){
            $asset_list = $asset_list->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
        }
        if($model){
            $asset_list = $asset_list->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
        }
        if($serial){
            $asset_list = $asset_list->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
        }
        if($brand){
            $asset_list = $asset_list->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
        }
        if($cost){
            $cost = Helpers::converts('currency_to_int', $cost);
            $asset_list = $asset_list->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
        }
        if($date_purchased){
            $date_purchased = date('Y-m-d', strtotime($date_purchased));
            $asset_list = $asset_list->whereDate('sys_asset.date_purchased', $date_purchased);
            $asset_custom_values = $asset_custom_values->whereDate('sys_asset.date_purchased', $date_purchased);
        }
        if($vendor){
            $asset_list = $asset_list->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
        }

        if($custom_data_fields){
            $asset_list = $asset_list->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
        }

        $asset_list = $asset_list->groupBy('sys_asset.asset_id')->get();
        $asset_custom_values = $asset_custom_values->groupBy('sys_asset.asset_id')->get();

        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();
                                    

        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        /*
        
        */

        return $response;
    }


    /*Old Codes*/
    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;

        $query_asset = "SELECT sys_asset.*, 
                sys_site.site_name, 
                sys_site_location.location_name, 
                sys_categories.cat_name, 
                sys_employees.emp_fullname, 
                sys_asset_inout_logs.inout_date, 
                sys_users.user_fullname,
                sys_asset_inout_items.item_remarks
                FROM `sys_asset_inout_logs`
                INNER JOIN sys_asset_inout_items 
                    ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                INNER JOIN sys_asset 
                    ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                    AND sys_asset.record_stat = 'active'
                INNER JOIN sys_employees 
                    ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                LEFT JOIN sys_site 
                    ON sys_site.site_id = sys_asset_inout_logs.site_id
                LEFT JOIN sys_site_location 
                    ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                LEFT JOIN sys_categories 
                    ON sys_categories.cat_id = sys_asset.cat_id
                INNER JOIN sys_users 
                    ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                WHERE 
                    (sys_asset_inout_logs.inout_type = 'checkout' 
                    OR sys_asset_inout_logs.inout_type = 'leased checkout')
                    AND sys_asset_inout_logs.account_id = '".$account_id."'";

        $query_custom_value = "SELECT sys_asset_custom_value.*
                        FROM `sys_asset_inout_logs`
                        INNER JOIN sys_asset_inout_items 
                            ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                        INNER JOIN sys_asset 
                            ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                            AND sys_asset.record_stat = 'active'
                        INNER JOIN sys_employees 
                            ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                        LEFT JOIN sys_site 
                            ON sys_site.site_id = sys_asset_inout_logs.site_id
                        LEFT JOIN sys_site_location 
                            ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        INNER JOIN sys_users 
                            ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                        INNER JOIN sys_asset_custom_value
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        WHERE 
                            (sys_asset_inout_logs.inout_type = 'checkout' 
                            OR sys_asset_inout_logs.inout_type = 'leased checkout')
                            AND sys_asset_inout_logs.account_id = '".$account_id."'";
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($request->type=='bydate'){
            $start = date('Y-m-d', strtotime($request->date_start));
            $end  = date('Y-m-d', strtotime($request->date_end));

            $query_asset .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
            $query_custom_value .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
        }else if($request->type=='bysitelocation'){
            $location_id = $request->id;

            $location_data = Location::where('account_id', $account_id)->where('location_id', $location_id)->where('record_stat',0)->first();

            $site_id = $location_data->site_id;

            $query_asset .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
            $query_custom_value .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
        }else if($request->type=='byrequestor'){
            $employee_id = $request->id;

            $query_asset .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
            $query_custom_value .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
        }

        $asset_list =  DB::select($query_asset);
        $asset_custom_values =  DB::select($query_custom_value);
        
        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        return $response;
    }
    
    public function index_report($type){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_lease')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';
                        
        $start = date('Y-m-01');
        $end  = date('Y-m-t');

        /*$query_asset = "SELECT
                        sys_asset.*,
                        sys_asset_lease_items.item_stat,
                        sys_employees.emp_fullname,
                        sys_asset_lease_logs.*,
                        sys_categories.cat_name,
                        sys_site.site_name,
                        sys_site_location.location_name,
                        sys_users.user_fullname
                        FROM sys_asset
                        LEFT JOIN sys_categories
                            ON sys_categories.cat_id = sys_asset.cat_id
                        LEFT JOIN sys_site
                            ON sys_site.site_id = sys_asset.site_id
                        LEFT JOIN sys_site_location
                            ON sys_site_location.location_id = sys_asset.location_id
                        LEFT JOIN sys_asset_lease_items
                            ON sys_asset_lease_items.asset_id = sys_asset.asset_id
                        LEFT JOIN sys_asset_lease_logs
                            ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id 
                            AND sys_asset_lease_logs.account_id = '".$account_id."' 
                            AND sys_asset_lease_items.item_stat = 0
                        LEFT JOIN sys_users 
                            ON sys_users.user_id = sys_asset_lease_logs.recorded_by
                        LEFT JOIN sys_employees
                            ON sys_employees.employee_id = sys_asset_lease_logs.requestor_id
                        WHERE sys_asset.is_leasable = 1 
                            AND sys_asset.account_id = '".$account_id."'";*/

        $asset_list = DB::table('sys_asset')
                        ->join('sys_asset_lease_items', 'sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id')
                        ->join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                        ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                        ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                        ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                        ->leftJoin('sys_users', 'sys_users.user_id', '=', 'sys_asset_lease_logs.recorded_by')
                        ->leftJoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
                        ->select('sys_asset.*', 'sys_asset_lease_items.item_stat','sys_employees.emp_fullname','sys_asset_lease_logs.*','sys_categories.cat_name','sys_site.site_name','sys_site_location.location_name','sys_users.user_fullname')
                        ->where('sys_asset.is_leasable', 1)
                        ->where('sys_asset.account_id', $account_id);
                        //->where('sys_asset.account_id', $account_id)
                        
        $query_custom_value = "SELECT
                        sys_asset_custom_value.*
                        FROM sys_asset
                        LEFT JOIN sys_asset_lease_items
                            ON sys_asset_lease_items.asset_id = sys_asset.asset_id
                        LEFT JOIN sys_asset_lease_logs
                            ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id 
                            AND sys_asset_lease_logs.account_id = '".$account_id."' 
                            AND sys_asset_lease_items.item_stat = 0
                        LEFT JOIN sys_employees
                            ON sys_employees.employee_id = sys_asset_lease_logs.requestor_id
                        INNER JOIN sys_asset_custom_value
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        WHERE sys_asset.is_leasable = 1 
                            AND sys_asset.account_id = '".$account_id."'";
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($type=='available'){
            //$query_asset .= " AND (sys_asset_lease_items.item_stat = 1 OR sys_asset_lease_items.item_stat IS NULL)";

            $asset_list = $asset_list->where('sys_asset_lease_items.item_stat', 1);

            $query_custom_value .= " AND (sys_asset_lease_items.item_stat = 1 OR sys_asset_lease_items.item_stat IS NULL)";
            //$query_custom_value .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";

            $data['form_title'] = 'Asset Lease Reports: By Available Asset';
            $data['type'] = $type;
        }else if($type=='leased'){
            //$query_asset .= " AND sys_asset_lease_items.item_stat = 0";
            $asset_list = $asset_list->where('sys_asset_lease_items.item_stat', 0);
            $query_custom_value .= " AND sys_asset_lease_items.item_stat = 0";

            $data['form_title'] = 'Asset Lease Reports: By Leased Asset';
            $data['type'] = $type;
        }
        
        //$asset_list =  DB::select($query_asset);
        $asset_list = $asset_list->groupBy('sys_asset.asset_id')->orderBy('sys_asset.asset_tag', 'asc')->get();
        $asset_custom_values =  DB::select($query_custom_value);

        
        
        

        $data['asset_list'] = $asset_list;
        $data['custom_field_list'] = $custom_fields;
        $data['asset_custom_value_list'] = $asset_custom_values;


        return view('assettracking::report_lease')->with($data);
    }
}