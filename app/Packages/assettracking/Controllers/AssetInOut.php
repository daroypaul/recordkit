<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\Categories;
use App\packages\assettracking\models\AssetInOutLogs;
use App\packages\assettracking\models\AssetInOutItems;
use App\packages\assettracking\models\AssetEvents;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\adminprofile\models\Employees;

use Validator;
use Session;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use Mail;

class AssetInOut extends Controller
{
    public function check_in($asset_id=''){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_inout')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$account_id = Auth::user()->account_id;
        $asset = [];
        $error_msg = ''; 

        if($asset_id){
            $asset = Assets::with(['categories'])->where('account_id', $account_id)->where('asset_id', $asset_id)->where('record_stat', 'active')->first();
            
            if(count($asset)==0){
                $error_msg = '<div class="alert alert-danger m-b-20">Asset record not found. Unable to load on the list.</div>';
            }else{
                $error_msg = '';
            }
        }

        //$employees = Employees::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('emp_last_name', 'asc')->get();
        //$categories = Categories::where('account_id', '=', $account_id)->orderBy('cat_name', 'asc')->get();
        //$sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
                "preload_asset"=>$asset,
                "error_msg"=>$error_msg,
                //"categories"=>$categories,
                //"sites"=>$sites,
                //"requestors"=>$employees,
                //"customers"=>$employees,
                "form_title"=> "Asset Check-In",
                "type"=> "checkin",
                "customer_label"=>"Return By",
                "status"=> ['available','leased','checked in','checked out','damage','lost','disposed','sold'],
        ];

        return view('assettracking::asset_inout_form')->with($data);

        
    }

    public function inout_store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'inout_date' => 'date_format:"F d, Y"|required',
            'requestor' => 'required',
            'customer' => 'required',
            'site' => 'required',
            'location' => 'required',
            'remarks' => 'max:500',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $val_msg = $validator->errors();
        
        //Check site and Location if not deleted
        $check_site = Site::where('account_id', $account_id)->where('site_id', $request->site)->where('record_stat',0)->count();
        $check_location = Location::where('account_id', $account_id)->where('location_id', $request->location)->where('record_stat',0)->count();

        if($check_site==0){
            $val_msg->add('site', 'Site already deleted');
            $val_msg->add('location', 'Location already deleted');
        }else{
            if($check_location==0){
                $val_msg->add('location', 'Location already deleted');
                $a = 'location error';
            }
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_inout')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()||count($val_msg)!=0) {
            $response['val_errors']= $val_msg;
        }else if( count(array_filter($request->get('input_asset_id')))==0 ){
            $response['stat']= 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg']= 'Please add atleast one (1) asset';
        }else{
            $new_inout = new AssetInOutLogs;
            $new_inout->account_id = $account_id;
            $new_inout->site_id = $request->get('site');
            $new_inout->location_id = $request->get('location');
            $new_inout->requestor_id = $request->get('requestor');
            $new_inout->customer_id = $request->get('customer');
            $new_inout->inout_type = $request->get('inout_type');
            $new_inout->inout_date = date('Y-m-d', strtotime($request->get('inout_date')));
            $new_inout->inout_remarks = $request->get('remarks');
            $new_inout->recorded_by = $user_id;
            $new_inout->updated_by = $user_id;
            $new_inout->record_stat = 'active';
            $new_inout->save();
            $new_id = $new_inout->inout_id;
            $rec = 0;//success storing to database
            if($new_inout){
                $i = 0;
                $rec++;
                foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                    $assets = Assets::with(['categories','site','image'])->find($asset_id);
                    $site = $assets->site_id;
                    $cat = ($assets->categories) ? $assets->categories->cat_id : 0;
                    $tag = $assets->asset_tag;
                    $desc = $assets->asset_desc;
                    $model = $assets->asset_model;
                    $serial = $assets->asset_serial;
                    $remarks = $request->get('input_asset_remarks')[$i];
    
                    if(count($assets)!=0){
                        $new_inout_items = new AssetInOutItems;
                        $new_inout_items->inout_id = $new_id;
                        $new_inout_items->account_id = $account_id;
                        $new_inout_items->site_id = $site;
                        $new_inout_items->asset_id = $asset_id;
                        $new_inout_items->cat_id = $cat;
                        $new_inout_items->asset_tag = $tag;
                        $new_inout_items->asset_desc = $desc;
                        $new_inout_items->asset_model = $model;
                        $new_inout_items->asset_serial = $serial;
                        $new_inout_items->item_remarks = $remarks;
                        $new_inout_items->save();

                        //Add Asset Events
                        $new_events = new AssetEvents;
                        $new_events->account_id = $account_id;
                        $new_events->asset_id = $asset_id;
                        $new_events->event_type = $request->get('inout_type');
                        $new_events->event_date = date('Y-m-d', strtotime($request->get('inout_date')));
                        $new_events->event_remarks = $remarks;
                        $new_events->ref_id = $new_id;
                        $new_events->prev_site_id = $assets->site_id;
                        $new_events->prev_location_id = $assets->location_id;
                        $new_events->new_site_id = $request->get('site');
                        $new_events->new_location_id = $request->get('location');
                        $new_events->requestor_id = $request->get('requestor');
                        $new_events->recorded_by = $user_id;
                        $new_events->record_stat = 0;
                        $new_events->date_recorded = Carbon::now('UTC')->toDateTimeString();
                        $new_events->save();

                        if($new_inout_items){
                            $rec++;
                        }
                    }
    
                    $i++;
                }
            }

            if($rec>1){
                $response['stat']= 'success';
                $response['stat_title'] = 'Success';

                if($request->get('is_draft')!=0){
                    $response['stat_msg']= 'Asset(s) successfully save as draft.';
                }else{
                    //Update the status of per asset
                    foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                        $update_asset = Assets::find($asset_id);

                        if($request->get('inout_type')=='checkin'){
                            $response['stat_msg']= 'Asset(s) successfully checked-in';
                            $update_asset->asset_status = 'available';
                            $update_asset->assign_to = '';
                        }else if($request->get('inout_type')=='checkout'){
                            $response['stat_msg']= 'Asset(s) successfully checked-out';
                            $update_asset->asset_status = 'checked out';
                            $update_asset->assign_to = $request->get('requestor');
                        }

                        //$update_asset = Assets::find($asset_id);
                        //$update_asset->asset_status = $stat;
                        $update_asset->site_id = $request->get('site');
                        $update_asset->location_id = $request->get('location');
                        $update_asset->updated_by = $user_id;
                        $update_asset->save();
                    }


                    $desc = 'added a '.$request->get('inout_type').' asset record';
                    Helpers::add_activity_logs(['ams-asset-inout',$new_id,$desc]);
                    
                    #-----------------------------
                    #   Send Email Notification
                    #-----------------------------
                    $request_id =$request->get('requestor');
                    $customer_id =$request->get('customer');
                    $cc = [];

                    $requestor_detail = Employees::where('account_id', $account_id)->where('employee_id', $request_id)->first();

                    if($requestor_detail->emp_email){
                        if($request_id==$customer_id){
                            $customer_name = $requestor_detail->emp_fullname;
                            $customer_email = $requestor_detail->emp_email;
                            
                        }else{
                            $customer_detail = Employees::where('account_id', $account_id)->where('employee_id', $customer_id)->first();
                            $customer_name = $customer_detail->emp_fullname;

                            if($customer_detail->emp_email!=$requestor_detail->emp_email){
                                $cc[] = $customer_detail->emp_email;
                            }
                        }

                        if(Auth::user()->user_email){
                            $cc[] = Auth::user()->user_email;
                        }

                    
                        $subject = ($request->get('inout_type')=='checkout') ? 'Asset Checked-Out' : 'Asset Returned';
                        $transaction_type = ($request->get('inout_type')=='checkout') ? 'checked-out' : 'checked-in';
                        $receiver = ($request->get('inout_type')=='checkout') ?  $customer_name : Auth::user()->user_fullname;
                        $hanover = ($request->get('inout_type')=='checkout') ?  Auth::user()->user_fullname : $customer_name;

                        $msg = '
                            <p>You have '.$transaction_type.' an asset(s). See details below.</p>
                            <table class="details">
                                <tr>
                                    <th>Reference No</th>
                                    <td>:</td>
                                    <td>'.$new_inout->inout_id.'</td>
                                </tr>
                                <tr>
                                    <th>Transaction Date</th>
                                    <td>:</td>
                                    <td>'.date('d-M-Y', strtotime($request->get('inout_date'))).'</td>
                                </tr>
                                <tr>
                                    <th>Received By</th>
                                    <td>:</td>
                                    <td>'.$receiver.'</td>
                                </tr>
                                <tr>
                                    <th>Handed-Over by</th>
                                    <td>:</td>
                                    <td>'.$hanover . '</td>
                                </tr>
                            </table>

                            <p>Asset List:</p>
                            <table class="bordered">
                                <tr>
                                    <th>Asset Description</th>
                                    <th>Model</th>
                                    <th>Serial</th>
                                    <th>Remarks</th>
                                </tr>';
                        $i=0;
                        foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                            $asset = Assets::find($asset_id);
                            $serial = ($asset->asset_serial) ? $asset->asset_serial : '---';
                            $remarks = ($request->get('input_asset_remarks')[$i]) ? $request->get('input_asset_remarks')[$i] : '---';

                            $msg .=     ' <tr>
                                            <td>'.$asset->asset_desc.'</td>
                                            <td>'.$asset->asset_model.'</td>
                                            <td>'.$serial.'</td>
                                            <td>'.$remarks.'</td>
                                        </tr>';
                            $i++;
                        }

                        $msg .= '</table>';

                        $email_data['email'] = $requestor_detail->emp_email;
                        $email_data['subject'] = $subject;
                        $email_data['bodymessage'] = $msg;
                        $email_data['requestor'] = $requestor_detail->emp_fullname;
                        $email_data['cc'] = $cc;

                        $response['a'] = $email_data['cc'];

                        Mail::send('mail.email', $email_data, function($send_email) use ($email_data){
                            $send_email->from('notification@recuda-apps.com', 'ICT Asset Tracking System');
                            $send_email->to($email_data['email']);
                            if(count($email_data['cc'])!=0){
                                $send_email->cc($email_data['cc']);
                            }
                            $send_email->subject($email_data['subject']);
                        });
                    }
                    # End of Email Notification
                }
            }else{
                $response['stat']= 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg']= 'Unable to check-in asset(s). Error occured during process.';
            }
        }

        
        return response()->json($response);
    }


    #Check Out
    public function check_out($asset_id=''){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_inout')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$account_id = Auth::user()->account_id;
        $asset = [];
        $error_msg = ''; 

        if($asset_id){
            $asset = Assets::with(['categories'])->where('account_id', $account_id)->where('asset_id', $asset_id)->where('record_stat', 'active')->first();
            
            if(count($asset)==0){
                $error_msg = '<div class="alert alert-danger m-b-20">Asset record not found. Unable to load on the list.</div>';
            }else{
                $error_msg = '';
            }
        }

        //$employees = Employees::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('emp_last_name', 'asc')->get();
        //$categories = Categories::where('account_id', '=', $account_id)->orderBy('cat_name', 'asc')->get();
        //$sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
            "preload_asset"=>$asset,
            "error_msg"=>$error_msg,
                //"categories"=>$categories,
                //"sites"=>$sites,
                //"requestors"=>$employees,
                //"customers"=>$employees,
                "form_title"=> "Asset Check-Out",
                "type"=> "checkout",
                "customer_label"=>"Receive By",
        ];

        return view('assettracking::asset_inout_form')->with($data);
    }
}
