<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\packages\assettracking\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\settings\models\IdsOption;//Get the Ides Option
use App\modules\settings\models\IdsCounter;//Get the last number used

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class SettingAssetTag extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_setting_assettag')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $last_number = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_value;
        $with_padding = Options::where('account_id', $account_id)->where('option_name','tag_with_padding')->first()->option_value;
        $padding_digit = Options::where('account_id', $account_id)->where('option_name','tag_padding_digits')->first()->option_value;

        $counter = ($with_padding) ? str_pad($last_number,  $padding_digit, "0", STR_PAD_LEFT) : $last_number;

        $prefix = Options::where('account_id', $account_id)->where('option_name','tag_prefix')->first()->option_value;
        $suffix = Options::where('account_id', $account_id)->where('option_name','tag_suffix')->first()->option_value;
        $with_dashed = Options::where('account_id', $account_id)->where('option_name','tag_with_dashed')->first()->option_value;

        $dashed_before = ($with_dashed && $prefix) ? '-':'';
        $dashed_after = ($with_dashed && $suffix) ? '-':'';

        $year_strlen = Options::where('account_id', $account_id)->where('option_name','tag_year_strlen')->first()->option_value;
        $cur_year = ($year_strlen==2) ? date('y') : date('Y');
        $with_year = Options::where('account_id', $account_id)->where('option_name','tag_with_year')->first()->option_value;
        $insert_year = Options::where('account_id', $account_id)->where('option_name','tag_insert_year')->first()->option_value;
        $asset_tag = '';

        if($with_year){
            if($insert_year=='BEF_PREF'){
                $asset_tag = $cur_year . $prefix . $dashed_before . $counter . $dashed_after . $suffix;
            }else if($insert_year=='AFT_PREF'){
                $asset_tag = $prefix . $cur_year . $dashed_before . $counter . $dashed_after . $suffix;
            }else if($insert_year=='BEF_SUF'){
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $cur_year. $suffix;
            }else if($insert_year=='AFT_SUF'){
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix . $cur_year;
            }else{
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix;
            }
        }else{
            $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix;
        }

        $id_options = new \StdClass();
        $id_options->ids_prefix = $prefix;
        $id_options->ids_suffix = $suffix;
        $id_options->ids_with_padding = $with_padding;
        $id_options->ids_padding_digits = $padding_digit;
        $id_options->ids_with_dashed = $with_dashed;
        $id_options->ids_with_year = $with_year;
        $id_options->ids_string = $year_strlen;
        $id_options->ids_insert_year = $insert_year;

        $data = [
            "ids_option"=>$id_options,
            "last_number"=>$last_number,
            "asset_tag"=>$asset_tag,
            "form_title"=> "Asset Tagging Settings",
        ];

        return view('assettracking::pkg_fixasset_setting_assettag')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'start_number'=>'min:1|numeric',
            //'suffix'=>'max:12|alpha_num',
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        if($request->prefix){
            $rules['prefix'] = 'max:12|alpha_num';
        }

        if($request->suffix){
            $rules['suffix'] = 'max:12|alpha_num';
        }

        /*if($request->start_number){
            $rules['start_number'] = 'required|min:1|numeric';
        }*/

        if($request->padding){
            $rules['digit'] = 'min:1|max:12|numeric';
        }

       /* $messages = [
            'category_name.regex' => 'The :attribute format is invalid (alphanumeric, dash and space only).',
            'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];*/

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            //Get Options IDs
            $with_padding_id = Options::where('account_id', $account_id)->where('option_name','tag_with_padding')->first()->option_id;
            $padding_digit_id = Options::where('account_id', $account_id)->where('option_name','tag_padding_digits')->first()->option_id;
            $prefix_id = Options::where('account_id', $account_id)->where('option_name','tag_prefix')->first()->option_id;
            $suffix_id = Options::where('account_id', $account_id)->where('option_name','tag_suffix')->first()->option_id;
            $with_dashed_id = Options::where('account_id', $account_id)->where('option_name','tag_with_dashed')->first()->option_id;
            $year_strlen_id = Options::where('account_id', $account_id)->where('option_name','tag_year_strlen')->first()->option_id;
            $with_year_id = Options::where('account_id', $account_id)->where('option_name','tag_with_year')->first()->option_id;
            $insert_year_id = Options::where('account_id', $account_id)->where('option_name','tag_insert_year')->first()->option_id;

            //Update
            $prefix = Options::find($prefix_id);
            $prefix->option_value = ($request->prefix) ? $request->prefix : '';
            $prefix->save();

            $suffix = Options::find($suffix_id);
            $suffix->option_value = ($request->suffix) ? $request->suffix : '';
            $suffix->save();

            $with_padding = Options::find($with_padding_id);
            $with_padding->option_value = ($request->padding) ? 1 : 0;
            $with_padding->save();

            $padding_digit = Options::find($padding_digit_id);
            $padding_digit->option_value = ($request->digit) ? $request->digit : 0;
            $padding_digit->save();

            $with_dashed = Options::find($with_dashed_id);
            $with_dashed->option_value = ($request->dashed) ? 1 : 0;
            $with_dashed->save();

            $year_strlen = Options::find($year_strlen_id);
            $year_strlen->option_value = ($request->string) ? $request->string : 2;
            $year_strlen->save();

            $with_year = Options::find($with_year_id);
            $with_year->option_value = ($request->year) ? 1 : 0;
            $with_year->save();

            $insert_year = Options::find($insert_year_id);
            $insert_year->option_value = ($request->insert_year) ? $request->insert_year : '';
            $insert_year->save();
            
            $counter_id = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_id;
            $update_counter = Options::find($counter_id);
            $update_counter->option_value = ($request->start_number) ? $request->start_number : 1;
            $update_counter->save();

            if($prefix||$suffix||$with_padding||$padding_digit||$with_dashed||$year_strlen||$with_year||$insert_year){
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = 'Asset Tagging has been successfully updated';

                //$update_counter = IdsCounter::where('account_id', $account_id)->where('counter_type', 'asset_tag')->update(['counter_value'=>($request->start_number) ? $request->start_number : 1]);
                
                $counter_id = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_id;
                $update_counter = Options::find($counter_id);
                $update_counter->option_value = ($request->start_number) ? $request->start_number : 1;
                $update_counter->save();

                //Add Logs
                $desc = 'updated the asset tagging settings';
                Helpers::add_activity_logs(['asset-tagging-settings',0,$desc]);
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to update asset tagging. Error occured during process.';
            }
        }

        return $response;
        
    }
}
