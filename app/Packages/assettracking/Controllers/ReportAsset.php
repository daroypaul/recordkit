<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\settings\models\CustomFields;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class ReportAsset extends Controller
{
    public function get_custom_fields(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;

        $custom_fields = DB::table('sys_custom_fields')
                                ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                ->where('sys_asset.account_id', $account_id)
                                ->where('sys_asset.record_stat', 'active')
                                ->groupBy('sys_custom_fields.field_id')
                                ->orderBy('sys_custom_fields.field_label', 'asc')
                                ->get();
        
        $response['custom_fields'] = $custom_fields;

        return $custom_fields;
    }

    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;

        $asset_list = Assets::with(['custom_values', 'categories', 'site', 'location','updator','assigned_to'])->where('account_id', $account_id)->where('record_stat', 'active');
        $asset_custom_values = DB::table('sys_asset')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                    ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id')
                                    ->where('account_id', $account_id)->where('record_stat', 'active');

        if($request->type=='bymodel'){
            $model = $request->id;

            $asset_list = $asset_list->where('asset_model', $model);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', $model);
        }else if($request->type=='bycategory'){
            $category = $request->id;

            $asset_list = $asset_list->where('cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }else if($request->type=='bysitelocation'){
            $location_id = $request->id;

            $location_data = Location::where('account_id', $account_id)->where('location_id', $location_id)->where('record_stat',0)->first();

            $site_id = $location_data->site_id;

            $asset_list = $asset_list->where('site_id', $site_id)->where('location_id', $location_id);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site_id)->where('sys_asset.location_id', $location_id);
        }else if($request->type=='byassignee'){
            $employee_id = $request->id;

            $asset_list = $asset_list->where('assign_to', $employee_id);
            $asset_custom_values = $asset_custom_values->where('sys_asset.assign_to', $employee_id);
        }

        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();

        $custom_fields = DB::table('sys_custom_fields')
                                ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                ->where('sys_asset.account_id', $account_id)
                                ->where('sys_asset.record_stat', 'active')
                                ->groupBy('sys_custom_fields.field_id')
                                ->orderBy('sys_custom_fields.field_label', 'asc')
                                ->get();
        
        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        return $response;
    }

    public function generate_report_NEW(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;
        
        //Primary Filter
        $category = (isset($request->category)) ? $request->category : '';
        $site = (isset($request->site)) ? $request->site : '';
        $location = (isset($request->location)) ? $request->location : '';
        $employee = (isset($request->employee)) ? $request->employee : '';

        $brand = (isset($request->brand)) ? $request->brand : '';
        $cost = (isset($request->cost)) ? $request->cost : '';
        $date_purchased = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $desc = (isset($request->desc)) ? $request->desc : '';
        $model = (isset($request->model)) ? $request->model : '';
        $serial = (isset($request->serial)) ? $request->serial : '';
        $status = (isset($request->status)) ? $request->status : '';
        $tag = (isset($request->tag)) ? $request->tag : '';
        $vendor = (isset($request->vendor)) ? $request->vendor : '';
        $status = (isset($request->status)) ? $request->status : '';
        $is_leasable = (isset($request->is_leasable)) ? $request->is_leasable : '';
        $custom_data_fields = (isset($request->custom_fields)) ? $request->custom_fields : '';

        /*$response['category'] = (isset($request->category)) ? $request->category : '';
        $response['site'] = (isset($request->site)) ? $request->site : '';
        $response['location'] = (isset($request->location)) ? $request->location : '';
        $response['employee'] = (isset($request->employee)) ? $request->employee : '';

        $response['brand'] = (isset($request->brand)) ? $request->brand : '';
        $response['cost'] = (isset($request->cost)) ? $request->cost : '';
        $response['date_purchased'] = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $response['desc'] = (isset($request->desc)) ? $request->desc : '';
        $response['model'] = (isset($request->model)) ? $request->model : '';
        $response['serial'] = (isset($request->serial)) ? $request->serial : '';
        $response['status'] = (isset($request->status)) ? $request->status : '';
        $response['tag'] = (isset($request->tag)) ? $request->tag : '';
        $response['vendor'] = (isset($request->vendor)) ? $request->vendor : '';*/

        /*$asset_list = Assets::with(['custom_values', 'categories', 'site', 'location','updator','assigned_to'])->where('account_id', $account_id)->where('record_stat', 'active');*/

        $asset_list = DB::table('sys_asset')
                                    ->leftjoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                                    ->leftjoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                                    ->leftjoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                                    ->leftjoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset.assign_to')
                                    ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                    ->leftjoin('sys_users', 'sys_users.user_id', '=', 'sys_asset.updated_by')
                                    ->select('sys_asset.*','sys_site.site_name','sys_categories.cat_name','sys_site_location.location_name','sys_employees.emp_fullname','sys_users.user_fullname')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active');
                                    /*->where(function ($query) use ($keywords) {
                                        $query->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_tag', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_desc', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_serial', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_model', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_vendor', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.purchased_cost', 'LIKE', '%'.Helpers::converts('currency_to_int', $keywords).'%')
                                            ->orwhere('sys_asset.asset_remarks', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_asset.asset_status', $keywords)
                                            ->orwhere('sys_site.site_name', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_site_location.location_name', 'LIKE', '%'.$keywords.'%')
                                            ->orwhere('sys_categories.cat_name', 'LIKE', '%'.$keywords.'%');
                                    })
                                    ->groupBy('sys_asset.asset_id')
                                    ->get();*/
        $asset_custom_values = DB::table('sys_asset')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                    ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id')
                                    ->where('sys_asset.account_id', $account_id)->where('sys_asset.record_stat', 'active');

        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($status){
            $asset_list = $asset_list->where('sys_asset.asset_status',$status);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status',$status);
        }
        if($category){
            $asset_list = $asset_list->where('sys_asset.cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }
        if($site){
            $asset_list = $asset_list->where('sys_asset.site_id', $site);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site);
        }
        if($location){
            $asset_list = $asset_list->where('sys_asset.location_id', $location);
            $asset_custom_values = $asset_custom_values->where('sys_asset.location_id', $location);
        }
        if($employee){
            $asset_list = $asset_list->where('sys_asset.assign_to', $employee);
            $asset_custom_values = $asset_custom_values->where('sys_asset.assign_to', $employee);
        }
        if($tag){
            $asset_list = $asset_list->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
        }
        if($desc){
            $asset_list = $asset_list->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
        }
        if($model){
            $asset_list = $asset_list->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
        }
        if($serial){
            $asset_list = $asset_list->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
        }
        if($brand){
            $asset_list = $asset_list->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
        }
        if($cost){
            $cost = Helpers::converts('currency_to_int', $cost);
            $asset_list = $asset_list->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
        }
        if($date_purchased){
            $date_purchased = date('Y-m-d', strtotime($date_purchased));
            $asset_list = $asset_list->whereDate('sys_asset.date_purchased', $date_purchased);
            $asset_custom_values = $asset_custom_values->whereDate('sys_asset.date_purchased', $date_purchased);
        }
        if($vendor){
            $asset_list = $asset_list->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_vendor', 'LIKE', '%'.$vendor.'%');
        }

        if($is_leasable){
            $is_leasable = ($is_leasable=='yes') ? 1 : 0;

            if($is_leasable){
                $asset_list = $asset_list->where('sys_asset.is_leasable',$is_leasable);
                $asset_custom_values = $asset_custom_values->where('sys_asset.is_leasable',$is_leasable);
            }else{
                //$asset_list = $asset_list->where('sys_asset.is_leasable','<>', 1);
                //$asset_custom_values = $asset_custom_values->where('sys_asset.is_leasable','<>', 1);
                $asset_list = $asset_list->where(function ($query) {
                    $query->whereNull('sys_asset.is_leasable')
                                ->orWhere('sys_asset.is_leasable', 0);
                });

                $asset_custom_values = $asset_custom_values->where(function ($query) {
                    $query->whereNull('sys_asset.is_leasable')
                                ->orWhere('sys_asset.is_leasable', 0);
                });
            }
        }

        if($custom_data_fields){
            $asset_list = $asset_list->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
        }

        $asset_list = $asset_list->groupBy('sys_asset.asset_id')->get();
        $asset_custom_values = $asset_custom_values->get();

        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        $response['a'] = $custom_data_fields;

        return $response;
    }

    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_asset')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';

        /*$asset_list = Assets::with(['custom_values', 'categories', 'site', 'location','updator','assigned_to'])
                                ->where('account_id', $account_id)
                                ->where('record_stat', 'active');
        $asset_custom_values = DB::table('sys_asset')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                    ->join('sys_custom_fields', 'sys_custom_fields.field_id', '=', 'sys_asset_custom_value.field_id')
                                    ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id', 'sys_custom_fields.field_label')
                                    ->where('sys_asset.account_id', $account_id)->where('sys_asset.record_stat', 'active');*/
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        /*$data['form_title'] = 'Asset Report: By Model';
        $data['model_list'] = [];
        
        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();*/

        //$data['asset_list'] = $asset_list;
        $data['custom_field_list'] = $custom_fields;
        $data['form_title'] = 'Asset Reports';
        //$data['asset_custom_value_list'] = $asset_custom_values;

        //return $employee;
        //return $data;
        return view('assettracking::report_asset_new')->with($data);


        /*$asset_list = Assets::leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                                ->leftJoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                ->groupBy('sys_asset.asset_id')
                                ->get();*/
        $keywords = 'Mindanao';

    }

    public function index_report($type){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_asset')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';

        $asset_list = Assets::with(['custom_values', 'categories', 'site', 'location','updator','assigned_to'])
                                ->where('account_id', $account_id)
                                ->where('record_stat', 'active');
        $asset_custom_values = DB::table('sys_asset')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                    ->join('sys_custom_fields', 'sys_custom_fields.field_id', '=', 'sys_asset_custom_value.field_id')
                                    ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id', 'sys_custom_fields.field_label')
                                    ->where('sys_asset.account_id', $account_id)->where('sys_asset.record_stat', 'active');
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($type=='bymodel'){
            $model_list = DB::table('sys_asset')
                                    ->select('asset_model')
                                    ->where('account_id', $account_id)
                                    ->where('record_stat', 'active')
                                    ->groupBy('asset_model')
                                    ->orderBy('asset_model', 'asc')
                                    ->get();
            
            if(count($model_list)!=0){
                $current_model = DB::table('sys_asset')
                                        ->select('asset_model')
                                        ->where('account_id', $account_id)
                                        ->where('record_stat', 'active')
                                        ->groupBy('asset_model')
                                        ->limit(1)->first();
                $model = $current_model->asset_model;

                $asset_list = $asset_list->where('asset_model', $model);
                $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', $model);
            }

            $data['form_title'] = 'Asset Report: By Model';
            $data['model_list'] = $model_list;
        }else if($type=='bycategory'){
            $category_list = Categories::where('account_id', $account_id)
                                            ->where('apps_type', 'asset')
                                            ->orderBy('cat_name', 'asc')
                                            ->get();
            

            if(count($category_list)!=0){   
                $category_id = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'asset')
                                                ->orderBy('cat_name', 'asc')
                                                ->limit(1)->first()->cat_id;
                
                $asset_list = $asset_list->where('cat_id', $category_id);
                $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category_id);
            }

            $data['form_title'] = 'Asset Report: By Category';
            $data['category_list'] = $category_list;
        }else if($type=='bysitelocation'){
            $site_location_list = DB::table('sys_site_location')
                                            ->join('sys_site', 'sys_site.site_id', '=', 'sys_site_location.site_id')
                                            ->select('sys_site_location.location_id', 'sys_site_location.location_name', 'sys_site.site_name','sys_site.site_id')
                                            ->where('sys_site_location.account_id', $account_id)
                                            ->where('sys_site_location.record_stat', 0)
                                            ->orderBy('sys_site.site_name', 'asc')
                                            ->orderBy('sys_site_location.location_name', 'asc')
                                            ->get();
            if(count($site_location_list)!=0){
                $current_site_location = DB::table('sys_site_location')
                                                ->join('sys_site', 'sys_site.site_id', '=', 'sys_site_location.site_id')
                                                ->select('sys_site_location.location_id', 'sys_site_location.location_name', 'sys_site.site_name','sys_site.site_id')
                                                ->where('sys_site_location.account_id', $account_id)
                                                ->where('sys_site_location.record_stat', 0)
                                                ->orderBy('sys_site.site_name', 'asc')
                                                ->orderBy('sys_site_location.location_name', 'asc')
                                                ->limit(1)
                                                ->first();
            
                $site_id = $current_site_location->site_id;
                $location_id = $current_site_location->location_id;
                
                $asset_list = $asset_list->where('site_id', $site_id)->where('location_id', $location_id);
                $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site_id)->where('sys_asset.location_id', $location_id);
            }

            $data['form_title'] = 'Asset Report: By Site/Location';
            $data['site_location_list'] = $site_location_list;
        }else if($type=='byassignee'){
            $employee_list = Employees::where('account_id', $account_id)
                                        ->where('record_stat', 0)
                                        ->orderBy('emp_fullname', 'asc')->get();
            
            if(count($employee_list)!=0){
                $employee_id = Employees::where('account_id', $account_id)
                                            ->where('record_stat', 0)
                                            ->orderBy('emp_fullname', 'asc')->limit(1)->first()->employee_id;

                $asset_list = $asset_list->where('assign_to', $employee_id);
                $asset_custom_values = $asset_custom_values->where('sys_asset.assign_to', $employee_id);
            }

            $data['form_title'] = 'Asset Report: By Assignee';
            $data['employee_list'] = $employee_list;
        }
        
        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();

        $data['asset_list'] = $asset_list;
        $data['custom_field_list'] = $custom_fields;
        $data['asset_custom_value_list'] = $asset_custom_values;

        //return $employee;
        //return $data;
        return view('assettracking::report_asset')->with($data);

    }
}
