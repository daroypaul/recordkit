<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\packages\assettracking\models\AssetLeaseLogs;
use App\packages\assettracking\models\AssetLeaseItems;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\settings\models\CustomFields;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class ReportStatus extends Controller
{
    public function index_report($type){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_status')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', '<>', 'deleted')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if(strtolower($type)=='available'){
            $data['form_title'] = 'Available Asset Reports';
        }else if(strtolower($type)=='leased'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else if(strtolower($type)=='donated'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else if(strtolower($type)=='damage'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else if(strtolower($type)=='disposed'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else if(strtolower($type)=='lost'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else if(strtolower($type)=='sold'){
            $data['form_title'] = ucfirst($type) . ' Asset Reports';
        }else{
            return redirect('dashboard');
        }

        $data['type'] = strtolower($type);
        //$data['asset_list'] = $asset_list;
        $data['custom_field_list'] = $custom_fields;
        //$data['asset_custom_value_list'] = $asset_custom_values;
        
        return view('assettracking::report_status')->with($data);
    }


    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;
        $type =  $request->type;

        //Primary Filter
        $start_date = (isset($request->start_date)) ? date('Y-m-d', strtotime($request->start_date)) : '';
        $end_date = (isset($request->end_date)) ? date('Y-m-d', strtotime($request->end_date)) : '';
        $category = (isset($request->category)) ? $request->category : '';
        $site = (isset($request->site)) ? $request->site : '';
        $location = (isset($request->location)) ? $request->location : '';
        $employee = (isset($request->employee)) ? $request->employee : '';

        $brand = (isset($request->brand)) ? $request->brand : '';
        $cost = (isset($request->cost)) ? $request->cost : '';
        $date_purchased = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $desc = (isset($request->desc)) ? $request->desc : '';
        $model = (isset($request->model)) ? $request->model : '';
        $serial = (isset($request->serial)) ? $request->serial : '';
        $status = (isset($request->status)) ? $request->status : '';
        $tag = (isset($request->tag)) ? $request->tag : '';
        $vendor = (isset($request->vendor)) ? $request->vendor : '';
        $status = (isset($request->status)) ? $request->status : '';
        $is_leasable = (isset($request->is_leasable)) ? $request->is_leasable : '';
        $custom_data_fields = (isset($request->custom_fields)) ? $request->custom_fields : '';
        $remarks = (isset($request->remarks)) ? $request->remarks : '';

        /*$asset_list = Assets::with(['custom_values', 'categories', 'site', 'location','updator','assigned_to'])
                                ->where('account_id', $account_id)
                                ->where('record_stat', '<>', 'deleted');*/
        $asset_list = DB::table('sys_asset')
                                ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                ->leftJoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                                ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                                ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                                ->leftJoin('sys_users', 'sys_users.user_id', '=', 'sys_asset.updated_by')
                                ->leftJoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset.assign_to')
                                ->select('sys_asset.*', 'sys_employees.emp_fullname','sys_categories.cat_name','sys_site.site_name','sys_site_location.location_name','sys_users.user_fullname')
                                ->where('sys_asset.account_id', $account_id)
                                ->where('sys_asset.record_stat', '<>', 'deleted');

        $asset_custom_values = DB::table('sys_asset')
                                ->join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                                ->join('sys_custom_fields', 'sys_custom_fields.field_id', '=', 'sys_asset_custom_value.field_id')
                                ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id', 'sys_custom_fields.field_label')
                                ->where('sys_asset.account_id', $account_id)->where('sys_asset.record_stat', '<>', 'deleted');
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', '<>', 'deleted')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if(strtolower($type)=='available'){
            $asset_list = $asset_list->where(function ($query) {
                                    $query->where('sys_asset.asset_status', 'available')
                                            ->orWhere('sys_asset.asset_status', 'checked out')
                                            ->orWhere('sys_asset.asset_status', 'leased');
                                    });

            $asset_custom_values = $asset_custom_values->where(function ($query) {
                                        $query->where('sys_asset.asset_status', 'available')   
                                        ->orWhere('sys_asset.asset_status', 'checked out')
                                        ->orWhere('sys_asset.asset_status', 'leased');
                                    });
        }else if(strtolower($type)=='donated'){
            $asset_list = $asset_list->where('sys_asset.asset_status', strtolower($type));
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status', strtolower($type));
        }else if(strtolower($type)=='damage'){
            $asset_list = $asset_list->where('sys_asset.asset_status', strtolower($type));
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status', strtolower($type));
        }else if(strtolower($type)=='disposed'){
            $asset_list = $asset_list->where('sys_asset.asset_status', strtolower($type));
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status', strtolower($type));
        }else if(strtolower($type)=='lost'){
            $asset_list = $asset_list->where('sys_asset.asset_status', strtolower($type));
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status', strtolower($type));
        }else if(strtolower($type)=='sold'){
            $asset_list = $asset_list->where('sys_asset.asset_status', strtolower($type));
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_status', strtolower($type));
        }

        if($category){
            $asset_list = $asset_list->where('sys_asset.cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }
        if($site){
            $asset_list = $asset_list->where('sys_asset.site_id', $site);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site);
        }
        if($location){
            $asset_list = $asset_list->where('sys_asset.location_id', $location);
            $asset_custom_values = $asset_custom_values->where('sys_asset.location_id', $location);
        }
        if($tag){
            $asset_list = $asset_list->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
        }
        if($desc){
            $asset_list = $asset_list->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
        }
        if($model){
            $asset_list = $asset_list->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
        }
        if($serial){
            $asset_list = $asset_list->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
        }
        if($brand){
            $asset_list = $asset_list->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
        }
        if($cost){
            $cost = Helpers::converts('currency_to_int', $cost);
            $asset_list = $asset_list->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
        }
        if($date_purchased){
            $date_purchased = date('Y-m-d', strtotime($date_purchased));
            $asset_list = $asset_list->whereDate('sys_asset.date_purchased', $date_purchased);
            $asset_custom_values = $asset_custom_values->whereDate('sys_asset.date_purchased', $date_purchased);
        }
        if($vendor){
            $asset_list = $asset_list->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
        }
        if($remarks){
            $asset_list = $asset_list->where('sys_asset.asset_remarks', 'LIKE', '%'.$remarks.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_remarks', 'LIKE', '%'.$remarks.'%');
        }
        if($custom_data_fields){
            $asset_list = $asset_list->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
        }
                                    
        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();

        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        /*
        
        */

        return $response;
    }
}
