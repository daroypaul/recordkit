<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\packages\assettracking\models\AssetMaintenance;
use App\packages\assettracking\models\AssetImages;
use App\packages\assettracking\models\AssetDocuments;
use App\packages\assettracking\models\AssetWarranties;
use App\packages\assettracking\models\Categories;
use App\packages\assettracking\models\AssetInOutLogs;
use App\packages\assettracking\models\AssetInOutItems;
use App\packages\assettracking\models\AssetLeaseLogs;
use App\packages\assettracking\models\AssetLeaseItems;
use App\packages\assettracking\models\AssetEvents;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\adminprofile\models\Users;//Get the last number used
use App\modules\adminprofile\models\Employees;//Get the last number used
use App\modules\settings\models\CustomFields;
use App\modules\settings\models\CustomFieldLinks;
use App\modules\settings\models\IdsCounter;//Get the last number used
use App\packages\insurances\models\Insurances;
use App\packages\insurances\models\AssetInsurances;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;
use Uploads;
use File as Files;

class AssetManagerController extends Controller
{
    ##############
    # Asset List #
    ##############

    public function all_list(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$asset_list = Assets::with(['categories','site','image'])->where('record_stat', 0)->get();
        $query = "SELECT 
						sys_uploads.upload_filename as image_name,
						sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_asset.asset_brand,
                        sys_asset.asset_status,
                        sys_asset.asset_vendor,
                        sys_asset.asset_remarks,
                        sys_asset.is_leasable,
                        sys_categories.cat_name,
                        sys_site.site_id,
                        sys_site.site_name,
                        sys_site.record_stat as site_deleted,
                        sys_site_location.location_id,
                        sys_site_location.location_name,
                        sys_site_location.record_stat as location_deleted,
                        inout_items.inout_type,
                        leased_items.lease_id is_leased,
                        sys_employees.emp_fullname
                        FROM `sys_asset` 
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        LEFT JOIN sys_site
                        	ON sys_site.site_id = sys_asset.site_id
                        LEFT JOIN sys_site_location
                        	ON sys_site_location.location_id = sys_asset.location_id
                        LEFT JOIN sys_uploads 
                            ON sys_uploads.owner_id = sys_asset.asset_id 
                            AND sys_uploads.owner = 'ams_img'
                            AND sys_uploads.upload_use = 1
                        LEFT JOIN sys_employees 
                        	ON sys_employees.employee_id = sys_asset.assign_to
                        LEFT JOIN 
                            (
                                SELECT 
                                	sys_asset_inout_items.item_id,
                                	sys_asset_inout_items.asset_id,
                                    sys_asset_inout_logs.inout_type,
                                    MAX(sys_asset_inout_logs.inout_date),
                                    MAX(sys_asset_inout_items.inout_id)
                                FROM sys_asset_inout_items 
                                INNER JOIN sys_asset_inout_logs
                                	ON sys_asset_inout_logs.inout_id = sys_asset_inout_items.inout_id
                                GROUP BY sys_asset_inout_items.asset_id
                            ) as inout_items 
                            ON (inout_items.asset_id = sys_asset.asset_id)
                        LEFT JOIN 
                            (
                                SELECT 
                                    max(sys_asset_lease_items.item_id) item_id, 
                                    sys_asset_lease_items.asset_id, 
                                    sys_asset_lease_logs.lease_id
                                FROM sys_asset_lease_items
                                INNER JOIN sys_asset_lease_logs
                                	ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                                WHERE sys_asset_lease_items.item_stat = 0
                                GROUP BY  sys_asset_lease_items.asset_id
                            ) as leased_items 
                            ON (leased_items.asset_id = sys_asset.asset_id)
                        WHERE sys_asset.record_stat = 'active' AND sys_asset.account_id='".$account_id."'";
        $asset_list =  DB::select($query);//check if asset tag or serial is existing

        $inout_query = "SELECT 
                            sys_asset_inout_logs.inout_id,
                            sys_asset_inout_logs.inout_type,
                            sys_asset_inout_logs.inout_date,
                            sys_asset_inout_items.asset_id
                        FROM sys_asset_inout_logs
                        INNER JOIN sys_asset_inout_items 
                            ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                        WHERE sys_asset_inout_logs.account_id = '".$account_id."'
                        ORDER BY sys_asset_inout_logs.inout_date DESC, sys_asset_inout_logs.inout_id DESC";
        $inout_list = DB::select($inout_query);

        $inout_asset = [];
        $x = [];
        foreach($inout_list as $inout){
            if(!in_array($inout->asset_id, $x)){
                $a = [];
                $a['asset_id'] = $inout->asset_id;
                $a['inout_type'] = $inout->inout_type;
                $a['inout_id'] = $inout->inout_id;
                $inout_asset[] = $a;
                $x[] = $inout->asset_id;
                //$x[] = $inout->asset_id;
            }else{
                //$a = [];
                //$a['asset_id'] = $inout->asset_id;
                //$a['inout_type'] = 'no-inout-record';
                //$inout_asset[] = $a;
            }
        }
        //return $x;

        $ss = '';
        foreach($asset_list as $asset){
            if(!in_array($asset->asset_id, $x)){
                $a = [];
                $a['asset_id'] = $asset->asset_id;
                $a['inout_type'] = 'no-inout-record';
                $inout_asset[] = $a;
                $x[] = $asset->asset_id;
            }
            /*foreach($inout_asset as $inout)
            {
                if($inout['asset_id']==$asset->asset_id){
                    $ss .= $inout['inout_type'] . ' on asset id: ' .$asset->asset_id . '<br/>'; 
                    break;
                }

            }*/
        }

        //return $ss;
        /*SELECT 
                                	sys_asset_inout_logs.inout_id,
                                    sys_asset_inout_logs.inout_type,
                                    sys_asset_inout_logs.inout_date,
                                    sys_asset_inout_items.asset_id
                                FROM sys_asset_inout_logs
                                INNER JOIN sys_asset_inout_items ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                                ORDER BY sys_asset_inout_logs.inout_date DESC, sys_asset_inout_logs.inout_id DESC*/

        $data = [
            "asset_record"=>$asset_list,
            "inout_assets"=>$inout_asset,
            "form_title"=> "All Asset",
        ];

        //return $inout_list; 
        //return $asset_list;
        //return $system_options;
        return view('assettracking::asset_all_list')->with($data);
    }

    public function leasable_list(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $site_id = Auth::user()->site_id;

            $site_list = Site::where('account_id', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

            $assets = Assets::with(['categories','site','location','image', 'is_leased'])->where('account_id', $account_id)->where('is_leasable', 1)->where('record_stat', 'active')->get();

            $lease = DB::table('sys_asset_lease_logs')
                                    ->join('sys_asset_lease_items', 'sys_asset_lease_items.lease_id', '=', 'sys_asset_lease_logs.lease_id')
                                    ->join('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
                                    ->select('sys_asset_lease_logs.lease_id', 'sys_asset_lease_logs.site_id', 'sys_asset_lease_logs.location_id', 'sys_asset_lease_logs.lease_start', 'sys_asset_lease_logs.lease_expire', 'sys_asset_lease_items.asset_id','sys_employees.emp_fullname')
                                    ->where('sys_asset_lease_logs.account_id', $account_id)
                                    ->where('sys_asset_lease_items.item_stat', 0)
                                    ->get();

            if($site_id!=0||$site_id){
                $assets = '';
                $assets = Assets::with(['categories','site','image', 'is_leased'])->where('account_id', $account_id)->where('is_leasable', 1)->where('record_stat', 'active')->where('site_id', $site_id)->get();
            }

            $data = [
                "asset_list"=>$assets,
                "site_list"=>$site_list,
                "user_site_id"=>$site_id,
                "lease_record"=>$lease,
                "form_title"=> "Leasable Asset",
            ];
            
            return view('assettracking::asset_leasable_list')->with($data);

    }

    public function fetch_leasable_list(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        //$user_site_id = Auth::user()->site_id;
        $site_id = $request->site_id;

        $asset_list = '';
        
            $assets = Assets::with(['categories','site','location','image', 'is_leased'])->where('account_id', $account_id)->where('is_leasable', 1)->where('record_stat', 'active')->get();

            $lease = DB::table('sys_asset_lease_logs')
                                    ->join('sys_asset_lease_items', 'sys_asset_lease_items.lease_id', '=', 'sys_asset_lease_logs.lease_id')
                                    ->join('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
                                    ->select('sys_asset_lease_logs.lease_id', 'sys_asset_lease_logs.site_id', 'sys_asset_lease_logs.location_id', 'sys_asset_lease_logs.lease_start', 'sys_asset_lease_logs.lease_expire', 'sys_asset_lease_items.asset_id','sys_employees.emp_fullname')
                                    ->where('sys_asset_lease_logs.account_id', $account_id)
                                    ->where('sys_asset_lease_items.item_stat', 0)
                                    ->get();

            if($site_id!=0||$site_id!='all'){
                $assets = '';
                $assets = Assets::with(['categories','site','image', 'is_leased'])->where('account_id', $account_id)->where('is_leasable', 1)->where('record_stat', 'active')->where('site_id', $site_id)->get();
            }

            $data = [
                "asset_list"=>$assets,
                "lease_record"=>$lease,
            ];

        return $data;
    }

    ##############
    #  #
    ##############
    public function details($id){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        
        //$asset_record = Assets::with(['custom_values', 'categories', 'site', 'location','creator','updator'])->where('account_id', '=', $account_id)->where('asset_id', '=', $id)->orwhere('asset_tag', '=', $id)->first();
        $asset_record = Assets::with(['custom_values', 'categories', 'site', 'location','creator','updator','image','assigned_to'])
                        ->where('account_id', '=', $account_id)
                        ->where(function ($query) use ($id) {
                            $query->where('asset_id', $id)
                                ->orwhere('asset_tag', $id);
                        })->where('record_stat', '<>', 'deleted')->first();

        if(count($asset_record)==0){
            //abort(404);
            session()->flash('record_status', 'Asset not found.');
            return redirect('ams/asset/list/all');
        }else{
            $cat_id = $asset_record->cat_id;
            $asset_id = $asset_record->asset_id;
    
            $asset_custom_values = AssetCustomValues::with(['custom_fields'])->where('asset_id', $asset_id)->where('cat_id', $cat_id)->get();
            $maintenances = AssetMaintenance::with(['users','suppliers'])->where('asset_id', $id)->orderBy('date_start', 'desc')->get();
            //$documents = AssetDocuments::with('users')->where('asset_id', $id)->orderBy('docu_name', 'desc')->get();
            $documents = Uploads::with('users')->where('account_id',$account_id)->where('owner_id', $id)->where('owner', 'ams_docs')->orderBy('date_uploaded', 'desc')->get();
            //$images = AssetImages::with('users')->where('asset_id', $id)->orderBy('date_recorded', 'desc')->get();
            $images = Uploads::with('users')->where('account_id',$account_id)->where('owner_id', $id)->where('owner', 'ams_img')->orderBy('date_uploaded', 'desc')->get();
            $warranties = AssetWarranties::with('users')->where('asset_id', $id)->orderBy('warranty_expire', 'desc')->get();

            //$logs = AssetInOutItems::with('logs')->where('asset_id', $id)->get();
            $query = "SELECT 
                            sys_asset_inout_logs.inout_type,
                            sys_asset_inout_logs.inout_date,
                            sys_asset_inout_items.item_remarks,
                            sys_employees.emp_fullname,
                            sys_site.site_name,
                            sys_site.record_stat as site_deleted,
                            sys_site_location.location_name,
                            sys_site_location.record_stat as location_deleted,
                            sys_users.user_fullname
                        FROM sys_asset_inout_items 
                        INNER JOIN sys_asset_inout_logs 
                            ON sys_asset_inout_logs.inout_id = sys_asset_inout_items.inout_id 
                        INNER JOIN sys_site 
                            ON sys_site.site_id = sys_asset_inout_logs.site_id
                        INNER JOIN sys_site_location 
                            ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                        INNER JOIN sys_employees 
                            ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                            AND  sys_employees.record_stat <> 2
                        INNER JOIN sys_users 
                            ON sys_users.user_id = sys_asset_inout_logs.updated_by 
                        WHERE sys_asset_inout_items.asset_id='".$id."' AND sys_asset_inout_logs.record_stat='active'  ORDER BY sys_asset_inout_logs.inout_date DESC, sys_asset_inout_logs.date_updated DESC";
            $logs = DB::select($query);


            $events = AssetEvents::with(['recorder','prev_site','prev_location','new_site','new_location'])->where('account_id', $account_id)->where('asset_id', $id)->where('record_stat', 0)->orderBy('event_date', 'desc')->orderBy('date_recorded', 'desc')->get();

            //$insurances = AssetInsurances::with('insurances')->where('account_id', $account_id)->where('asset_id', $id)->get();

            $insurances = DB::table('sys_asset_insurance_link')
                            ->select('sys_asset_insurance_link.id', 'sys_asset_insurance_link.asset_id', 'sys_users.user_fullname', 'sys_insurances.*')
                            ->join('sys_insurances', 'sys_insurances.insurance_id', '=', 'sys_asset_insurance_link.insurance_id')
                            ->join('sys_users', 'sys_users.user_id', '=', 'sys_asset_insurance_link.recorded_by')                            ->where('sys_asset_insurance_link.account_id', $account_id)
                            ->where('sys_asset_insurance_link.asset_id', $id)
                            ->orderBy('sys_insurances.insurance_name', 'asc')
                            ->get();

            $data = [
                "filetypes"=>Helpers::file_types(),
                "logs"=>$logs,
                "events"=>$events,
                "maintenances"=>$maintenances,
                "documents"=>$documents,
                "images"=>$images,
                "warranties"=>$warranties,
                "insurances"=>$insurances,
                "asset_id"=>$asset_id,
                "asset_record"=>$asset_record,
                "custom_values"=>$asset_custom_values,
                "form_title"=> "Update Asset",
                "status"=> ['available','leased','checked in','checked out','damage','lost','disposed','sold'],
            ];

            return view('assettracking::asset_details')->with($data);
        }
    }

    public function add(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_add')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$company_id = Auth::user()->company_id;
        $categories = Categories::where('account_id', '=', $account_id)->where('apps_type', 'asset')->orderBy('cat_name', 'asc')->get();
        $sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
                "categories"=>$categories,
                "sites"=>$sites,
                "form_title"=> "Add New Asset",
                "action_type"=>"add",
                "status"=> ['available','leased','checked in','checked out','damage','lost','disposed','sold'],
        ];

        return view('assettracking::asset_form')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        //Custom fields validation
        $custom_error = [];
        $custom_error_index = [];
        $err = 0;
        if($request->input('custom_field_value')){
            foreach($request->input('custom_field_value') as $key => $val) { 
                $custom_validate = '';
                $custom_validate = '';
                $is_required = $request->input('custom_field_required.'.$key);
                $data_type = $request->input('custom_field_type.'.$key);
                $label = $request->input('custom_field_label.'.$key);
                $value = $request->input('custom_field_value.'.$key);

                if($data_type=='email'){
                    if($is_required==1){
                        if (empty($value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'This field is required';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }else if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Not a valid email address';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                    }else{
                        if(!filter_var($value, FILTER_VALIDATE_EMAIL)&&isset($value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid email format';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                    }
                }else if($data_type=='text'){
                    if($is_required==1){
                        if (empty($value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'This field is required';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                        /*else if (!preg_match("/^[a-zA-Z0-9 ,.]*$/",$value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid characters. Only Alphanumber, commas, space and dot';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }*/
                    }else{
                        /*if (!preg_match("/^[a-zA-Z0-9 ,.]*$/",$value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid characters. Only Alphanumber, commas, space and dot';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }*/
                    }
                }else if($data_type=='number'){
                    if($is_required==1){
                        if (empty($value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'This field is required';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                        else if (!preg_match("/^[0-9]*$/",$value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid characters. Only numbers';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                    }else{
                        if (!preg_match("/^[0-9]*$/",$value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid characters. Only numbers';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                    }
                }else if($data_type=='currency'){
                    if($is_required==1){
                        if (empty($value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'This field is required';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }else if (!preg_match("/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",$value)) {
                            $custom_error_index['key'] = $key;
                            $custom_error_index['msg'] = 'Invalid currency format';
                            $custom_error[$key] = $custom_error_index;
                            $err++;
                        }
                    }else{
                        if ($value) {
                            if (!preg_match("/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",$value)) {
                                $custom_error_index['key'] = $key;
                                $custom_error_index['msg'] = 'Invalid currency format';
                                $custom_error[$key] = $custom_error_index;
                                $err++;
                            }
                        }
                    }
                }
            }
        }

        $rules = [
            'asset_tag' => 'required|max:100',
            'asset_desc' => 'required|min:3|max:100',
            'asset_model' => 'required|min:2|max:50',
            'asset_serial' => 'max:100',
            'asset_brand' => 'max:50',
            'asset_vendor' => 'max:100',
            'asset_remarks' => 'max:1000',
            'asset_category' => 'required',
            'asset_site' => 'required',
            'asset_location' => 'required',
            'asset_status' => 'required|max:100',
            'po_number' => 'max:100',
            'purchased_cost' => "regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",
            //'asset_image' => 'mimes:jpeg,jpg,png,gif|max:1024',
        ];

        //if(count($request->file('asset_image'))!=0){
        if($request->hasFile('asset_image')){
            $rules['asset_image'] = 'image|max:1024';
        }


        if($request->input('date_purchased')){
           $rules['date_purchased'] = 'nullable|date_format:"Y-m-d"';
           //'date_purchased' => 'nullable|date_format:"Y-m-d"'
        }

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $val_msg = $validator->errors();

        //Check site and Location if not
        $check_site = Site::where('account_id', $account_id)->where('site_id', $request->asset_site)->where('record_stat',0)->count();
        $check_location = Location::where('account_id', $account_id)->where('location_id', $request->asset_location)->where('record_stat',0)->count();

        if($check_site==0){
            $val_msg->add('asset_site', 'Site already deleted');
            $val_msg->add('asset_location', 'Location already deleted');
        }else{
            if($check_location==0){
                $val_msg->add('asset_location', 'Location already deleted');
                $a = 'location error';
            }
        }

        #This prevent error
        if(count($custom_error)!=0){
            $response['custom_errors'] = $custom_error;
        }

        if($request->action=='add'&&!Helpers::get_user_privilege('pkg_fixasset_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->action=='edit'&&!Helpers::get_user_privilege('pkg_fixasset_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()||count($custom_error)!=0||count($val_msg)!=0) {
            $response['errors']= $val_msg;
        }else{
            #remove extra whitespaces
            //$tag = preg_replace('/\s+/', ' ', $request->input('asset_tag'));
            //$serial = preg_replace('/\s+/', ' ', $request->input('asset_serial'));
            #remove extra whitespaces
            $tag = Helpers::remove_whitespaces($request->input('asset_tag'));
            $serial = Helpers::remove_whitespaces($request->input('asset_serial'));
                
            if($request->action=='add'){

                if($serial){
                    #Asset tag and serial to be check
                    $query = "select * from sys_asset WHERE account_id='".$account_id."' AND (asset_tag='".$tag."' OR asset_serial='".$serial."') AND record_stat <> 'deleted'";
                }else{
                    $query = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_tag='".$tag."' AND record_stat <> 'deleted'";#Only Asset tag will be check along with account
                }

                $check_asset =  DB::select($query);//check if asset tag or serial is existing    

                if(count($check_asset)!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = "Asset already exists.";
                }else{#proceed to data insertion
                    if($request->hasFile('asset_image')){
                        //$file = $request->file('asset_image');
                        /*$image_filename_with_extension = $file->getClientOriginalName();
                        $image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
                        $image_fileExt = $file->getClientOriginalExtension();
                        $image_size = $file->getSize();
                        $generated_key = Helpers::generate_key();
                        $file_name_to_store = $generated_key . '-' . time() .  '.' . $image_fileExt;//Prevent file duplication*/
                        
                        /*$file = $this->get_image_info('asset_image', $request);
                        $file_name = $file['name'];
                        $file_extension = $file['ext'];
                        $file_size = $file['size'];
                        $generated_key = Helpers::generate_key();

                        $file_name_to_store = $generated_key . '-' . time() . '.' . $file_extension;

                        $response['file_name'] = $file_name_to_store;*/

                    }

                    $cost = Helpers::converts('currency_to_int', $request->input('purchased_cost'));
                    $asset = new Assets;
                    $asset->account_id = $account_id;
                    $asset->cat_id = $request->input('asset_category');
                    $asset->site_id = $request->input('asset_site');
                    $asset->location_id = $request->input('asset_location');
                    $asset->asset_tag = strtoupper($request->input('asset_tag'));
                    $asset->asset_desc = ucwords($request->input('asset_desc'));
                    $asset->asset_model = $request->input('asset_model');
                    $asset->asset_serial = strtoupper($request->input('asset_serial'));
                    $asset->asset_brand = ucfirst($request->input('asset_brand'));
                    $asset->asset_vendor = ucwords($request->input('asset_vendor'));
                    $asset->date_purchased = $request->input('date_purchased');
                    $asset->purchased_cost = $cost;
                    $asset->asset_remarks = $request->input('asset_remarks');
                    $asset->asset_status = $request->input('asset_status');
                    $asset->is_leasable = $request->input('is_leasable');
                    $asset->recorded_by = $user_id;
                    $asset->updated_by = $user_id;
                    $asset->record_stat = 'active';
                    $asset->po_number = $request->input('po_number');
                    $asset->save();

                    $asset_id = $asset->asset_id;

                    

                    if($request->input('custom_field_value')){
                        foreach($request->input('custom_field_value') as $key => $val) {#check each custom fields
                            if($request->input('custom_field_value.'.$key)){#Only those fields with value to be insert
                                $custom_value = new AssetCustomValues;
                                $custom_value->asset_id = $asset_id;
                                $custom_value->cat_id = $request->input('asset_category');
                                $custom_value->field_id = $request->input('custom_field_id.'.$key);
                                $custom_value->custom_value = $request->input('custom_field_value.'.$key);
                                $custom_value->save();
                            }
                        }
                    }

                    if($asset){#If data insertion is successful
                        $file_name_to_store = '';
                        if($request->hasFile('asset_image')){
                            $get_file = $request->file('asset_image');
                            $file_detail = $this->get_image_info('asset_image', $request);
                            $file_name = $file_detail['name'];
                            $file_extension = $file_detail['ext'];
                            $file_size = $file_detail['size'];
                            $generated_key = Helpers::generate_key();
                            $file_name_to_store = $generated_key . '-' . time() . '.' . $file_extension;

                            $upload_file = $get_file->storeAs('public/',$file_name_to_store);//Upload the file

                            $upload = new Uploads;
                            $upload->account_id = $account_id;
                            $upload->owner_id = $asset_id;
                            $upload->owner = 'ams_img';
                            $upload->upload_filename = $file_name_to_store;
                            $upload->upload_extension = $file_extension;
                            $upload->upload_size = $file_size;
                            $upload->upload_use = 1;
                            $upload->date_uploaded = Carbon::now('UTC')->toDateTimeString();
                            $upload->uploaded_by = $user_id;
                            $upload->save();
                        }

                        //Add Logs
                        $desc = 'added an asset [Tag No: '.$asset->asset_tag.']';
                        Helpers::add_activity_logs(['ams-asset-added',$asset_id,$desc]);

                        #Update the asset tag counter
                        $last_counter = ($request->tag_number) ? $request->tag_number : 1;
                        $counter_id = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_id;
                        $update_counter = Options::find($counter_id);
                        $update_counter->option_value = $last_counter;
                        $update_counter->save();

                        $response['success'] = '<div class="alert alert-success alert-rounded m-t-10">New Asset successfully added</div>';
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = "New Asset successfully added";
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = "Unable to add asset. Error occured during process. Error code: ErAN0001";
                    }
                }
            }else if($request->action=='edit'){
                $id = $request->id;
                #Check if Asset Tag and Serial no changes
                $query1 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_id='".$id."' AND asset_tag='".$tag."' AND asset_serial='".$serial."'";
                #Check if Asset Tag no changes
                $query2 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_id='".$id."' AND asset_tag='".$tag."' AND asset_serial<>'".$serial."'";
                #Check if Asset Tag is changes
                $query3 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_id='".$id."' AND asset_tag<>'".$tag."' AND asset_serial='".$serial."'";

                $query4 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_serial='".$serial."'";
                $query5 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_tag='".$tag."'";

                if($serial){
                    $query6 = "select * from sys_asset WHERE account_id='".$account_id."' AND (asset_tag='".$tag."' OR asset_serial='".$serial."')";
                }else{
                    $query6 = "select * from sys_asset WHERE account_id='".$account_id."' AND asset_tag='".$tag."'";#Only Asset tag will be check along with account
                }

                if(count(DB::select($query1))){
                    $response['a'] = "Proceed";
                    $update = $this->query_update($id, $request);
                }else if(count(DB::select($query2))){
                    if(count(DB::select($query4))){
                        $response['error'] = '<div class="alert alert-danger alert-rounded m-t-10">Asset already exists.</div>';
                    }else{
                        $response['a'] = "Proceed to Update. Serial has change";
                        $update = $this->query_update($id, $request);
                    }
                }else if(count(DB::select($query3))){
                    if(count(DB::select($query5))){
                        $response['error'] = '<div class="alert alert-danger alert-rounded m-t-10">Asset already exists.</div>';
                    }else{
                        $response['a'] = "Proceed to Update. Tag has change";
                        $update = $this->query_update($id, $request);
                    }
                }else{
                    if(count(DB::select($query6))){
                        $response['error'] = '<div class="alert alert-danger alert-rounded m-t-10">Asset already exists.</div>';
                    }else{
                        $response['a'] = "Proceed to Update. Tag and Serial are change";
                        $update = $this->query_update($id, $request);
                    }
                }

                if($update){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = "Asset successfully updated";
                    
                    if($request->tag_number){
                        $last_counter = ($request->tag_number) ? $request->tag_number : 1;
                        $counter_id = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_id;
                        $update_counter = Options::find($counter_id);
                        $update_counter->option_value = $last_counter;
                        $update_counter->save();
                    }

                    //Add Logs
                    $desc = 'updated an asset [Tag No: '.$request->asset_tag.']';
                    Helpers::add_activity_logs(['ams-asset-updated',$id,$desc]);

                    //Update the category foreign key
                    //$update_inout = AssetInOutItems::where('account_id', $account_id)->where('asset_id', $id)->update(['cat_id' => $request->input('asset_category')]);
                    //$update_lease = AssetLeaseItems::where('account_id', $account_id)->where('asset_id', $id)->update(['cat_id' => $request->input('asset_category')]);
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = "Unable to update asset. Error occured during process";
                }
            }//End of else if
        }
        //return response()->json(['errors'=>$validator->errors(),'custom_errors'=>$custom_error]);

        return response()->json($response);
    }

    ##############
    # Edit
    #############

    public function edit($id){
        $account_id = Auth::user()->account_id;
        //$asset_record = Assets::with(['custom_values','custom_fields'])->where('account_id', '=', $account_id)->where('asset_id', '=', $id)->orwhere('asset_tag', '=', $id)->first();
        //$asset_record = Assets::with(['custom_values'])->where('account_id', '=', $account_id)->where('asset_id', '=', $id)->orwhere('asset_tag', '=', $id)->first();
        $asset_record = Assets::with(['custom_values'])
                        ->where('account_id', '=', $account_id)
                        ->where(function ($query) use ($id) {
                            $query->where('asset_id', $id)
                                ->orwhere('asset_tag', $id);
                        })->where('record_stat', '<>', 'deleted')->first();

        if(!Helpers::get_user_privilege('pkg_fixasset_edit')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        if(count($asset_record)==0){
            session()->flash('record_status', 'Record not found');
            return redirect('ams/asset/list/all');
        }else{
            $cat_id = $asset_record->cat_id;
            $asset_id = $asset_record->asset_id;
            $site_id = $asset_record->site_id;
            $location_id = $asset_record->location_id;

            $asset_category = Categories::where('account_id',$account_id)->where('apps_type', 'asset')->where('cat_id',$cat_id)->first();

            ########################
            # Display custom fields #
            ########################
            $custom_fields = [];
            if($asset_category){
                $custom_fields = CustomFields::whereHas('custom_field_links', function ($query) use ($cat_id) {
                    $query->where('ref_id', $cat_id);
                })->where('field_type', 'asset')->get();//Get Avaliable Custom Fields
            }

                $append_custom_fields = '';
                
                if(count($custom_fields)!=0){
                    $append_custom_fields = '<div class="col-md-12"><h4 class="card-title">Custom Input(s)</h4><hr class="m-b-20" style="border-top: 2px solid rgba(0,0,0,.1) !important;"/></div>';
                }

                # Get Custom Fields Datas
                foreach($custom_fields as $custom_field){
                    $field_id = $custom_field->field_id;
                    if($custom_field->field_data_type=='date'){
                        $custom_class = 'date-picker-default';
                        $default_value = '';
                    }else if($custom_field->field_data_type=='currency'){
                        $custom_class = 'currency-input';
                        $default_value = '';
                    }else{
                        $custom_class = '';
                        $default_value = '';
                    }

                    if($custom_field->is_required==1){
                        $require_helper='<i class="mdi mdi-check-circle form-required-helper text-danger"></i>';
                    }else{
                        $require_helper='';
                    }

                    $custom_value = AssetCustomValues::where('asset_id', $asset_id)->where('cat_id', $cat_id)->where('field_id', $field_id)->first();
                    $default_value = $custom_value['custom_value'];

                    $append_custom_fields .= '<div class="col-md-3">
                                            <div class="form-group">
                                                <label class="form-control-label">'.$custom_field->field_label.' '.$require_helper.'</label>
                                                <input type="hidden" name="custom_field_id[]" class="form-control" placeholder="" value="'.$custom_field->field_id.'">
                                                <input type="hidden" name="custom_field_required[]" class="form-control" placeholder="" value="'.$custom_field->is_required.'">
                                                <input type="hidden" name="custom_field_type[] class="form-control" placeholder="" value="'.$custom_field->field_data_type.'">
                                                <input type="hidden" name="custom_field_label[] class="form-control" placeholder="" value="'.$custom_field->field_label.'">
                                                <input type="text" name="custom_field_value[]" class="form-control '.$custom_class.'" placeholder="" name="asset_desc" value="'.$default_value.'">
                                            </div>
                                        </div>';
                }

                if(count($custom_fields)!=0){
                    $append_custom_fields .= '<div class="col-md-12"><hr class="m-t-5" style="border-top: 2px solid rgba(0,0,0,.1) !important;"/></div>';
                }
                ############################

                if(count($asset_record)==0){
                    abort(404);
                }else{
                    $asset_site = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->first();
                    $asset_location = Location::where('account_id', $account_id)->where('site_id', $site_id)->where('location_id', $location_id)->where('record_stat',0)->first();
                    
                    /*$categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'asset')
                                                ->orderBy('cat_name', 'asc')
                                                ->get();
                    $sites = Site::where('account_id', $account_id)
                                            ->where('record_stat',0)
                                            ->orderBy('site_name', 'asc')
                                            ->get();
                    $locations = [];                    
                    if($asset_site!=0){   
                        $locations = Location::where('site_id', $site_id)
                                                ->where('record_stat',0)
                                                ->get();
                    }*/

                    $data = [
                        "asset_id"=>$asset_id,
                        //"categories"=>$categories,
                        //"sites"=>$sites,
                        //"locations"=>$locations,
                        "asset_record"=>$asset_record,
                        "asset_category"=>$asset_category,
                        "asset_site"=>$asset_site,
                        "asset_location"=>$asset_location,
                        "custom_fields"=>$append_custom_fields,
                        "is_update"=>1,
                        "action_type"=>'edit',
                        "form_title"=> "Update Asset",
                        "status"=> ['available','leased','checked in','checked out','damage','lost','disposed','sold'],
                    ];
                    
                    //return $asset_record;
                    return view('assettracking::asset_form')->with($data);
                }
        }
    }


    function query_update($id, $request){
        $user_id = Auth::user()->user_id;
        $account_id = Auth::user()->account_id;
        $asset_record = Assets::find($id);

        $data = [
            'cat_id' => $request->input('asset_category'),
            'site_id' => $request->input('asset_site'),
            'location_id' => $request->input('asset_location'),
            'asset_tag' => strtoupper($request->input('asset_tag')),
            'asset_desc' => ucwords($request->input('asset_desc')),
            'asset_model' => ucwords($request->input('asset_model')),
            'asset_serial' => strtoupper($request->input('asset_serial')),
            'asset_brand' => ucfirst($request->input('asset_brand')),
            'asset_vendor' => ucwords($request->input('asset_vendor')),
            'date_purchased' => $request->input('date_purchased'),
            'purchased_cost' => Helpers::converts('currency_to_int', $request->input('purchased_cost')),
            'asset_remarks' => $request->input('asset_remarks'),
            'asset_status' => $request->input('asset_status'),
            'is_leasable' => $request->input('is_leasable'),
            'updated_by' => $user_id,
            'po_number' =>$request->input('po_number'),
        ];

        if($request->hasFile('asset_image')){
            $generated_key = Helpers::generate_key($id);
            $file = $request->file('asset_image');
            $image_filename_with_extension = $file->getClientOriginalName();
            $image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
            $image_fileExt = $file->getClientOriginalExtension();
            $image_size = $file->getSize();
            $file_name_to_store = $generated_key . '-' . time() . '.' . $image_fileExt;
            $upload = $file->storeAs('public/',$file_name_to_store);//Upload the file

            //$data["asset_image"] = $file_name_to_store;

            //Delete existing file uploaded if existing
            /*if($asset_record->asset_image){
                $file_to_be_deleted = public_path(). '/storage/ams_files/'.$asset_record->asset_image;
                $delete_file = File::delete($file_to_be_deleted);
            }*/

            $uploaded_file = storage_path('app/public/' . $file_name_to_store);//File uploaded path
            
            if (File::exists($uploaded_file)) {//Check file is already uploaded
                if(count(AssetImages::where('asset_id', $id)->where('is_primary', '1')->get())!=0){
                    AssetImages::where('asset_id', $id)->where('is_primary', '1')->update(['is_primary' => '0']);
                }

                $asset_images = new AssetImages;
                $asset_images->asset_id = $id;
                $asset_images->account_id = $account_id;
                $asset_images->image_name = $file_name_to_store;
                $asset_images->image_size = $image_size;
                $asset_images->is_primary = 1;
                $asset_images->recorded_by = $user_id;
                $asset_images->date_recorded = Carbon::now('UTC')->toDateTimeString();
                $asset_images->save();
            }
        }
        /*else if($request->input('remove_image')!=0){//If remove image is selected
            $file_to_be_deleted = public_path(). '/storage/ams_files/'.$asset_record->asset_image;
            $data["asset_image"] = '';
            $delete_file = File::delete($file_to_be_deleted);
        }*/

        //$b = [];
        if($request->input('custom_field_value')){
            //$custom_values = AssetCustomValues::where('asset_id', $id)->get();

            foreach($request->input('custom_field_value') as $key => $val) {//check each custom fields
                if($request->input('custom_field_value.'.$key)){//Only those fields with value to be insert
                    $field_id = $request->input('custom_field_id.'.$key);
                    $value = $request->input('custom_field_value.'.$key);

                    $existing = AssetCustomValues::where('asset_id', $id)->where('cat_id', $request->input('asset_category'))->where('field_id', $field_id)->get();

                    $update_custom_value = AssetCustomValues::where('asset_id', $id)->where('field_id', $field_id)->update(['custom_value'=>$value]);

                    if(!count($existing)){//If asset custom is not yet added
                        $custom_value = new AssetCustomValues;
                        $custom_value->asset_id = $id;
                        $custom_value->cat_id = $request->input('asset_category'); 
                        $custom_value->field_id = $field_id;
                        $custom_value->custom_value = $value;
                        $custom_value->save();
                    }

                    $custom_field_id[] = $field_id;
                }
            }

            //Delete current custom value that has no new value this clean unnecessary in the database
            $custom_values = AssetCustomValues::where('asset_id', $id)->get();//Get the existing values
            foreach($custom_values as $custom_value){
                $exist = in_array($custom_value->field_id, $custom_field_id);// Check if specific existing custom field id is not in the new set of custom field id
                if(!$exist){
                    $custom_value_id = $custom_value->id;
                    AssetCustomValues::find($custom_value_id)->delete();
                }
            }
        }

        //$attempt_update = Assets::where('account_id', $account_id)->where('asset_id', $id)->update($data);

        //if($attempt_update){
        
        //}

        return Assets::where('account_id', $account_id)->where('asset_id', $id)->update($data);
    }

    #############
    # Bulk Action
    ##############
    public function bulk_action(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'date' => 'required|date_format:"M d, Y"',
            //'maintenance_type' => 'required',
            //'service_provider'=>'required',
            //'start_date' => 'required|date_format:"Y-m-d"',
            //'completion_date' => 'nullable|date_format:"Y-m-d"',
            //'remarks' => 'max:500',
            //'cost' => "nullable|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",
            //'asset_image' => 'mimes:jpeg,jpg,png,gif|max:1024',
        ];

        if(isset($request->to)||$request->to){
            $rules['to'] = 'min:2|max:100';
        }

        if($request->notes){
            $rules['notes'] = 'min:2|max:1000';
        }

        if($request->amount){
            $rules['amount'] = 'regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/';
        }

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];
        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $x = [];
            $s = [];
            $stat = '';
            $_log_desc = '';
            foreach($request->id as $asset_id){
                //$asset_record = Assets::find($id);
                
                $check_record = Assets::where('account_id',$account_id)->where('asset_id',$asset_id)->count();
                
                if($check_record!=0){
                    $asset = Assets::find($asset_id);
                        $data = [
                            'date_stat' => date('Y-m-d', strtotime($request->date)),
                            'stat_to' => $request->to,
                            'stat_amount'=>$request->amount,
                            'stat_remarks'=>$request->notes,
                            'asset_status' => $request->stat,
                            'updated_by' => $user_id,
                        ];

                    if(Assets::where('account_id', $account_id)->where('asset_id', $asset_id)->update($data)){
                        $s[] = $asset_id;

                        $_log_desc .= $asset->asset_tag . ', ';

                        //Add Logs per asset
                        $desc = 'updated the status of asset [Tag No: '.$asset->asset_tag .'] to ' . ucwords($request->stat);
                        Helpers::add_activity_logs(['ams-asset-updated-status',$asset_id,$desc]);
                    }
                }else{
                    $x[] = $asset_id;
                }
            }

            if($_log_desc){
                //$desc = 'updated the status of asset [Tag No: '.substr($_log_desc, 0, -2) .'] to ' . ucwords($request->stat);
                //Helpers::add_activity_logs(['ams-asset-updated',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
            $response['stat'] = ucfirst($request->stat);
        }

        //return $request;
        return $response;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $x = [];
        $s = [];
        $_log_desc = '';

        if(!Helpers::get_user_privilege('pkg_fixasset_delete')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{

            foreach($request->id as $asset_id){
                //$asset_record = Assets::find($id);
                
                $check_record = Assets::where('account_id',$account_id)->where('asset_id',$asset_id)->where('record_stat','<>','deleted')->count();
                if($check_record!=0){
                    $assets = Assets::find($asset_id);
                    //Carbon::now('UTC')->toDateTimeString();
                    $assets->date_deleted = Carbon::now('UTC')->toDateTimeString();
                    $assets->deleted_by = $user_id;
                    $assets->record_stat = 'deleted';
                    $assets->save();

                    if($assets){
                        $s[] = $asset_id;

                        $_log_desc .= $assets->asset_tag . ', ';

                        $desc = 'delete an asset [Tag No: '.$assets->asset_tag .']';
                    Helpers::add_activity_logs(['ams-asset-deleted',$asset_id,$desc]);
                    }
                }else{
                    $x[] = $asset_id;
                }
            }

            if($_log_desc){
                //$desc = 'delete an asset [Tag No: '.substr($_log_desc, 0, -2) .']';
                //Helpers::add_activity_logs(['ams-asset-deleted',0,$desc]);
            }
        }


        $response['eid'] = $x;
        $response['sid'] = $s;

        return $response;
    }

    public function archive(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $check_record = Assets::where('account_id',$account_id)->where('asset_id',$id)->count();
        
        if($check_record!=0){
            $assets = Assets::find($id);
            $assets->record_stat = 'archived';
            $assets->save();

            if(count($assets)!=0){
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = "Asset successfully archived";
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = "Unable to archive asset. Error occured during process.";
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = "No record found";
        }
        return $response;
    }

    ///////////////////
    // Image Upload //
    /////////////////

    public function images(Request $request)//Uplaod images
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $id = $request->get('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $check_record = Assets::where('account_id', '=', $account_id)->where('asset_id', $id)->count();

        $rules = [
            'asset_image' => 'required|image|max:1024',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($check_record!=0){
                $asset = Assets::find($id);

                $generated_key = Helpers::generate_key($id);

                $file = $this->get_image_info('asset_image', $request);
                $file_name = $file['name'];
                $file_extension = $file['ext'];
                $file_size = $file['size'];

                //$file = $request->file('asset_image');
                //$image_filename_with_extension = $file->getClientOriginalName();
                //$image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
                //$image_fileExt = $file->getClientOriginalExtension();
                //$image_size = $file->getSize();

                $file_name_to_store = $generated_key . '-' . time() . '.' . $file_extension;
                $upload = $request->file('asset_image')->storeAs('public/',$file_name_to_store);//Attempt to upload
                $uploaded_file = storage_path('app/public/' . $file_name_to_store);//File uploaded path

                if (!File::exists($uploaded_file)) {//Check file is already uploaded
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to upload image. Error occured during process. 1';
                }
                else{
                        //Add to database

                        /*$asset_images = new AssetImages;
                        $asset_images->asset_id = $id;
                        $asset_images->account_id = $account_id;
                        $asset_images->image_name = $file_name_to_store;
                        $asset_images->image_size = $file_size;
                        $asset_images->is_primary = 0;
                        $asset_images->recorded_by = $user_id;
                        $asset_images->date_recorded = Carbon::now('UTC')->toDateTimeString();
                        $asset_images->save();*/

                        $upload = new Uploads;
                        $upload->account_id = $account_id;
                        $upload->owner_id = $id;
                        $upload->owner = 'ams_img';
                        $upload->upload_filename = $file_name_to_store;
                        $upload->upload_extension = $file_extension;
                        $upload->upload_size = $file_size;
                        $upload->upload_use = 0;
                        $upload->date_uploaded = Carbon::now('UTC')->toDateTimeString();
                        $upload->uploaded_by = $user_id;
                        $upload->save();
                        
                        
                        if($upload){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Image successfully uploaded';
                            $response['name'] = $file_name_to_store;
                            $response['id'] = $upload->upload_id;

                            //Add Logs
                            $asset= Assets::find($id);
                            $desc = 'uploaded an asset photo to asset [Tag No: '.$asset->asset_tag.']';
                            Helpers::add_activity_logs(['ams-asset-uploaded-photo',$asset->asset_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to upload image. Error occured during process. 2';
                        }
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_msg'] = 'Record not found';
            }
        }
        return response()->json($response);
    }

    //Setting image to default cover/primary image photo
    public function image_cover(Request $request)//Delete images
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');
        $img_id = $request->get('id');

        //$check_record = AssetImages::where('id',$img_id)->where('account_id',$account_id)->where('asset_id',$asset_id)->first();
        $check_record = Uploads::where('upload_id', $img_id)->where('account_id',$account_id)->where('owner_id',$asset_id)->first();

        if(count($check_record)!=0){
            $name = $check_record->image_name;
            
            $image_to_be_cover = storage_path('app/public/' . $name);

            if (!File::exists($image_to_be_cover)) {
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                //$response['stat_msg'] = 'Image can not set as cover. It might. It might already be deleted. Error Code: ErAIC0001';
                $response['stat_msg'] = 'Image not found or already deleted. Error Code: ErAIC0001';
            }else{
                /*if(count(AssetImages::where('asset_id', $asset_id)->where('is_primary', '1')->get())!=0){
                    AssetImages::where('asset_id', $asset_id)->where('is_primary', '1')->update(['is_primary' => '0']);
                }
                
                $set_as_cover = AssetImages::where('id', $img_id)->update(['is_primary' => '1']);//Attemp to make image as cover*/
                if(count(Uploads::where('account_id',$account_id)->where('owner_id',$asset_id)->where('owner','ams_img')->get())!=0){
                    Uploads::where('account_id',$account_id)->where('owner_id',$asset_id)->where('owner','ams_img')->where('upload_use', '1')->update(['upload_use' => '0']);
                }

                $set_as_cover = Uploads::where('upload_id', $img_id)->update(['upload_use' => '1']);//Attemp to make image as cover

                if($set_as_cover){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Successfully set as cover';

                    $asset= Assets::find($asset_id);
                    if($asset){
                        $desc = 'set new image cover for asset [Tag No: '.$asset->asset_tag.']';
                        Helpers::add_activity_logs(['ams-asset-set-photo',$asset->asset_id,$desc]);
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to set as cover. Error occured during process. Error Code: ErAIC0002';
                }
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Image not found or already deleted. Error Code: ErAIC0003';
        }

        return $response;
    }

    //Delete Image Uploaded
    public function image_delete(Request $request)//Delete images
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');
        $img_id = $request->get('id');

        //$check_record = AssetImages::where('id',$img_id)->where('account_id',$account_id)->where('asset_id',$asset_id)->first();
        $check_record = Uploads::where('upload_id', $img_id)->where('account_id',$account_id)->where('owner_id',$asset_id)->first();

        if(count($check_record)!=0){
            $name = $check_record->upload_filename;
        
            $file_to_be_deleted = storage_path('app/public/' . $name);

            if (!File::exists($file_to_be_deleted)) {
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Image not found or already deleted. Error Code: E1ADIG0001x';
                $delete_record = Uploads::find($img_id)->delete();
            }else{
                $delete_file = File::delete($file_to_be_deleted);//Attemp to delete
                
                if (!File::exists($file_to_be_deleted)) {//Check if file is already deleted
                    //AssetImages::find($img_id)->delete();//Attemp to delete record
                    $delete_record = Uploads::find($img_id)->delete();

                    if(count(Uploads::find($img_id))){//Check if it is already deleted
                        $response['error'] = 'Unable to delete image. Error occured during process';
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to delete image. Error occured during process. Code: E1ADIG0002x';
                    }else{
                        $response['success'] = 'Image Successfully Deleted';
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Image Successfully Deleted';

                        $asset= Assets::find($asset_id);

                        //Add Logs
                        if($asset){
                            $desc = 'deleted a photo of asset [Tag No: '.$asset->asset_tag.']';
                            Helpers::add_activity_logs(['ams-asset-deleted-photo',$asset_id,$desc]);
                        }
                    }
                }else{
                    $response['error'] = 'Unable to delete image. Error occured during process';
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to delete image. Error occured during process. Code: E1ADIG0003x';
                }
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Image not found or already deleted. Error Code: E1ADIG0004x';
        }

        return $response;
    }


    /////////////////////
    // Asset Document //
    ///////////////////

    public function document_upload(Request $request)//Uplaod images
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $id = $request->get('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $check_record = Assets::where('account_id', $account_id)->where('asset_id', $id)->count();
        $allowed_filetype = ['doc','docx','xls','xlsx','ppt','pptx','txt','pdf','rar','zip'];
        $timezone = Options::apps('timezone');
        $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';

        $rules = [
            'file_desc' => 'required|min:4|max:255',
            'asset_file' => 'required|mimes:doc,docx,xls,xlsx,ppt,pptx,txt,pdf,rar,zip',//5MB Max
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation

        
        $response['allowed'] = $allowed_filetype;

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($check_record!=0){
                $generated_key = Helpers::generate_key($id);
                $get_file = $request->file('asset_file');
                $file_detail = $this->get_image_info('asset_file', $request);
                $file_name = $file_detail['name'];
                $file_extension = $file_detail['ext'];
                $file_size = $file_detail['size'];
                $generated_key = Helpers::generate_key();
                $file_name_to_store = $generated_key . '-' . time() . '.' . $file_extension;

                if(!in_array($file_extension, $allowed_filetype)){
                    $a[0] = 'The asset file must be a file of type: doc, docx, xls, xlsx, ppt, pptx, txt, pdf, rar, zip.';
                    $b['asset_file'] = $a;
                    $response['val_error']=$b;
                }else{

                    $upload_file = $get_file->storeAs('public/',$file_name_to_store);//Upload the file
                    $uploaded_file = storage_path('app/public/' . $file_name_to_store);//File uploaded path

                    if (!File::exists($uploaded_file)) {//Check file is already uploaded
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to upload document. Error occured during process. Error Code: ErAUPDCS001';
                    }
                    else{
                            $asset_document = new Uploads;
                            $asset_document->account_id = $account_id;
                            $asset_document->owner_id = $id;
                            $asset_document->owner = 'ams_docs';
                            $asset_document->upload_filename = $file_name_to_store;
                            $asset_document->upload_extension = $file_extension;
                            $asset_document->upload_desc = ucfirst($request->get('file_desc'));
                            $asset_document->upload_size = $file_size;
                            $asset_document->date_uploaded = Carbon::now('UTC')->toDateTimeString();
                            $asset_document->uploaded_by = $user_id;
                            $asset_document->save();
                            
                            $file_icon = Helpers::file_types($file_extension);
                            $document_id = $asset_document->upload_id;

                            $document_info = Uploads::with('users')->find($document_id);

                            if($asset_document){
                                $response['stat'] = 'success';
                                $response['stat_title'] = 'Success';
                                $response['stat_msg'] = 'File has been uploaded';

                                $response['name'] = $file_name_to_store;
                                $response['id'] = $document_info->upload_id;
                                $response['desc'] = $document_info->upload_desc;
                                $response['uploaded_date'] = date('d-M-Y h:iA', strtotime($document_info->date_upload .  $interval));
                                $response['uploaded_by'] = $document_info->users->user_fullname;
                                $response['ext'] = '<i class="'. $file_icon['icon'].' m-r-5" style="font-size:20px !important;"></i> '. $file_icon['name'];

                                //Add Logs
                                $asset = Assets::find($id);
                                $desc = 'uploaded an document to asset [Tag No: '.$asset->asset_tag.']';
                                Helpers::add_activity_logs(['ams-asset-uploaded-docs',$id,$desc]);
                            }else{
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Unable to upload document. Error occured during process. Error Code: ErAUPDCS002';
                            }
                    }
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to upload document. Invalid reference ID or record may already deleted.';
            }
        }

        return $response;
        //return response()->json($response);

    }

    public function document_delete(Request $request)//Uplaod images
    { 
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');
        $document_id = $request->get('id');

        //$check_record = AssetDocuments::where('id',$document_id)->where('account_id',$account_id)->where('asset_id',$asset_id)->first();

        $check_record = Uploads::where('upload_id', $document_id)->where('account_id',$account_id)->where('owner_id',$asset_id)->first();

        if(count($check_record)!=0){
            $name = $check_record->upload_filename;
        
            $file_to_be_deleted = storage_path('app/public/' . $name);

            if (!File::exists($file_to_be_deleted)) {
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Document not found. It might already be deleted. Error Code: ErADEDCS001';
                $delete_record = Uploads::find($document_id)->delete();
            }else{
                $delete_file = File::delete($file_to_be_deleted);//Attempt to delete file

                if (!File::exists($file_to_be_deleted)) {//Check if file is already deleted
                    $delete_record = Uploads::find($document_id)->delete();//Attemp to delete record

                    if(count(Uploads::find($document_id))){//Check if it is already deleted
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to delete document. Error occured during process. Error Code: ErADEDCS002';
                    }else{
                        $response['success'] = 'Document Successfully Deleted';
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Document has been successfully deleted';

                        //Add Logs
                        $asset= Assets::find($asset_id);
                        if($asset){
                            $desc = 'deleted a document of asset [Tag No: '.$asset->asset_tag.']';
                            Helpers::add_activity_logs(['ams-asset-deleted-docs',$asset_id,$desc]);
                        }
                    }
                }else{
                    $response['error'] = 'Unable to delete document. Error occured during process';
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to delete document. Error occured during process. Error Code: ErADEDCS003';
                }
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Document not found. It might already be deleted. Error Code: ErADEDCS004';
        }

        return $response;
    }

    ##############
    # Maintenace #
    ##############
    public function maintenance_fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $id = $request->id;

        $check_maintenace = AssetMaintenance::with(['suppliers'])->where('id',$id)->where('account_id',$account_id)->first();
        return $check_maintenace;
    }

    //maintenance_add
    public function maintenance_store(Request $request)//Uplaod images
    { 
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');

        $rules = [
            'maintenance_title' => 'required|min:4|max:200',
            'maintenance_type' => 'required',
            'service_provider'=>'required',
            'stat'=>'required',
            'start_date' => 'required|date_format:"Y-m-d"',
            'completion_date' => 'nullable|date_format:"Y-m-d"',
            'remarks' => 'max:500',
            'cost' => "nullable|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",
            //'asset_image' => 'mimes:jpeg,jpg,png,gif|max:1024',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];
        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $check_record = Assets::where('account_id',$account_id)->where('asset_id',$asset_id)->count();
            $timezone = Options::apps('timezone');
            $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
            
            if($check_record!=0){
                if($request->ref=='add'){
                    $new_maintenance = new AssetMaintenance;
                    $new_maintenance->account_id = $account_id;
                    $new_maintenance->asset_id = $asset_id;
                    $new_maintenance->supplier_id = $request->get('service_provider');
                    $new_maintenance->maintain_title = ucfirst($request->get('maintenance_title'));
                    $new_maintenance->maintain_type = $request->get('maintenance_type');
                    $new_maintenance->maintain_cost = Helpers::converts('currency_to_int', $request->input('cost'));
                    $new_maintenance->maintain_remarks = ucfirst($request->get('remarks'));
                    $new_maintenance->is_warranty = $request->get('is_warranty');
                    $new_maintenance->date_start = $request->get('start_date');
                    $new_maintenance->date_completion = $request->get('completion_date');
                    $new_maintenance->date_completed = $request->get('completed_date');
                    $new_maintenance->recorded_by = $user_id;
                    $new_maintenance->updated_by = $user_id;
                    $new_maintenance->maintain_stat = ($request->stat) ? $request->stat : 'active';
                    $new_maintenance->save();

                    if($new_maintenance){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Asset maintenance has been added';


                        $response['id'] = $new_maintenance->id;
                        $response['title'] = $new_maintenance->maintain_title;
                        $response['remarks'] = ($new_maintenance->maintain_remarks) ? $new_maintenance->maintain_remarks : '<center>---</center>';
                        $response['type'] = ucfirst($new_maintenance->maintain_type);
                        $response['cost'] = $new_maintenance->maintain_cost;
                        $response['start_date'] = date('d-M-Y', strtotime($new_maintenance->date_start . $interval));
                        $response['completion_date'] = ($new_maintenance->date_completion) ? date('d-M-Y', strtotime($new_maintenance->date_completion . $interval)) : '<center>---</center>';
                        $response['completed_date'] = ($new_maintenance->date_completed) ? date('d-M-Y', strtotime($new_maintenance->date_completed . $interval)) : '<center>---</center>';
                        $response['supplier'] = $new_maintenance->suppliers->supplier_name;
                        $response['recorded_date'] = date('d-M-Y', strtotime($new_maintenance->date_recorded));
                        $response['recorded_by'] = $new_maintenance->users->user_fullname;
                        $response['status'] = ucwords($new_maintenance->maintain_stat);
                        $response['warranty'] = ($new_maintenance->is_warranty==1) ? '<center><i class="fa fa-check-circle text-success"></i></center>' : "<center>---</center>";

                        //Add Logs
                        $asset= Assets::find($asset_id);
                        if($asset){
                            $desc = 'added a maintenance record of asset [Tag No: '.$asset->asset_tag.']';
                            Helpers::add_activity_logs(['ams-asset-added-maintenance',$asset_id,$desc]);
                        }
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add maintenance. Error occured during process. Error Code: ErAUPDCS001';
                    }
                }else{
                    $id = $request->id;

                    $check_maintenace = AssetMaintenance::where('id',$id)->where('account_id',$account_id)->count();

                    if($check_maintenace){
                        $update_maintenace = AssetMaintenance::find($id);
                        $update_maintenace->supplier_id = $request->get('service_provider');
                        $update_maintenace->maintain_title = ucfirst($request->get('maintenance_title'));
                        $update_maintenace->maintain_type = $request->get('maintenance_type');
                        $update_maintenace->maintain_cost = Helpers::converts('currency_to_int', $request->input('cost'));
                        $update_maintenace->maintain_remarks = ucfirst($request->get('remarks'));
                        $update_maintenace->is_warranty = $request->get('is_warranty');
                        $update_maintenace->date_start = $request->get('start_date');
                        $update_maintenace->date_completion = $request->get('completion_date');
                        $update_maintenace->date_completed = $request->get('completed_date');
                        $update_maintenace->updated_by = $user_id;
                        $update_maintenace->maintain_stat = ($request->stat) ? $request->stat : 'active';
                        $update_maintenace->save();

                        if($update_maintenace){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Asset maintenance has been successfully updated';
    
    
                            $response['id'] = $update_maintenace->id;
                            $response['title'] = $update_maintenace->maintain_title;
                            $response['remarks'] = ($update_maintenace->maintain_remarks) ? $update_maintenace->maintain_remarks : '<center>---</center>';
                            $response['type'] = ucfirst($update_maintenace->maintain_type);
                            $response['cost'] = $update_maintenace->maintain_cost;
                            $response['start_date'] = date('d-M-Y', strtotime($update_maintenace->date_start . $interval));
                            $response['completion_date'] = ($update_maintenace->date_completion) ? date('d-M-Y', strtotime($update_maintenace->date_completion . $interval)) : '<center>---</center>';
                            $response['completed_date'] = ($update_maintenace->date_completed) ? date('d-M-Y', strtotime($update_maintenace->date_completed . $interval)) : '<center>---</center>';
                            $response['supplier'] = $update_maintenace->suppliers->supplier_name;
                            $response['recorded_date'] = date('d-M-Y', strtotime($update_maintenace->date_recorded));
                            $response['recorded_by'] = $update_maintenace->users->user_fullname;
                            $response['status'] = ucwords($update_maintenace->maintain_stat);
                            $response['warranty'] = ($update_maintenace->is_warranty==1) ? '<center><i class="fa fa-check-circle text-success"></i></center>' : "<center>---</center>";
    
                            //Add Logs
                            $asset= Assets::find($asset_id);
                            if($asset){
                                $desc = 'updated a maintenance record of asset [Tag No: '.$asset->asset_tag.']';
                                Helpers::add_activity_logs(['ams-asset-updated-maintenance',$asset_id,$desc]);
                            }
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update maintenance. Error occured during process.';
                        }
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update maintenance. Error occured during process.';
                    }
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to add maintenance. Error occured during process. Error Code: ErANM0002';
            }
        }

        return $response;
    }

    public function maintenance_delete(Request $request)//Uplaod images
    { 
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->asset_id;
        $id = $request->id;
        $response = [];

        $check_record = AssetMaintenance::where('id',$id)->where('asset_id',$asset_id)->count();

        if(count($check_record)!=0){
            $delete = AssetMaintenance::find($id)->delete();
            
            if($delete){
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = 'Maintenance record has been successfully deleted';

                //Add Logs
                $asset= Assets::find($asset_id);
                if($asset){
                    $desc = 'deleted a maintenance record of asset [Tag No: '.$asset->asset_tag.']';
                    Helpers::add_activity_logs(['ams-asset-deleted-maintenance',$asset_id,$desc]);
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to delete maintenance record. Error occured during process.';
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to delete maintenance record. Invalid reference ID  or may already deleted';
        }

        return $response;
    }

    public function warranty_add(Request $request)
    { 
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');
        
        $response['stat'] = $asset_id;

        $rules = [
            'months'=>'required|max:99|numeric',
            'warranty_expire' => 'required|date_format:"d-M-Y"',
            //'remarks' => 'max:500',
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:500';
        }

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];
        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $check_record = Assets::where('account_id',$account_id)->where('asset_id',$asset_id)->count();
            
            if($check_record!=0){
                $new_warranty = new AssetWarranties;
                $new_warranty->account_id = $account_id;
                $new_warranty->asset_id = $asset_id;
                $new_warranty->months = $request->months;
                $new_warranty->warranty_expire = date('Y-m-d', strtotime($request->warranty_expire));
                $new_warranty->warranty_remarks = ucfirst($request->remarks);
                $new_warranty->date_recorded = Carbon::now('UTC')->toDateTimeString();
                $new_warranty->recorded_by = $user_id;
                $new_warranty->save();

                if($new_warranty){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Asset warranty successfully added';

                    $response['id'] = $new_warranty->id;
                    $response['months'] = $request->months;
                    $response['expire'] = $request->warranty_expire;
                    $response['recorded_by'] = $new_warranty->users->user_fullname;
                    $response['remarks'] = ($request->remarks) ? ucfirst($request->remarks) : "<center>---</center>";

                    $todate = new DateTime(date('Y-m-d'));
                    $expire = new DateTime(date('Y-m-d', strtotime($request->warranty_expire)));
                    $diff = $expire->diff($todate)->format("%a");
                    $response['remaining'] = ($diff<2) ? $diff . ' Day' : $diff . ' Days';

                    //Add Logs
                    $asset= Assets::find($asset_id);
                    if($asset){
                        $desc = 'added a warranty record of asset [Tag No: '.$asset->asset_tag.']';
                        Helpers::add_activity_logs(['ams-asset-added-warranty',$asset->asset_id,$desc]);
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to add warranty. Error occured during process. Error Code: ErAWTY0001';
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to add maintenance. Error occured during process. Error Code: ErAWTY0002';
            }
        }

        return $response;
    }

    public function warranty_delete(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $asset_id = $request->get('asset_id');
        $warranty_id = $request->get('id');

        $check_record = AssetWarranties::where('id',$warranty_id)->where('account_id',$account_id)->where('asset_id',$asset_id)->count();
        
        if($check_record!=0){
            $delete = AssetWarranties::find($warranty_id)->delete();//Attemp to delete record
            
            if($delete){//Check if it is already deleted
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = 'Asset warranty successfully deleted';

                //Add Logs
                $asset= Assets::find($asset_id);
                if($asset){
                    $desc = 'deleted a warranty record of asset [Tag No: '.$asset->asset_tag.']';
                    Helpers::add_activity_logs(['ams-asset-deleted-warranty',$asset->asset_id,$desc]);
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to delete warranty. Error occured during process. Error Code: ErDWTY0001';
            }
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to delete warranty. Error occured during process. Error Code: ErDWTY0002';
        }
        $response['a'] = $warranty_id;

        return $response;
    }


    public function fetch_custom_field(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        $ref = $request->ref;
        $id = $request->id;
        
        $link_id = $request->link_id;//Associate to Asset ID

        $custom_fields = CustomFields::whereHas('custom_field_links', function ($query) use ($id) {
            $query->where('ref_id', '=', $id);
        })->where('field_type', '=', $ref)->get();
        

        $append_fields = '';

        if(count($custom_fields)!=0){
            $append_fields = '<div class="col-md-12"><h4 class="card-title m-b-0">Custom Input Field(s)</h4><hr class="m-t-10 m-b-20" style="border-top: 2px solid rgba(0,0,0,.1) !important;"/></div>';
        }

        foreach($custom_fields as $custom_field){
            $field_id = $custom_field->field_id;

            if($custom_field->field_data_type=='date'){
                $custom_class = 'date-picker-default';
                $default_value = '';
            }else if($custom_field->field_data_type=='currency'){
                $custom_class = 'currency-input';
                $default_value = '';
            }else{
                $custom_class = '';
                $default_value = '';
            }

            if($custom_field->is_required==1){
                $require_helper='<i class="mdi mdi-check-circle form-required-helper text-danger"></i>';
            }else{
                $require_helper='';
            }

            if($ref=='asset'){
                $custom_value = AssetCustomValues::where('asset_id', $link_id)->where('field_id', $field_id)->first();
                $default_value = $custom_value['custom_value'];
            }

            $append_fields .= '<div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label text-bold">'.$custom_field->field_label.' '.$require_helper.'</label>
                                        <input type="hidden" name="custom_field_id[]" class="form-control" placeholder="" value="'.$custom_field->field_id.'">
                                        <input type="hidden" name="custom_field_required[]" class="form-control" placeholder="" value="'.$custom_field->is_required.'">
                                        <input type="hidden" name="custom_field_type[]" class="form-control" placeholder="" value="'.$custom_field->field_data_type.'">
                                        <input type="hidden" name="custom_field_label[]" class="form-control" placeholder="" value="'.$custom_field->field_label.'">
                                        <input type="text" name="custom_field_value[]" class="form-control '.$custom_class.'" placeholder="" name="asset_desc" value="'.$default_value.'">
                                    </div>
                                </div>';
        }

        if(count($custom_fields)!=0){
            //$append_fields .= '<div class="col-md-12"><hr class="m-t-5" style="border-top: 2px solid rgba(0,0,0,.1) !important;"/></div>';
        }

        return $append_fields;
    }

    function get_image_info($name='', Request $request){
        $file = $request->file($name);

        $image_filename_with_extension = $file->getClientOriginalName();
        $image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
        //$image_fileExt = $file->getClientOriginalExtension();
        $image_fileExt = pathinfo($image_filename_with_extension, PATHINFO_EXTENSION);
        $image_size = $file->getSize();
        //$file_name_to_store = $generated_key . '-' . time() . '.' . $image_fileExt;
        //$upload = $file->storeAs('public/ams_files/',$file_name_to_store);//Attempt to upload
        //$uploaded_file = storage_path('app/public/ams_files/' . $file_name_to_store);


        $b['name_with_ext'] = $image_filename_with_extension;
        $b['name'] = $image_fileName;
        $b['ext'] = $image_fileExt;
        $b['size'] = $image_size;
        $b['fullname'] = $image_fileName . '.' . $image_fileExt;
        $a = $b;

        return $a;
    }


    ##### Fetching Assets Data List ###########
    public function get_all_asset(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $ref = $request->ref;
        $keywords = $request->keywords;
        $account_id = Auth::user()->account_id;

        /*$query = "SELECT 
                        sys_asset.cat_id,
                        sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name,
                        sys_uploads.upload_filename as image_name
                    FROM sys_asset 
                    LEFT JOIN sys_uploads 
                        ON sys_uploads.owner_id = sys_asset.asset_id 
                        AND sys_uploads.owner = 'ams_img'
                        AND sys_uploads.upload_use = 1
                    LEFT JOIN sys_categories 
                        ON sys_categories.cat_id = sys_asset.cat_id
                    LEFT JOIN sys_asset_custom_value 
                        ON sys_asset_custom_value.asset_id = sys_asset.asset_id 
                    WHERE
                        sys_asset.account_id='$account_id'
                    AND sys_asset.record_stat<>'deleted'
                    AND (sys_asset.asset_status='available' OR sys_asset.asset_status='checked out' OR sys_asset.asset_status='leased')
                    AND (
                            sys_asset.asset_tag LIKE '%$keywords%' OR
                            sys_asset.asset_desc LIKE '%$keywords%' OR
                            sys_asset.asset_serial LIKE '%$keywords%' OR
                            sys_asset.asset_model LIKE '%$keywords%' OR
                            sys_asset.asset_brand LIKE '%$keywords%' OR
                            sys_asset.asset_vendor LIKE '%$keywords%' OR
                            sys_asset.purchased_cost LIKE '%$keywords%' OR
                            sys_asset.asset_remarks LIKE '%$keywords%' OR
                            sys_asset_custom_value.custom_value LIKE '%$keywords%'
                        ) GROUP BY sys_asset.asset_id";
        $asset_list =  DB::select($query);//check if asset tag or serial is existing*/

        //$ref = 'checkin';

        $asset_list = DB::table('sys_asset')
                            ->leftJoin('sys_uploads', function($join){
                                $join->on('sys_uploads.owner_id', '=', 'sys_asset.asset_id');
                                $join->where('sys_uploads.owner', 'ams_img');
                                $join->where('sys_uploads.upload_use',1);
                            })
                            ->leftJoin('sys_categories', function($join){
                                $join->on('sys_categories.cat_id', '=', 'sys_asset.cat_id');
                                $join->where('sys_categories.record_stat', 0);
                            })
                            ->leftJoin('sys_site', function($join){
                                $join->on('sys_site.site_id', '=', 'sys_asset.site_id');
                                $join->where('sys_site.record_stat', 0);
                            })
                            ->leftJoin('sys_site_location', function($join){
                                $join->on('sys_site_location.location_id', '=', 'sys_asset.location_id');
                                $join->where('sys_site_location.record_stat', 0);
                            })
                            ->leftJoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                            ->leftJoin('sys_employees', function($join){
                                $join->on('sys_employees.employee_id', '=', 'sys_asset.assign_to');
                                $join->where('sys_employees.record_stat', 0);
                            })
                            ->select( 'sys_asset.*',
                                        'sys_categories.cat_name as category',
                                        'sys_site.site_name as site',
                                        'sys_site_location.location_name as location',
                                        'sys_employees.emp_fullname as custodian',
                                        'sys_uploads.upload_filename as image_name'
                                    )
                            ->where('sys_asset.account_id', $account_id)
                            ->where('sys_asset.record_stat', 'active')
                            ->where(function ($query) use ($keywords) {
                                $query->where('sys_asset.asset_tag', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_desc', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_serial', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_model', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_brand', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_vendor', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.purchased_cost', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_remarks', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset_custom_value.custom_value', 'LIKE', '%'.$keywords.'%');
                            });
        if($ref=='checkin'){                    
            $asset_list = $asset_list->where(function ($query) use ($keywords) {
                                    $query->where('sys_asset.asset_status', 'checked out')
                                            ->orwhere('sys_asset.asset_status', 'leased');
                                });
        }
        else if($ref=='checkout'){  
            $asset_list = $asset_list->where('sys_asset.asset_status', 'available');
        }
                            
        $asset_list = $asset_list
                            ->take(100)
                            ->groupBy('sys_asset.asset_id')
                            ->get();

        //->take(20)

        $response['list'] = $asset_list;

        if(count($asset_list)==0){
            
            if($keywords){
                $response['stat_msg'] = '<div class="alert alert-warning text-center m-b-0">No asset record that matched keyword(s) <b>"'.$keywords.'"</b>.</div>';
            }else{
                $response['stat_msg'] = '<div class="alert alert-danger text-center m-b-0">No Found Asset</div>';
            }
        }
        
        //return $asset_list;
        return $response;
    }

    public function get_available_leasable_asset(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->get('ref');
        $lease_start = date('Y-m-d', strtotime($request->get('lease_start')));
        $lease_expire = date('Y-m-d', strtotime($request->get('lease_expire')));

        /*$query = "SELECT 
                        sys_asset.cat_id,
                        sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name,
                        sys_uploads.upload_filename as image_name
                    FROM sys_asset 
                    LEFT JOIN sys_uploads 
                        ON sys_uploads.owner_id = sys_asset.asset_id 
                        AND sys_uploads.owner = 'ams_img'
                        AND sys_uploads.upload_use = 1
                    LEFT JOIN sys_categories 
                        ON sys_categories.cat_id = sys_asset.cat_id
                    LEFT JOIN sys_asset_custom_value 
                        ON sys_asset_custom_value.asset_id = sys_asset.asset_id 
                    WHERE sys_asset.account_id='$account_id'
                    AND sys_asset.record_stat='active'
                    AND sys_asset.is_leasable=1
                    AND (
                            sys_asset.asset_tag LIKE '%$keywords%' OR
                            sys_asset.asset_desc LIKE '%$keywords%' OR
                            sys_asset.asset_serial LIKE '%$keywords%' OR
                            sys_asset.asset_model LIKE '%$keywords%' OR
                            sys_asset.asset_brand LIKE '%$keywords%' OR
                            sys_asset.asset_vendor LIKE '%$keywords%' OR
                            sys_asset.purchased_cost LIKE '%$keywords%' OR
                            sys_asset.asset_remarks LIKE '%$keywords%' OR
                            sys_asset_custom_value.custom_value LIKE '%$keywords%'
                        )
                    AND (
                        sys_asset.asset_id NOT IN (
                            SELECT sys_asset_lease_items.asset_id FROM sys_asset_lease_items
                            INNER JOIN sys_asset_lease_logs 
                            ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                            WHERE
                                (
                                    (sys_asset_lease_logs.lease_start <= '$lease_start' AND sys_asset_lease_logs.lease_expire >= '$lease_expire')
                                    OR
                                    (sys_asset_lease_logs.lease_start <= '$lease_expire' AND sys_asset_lease_logs.lease_expire >= '$lease_start')
                                    OR
                                    (sys_asset_lease_logs.lease_start >= '$lease_start' AND sys_asset_lease_logs.lease_expire <= '$lease_expire')
                                )
                                AND
                                sys_asset_lease_items.item_stat = 0 AND sys_asset_lease_logs.account_id='$account_id'
                        )
                    ) 
                    AND (
                        sys_asset.asset_id NOT IN (
                            SELECT sys_asset_lease_items.asset_id 
                                FROM sys_asset_lease_items
                            INNER JOIN sys_asset_lease_logs 
                                ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                            WHERE
                                sys_asset_lease_items.item_stat = 0 
                                AND sys_asset_lease_logs.account_id='$account_id'
                        )
                    )
                    GROUP BY sys_asset.asset_id";
        $asset_list =  DB::select($query);//check if asset tag or serial is existing   */

        $asset_list = DB::table('sys_asset')
                            ->leftJoin('sys_uploads', function($join){
                                $join->on('sys_uploads.owner_id', '=', 'sys_asset.asset_id');
                                $join->where('sys_uploads.owner', 'ams_img');
                                $join->where('sys_uploads.upload_use',1);
                            })
                            ->leftJoin('sys_categories', function($join){
                                $join->on('sys_categories.cat_id', '=', 'sys_asset.cat_id');
                                $join->where('sys_categories.record_stat', 0);
                            })
                            ->leftJoin('sys_site', function($join){
                                $join->on('sys_site.site_id', '=', 'sys_asset.site_id');
                                $join->where('sys_site.record_stat', 0);
                            })
                            ->leftJoin('sys_site_location', function($join){
                                $join->on('sys_site_location.location_id', '=', 'sys_asset.location_id');
                                $join->where('sys_site_location.record_stat', 0);
                            })
                            ->leftJoin('sys_employees', function($join){
                                $join->on('sys_employees.employee_id', '=', 'sys_asset.assign_to');
                                $join->where('sys_employees.record_stat', 0);
                            })
                            ->leftJoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                            ->select( 'sys_asset.*',
                                        'sys_categories.cat_name as category',
                                        'sys_site.site_name as site',
                                        'sys_site_location.location_name as location',
                                        'sys_employees.emp_fullname as custodian',
                                        'sys_uploads.upload_filename as image_name'
                                    )
                            ->where('sys_asset.account_id', $account_id)
                            ->where('sys_asset.record_stat', 'active')
                            ->where('sys_asset.is_leasable', 1)
                            ->where(function ($query) use ($keywords) {
                                $query->where('sys_asset.asset_tag', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_desc', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_serial', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_model', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_brand', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_vendor', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.purchased_cost', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_remarks', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset_custom_value.custom_value', 'LIKE', '%'.$keywords.'%');
                            })
                            ->whereNotIn('sys_asset.asset_id',function($query) use ($account_id, $lease_start, $lease_expire) {
                                $query->select('sys_asset_lease_items.asset_id')
                                    ->from('sys_asset_lease_items')
                                    ->Join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                                    ->where(function ($query) use ($lease_start, $lease_expire) {
                                        $query->where(function ($query) use ($lease_start, $lease_expire) {
                                            $query->where('sys_asset_lease_logs.lease_start', '<=', $lease_start);
                                            $query->where('sys_asset_lease_logs.lease_expire', '>=', $lease_expire);
                                        });
                                        $query->where(function ($query) use ($lease_start, $lease_expire) {
                                            $query->where('sys_asset_lease_logs.lease_start', '<=', $lease_expire);
                                            $query->where('sys_asset_lease_logs.lease_expire', '>=', $lease_start);
                                        });
                                        $query->where(function ($query) use ($lease_start, $lease_expire) {
                                            $query->where('sys_asset_lease_logs.lease_start', '>=', $lease_start);
                                            $query->where('sys_asset_lease_logs.lease_expire', '<=', $lease_expire);
                                        });
                                    })
                                    ->where('sys_asset_lease_items.item_stat', 0)
                                    ->where('sys_asset_lease_logs.account_id', $account_id);
                            })
                            ->whereNotIn('sys_asset.asset_id',function($query) use ($account_id) {
                                $query->select('sys_asset_lease_items.asset_id')
                                ->from('sys_asset_lease_items')
                                ->Join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                                ->where('sys_asset_lease_items.item_stat', 0)
                                ->where('sys_asset_lease_logs.account_id', $account_id);
                            })
                            ->take(100)
                            ->groupBy('sys_asset.asset_id')
                            ->get();

        $response['list'] = $asset_list;

        if(count($asset_list)==0){
            if($keywords){
                $response['stat_msg'] = '<div class="alert alert-warning text-center m-b-0">No available leasable asset that matched keyword(s) <b>"'.$keywords.'"</b>.</div>';
            }else{
                $response['stat_msg'] = '<div class="alert alert-warning text-center m-b-0">No Available Leasable Asset</div>';
            }
        }
        
        //return $asset_list;
        return $response;
    } 

    public function get_current_leased_asset(Request $request){
        $keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->get('ref');
        //$requestor = $request->get('requestor');

        /*$query = "SELECT sys_asset.cat_id,
                        sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name,
                        sys_uploads.upload_filename as image_name
                        FROM `sys_asset`
                        LEFT JOIN sys_uploads 
                            ON sys_uploads.owner_id = sys_asset.asset_id 
                            AND sys_uploads.owner = 'ams_img'
                            AND sys_uploads.upload_use = 1
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        LEFT JOIN sys_asset_custom_value 
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        INNER JOIN sys_asset_lease_items 
                            ON sys_asset_lease_items.asset_id = sys_asset.asset_id
                        INNER JOIN sys_asset_lease_logs 
                            ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                        WHERE 
                            sys_asset.account_id='".$account_id."' AND 
                            sys_asset.record_stat='active' AND 
                            sys_asset_lease_items.item_stat = 0 
                            AND 
                            (   sys_asset.asset_tag LIKE '%$keywords%' OR
                                sys_asset.asset_desc LIKE '%$keywords%' OR
                                sys_asset.asset_serial LIKE '%$keywords%' OR
                                sys_asset.asset_model LIKE '%$keywords%' OR
                                sys_asset.asset_brand LIKE '%$keywords%' OR
                                sys_asset.asset_vendor LIKE '%$keywords%' OR
                                sys_asset.purchased_cost LIKE '%$keywords%' OR
                                sys_asset.asset_remarks LIKE '%$keywords%' OR
                                sys_asset_custom_value.custom_value LIKE '%$keywords%'
                            )
                        GROUP BY sys_asset.asset_id";
        $asset_list =  DB::select($query);//check if asset tag or serial is existing    */

        $asset_list = DB::table('sys_asset')
                            ->leftJoin('sys_uploads', function($join){
                                $join->on('sys_uploads.owner_id', '=', 'sys_asset.asset_id');
                                $join->where('sys_uploads.owner', 'ams_img');
                                $join->where('sys_uploads.upload_use',1);
                            })
                            ->leftJoin('sys_categories', function($join){
                                $join->on('sys_categories.cat_id', '=', 'sys_asset.cat_id');
                                $join->where('sys_categories.record_stat', 0);
                            })
                            ->leftJoin('sys_site', function($join){
                                $join->on('sys_site.site_id', '=', 'sys_asset.site_id');
                                $join->where('sys_site.record_stat', 0);
                            })
                            ->leftJoin('sys_site_location', function($join){
                                $join->on('sys_site_location.location_id', '=', 'sys_asset.location_id');
                                $join->where('sys_site_location.record_stat', 0);
                            })
                            ->leftJoin('sys_employees', function($join){
                                $join->on('sys_employees.employee_id', '=', 'sys_asset.assign_to');
                                $join->where('sys_employees.record_stat', 0);
                            })
                            ->leftJoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                            ->join('sys_asset_lease_items', 'sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id')
                            ->join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                            ->select( 'sys_asset.*',
                                        'sys_categories.cat_name as category',
                                        'sys_site.site_name as site',
                                        'sys_site_location.location_name as location',
                                        'sys_employees.emp_fullname as custodian',
                                        'sys_uploads.upload_filename as image_name'
                                    )
                            ->where('sys_asset.account_id', $account_id)
                            ->where('sys_asset.record_stat', 'active')
                            ->where('sys_asset_lease_items.item_stat', 0)
                            ->where(function ($query) use ($keywords) {
                                $query->where('sys_asset.asset_tag', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_desc', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_serial', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_model', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_brand', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_vendor', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.purchased_cost', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset.asset_remarks', 'LIKE', '%'.$keywords.'%')
                                        ->orwhere('sys_asset_custom_value.custom_value', 'LIKE', '%'.$keywords.'%');
                            })
                            ->take(100)
                            ->groupBy('sys_asset.asset_id')
                            ->get();

        $response['list'] = $asset_list;

        if(count($asset_list)==0){
            if($keywords){
                $response['stat_msg'] = '<div class="alert alert-warning text-center m-b-0">No asset leased that matched keyword(s) <b>"'.$keywords.'"</b>.</div>';
            }else{
                $response['stat_msg'] = '<div class="alert alert-info m-b-0 text-center">No Aasset Leased Record.</div>';
            }
        }
        
        //return $asset_list;
        return $response;
    }


    public function get_leased_asset_by_requestor(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $id = $request->get('id');
        //$id =1;

        /*$query = "SELECT 
                        sys_asset.cat_id,
                        sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name,
                        sys_uploads.upload_filename as image_name
                    FROM `sys_asset_lease_items` 
                    INNER JOIN sys_asset ON sys_asset.asset_id = sys_asset_lease_items.asset_id 
                    INNER JOIN sys_asset_lease_logs ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                    LEFT JOIN sys_uploads 
                            ON sys_uploads.owner_id = sys_asset.asset_id 
                            AND sys_uploads.owner = 'ams_img'
                            AND sys_uploads.upload_use = 1
                    LEFT JOIN sys_categories ON sys_categories.cat_id = sys_asset.cat_id
                    WHERE sys_asset_lease_logs.requestor_id = '$id' 
                        AND sys_asset_lease_logs.account_id = '$account_id' 
                        AND sys_asset_lease_items.item_stat = 0
                    GROUP BY sys_asset.asset_id";

        $asset_list =  DB::select($query);//check if asset tag or serial is existing*/
        $asset_list = DB::table('sys_asset_lease_items')
                                            ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_lease_items.asset_id')
                                            ->join('sys_asset_lease_logs', 'sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                                            ->leftJoin('sys_uploads', function($join){
                                                $join->on('sys_uploads.owner_id', '=', 'sys_asset.asset_id');
                                                $join->where('sys_uploads.owner', 'ams_img');
                                                $join->where('sys_uploads.upload_use',1);
                                            })
                                            ->leftJoin('sys_employees', function($join){
                                                $join->on('sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id');
                                                $join->where('sys_employees.record_stat', 0);
                                            })
                                            ->leftJoin('sys_categories', function($join){
                                                $join->on('sys_categories.cat_id', '=', 'sys_asset.cat_id');
                                                $join->where('sys_categories.record_stat', 0);
                                            })
                                            ->leftJoin('sys_site', function($join){
                                                $join->on('sys_site.site_id', '=', 'sys_asset.site_id');
                                                $join->where('sys_site.record_stat', 0);
                                            })
                                            ->leftJoin('sys_site_location', function($join){
                                                $join->on('sys_site_location.location_id', '=', 'sys_asset.location_id');
                                                $join->where('sys_site_location.record_stat', 0);
                                            })
                                            ->select(
                                                'sys_asset.*',
                                                'sys_categories.cat_name as category',
                                                'sys_site.site_name as site',
                                                'sys_site_location.location_name as location',
                                                'sys_employees.emp_fullname as custodian',
                                                'sys_uploads.upload_filename as image_name'
                                            )
                                            ->where('sys_asset_lease_logs.requestor_id', $id)
                                            ->where('sys_asset_lease_logs.account_id', $account_id)
                                            ->where('sys_asset_lease_items.item_stat', 0)
                                            ->take(20)
                                            ->groupBy('sys_asset.asset_id')
                                            ->get();
        
        $response['list'] = $asset_list;

        $employee_record = Employees::where('account_id', $account_id)->where('employee_id', $id)->first();

        $staff_name = ($employee_record) ? $employee_record->emp_fullname : '';

        if(count($asset_list)==0){
            $response['stat_msg'] = '<div class="alert alert-info text-center m-b-0">No leased record for <b>'.$staff_name. '</b></div>';
        }
                
        return $response;
    }


    public function fetch_all_list(){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;

        $query = "SELECT 
						sys_uploads.upload_filename as image_name,
						sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_asset.asset_brand,
                        sys_asset.asset_status,
                        sys_asset.asset_vendor,
                        sys_asset.asset_remarks,
                        sys_asset.is_leasable,
                        sys_categories.cat_name,
                        sys_site.site_id,
                        sys_site.site_name,
                        sys_site.record_stat as site_deleted,
                        sys_site_location.location_id,
                        sys_site_location.location_name,
                        sys_site_location.record_stat as location_deleted,
                        inout_items.inout_type,
                        leased_items.lease_id is_leased,
                        sys_employees.emp_fullname
                        FROM `sys_asset` 
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        LEFT JOIN sys_site
                        	ON sys_site.site_id = sys_asset.site_id
                        LEFT JOIN sys_site_location
                        	ON sys_site_location.location_id = sys_asset.location_id
                            image_name
                        LEFT JOIN sys_uploads 
                            ON sys_uploads.owner_id = sys_asset.asset_id 
                            AND sys_uploads.owner = 'ams_img'
                            AND sys_uploads.upload_use = 1
                        LEFT JOIN sys_employees 
                        	ON sys_employees.employee_id = sys_asset.assign_to
                        LEFT JOIN 
                            (
                                SELECT 
                                	sys_asset_inout_items.item_id,
                                	sys_asset_inout_items.asset_id,
                                    sys_asset_inout_logs.inout_type,
                                    MAX(sys_asset_inout_logs.inout_date),
                                    MAX(sys_asset_inout_items.inout_id)
                                FROM sys_asset_inout_items 
                                INNER JOIN sys_asset_inout_logs
                                	ON sys_asset_inout_logs.inout_id = sys_asset_inout_items.inout_id
                                GROUP BY sys_asset_inout_items.asset_id
                            ) as inout_items 
                            ON (inout_items.asset_id = sys_asset.asset_id)
                        LEFT JOIN 
                            (
                                SELECT 
                                    max(sys_asset_lease_items.item_id) item_id, 
                                    sys_asset_lease_items.asset_id, 
                                    sys_asset_lease_logs.lease_id
                                FROM sys_asset_lease_items
                                INNER JOIN sys_asset_lease_logs
                                	ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                                WHERE sys_asset_lease_items.item_stat = 0
                                GROUP BY  sys_asset_lease_items.asset_id
                            ) as leased_items 
                            ON (leased_items.asset_id = sys_asset.asset_id)
                        WHERE sys_asset.record_stat = 'active' AND sys_asset.account_id='".$account_id."'";
        $asset_list =  DB::select($query);


        return $asset_list;
    }
}
