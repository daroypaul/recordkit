<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\settings\models\CustomFields;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class Checkin extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_asset')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        $data['custom_field_list'] = $custom_fields;
        $data['form_title'] = 'Asset Check-In Reports';

        return view('assettracking::checkin')->with($data);
    }

    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        $account_id = Auth::user()->account_id;
        $response = [];

        //Primary Filter
        $start_date = (isset($request->start_date)) ? date('Y-m-d', strtotime($request->start_date)) : '';
        $end_date = (isset($request->end_date)) ? date('Y-m-d', strtotime($request->end_date)) : '';
        $category = (isset($request->category)) ? $request->category : '';
        $site = (isset($request->site)) ? $request->site : '';
        $location = (isset($request->location)) ? $request->location : '';
        $employee = (isset($request->employee)) ? $request->employee : '';

        $brand = (isset($request->brand)) ? $request->brand : '';
        $cost = (isset($request->cost)) ? $request->cost : '';
        $date_purchased = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $desc = (isset($request->desc)) ? $request->desc : '';
        $model = (isset($request->model)) ? $request->model : '';
        $serial = (isset($request->serial)) ? $request->serial : '';
        $status = (isset($request->status)) ? $request->status : '';
        $tag = (isset($request->tag)) ? $request->tag : '';
        $vendor = (isset($request->vendor)) ? $request->vendor : '';
        $status = (isset($request->status)) ? $request->status : '';
        $is_leasable = (isset($request->is_leasable)) ? $request->is_leasable : '';
        $custom_data_fields = (isset($request->custom_fields)) ? $request->custom_fields : '';
        $inout_remarks = (isset($request->inout_remarks)) ? $request->inout_remarks : '';

        $asset_list = DB::table('sys_asset_inout_logs')
                        ->Join('sys_asset_inout_items', 'sys_asset_inout_items.inout_id', '=', 'sys_asset_inout_logs.inout_id')
                        ->Join('sys_asset', function($join){
                            $join->on('sys_asset.asset_id', '=', 'sys_asset_inout_items.asset_id');
                            //->On('sys_asset.record_stat', '=', 'active');
                        })
                        ->leftjoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_inout_logs.requestor_id')
                        ->leftjoin('sys_site', 'sys_site.site_id', '=', 'sys_asset_inout_logs.location_id')
                        ->leftjoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset_inout_logs.location_id')
                        ->leftjoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                        ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                        ->leftjoin('sys_users', 'sys_users.user_id', '=', 'sys_asset_inout_logs.recorded_by')
                        ->select('sys_asset.*','sys_site.site_name','sys_categories.cat_name','sys_site_location.location_name','sys_employees.emp_fullname','sys_users.user_fullname','sys_asset_inout_logs.inout_date','sys_asset_inout_items.item_remarks')
                        ->where('sys_asset_inout_logs.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->where(function ($query) {
                            $query->where('sys_asset_inout_logs.inout_type', 'checkin')
                                        ->orWhere('sys_asset_inout_logs.inout_type', 'return leased');
                        })
                        ->whereBetween('sys_asset_inout_logs.inout_date', [$start_date, $end_date]);

            $asset_custom_values = DB::table('sys_asset_inout_logs')
                        ->Join('sys_asset_inout_items', 'sys_asset_inout_items.inout_id', '=', 'sys_asset_inout_logs.inout_id')
                        ->Join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_inout_items.asset_id')
                        ->Join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                        ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id')
                        ->where('sys_asset_inout_logs.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->where(function ($query) {
                            $query->where('sys_asset_inout_logs.inout_type', 'checkin')
                                        ->orWhere('sys_asset_inout_logs.inout_type', 'return leased');
                        })
                        ->whereBetween('sys_asset_inout_logs.inout_date', [$start_date, $end_date]);

            $custom_fields = DB::table('sys_custom_fields')
                        ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                        ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                        ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                        ->where('sys_asset.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->groupBy('sys_custom_fields.field_id')
                        ->orderBy('sys_custom_fields.field_label', 'asc')
                        ->get();

        if($category){
            $asset_list = $asset_list->where('sys_asset.cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }
        if($site){
            $asset_list = $asset_list->where('sys_asset.site_id', $site);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site);
        }
        if($location){
            $asset_list = $asset_list->where('sys_asset.location_id', $location);
            $asset_custom_values = $asset_custom_values->where('sys_asset.location_id', $location);
        }
        if($employee){
            $asset_list = $asset_list->where('sys_asset_inout_logs.requestor_id', $employee);
            $asset_custom_values = $asset_custom_values->where('sys_asset_inout_logs.requestor_id', $employee);
        }

        if($tag){
            $asset_list = $asset_list->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
        }
        if($desc){
            $asset_list = $asset_list->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
        }
        if($model){
            $asset_list = $asset_list->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
        }
        if($serial){
            $asset_list = $asset_list->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
        }
        if($brand){
            $asset_list = $asset_list->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
        }
        if($cost){
            $cost = Helpers::converts('currency_to_int', $cost);
            $asset_list = $asset_list->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
        }
        if($date_purchased){
            $date_purchased = date('Y-m-d', strtotime($date_purchased));
            $asset_list = $asset_list->whereDate('sys_asset.date_purchased', $date_purchased);
            $asset_custom_values = $asset_custom_values->whereDate('sys_asset.date_purchased', $date_purchased);
        }
        if($vendor){
            $asset_list = $asset_list->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
        }
        if($inout_remarks){
            $asset_list = $asset_list->where('sys_asset_inout_items.item_remarks', 'LIKE', '%'.$inout_remarks.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_inout_items.item_remarks', 'LIKE', '%'.$inout_remarks.'%');
        }
        if($custom_data_fields){
            $asset_list = $asset_list->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
        }

        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();
    
        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        return $response;
    }
}
