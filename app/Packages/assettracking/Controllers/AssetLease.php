<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\Categories;
use App\packages\assettracking\models\AssetInOutLogs;
use App\packages\assettracking\models\AssetInOutItems;
use App\packages\assettracking\models\AssetLeaseLogs;
use App\packages\assettracking\models\AssetLeaseItems;
use App\packages\assettracking\models\AssetEvents;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\adminprofile\models\Employees;//Get the last number used

use Validator;
use Session;
use Helpers;#Custom Helper Class
use Mail;
use Carbon\Carbon;

class AssetLease extends Controller
{

    /*public function fetch_leased_asset(Request $request){
        $keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->get('ref');
        //$requestor = $request->get('requestor');

        $query = "SELECT sys_asset.cat_id,
                        sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name,
                        sys_uploads.upload_filename as image_name
                        FROM `sys_asset`
                        LEFT JOIN sys_uploads 
                            ON sys_uploads.owner_id = sys_asset.asset_id 
                            AND sys_uploads.owner = 'ams_img'
                            AND sys_uploads.upload_use = 1
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        LEFT JOIN sys_asset_custom_value 
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        INNER JOIN sys_asset_lease_items 
                            ON sys_asset_lease_items.asset_id = sys_asset.asset_id
                        INNER JOIN sys_asset_lease_logs 
                            ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                        WHERE 
                            sys_asset.account_id='".$account_id."' AND 
                            sys_asset.record_stat='active' AND 
                            sys_asset_lease_items.item_stat = 0 
                            AND 
                            (   sys_asset.asset_tag LIKE '%$keywords%' OR
                                sys_asset.asset_desc LIKE '%$keywords%' OR
                                sys_asset.asset_serial LIKE '%$keywords%' OR
                                sys_asset.asset_model LIKE '%$keywords%' OR
                                sys_asset.asset_brand LIKE '%$keywords%' OR
                                sys_asset.asset_vendor LIKE '%$keywords%' OR
                                sys_asset.purchased_cost LIKE '%$keywords%' OR
                                sys_asset.asset_remarks LIKE '%$keywords%' OR
                                sys_asset_custom_value.custom_value LIKE '%$keywords%'
                            )
                        GROUP BY sys_asset.asset_id";
        $asset_list =  DB::select($query);//check if asset tag or serial is existing    

        $response['list'] = $asset_list;

        if(count($asset_list)==0){
            $response['stat_msg'] = '<div class="alert alert-info m-b-0 text-center">No found data</div>';
        }
        
        //return $asset_list;
        return $response;
    }*/

    public function lease($asset_id=''){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_lease')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$account_id = Auth::user()->account_id;
        $asset = [];
        $error_msg = ''; 

        if($asset_id){
            //$asset = Assets::with(['categories'])->where('account_id', $account_id)->where('asset_id', $asset_id)->where('record_stat', 0)->first();

            $query = "SELECT sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name
                        FROM `sys_asset` 
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        WHERE sys_asset.is_leasable = 1  
                        	AND sys_asset.account_id = '$account_id'
                            AND sys_asset.asset_id = '$asset_id'
                        AND sys_asset.asset_id
                        	NOT IN (
                            	SELECT sys_asset_lease_items.asset_id FROM `sys_asset_lease_items` 
                                INNER JOIN sys_asset_lease_logs 
                                    ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                                INNER JOIN sys_employees
                                    ON sys_employees.employee_id = sys_asset_lease_logs.requestor_id
                                    AND  sys_employees.record_stat <> 2
                                WHERE sys_asset_lease_items.item_stat = 0
                            )";
            $asset =  DB::select($query);
            
            if(count($asset)==0){
                $error_msg = '<div class="alert alert-danger m-b-20">No found asset. Unable to load on the item list.</div>';
            }else{
                $error_msg = '';
            }
        }

        //$employees = Employees::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('emp_last_name', 'asc')->get();
        //$categories = Categories::where('account_id', '=', $account_id)->orderBy('cat_name', 'asc')->get();
        //$sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
                "preload_asset"=>$asset,
                "error_msg"=>$error_msg,
                //"categories"=>$categories,
                //"sites"=>$sites,
                //"requestors"=>$employees,
                //"customers"=>$employees,
                "form_title"=> "Asset Lease",
                "type"=> "lease",
                "customer_label"=>"Receive By",
                "status"=> ['available','leased','checked in','checked out','damage','lost','disposed','sold'],
        ];
        return view('assettracking::asset_lease_form')->with($data);
    }

    public function lease_store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'lease_start' => 'required|date_format:"M d, Y"',
            'lease_expire' => 'required|date_format:"M d, Y"',
            'requestor' => 'required',
            'customer' => 'required',
            'site' => 'required',
            'location' => 'required',
            'remarks' => 'max:500',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $val_msg = $validator->errors();
        
        //Check site and Location if not deleted
        $check_site = Site::where('account_id', $account_id)->where('site_id', $request->site)->where('record_stat',0)->count();
        $check_location = Location::where('account_id', $account_id)->where('location_id', $request->location)->where('record_stat',0)->count();

        if($check_site==0){
            $val_msg->add('site', 'Site already deleted');
            $val_msg->add('location', 'Location already deleted');
        }else{
            if($check_location==0){
                $val_msg->add('location', 'Location already deleted');
                $a = 'location error';
            }
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_lease')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()||count($val_msg)!=0) {
            $response['val_errors']= $val_msg;
        }else if( count(array_filter($request->get('input_asset_id')))==0 ){
            $response['stat']= 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg']= 'Add at least one (1) asset';
        }else{
            $new_lease = new AssetLeaseLogs;
            $new_lease->account_id = $account_id;
            $new_lease->site_id = $request->get('site');
            $new_lease->location_id = $request->get('location');
            $new_lease->requestor_id = $request->get('requestor');
            $new_lease->lease_start = date('Y-m-d', strtotime($request->get('lease_start')));
            $new_lease->lease_expire = date('Y-m-d', strtotime($request->get('lease_expire')));
            $new_lease->lease_remarks = $request->get('remarks');
            $new_lease->lease_stat = 0;
            $new_lease->recorded_by = $user_id;
            $new_lease->updated_by = $user_id;
            $new_lease->record_stat = 'active';
            $new_lease->save();
            $new_lease_id = $new_lease->lease_id;

            //Create In/Out Logs
            $new_inout = new AssetInOutLogs;
            $new_inout->account_id = $account_id;
            $new_inout->site_id = $request->get('site');
            $new_inout->location_id = $request->get('location');
            $new_inout->requestor_id = $request->get('requestor');
            $new_inout->customer_id = $request->get('customer');
            $new_inout->inout_type = 'leased checkout';
            $new_inout->inout_date = date('Y-m-d', strtotime($request->get('lease_start')));
            $new_inout->inout_remarks = $request->get('remarks');
            $new_inout->recorded_by = $user_id;
            $new_inout->updated_by = $user_id;
            $new_inout->record_stat = 'active';
            $new_inout->save();
            $new_inout_id = $new_inout->inout_id;

            if($new_lease){
                $rec = 1;

                $i = 0;
                foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                    $assets = Assets::find($asset_id);
                    $site = $assets->site_id;
                    $cat = $assets->cat_id;
                    $tag = $assets->asset_tag;
                    $desc = $assets->asset_desc;
                    $model = $assets->asset_model;
                    $serial = $assets->asset_serial;
                    $remarks = $request->get('input_asset_remarks')[$i];
    
                    if(count($assets)!=0){
                        $new_lease_items = new AssetLeaseItems;
                        $new_lease_items->lease_id = $new_lease_id;
                        $new_lease_items->account_id = $account_id;
                        $new_lease_items->site_id = $site;
                        $new_lease_items->asset_id = $asset_id;
                        $new_lease_items->cat_id = $cat;
                        $new_lease_items->asset_tag = $tag;
                        $new_lease_items->asset_desc = $desc;
                        $new_lease_items->asset_model = $model;
                        $new_lease_items->asset_serial = $serial;
                        $new_lease_items->release_remarks = $remarks;
                        $new_lease_items->item_stat = 0;
                        $new_lease_items->save();

                        //Add In/Out Item
                        $new_inout_items = new AssetInOutItems;
                        $new_inout_items->inout_id = $new_inout_id;
                        $new_inout_items->site_id = $site;
                        $new_inout_items->asset_id = $asset_id;
                        $new_inout_items->cat_id = $cat;
                        $new_inout_items->asset_tag = $tag;
                        $new_inout_items->asset_desc = $desc;
                        $new_inout_items->asset_model = $model;
                        $new_inout_items->asset_serial = $serial;
                        $new_inout_items->item_remarks = $remarks;
                        $new_inout_items->save();

                        //Add Asset Events
                        $new_events = new AssetEvents;
                        $new_events->account_id = $account_id;
                        $new_events->asset_id = $asset_id;
                        $new_events->event_type = 'leased';
                        $new_events->event_date = date('Y-m-d', strtotime($request->get('lease_start')));
                        $new_events->event_remarks = $remarks;
                        $new_events->ref_id = $new_inout_id;
                        $new_events->prev_site_id = $assets->site_id;
                        $new_events->prev_location_id = $assets->location_id;
                        $new_events->new_site_id = $request->get('site');
                        $new_events->new_location_id = $request->get('location');
                        $new_events->requestor_id = $request->get('requestor');
                        $new_events->recorded_by = $user_id;
                        $new_events->record_stat = 0;
                        $new_events->date_recorded = Carbon::now('UTC')->toDateTimeString();
                        $new_events->save();

                        if($new_lease_items){
                            $rec++;
                        }
                    }
    
                    $i++;
                }
            }

            if($rec>1){
                $response['stat']= 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg']= 'Asset(s) successfully leased';

                //Update the status of per asset
                foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                    //Update the asset
                    $update_asset = Assets::find($asset_id);
                    $update_asset->asset_status = 'leased';
                    $update_asset->site_id = $request->get('site');
                    $update_asset->location_id = $request->get('location');
                    $update_asset->assign_to = $request->get('requestor');
                    $update_asset->updated_by = $user_id;
                    $update_asset->save();
                }

                $desc = 'added an leased asset record';
                Helpers::add_activity_logs(['ams-asset-lease',$new_lease_id,$desc]);


                #-------------------------------
                        #   Send Email Notification
                        #-------------------------------
                        $request_id =$request->get('requestor');
                        $customer_id =$request->get('customer');
                        $cc = [];

                        $requestor_detail = Employees::where('account_id', $account_id)->where('employee_id', $request_id)->first();

                        if($requestor_detail->emp_email){
                            if($request_id==$customer_id){
                                $customer_name = $requestor_detail->emp_fullname;
                                $customer_email = $requestor_detail->emp_email;
                                
                            }else{
                                $customer_detail = Employees::where('account_id', $account_id)->where('employee_id', $customer_id)->first();
                                $customer_name = $customer_detail->emp_fullname;

                                if($customer_detail->emp_email!=$requestor_detail->emp_email){
                                    $cc[] = $customer_detail->emp_email;
                                }
                            }
                            
                            if(Auth::user()->user_email){
                                $cc[] = Auth::user()->user_email;
                            }

                            $subject = 'Leasing Asset';
                            $receiver = $customer_name;
                            $hanover = Auth::user()->user_fullname;

                            $msg = '
                                <p>You have leased an asset. See details below.</p>
                                <table class="details">
                                    <tr>
                                        <th>Reference No</th>
                                        <td>:</td>
                                        <td>'.$new_inout_id.'</td>
                                    </tr>
                                    <tr>
                                        <th>Lease Start</th>
                                        <td>:</td>
                                        <td>'.date('d-M-Y', strtotime($request->get('lease_start'))).'</td>
                                    </tr>
                                    <tr>
                                        <th>Lease Expire</th>
                                        <td>:</td>
                                        <td>'.date('d-M-Y', strtotime($request->get('lease_expire'))).'</td>
                                    </tr>
                                    <tr>
                                        <th>Received By</th>
                                        <td>:</td>
                                        <td>'.$receiver.'</td>
                                    </tr>
                                    <tr>
                                        <th>Handed-Over By</th>
                                        <td>:</td>
                                        <td>'.$hanover . '</td>
                                    </tr>
                                </table>

                                <p>Leased Asset List:</p>
                                <table class="bordered">
                                    <tr>
                                        <th>Asset Description</th>
                                        <th>Model</th>
                                        <th>Serial</th>
                                        <th>Remarks</th>
                                    </tr>';
                            $i=0;
                            foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                                $asset = Assets::find($asset_id);
                                $serial = ($asset->asset_serial) ? $asset->asset_serial : '---';
                                $remarks = ($request->get('input_asset_remarks')[$i]) ? $request->get('input_asset_remarks')[$i] : '---';

                                $msg .=     ' <tr>
                                                <td>'.$asset->asset_desc.'</td>
                                                <td>'.$asset->asset_model.'</td>
                                                <td>'.$serial.'</td>
                                                <td>'.$remarks.'</td>
                                            </tr>';
                                $i++;
                            }  

                            $msg .= '</table>';

                            $email_data['email'] = $requestor_detail->emp_email;
                            $email_data['subject'] = $subject;
                            $email_data['bodymessage'] = $msg;
                            $email_data['requestor'] = $requestor_detail->emp_fullname;
                            $email_data['cc'] = $cc;

                            $response['a'] = $email_data['cc'];

                            Mail::send('mail.email', $email_data, function($send_email) use ($email_data){
                                $send_email->from('notification@recuda-apps.com', 'ICT Asset Tracking System');
                                $send_email->to($email_data['email']);
                                if(count($email_data['cc'])!=0){
                                    $send_email->cc($email_data['cc']);
                                }
                                $send_email->subject($email_data['subject']);
                            });
                        }
                        # End of Email Notification
            }else{
                $response['stat']= 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg']= 'Unable to leased asset(s). Error occured during process.';
            }
        }

        return response()->json($response);
    }


    public function return_leased($asset_id=''){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_lease')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        //$account_id = Auth::user()->account_id;
        $asset = [];
        $error_msg = ''; 

        if($asset_id){
            $query = "SELECT sys_asset.asset_id,
                        sys_asset.asset_tag,
                        sys_asset.asset_desc,
                        sys_asset.asset_serial,
                        sys_asset.asset_model,
                        sys_categories.cat_name
                        FROM `sys_asset` 
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        WHERE sys_asset.is_leasable = 1  
                        	AND sys_asset.account_id = '$account_id'
                            AND sys_asset.asset_id = '$asset_id'
                        AND sys_asset.asset_id
                        	IN (
                            	SELECT sys_asset_lease_items.asset_id FROM `sys_asset_lease_items` 
                                INNER JOIN sys_asset_lease_logs 
                                    ON sys_asset_lease_logs.lease_id = sys_asset_lease_items.lease_id
                                INNER JOIN sys_employees
                                    ON sys_employees.employee_id = sys_asset_lease_logs.requestor_id
                                    AND  sys_employees.record_stat <> 2
                                WHERE sys_asset_lease_items.item_stat = 0
                            )";
            $asset =  DB::select($query);
            
            if(count($asset)==0){
                $error_msg = '<div class="alert alert-danger m-b-20">No found asset record. Unable to load on the item list.</div>';
            }else{
                $error_msg = '';
            }
        }

        //$employees = Employees::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('emp_last_name', 'asc')->get();
        //$categories = Categories::where('account_id', '=', $account_id)->orderBy('cat_name', 'asc')->get();
        //$sites = Site::where('account_id', '=', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();

        $data = [
                "preload_asset"=>$asset,
                "error_msg"=>$error_msg,
                //"categories"=>$categories,
                //"sites"=>$sites,
                //"requestors"=>$employees,
                //"customers"=>$employees,
                "form_title"=> "Return Leased Asset",
                "type"=> "return_lease",
                "customer_label"=>"Receive By",
        ];
       
        return view('assettracking::asset_return_lease_form')->with($data);
    }

    public function return_leased_store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'return_date' => 'required|date_format:"M d, Y"',
            'requestor' => 'required',
            'customer' => 'required',
            'site' => 'required',
            'location' => 'required',
            'remarks' => 'max:500',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation

        if(!Helpers::get_user_privilege('pkg_fixasset_lease')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()) {
            $response['val_errors']= $validator->errors();
        }else if( count(array_filter($request->get('input_asset_id')))==0 ){
            $response['stat']= 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg']= 'Add at least one (1) asset';
        }else{
            //Create In/Out Logs
            $new_inout = new AssetInOutLogs;
            $new_inout->account_id = $account_id;
            $new_inout->site_id = $request->get('site');
            $new_inout->location_id = $request->get('location');
            $new_inout->requestor_id = $request->get('requestor');
            $new_inout->customer_id = $request->get('customer');
            $new_inout->inout_type = 'return leased';
            $new_inout->inout_date = date('Y-m-d', strtotime($request->get('return_date')));
            $new_inout->inout_remarks = $request->get('remarks');
            $new_inout->recorded_by = $user_id;
            $new_inout->updated_by = $user_id;
            $new_inout->record_stat = 'active';
            $new_inout->save();
            $new_inout_id = $new_inout->inout_id;


            if($new_inout){
                $rec = 1;

                $i = 0;
                foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                    $assets = Assets::with(['categories','site','image'])->find($asset_id);
                    $site = $assets->site_id;
                    $cat = $assets->cat_id;
                    $tag = $assets->asset_tag;
                    $desc = $assets->asset_desc;
                    $model = $assets->asset_model;
                    $serial = $assets->asset_serial;
                    $remarks = $request->get('input_asset_remarks')[$i];
    
                    if(count($assets)!=0){
                        $update_leased_item = AssetLeaseItems::where('asset_id', $asset_id)
                                                ->where('item_stat', 0)
                                                ->update(['return_remarks' => $remarks, 'item_stat' => 1]);

                        if($update_leased_item){
                            //Add In/Out Item
                            $new_inout_items = new AssetInOutItems;
                            $new_inout_items->inout_id = $new_inout_id;
                            $new_inout_items->site_id = $site;
                            $new_inout_items->asset_id = $asset_id;
                            $new_inout_items->cat_id = $cat;
                            $new_inout_items->asset_tag = $tag;
                            $new_inout_items->asset_desc = $desc;
                            $new_inout_items->asset_model = $model;
                            $new_inout_items->asset_serial = $serial;
                            $new_inout_items->item_remarks = $remarks;
                            $new_inout_items->save();

                            //
                            $update_asset = Assets::find($asset_id);
                            $update_asset->asset_status = 'available';
                            $update_asset->location_id = $request->get('location');
                            $update_asset->site_id = $request->get('site');
                            $update_asset->assign_to = '';
                            $update_asset->updated_by = $user_id;
                            $update_asset->save();

                            //Add Asset Events
                            $new_events = new AssetEvents;
                            $new_events->account_id = $account_id;
                            $new_events->asset_id = $asset_id;
                            $new_events->event_type = 'return-leased';
                            $new_events->event_date = date('Y-m-d', strtotime($request->get('return_date')));
                            $new_events->event_remarks = $remarks;
                            $new_events->ref_id = $new_inout_id;
                            $new_events->prev_site_id = $assets->site_id;
                            $new_events->prev_location_id = $assets->location_id;
                            $new_events->new_site_id = $request->get('site');
                            $new_events->new_location_id = $request->get('location');
                            $new_events->requestor_id = $request->get('requestor');
                            $new_events->recorded_by = $user_id;
                            $new_events->record_stat = 0;
                            $new_events->date_recorded = Carbon::now('UTC')->toDateTimeString();
                            $new_events->save();

                            if($new_inout_items){
                                $rec++;
                            }
                        }
                    }
    
                    $i++;
                }
            }
            
            if($rec>1){
                $response['stat']= 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg']= 'Leased Asset(s) successfully returned';

                $desc = 'added an return leased asset record';
                Helpers::add_activity_logs(['ams-asset-lease-return',$new_inout_id,$desc]);

                #-------------------------------
                #   Send Email Notification
                #-------------------------------
                $request_id =$request->get('requestor');
                $customer_id =$request->get('customer');
                $cc = [];

                $requestor_detail = Employees::where('account_id', $account_id)->where('employee_id', $request_id)->first();

                if($requestor_detail->emp_email){
                    if($request_id==$customer_id){
                        $customer_name = $requestor_detail->emp_fullname;
                        $customer_email = $requestor_detail->emp_email;
                        
                    }else{
                        $customer_detail = Employees::where('account_id', $account_id)->where('employee_id', $customer_id)->first();
                        $customer_name = $customer_detail->emp_fullname;

                        if($customer_detail->emp_email!=$requestor_detail->emp_email){
                            $cc[] = $customer_detail->emp_email;
                        }
                    }
                    
                    if(Auth::user()->user_email){
                        $cc[] = Auth::user()->user_email;
                    }

                    $subject = 'Leased Asset Return';
                    $receiver = Auth::user()->user_fullname;
                    $hanover = $customer_name;

                    $msg = '
                        <p>You have returned a leased asset. See details below.</p>
                        <table class="details">
                            <tr>
                                <th>Reference No</th>
                                <td>:</td>
                                <td>'.$new_inout_id.'</td>
                            </tr>
                            <tr>
                                <th>Date Returned</th>
                                <td>:</td>
                                <td>'.date('d-M-Y', strtotime($request->get('return_date'))).'</td>
                            </tr>
                            <tr>
                                <th>Received By</th>
                                <td>:</td>
                                <td>'.$receiver.'</td>
                            </tr>
                            <tr>
                                <th>Handed-Over By</th>
                                <td>:</td>
                                <td>'.$hanover . '</td>
                            </tr>
                        </table>

                        <p>Leased Asset List:</p>
                        <table class="bordered">
                            <tr>
                                <th>Asset Description</th>
                                <th>Model</th>
                                <th>Serial</th>
                                <th>Remarks</th>
                            </tr>';
                    $i=0;
                    foreach(array_filter($request->get('input_asset_id')) as $asset_id){
                        $asset = Assets::find($asset_id);
                        $serial = ($asset->asset_serial) ? $asset->asset_serial : '---';
                        $remarks = ($request->get('input_asset_remarks')[$i]) ? $request->get('input_asset_remarks')[$i] : '---';
                        $msg .=     ' <tr>
                                        <td>'.$asset->asset_desc.'</td>
                                        <td>'.$asset->asset_model.'</td>
                                        <td>'.$serial.'</td>
                                        <td>'.$remarks.'</td>
                                    </tr>';
                        $i++;
                    }  

                    $msg .= '</table>';

                    $email_data['email'] = $requestor_detail->emp_email;
                    $email_data['subject'] = $subject;
                    $email_data['bodymessage'] = $msg;
                    $email_data['requestor'] = $requestor_detail->emp_fullname;
                    $email_data['cc'] = $cc;

                    $response['a'] = $email_data;

                    Mail::send('mail.email', $email_data, function($send_email) use ($email_data){
                        $send_email->from('notification@recuda-apps.com', 'ICT Asset Tracking System');
                        $send_email->to($email_data['email']);
                        if(count($email_data['cc'])!=0){
                            $send_email->cc($email_data['cc']);
                        }
                        $send_email->subject($email_data['subject']);
                    });
                }
                # End of Email Notification
            }else{
                $response['stat']= 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg']= 'Unable to returned asset(s). Error occured during process.';
            }
        }

        return response()->json($response);
    }

}
