<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\packages\assettracking\models\Assets;//Get all assets record
use App\modules\settings\models\IdsOption;//Get the Ides Option
use App\modules\settings\models\IdsCounter;//Get the last number used
use Illuminate\Support\Facades\Auth;
use Options;

class AssetTagGenerator
{
    //

    public function asset_tag(){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        $account_id = Auth::user()->account_id;
        $total_asset = Assets::where('account_id', $account_id)->count() + 10;
        $new_counter = 0;
        $last_counter = Options::where('account_id', $account_id)->where('option_name','tag_last_counter')->first()->option_value;

        for($i=1;$i<=$total_asset;$i++){
            $counter = $last_counter++;

            $new_generated_id = $this->generate_code($account_id, 'asset_tag',$counter);

            $check_existing = Assets::where('account_id', $account_id)->where('asset_tag', $new_generated_id)->count();

            if($check_existing==0){
                break;
            }
        }

        $response['new_id'] = $new_generated_id;
        $response['new_counter'] = $counter;

        return $response;
    }

    function generate_code($account_id, $type, $last_number){
        $with_padding = Options::where('account_id', $account_id)->where('option_name','tag_with_padding')->first()->option_value;
        $padding_digit = Options::where('account_id', $account_id)->where('option_name','tag_padding_digits')->first()->option_value;

        $counter = ($with_padding) ? str_pad($last_number,  $padding_digit, "0", STR_PAD_LEFT) : $last_number;

        $prefix = Options::where('account_id', $account_id)->where('option_name','tag_prefix')->first()->option_value;
        $suffix = Options::where('account_id', $account_id)->where('option_name','tag_suffix')->first()->option_value;
        $with_dashed = Options::where('account_id', $account_id)->where('option_name','tag_with_dashed')->first()->option_value;

        $dashed_before = ($with_dashed && $prefix) ? '-':'';
        $dashed_after = ($with_dashed && $suffix) ? '-':'';

        $year_strlen = Options::where('account_id', $account_id)->where('option_name','tag_year_strlen')->first()->option_value;
        $cur_year = ($year_strlen==2) ? date('y') : date('Y');
        $with_year = Options::where('account_id', $account_id)->where('option_name','tag_with_year')->first()->option_value;
        $insert_year = Options::where('account_id', $account_id)->where('option_name','tag_insert_year')->first()->option_value;
        $asset_tag = '';

        if($with_year){
            if($insert_year=='BEF_PREF'){
                $asset_tag = $cur_year . $prefix . $dashed_before . $counter . $dashed_after . $suffix;
            }else if($insert_year=='AFT_PREF'){
                $asset_tag = $prefix . $cur_year . $dashed_before . $counter . $dashed_after . $suffix;
            }else if($insert_year=='BEF_SUF'){
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $cur_year. $suffix;
            }else if($insert_year=='AFT_SUF'){
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix . $cur_year;
            }else{
                $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix;
            }
        }else{
            $asset_tag = $prefix . $dashed_before . $counter . $dashed_after . $suffix;
        }

        return $asset_tag;
    }
}