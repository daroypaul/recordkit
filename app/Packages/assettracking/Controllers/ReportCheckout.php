<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Packages\Assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetCustomValues;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;
use App\modules\settings\models\CustomFields;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class ReportCheckout extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_asset')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        $data['custom_field_list'] = $custom_fields;
        $data['form_title'] = 'Asset Check-Out Reports';

        
        $asset_list = DB::table('sys_asset_inout_logs')
        ->Join('sys_asset_inout_items', 'sys_asset_inout_items.inout_id', '=', 'sys_asset_inout_logs.inout_id')
        ->Join('sys_asset', function($join){
            $join->on('sys_asset.asset_id', '=', 'sys_asset_inout_items.asset_id');
            //->On('sys_asset.record_stat', '=', 'active');
        })
        ->leftjoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_inout_logs.requestor_id')
        ->leftjoin('sys_site', 'sys_site.site_id', '=', 'sys_asset_inout_logs.location_id')
        ->leftjoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset_inout_logs.location_id')
        ->leftjoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
        ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
        ->leftjoin('sys_users', 'sys_users.user_id', '=', 'sys_asset_inout_logs.recorded_by')
        ->select('sys_asset.*','sys_site.site_name','sys_categories.cat_name','sys_site_location.location_name','sys_employees.emp_fullname','sys_users.user_fullname','sys_asset_inout_logs.inout_date','sys_asset_inout_items.item_remarks')
        ->where('sys_asset_inout_logs.account_id', $account_id)
        ->where('sys_asset.record_stat', 'active')
        ->where(function ($query) {
            $query->where('sys_asset_inout_logs.inout_type', 'checkout')
                        ->orWhere('sys_asset_inout_logs.inout_type', 'leased checkout');
        })->get();

        //return $asset_list;

        return view('assettracking::report_checkout_new')->with($data);
    }

    public function generate_report_new(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }


        
        $account_id = Auth::user()->account_id;
        $response = [];

        //Primary Filter
        $start_date = (isset($request->start_date)) ? date('Y-m-d', strtotime($request->start_date)) : '';
        $end_date = (isset($request->end_date)) ? date('Y-m-d', strtotime($request->end_date)) : '';
        $category = (isset($request->category)) ? $request->category : '';
        $site = (isset($request->site)) ? $request->site : '';
        $location = (isset($request->location)) ? $request->location : '';
        $employee = (isset($request->employee)) ? $request->employee : '';

        $brand = (isset($request->brand)) ? $request->brand : '';
        $cost = (isset($request->cost)) ? $request->cost : '';
        $date_purchased = (isset($request->date_purchased)) ? $request->date_purchased : '';
        $desc = (isset($request->desc)) ? $request->desc : '';
        $model = (isset($request->model)) ? $request->model : '';
        $serial = (isset($request->serial)) ? $request->serial : '';
        $status = (isset($request->status)) ? $request->status : '';
        $tag = (isset($request->tag)) ? $request->tag : '';
        $vendor = (isset($request->vendor)) ? $request->vendor : '';
        $status = (isset($request->status)) ? $request->status : '';
        $is_leasable = (isset($request->is_leasable)) ? $request->is_leasable : '';
        $custom_data_fields = (isset($request->custom_fields)) ? $request->custom_fields : '';
        $inout_remarks = (isset($request->inout_remarks)) ? $request->inout_remarks : '';

        $asset_list = DB::table('sys_asset_inout_logs')
                        ->Join('sys_asset_inout_items', 'sys_asset_inout_items.inout_id', '=', 'sys_asset_inout_logs.inout_id')
                        ->Join('sys_asset', function($join){
                            $join->on('sys_asset.asset_id', '=', 'sys_asset_inout_items.asset_id');
                            //->On('sys_asset.record_stat', '=', 'active');
                        })
                        ->leftjoin('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_inout_logs.requestor_id')
                        ->leftjoin('sys_site', 'sys_site.site_id', '=', 'sys_asset_inout_logs.location_id')
                        ->leftjoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset_inout_logs.location_id')
                        ->leftjoin('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                        ->leftjoin('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                        ->leftjoin('sys_users', 'sys_users.user_id', '=', 'sys_asset_inout_logs.recorded_by')
                        ->select('sys_asset.*','sys_site.site_name','sys_categories.cat_name','sys_site_location.location_name','sys_employees.emp_fullname','sys_users.user_fullname','sys_asset_inout_logs.inout_date','sys_asset_inout_items.item_remarks')
                        ->where('sys_asset_inout_logs.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->where(function ($query) {
                            $query->where('sys_asset_inout_logs.inout_type', 'checkout')
                                        ->orWhere('sys_asset_inout_logs.inout_type', 'leased checkout');
                        })
                        ->whereBetween('sys_asset_inout_logs.inout_date', [$start_date, $end_date]);

            $asset_custom_values = DB::table('sys_asset_inout_logs')
                        ->Join('sys_asset_inout_items', 'sys_asset_inout_items.inout_id', '=', 'sys_asset_inout_logs.inout_id')
                        ->Join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_inout_items.asset_id')
                        ->Join('sys_asset_custom_value', 'sys_asset_custom_value.asset_id', '=', 'sys_asset.asset_id')
                        ->select('sys_asset_custom_value.*', 'sys_asset.account_id', 'sys_asset.asset_id')
                        ->where('sys_asset_inout_logs.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->where(function ($query) {
                            $query->where('sys_asset_inout_logs.inout_type', 'checkout')
                                        ->orWhere('sys_asset_inout_logs.inout_type', 'leased checkout');
                        })
                        ->whereBetween('sys_asset_inout_logs.inout_date', [$start_date, $end_date]);

            $custom_fields = DB::table('sys_custom_fields')
                        ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                        ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                        ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                        ->where('sys_asset.account_id', $account_id)
                        ->where('sys_asset.record_stat', 'active')
                        ->groupBy('sys_custom_fields.field_id')
                        ->orderBy('sys_custom_fields.field_label', 'asc')
                        ->get();

        if($category){
            $asset_list = $asset_list->where('sys_asset.cat_id', $category);
            $asset_custom_values = $asset_custom_values->where('sys_asset.cat_id', $category);
        }
        if($site){
            $asset_list = $asset_list->where('sys_asset.site_id', $site);
            $asset_custom_values = $asset_custom_values->where('sys_asset.site_id', $site);
        }
        if($location){
            $asset_list = $asset_list->where('sys_asset.location_id', $location);
            $asset_custom_values = $asset_custom_values->where('sys_asset.location_id', $location);
        }
        if($employee){
            $asset_list = $asset_list->where('sys_asset_inout_logs.requestor_id', $employee);
            $asset_custom_values = $asset_custom_values->where('sys_asset_inout_logs.requestor_id', $employee);
        }

        if($tag){
            $asset_list = $asset_list->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_tag', 'LIKE', '%'.$tag.'%');
        }
        if($desc){
            $asset_list = $asset_list->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_desc', 'LIKE', '%'.$desc.'%');
        }
        if($model){
            $asset_list = $asset_list->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_model', 'LIKE', '%'.$model.'%');
        }
        if($serial){
            $asset_list = $asset_list->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_serial', 'LIKE', '%'.$serial.'%');
        }
        if($brand){
            $asset_list = $asset_list->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_brand', 'LIKE', '%'.$brand.'%');
        }
        if($cost){
            $cost = Helpers::converts('currency_to_int', $cost);
            $asset_list = $asset_list->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset.purchased_cost', 'LIKE', '%'.$cost.'%');
        }
        if($date_purchased){
            $date_purchased = date('Y-m-d', strtotime($date_purchased));
            $asset_list = $asset_list->whereDate('sys_asset.date_purchased', $date_purchased);
            $asset_custom_values = $asset_custom_values->whereDate('sys_asset.date_purchased', $date_purchased);
        }
        if($vendor){
            $asset_list = $asset_list->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
            $asset_custom_values = $asset_custom_values->where('sys_asset.asset_vendor', 'LIKE', '%'. $vendor);
        }
        if($inout_remarks){
            $asset_list = $asset_list->where('sys_asset_inout_items.item_remarks', 'LIKE', '%'.$inout_remarks.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_inout_items.item_remarks', 'LIKE', '%'.$inout_remarks.'%');
        }
        if($custom_data_fields){
            $asset_list = $asset_list->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
            $asset_custom_values = $asset_custom_values->where('sys_asset_custom_value.custom_value', 'LIKE', '%'.$custom_data_fields.'%');
        }

        $asset_list = $asset_list->get();
        $asset_custom_values = $asset_custom_values->get();
    
        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        return $response;
    }


    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;

        $query_asset = "SELECT sys_asset.*, 
                sys_site.site_name, 
                sys_site_location.location_name, 
                sys_categories.cat_name, 
                sys_employees.emp_fullname, 
                sys_asset_inout_logs.inout_date, 
                sys_users.user_fullname,
                sys_asset_inout_items.item_remarks
                FROM `sys_asset_inout_logs`
                INNER JOIN sys_asset_inout_items 
                    ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                INNER JOIN sys_asset 
                    ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                    AND sys_asset.record_stat = 'active'
                INNER JOIN sys_employees 
                    ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                INNER JOIN sys_site 
                    ON sys_site.site_id = sys_asset_inout_logs.site_id
                INNER JOIN sys_site_location 
                    ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                LEFT JOIN sys_categories 
                    ON sys_categories.cat_id = sys_asset.cat_id
                INNER JOIN sys_users 
                    ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                WHERE 
                    (sys_asset_inout_logs.inout_type = 'checkout' 
                    OR sys_asset_inout_logs.inout_type = 'leased checkout')
                    AND sys_asset_inout_logs.account_id = '".$account_id."'";

        $query_custom_value = "SELECT sys_asset_custom_value.*
                        FROM `sys_asset_inout_logs`
                        INNER JOIN sys_asset_inout_items 
                            ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                        INNER JOIN sys_asset 
                            ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                            AND sys_asset.record_stat = 'active'
                        INNER JOIN sys_employees 
                            ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                        INNER JOIN sys_site 
                            ON sys_site.site_id = sys_asset_inout_logs.site_id
                        INNER JOIN sys_site_location 
                            ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        INNER JOIN sys_users 
                            ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                        INNER JOIN sys_asset_custom_value
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        WHERE 
                            (sys_asset_inout_logs.inout_type = 'checkout' 
                            OR sys_asset_inout_logs.inout_type = 'leased checkout')
                            AND sys_asset_inout_logs.account_id = '".$account_id."'";
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_asset.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($request->type=='bydate'){
            $start = date('Y-m-d', strtotime($request->date_start));
            $end  = date('Y-m-d', strtotime($request->date_end));

            $query_asset .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
            $query_custom_value .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
        }else if($request->type=='bysitelocation'){
            $location_id = $request->id;

            $location_data = Location::where('account_id', $account_id)->where('location_id', $location_id)->where('record_stat',0)->first();

            $site_id = $location_data->site_id;

            $query_asset .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
            $query_custom_value .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
        }else if($request->type=='byrequestor'){
            $employee_id = $request->id;

            $query_asset .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
            $query_custom_value .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
        }

        $asset_list =  DB::select($query_asset);
        $asset_custom_values =  DB::select($query_custom_value);
        
        $response['asset_list'] = $asset_list;
        $response['asset_custom_values'] = $asset_custom_values;
        $response['custom_fields'] = $custom_fields;

        return $response;
    }
    
    public function index_report($type){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_fixasset_report_checkout')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $model_list = '';
        $category_list = '';
        $employee_list = '';
        $data = [];
        $form_title = '';
                        
        $start = date('Y-m-01');
        $end  = date('Y-m-t');

        $query_asset = "SELECT sys_asset.*, 
                sys_site.site_name, 
                sys_site_location.location_name, 
                sys_categories.cat_name, 
                sys_employees.emp_fullname, 
                sys_asset_inout_logs.inout_date, 
                sys_users.user_fullname,
                sys_asset_inout_items.item_remarks
                FROM `sys_asset_inout_logs`
                INNER JOIN sys_asset_inout_items 
                    ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                INNER JOIN sys_asset 
                    ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                    AND sys_asset.record_stat = 'active'
                INNER JOIN sys_employees 
                    ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                INNER JOIN sys_site 
                    ON sys_site.site_id = sys_asset_inout_logs.site_id
                INNER JOIN sys_site_location 
                    ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                LEFT JOIN sys_categories 
                    ON sys_categories.cat_id = sys_asset.cat_id
                INNER JOIN sys_users 
                    ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                WHERE 
                    (sys_asset_inout_logs.inout_type = 'checkout' 
                    OR sys_asset_inout_logs.inout_type = 'leased checkout')
                    AND sys_asset_inout_logs.account_id = '".$account_id."'";

        $query_custom_value = "SELECT sys_asset_custom_value.*
                        FROM `sys_asset_inout_logs`
                        INNER JOIN sys_asset_inout_items 
                            ON sys_asset_inout_items.inout_id = sys_asset_inout_logs.inout_id
                        INNER JOIN sys_asset 
                            ON sys_asset.asset_id = sys_asset_inout_items.asset_id 
                            AND sys_asset.record_stat = 'active'
                        INNER JOIN sys_employees 
                            ON sys_employees.employee_id = sys_asset_inout_logs.requestor_id
                        LEFT JOIN sys_site 
                            ON sys_site.site_id = sys_asset_inout_logs.site_id
                        LEFT JOIN sys_site_location 
                            ON sys_site_location.location_id = sys_asset_inout_logs.location_id
                        LEFT JOIN sys_categories 
                            ON sys_categories.cat_id = sys_asset.cat_id
                        INNER JOIN sys_users 
                            ON sys_users.user_id = sys_asset_inout_logs.recorded_by
                        INNER JOIN sys_asset_custom_value
                            ON sys_asset_custom_value.asset_id = sys_asset.asset_id
                        WHERE 
                            (sys_asset_inout_logs.inout_type = 'checkout' 
                            OR sys_asset_inout_logs.inout_type = 'leased checkout')
                            AND sys_asset_inout_logs.account_id = '".$account_id."'";
        
        $custom_fields = DB::table('sys_custom_fields')
                                    ->join('sys_asset_custom_value', 'sys_asset_custom_value.field_id', '=', 'sys_custom_fields.field_id')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_custom_value.asset_id')
                                    ->select('sys_custom_fields.field_id', 'sys_custom_fields.field_label','sys_custom_fields.field_data_type')
                                    ->where('sys_custom_fields.account_id', $account_id)
                                    ->where('sys_asset.record_stat', 'active')
                                    ->groupBy('sys_custom_fields.field_id')
                                    ->orderBy('sys_custom_fields.field_label', 'asc')
                                    ->get();

        if($type=='bydate'){
            $query_asset .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
            $query_custom_value .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";

            $data['form_title'] = 'Check-Out Reports: By Date';
            $data['type'] = 'bydate';
        }else if($type=='bysitelocation'){
            $site_location_list = DB::table('sys_site_location')
                                            ->join('sys_site', 'sys_site.site_id', '=', 'sys_site_location.site_id')
                                            ->select('sys_site_location.location_id', 'sys_site_location.location_name', 'sys_site.site_name','sys_site.site_id')
                                            ->where('sys_site_location.account_id', $account_id)
                                            ->where('sys_site_location.record_stat', 0)
                                            ->orderBy('sys_site.site_name', 'asc')
                                            ->orderBy('sys_site_location.location_name', 'asc')
                                            ->get();
            if(count($site_location_list)){                     
                $current_site_location = DB::table('sys_site_location')
                                                ->join('sys_site', 'sys_site.site_id', '=', 'sys_site_location.site_id')
                                                ->select('sys_site_location.location_id', 'sys_site_location.location_name', 'sys_site.site_name','sys_site.site_id')
                                                ->where('sys_site_location.account_id', $account_id)
                                                ->where('sys_site_location.record_stat', 0)
                                                ->orderBy('sys_site.site_name', 'asc')
                                                ->orderBy('sys_site_location.location_name', 'asc')
                                                ->limit(1)
                                                ->first();
            
                $site_id = $current_site_location->site_id;
                $location_id = $current_site_location->location_id;

                $query_asset .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
                $query_custom_value .= " AND sys_asset_inout_logs.site_id='".$site_id."' AND sys_asset_inout_logs.location_id='".$location_id."'";
                //$query_custom_value .= " AND sys_asset_inout_logs.inout_date BETWEEN '".$start."' AND '".$end."'";
            }

            $data['form_title'] = 'Check-Out Reports: By Site/Location';
            $data['type'] = 'bysitelocation';
            $data['site_location_list'] = $site_location_list;
        }else if($type=='byrequestor'){
            $employee_list = Employees::where('account_id', $account_id)
                                        ->where('record_stat', 0)
                                        ->orderBy('emp_fullname', 'asc')->get();
            
            if(count($employee_list)){
                $employee_id = Employees::where('account_id', $account_id)
                                            ->where('record_stat', 0)
                                            ->orderBy('emp_fullname', 'asc')->limit(1)->first()->employee_id;

                $query_asset .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
                $query_custom_value .= " AND sys_asset_inout_logs.requestor_id='".$employee_id."'";
            }

            $data['form_title'] = 'Check-Out Reports: By Requestor';
            $data['type'] = 'byrequestor';
            $data['employee_list'] = $employee_list;
        }
        
        $asset_list =  DB::select($query_asset);

        //$query_custom_value .= " GROUP BY sys_asset_custom_value.field_id";
        $asset_custom_values =  DB::select($query_custom_value);
        

        $data['asset_list'] = $asset_list;
        $data['custom_field_list'] = $custom_fields;
        $data['asset_custom_value_list'] = $asset_custom_values;

        //return $asset_custom_values;
        //return $asset_custom_values;
        //return $custom_fields;
        return view('assettracking::report_checkout')->with($data);
    }
}
