<?php

namespace App\Packages\assettracking\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Utilities\Migrations as Migrations;
use App\modules\adminprofile\models\Categories;
use Artisan;
use Menus;
use Packages;
use Options;

class Installer
{
    public static function menu_data($type='all'){
        if($type=='all'){
            $menus = [
                [
                    "level"=> 'main',
                    "action"=> 'add',
                    "account_id"=> 1,
                    "parent_menu"=> 0,
                    "menu_name"=> "fix-asset-main",
                    "menu_label"=> "Asset",
                    "menu_url"=> "#",
                    "menu_icon"=> "mdi mdi-barcode",
                    "privilege_key"=> '["all","pkg_fixasset_view","pkg_fixasset_inout","pkg_fixasset_lease"]',
                    "menu_order"=> null,
                    "is_disable"=> 0,
                    "childs" => [
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-list",
                            "menu_label"=> "Asset List",
                            "menu_url"=> "#",
                            "menu_icon"=> "ti-list m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_view"]',
                            "menu_order"=> null,
                            "is_disable"=> 0,
                            "childs" => [
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 3,
                                    "menu_name"=> "fix-asset-list-all",
                                    "menu_label"=> "All Asset List",
                                    "menu_url"=> "asset/list/all",
                                    "menu_icon"=> "ti-control-record m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_view"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 3,
                                    "menu_name"=> "fix-asset-list-leasable",
                                    "menu_label"=> "Leasable List",
                                    "menu_url"=> "asset/list/leasable",
                                    "menu_icon"=> "ti-control-record m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_view"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                            ]
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-add",
                            "menu_label"=> "Add New Asset",
                            "menu_url"=> "asset/add",
                            "menu_icon"=> "mdi mdi-plus-circle m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_add"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-checkin",
                            "menu_label"=> "Check-In",
                            "menu_url"=> "asset/checkin",
                            "menu_icon"=> "mdi mdi-login m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_inout"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-checkout",
                            "menu_label"=> "Check-Out",
                            "menu_url"=> "asset/checkout",
                            "menu_icon"=> "mdi mdi-logout m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_inout"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-lease",
                            "menu_label"=> "Leasing",
                            "menu_url"=> "asset/lease",
                            "menu_icon"=> "mdi mdi-calendar-clock m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_lease"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-lease-return",
                            "menu_label"=> "Return Leased",
                            "menu_url"=> "asset/return-lease",
                            "menu_icon"=> "mdi mdi-calendar-check m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_lease"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 2,
                            "menu_name"=> "fix-asset-transfer",
                            "menu_label"=> "Bulk Transfer",
                            "menu_url"=> "asset/transfer",
                            "menu_icon"=> "mdi mdi-directions-fork m-r-10",
                            "privilege_key"=> '["exempt"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                    ]
                ],
                [
                    "level"=> 'main',
                    "action"=> 'find-parent-key',
                    "menu_name"=> "reports-main",
                    "childs" => [
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 16,
                            "menu_name"=> "reports-asset",
                            "menu_label"=> "Asset",
                            "menu_url"=> "reports/asset",
                            "menu_icon"=> "fa fa-barcode m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_report_asset"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 16,
                            "menu_name"=> "reports-checkout",
                            "menu_label"=> "Checked-Out Asset",
                            "menu_url"=> "reports/checkout",
                            "menu_icon"=> "mdi mdi-login m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_report_checkout"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 16,
                            "menu_name"=> "reports-checkin",
                            "menu_label"=> "Checked-In Asset",
                            "menu_url"=> "reports/checkin",
                            "menu_icon"=> "mdi mdi-logout m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_report_checkout"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 16,
                            "menu_name"=> "reports-asset-leasable",
                            "menu_label"=> "Leased Asset",
                            "menu_url"=> "#",
                            "menu_icon"=> "mdi mdi-calendar-clock m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_report_lease"]',
                            "menu_order"=> null,
                            "is_disable"=> 0,
                            "childs" => [
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 20,
                                    "menu_name"=> "reports-asset-leasable-all",
                                    "menu_label"=> "All Leasable",
                                    "menu_url"=> "reports/leasable-asset/all",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_lease"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 20,
                                    "menu_name"=> "reports-asset-leasable-available",
                                    "menu_label"=> "Available",
                                    "menu_url"=> "reports/leasable-asset/available",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_lease"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 20,
                                    "menu_name"=> "reports-asset-leasable-leased",
                                    "menu_label"=> "Leased",
                                    "menu_url"=> "reports/leasable-asset/leased",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_lease"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                            ]
                        ],
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 16,
                            "menu_name"=> "reports-status",
                            "menu_label"=> "Asset Status",
                            "menu_url"=> "#",
                            "menu_icon"=> "mdi mdi-check-circle m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                            "menu_order"=> null,
                            "is_disable"=> 0,
                            "childs" => [
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-available",
                                    "menu_label"=> "Available Assets",
                                    "menu_url"=> "reports/status/available",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-donated",
                                    "menu_label"=> "Donated Assets",
                                    "menu_url"=> "reports/status/donated",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-damage",
                                    "menu_label"=> "Damage Assets",
                                    "menu_url"=> "reports/status/damage",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-disposed",
                                    "menu_label"=> "Disposed Assets",
                                    "menu_url"=> "reports/status/disposed",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-lost",
                                    "menu_label"=> "Lost Assets",
                                    "menu_url"=> "reports/status/lost",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                                [
                                    "account_id"=> 1,
                                    "parent_menu"=> 24,
                                    "menu_name"=> "reports-status-sold",
                                    "menu_label"=> "Sold Assets",
                                    "menu_url"=> "reports/status/sold",
                                    "menu_icon"=> "ti-arrow-right m-r-10",
                                    "privilege_key"=> '["all","pkg_fixasset_report_status"]',
                                    "menu_order"=> null,
                                    "is_disable"=> 0
                                ],
                            ]
                        ],           
                    ]
                ],
                [
                    "level"=> 'main',
                    "action"=> 'find-parent-key',
                    "menu_name"=> "settings-main",
                    "childs" => [
                        [
                            "account_id"=> 1,
                            "parent_menu"=> 17,
                            "menu_name"=> "settings-tagging",
                            "menu_label"=> "Asset Tag",
                            "menu_url"=> "settings/tag",
                            "menu_icon"=> "mdi mdi-tag m-r-10",
                            "privilege_key"=> '["all","pkg_fixasset_setting_assettag"]',
                            "menu_order"=> null,
                            "is_disable"=> 0
                        ],                
                    ]
                ]
            ];
        }else if($type=='name_only'){
            $menus = [
                ["menu_name"=> 'fix-asset-main'],
                ["menu_name"=> 'fix-asset-list'],
                ["menu_name"=> 'fix-asset-list-all'],
                ["menu_name"=> 'fix-asset-list-leasable'],
                ["menu_name"=> 'fix-asset-add'],
                ["menu_name"=> 'fix-asset-checkin'],
                ["menu_name"=> 'fix-asset-checkout'],
                ["menu_name"=> 'fix-asset-lease'],
                ["menu_name"=> 'fix-asset-lease-return'],
                ["menu_name"=> 'fix-asset-transfer'],
                ["menu_name"=> 'reports-asset'],
                ["menu_name"=> 'reports-checkout'],
                ["menu_name"=> 'reports-checkin'],
                ["menu_name"=> 'reports-asset-leasable'],
                ["menu_name"=> 'reports-asset-leasable-all'],
                ["menu_name"=> 'reports-asset-leasable-available'],
                ["menu_name"=> 'reports-asset-leasable-leased'],
                ["menu_name"=> 'reports-status'],
                ["menu_name"=> 'reports-status-available'],
                ["menu_name"=> 'reports-status-donated'],
                ["menu_name"=> 'reports-status-damage'],
                ["menu_name"=> 'reports-status-disposed'],
                ["menu_name"=> 'reports-status-lost'],
                ["menu_name"=> 'reports-status-sold'],
                ["menu_name"=> 'settings-tagging'],
            ];
        }

        return $menus;
    }

    public function install($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

       ### Menu Data ###
       $get_primary_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->orderBy('menu_order')->get();
       $dashboard_menu_id = Menus::where('account_id', $account_id)->where('menu_name', 'dashboard')->first()->menu_id;
       $menu_order = [];

        foreach($get_primary_menu as $key => $menu){
            $menu_order[] = $menu['menu_id'];
        }

        $index = array_search($dashboard_menu_id, $menu_order) + 1;
        $update_ordering = array_splice($menu_order , $index, 0, 'new-menu');//Insert before Reports Menu

            
        $new_menus = static::menu_data();


        //return $menu_order;
        $order_num = 1;
        $menu_stat = [];
        foreach($menu_order as $item){
            if($item!='new-menu'){
                $menu = Menus::find($item);
                $menu->menu_order = $order_num;
                $menu->save();
                $menu_stat[] = $menu;                
            }else{
                foreach($new_menus as $menu){
                    $parent_menu_id = '';

                    if($menu['action']=='add'){
                        $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $menu['menu_name'])->first();
                        
                        if(!count($check_menu)){
                            $add_menu = new Menus;
                            $add_menu->account_id = $account_id;
                            $add_menu->parent_menu = 0;
                            $add_menu->menu_name = $menu['menu_name'];
                            $add_menu->menu_label = $menu['menu_label'];
                            $add_menu->menu_url = $menu['menu_url'];
                            $add_menu->menu_icon = $menu['menu_icon'];
                            $add_menu->privilege_key = $menu['privilege_key'];
                            $add_menu->is_disable = 0;
                            $add_menu->menu_order = $order_num;
                            $add_menu->save();

                            $parent_menu_id = $add_menu->menu_id;
                            $menu_stat[] = $add_menu;
                        }else{
                            $parent_menu_id = $check_menu->menu_id;
                            $menu_stat[] = $parent_menu_id;   
                            $order_num--;
                        }
                    }else{
                        $get_parent_menu_id = Menus::where('account_id', $account_id)->where('menu_name', $menu['menu_name'])->first();

                        if($get_parent_menu_id){
                            $parent_menu_id = $get_parent_menu_id->menu_id;
                        }
                    }

                    //Check if has child menu
                    if(isset($menu['childs'])){
                        foreach($menu['childs'] as $sub_menu){
                            $sub_parent_menu_id = '';
                            $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $sub_menu['menu_name'])->first();

                            if(!count($check_menu)){
                                $add_sub_menu = new Menus;
                                $add_sub_menu->account_id = $account_id;
                                $add_sub_menu->parent_menu = $parent_menu_id;
                                $add_sub_menu->menu_name = $sub_menu['menu_name'];
                                $add_sub_menu->menu_label = $sub_menu['menu_label'];
                                $add_sub_menu->menu_url = $sub_menu['menu_url'];
                                $add_sub_menu->menu_icon = $sub_menu['menu_icon'];
                                $add_sub_menu->privilege_key = $sub_menu['privilege_key'];
                                $add_sub_menu->is_disable = 0;
                                $add_sub_menu->save();
    
                                $sub_parent_menu_id = $add_sub_menu->menu_id;
                                $menu_stat[] = $add_menu;   
                            }else{
                                $sub_parent_menu_id = $check_menu->menu_id;
                                $menu_stat[] = $sub_parent_menu_id;   
                            }

                            //Check if has child menu
                            if(isset($sub_menu['childs'])){
                                foreach($sub_menu['childs'] as $sub_sub_menu){
                                    $sub_sub_parent_menu_id = '';
                                    $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $sub_sub_menu['menu_name'])->first();

                                    if(!count($check_menu)){
                                        $add_sub_sub_menu = new Menus;
                                        $add_sub_sub_menu->account_id = $account_id;
                                        $add_sub_sub_menu->parent_menu = $sub_parent_menu_id;
                                        $add_sub_sub_menu->menu_name = $sub_sub_menu['menu_name'];
                                        $add_sub_sub_menu->menu_label = $sub_sub_menu['menu_label'];
                                        $add_sub_sub_menu->menu_url = $sub_sub_menu['menu_url'];
                                        $add_sub_sub_menu->menu_icon = $sub_sub_menu['menu_icon'];
                                        $add_sub_sub_menu->privilege_key = $sub_sub_menu['privilege_key'];
                                        $add_sub_sub_menu->is_disable = 0;
                                        $add_sub_sub_menu->save();
            
                                        $sub_sub_parent_menu_id = $add_sub_sub_menu->menu_id;
                                        $menu_stat[] = $add_menu;   
                                    }else{
                                        $sub_sub_parent_menu_id = $check_menu->menu_id;
                                        $menu_stat[] = $sub_sub_parent_menu_id;   
                                    }
                                }//End of For
                            }//End of IF
                        }//End of For
                    }//End of IF
                }
            }
            $order_num++;
        }//End of For loop
        ### End of Menu Data ###

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }
        ######################
        
        ### Option Data ###
        $opts = [   ["option_name" => "apps_cat_type","option_value" => "asset"],
                    ["option_name" => "tag_prefix","option_value" => "FA"],
                    ["option_name" => "tag_suffix","option_value" => ""],
                    ["option_name" => "tag_with_padding","option_value" => 1],
                    ["option_name" => "tag_padding_digits","option_value" => 7],
                    ["option_name" => "tag_with_dashed","option_value" => 1],
                    ["option_name" => "tag_with_year","option_value" => 0],
                    ["option_name" => "tag_year_strlen","option_value" => 2],
                    ["option_name" => "tag_insert_year","option_value" => 'AFT_PREF'],
                    ["option_name" => "tag_last_counter","option_value" => 1]
                ];

        $opt_stat = [];
        foreach($opts as $opt){
            $check_option = Options::where('account_id', $account_id)->where('option_name', $opt['option_name'])->first();

            if(!count($check_option)){
                $opt_action = new Options;
                $opt_action->account_id = $account_id;
                $opt_action->option_name = $opt['option_name'];
                $opt_action->option_value = $opt['option_value'];
                $opt_action->save();

                if($opt_action){
                    $opt_stat[] = 1;
                }
            }else{
                $opt_id = $check_option->option_id;
                $opt_action = Options::find($opt_id);
                $opt_action->option_value = $opt['option_value'];
                $opt_action->save();

                if($opt_action){
                    $opt_stat[] = 1;
                }
            }
        }
        ### End of Option Data ###

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
        $package_uninstalled = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

        if($output||$menu_stat||$opt_stat||$package_installed||$package_activate||$category_stat){
            return 'true';
        }else{
            return 'false';
        }
    }


    public static function activate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Update Menu Data
        $menus = static::menu_data('name_only');

        $menu_stat = [];
        foreach($menus as $menu){
            $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $menu['menu_name'])->first();
                        
            if($check_menu){
                $menu_id  = $check_menu->menu_id;
                
                $action = Menus::find($menu_id);
                $action->is_disable = 0;
                $action->save();

                $menu_stat[] = $action;
            }
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'asset')
                                                ->update(['record_stat' => 0]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }
        ####################

        ### Option Data ###
        $opts = [   ["option_name" => "apps_cat_type","option_value" => "asset"],
                    ["option_name" => "tag_prefix","option_value" => "FA"],
                    ["option_name" => "tag_suffix","option_value" => ""],
                    ["option_name" => "tag_with_padding","option_value" => 1],
                    ["option_name" => "tag_padding_digits","option_value" => 7],
                    ["option_name" => "tag_with_dashed","option_value" => 1],
                    ["option_name" => "tag_with_year","option_value" => 0],
                    ["option_name" => "tag_year_strlen","option_value" => 2],
                    ["option_name" => "tag_insert_year","option_value" => 'AFT_PREF'],
                    ["option_name" => "tag_last_counter","option_value" => 1]
                ];


        $opt_stat = [];
        foreach($opts as $opt){
            $check_option = Options::where('account_id', $account_id)->where('option_name', $opt['option_name'])->where('option_value', $opt['option_value'])->first();

            if(!count($check_option)){
                $opt_action = new Options;
                $opt_action->account_id = $account_id;
                $opt_action->option_name = $opt['option_name'];
                $opt_action->option_value = $opt['option_value'];
                $opt_action->save();

                $opt_stat[] = $opt_action;
            }
        }


        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];

        if($menu_stat||$opt_stat||$package_activate||$category_stat){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function uninstall($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations/drop';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Delete Migration Data ###
        $migrated_names = ['2018_07_21_041613_drop_sys_asset_table','2018_04_09_011604_create_sys_asset_table', '2018_04_09_014507_create_sys_asset_custom_value_table', '2018_04_09_021536_create_sys_asset_events_table','2018_04_09_040959_create_sys_asset_inout_items_table','2018_04_09_040430_create_sys_asset_inout_logs_table','2018_04_09_042513_create_sys_asset_lease_logs_table','2018_04_09_042846_create_sys_asset_lease_items_table','2018_04_09_043407_create_sys_asset_maintenances_table','2018_04_09_044000_create_sys_asset_warranties_table'];

        $delete_migration = [];
        foreach($migrated_names as $migration){
            $check_migration = Migrations::where('migration', $migration)->first();
            
            if($check_migration){
                $id = $check_migration->id;
                
                //Delete Migration Data
                $delete_migration = Migrations::find($id)->delete();
            }
        }

        ### Deleting Menu Data ###

        $delete_menus = static::menu_data('name_only');

        $menu_stat = [];
        foreach($delete_menus as $menu){
            $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $menu['menu_name'])->first();
                        
            if($check_menu){
                $menu_id  = $check_menu->menu_id;
                
                $delete = Menus::find($menu_id)->delete();

                $menu_stat[] = $delete;
            }
        }
        ### End of Menu Data ###

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->count();
        $category_stat = '';

        if($categories){
            $delete_categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->delete();
            
            if($delete_categories){
                $category_stat = $delete_categories;
            }
        }
        #####################

        ### Deleting Option Data ###
        $opts = [   ["option_name" => "apps_cat_type","option_value" => "asset"],
                    ["option_name" => "tag_prefix"],
                    ["option_name" => "tag_suffix"],
                    ["option_name" => "tag_with_padding"],
                    ["option_name" => "tag_padding_digits"],
                    ["option_name" => "tag_with_dashed"],
                    ["option_name" => "tag_with_year"],
                    ["option_name" => "tag_year_strlen"],
                    ["option_name" => "tag_insert_year"],
                    ["option_name" => "tag_last_counter"]
                ];

        $opt_stat = [];
        foreach($opts as $opt){
            if($opt['option_name']=="apps_cat_type"){
                $check_option = Options::where('account_id', $account_id)->where('option_name', $opt['option_name'])->where('option_value', $opt['option_name'])->first();

                if($check_option){
                    $opt_id = $check_option->option_id;
    
                    $opt_action = Options::find($opt_id)->delete();
                    $opt_stat[] = $opt_action;
                }
            }else{
                $check_option = Options::where('account_id', $account_id)->where('option_name', $opt['option_name'])->first();

                if($check_option){
                    $opt_id = $check_option->option_id;

                    $opt_action = Options::find($opt_id)->delete();
                    $opt_stat[] = $opt_action;
                }
            }
        }

        ### End of Option Data ###

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        $package_uninstalled = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];

        if($output||$menu_stat||$opt_stat||$package_uninstalled||$package_deactive||$category_stat){
            return 'true';
        }else{
            return 'false';
        }
    }


    public static function deactivate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Update Menu Data
        $menus = static::menu_data('name_only');

        $menu_stat = [];
        foreach($menus as $menu){
            $check_menu = Menus::where('account_id', $account_id)->where('menu_name', $menu['menu_name'])->first();
                        
            if($check_menu){
                $menu_id  = $check_menu->menu_id;
                
                $action = Menus::find($menu_id);
                $action->is_disable = 1;
                $action->save();

                $menu_stat[] = $action;
            }
        }

        ### Category Data ###
        $categories = Categories::where('account_id', $account_id)->where('apps_type', 'asset')->count();
        $category_stat = '';

        if($categories){
            $update_categories = Categories::where('account_id', $account_id)
                                                ->where('apps_type', 'asset')
                                                ->update(['record_stat' => 1]);
            
            if($update_categories){
                $category_stat = $update_categories;
            }
        }
        ### Option Data ###
        $opts = [["option_name" => "apps_cat_type","option_value" => "asset"]];


        $opt_stat = [];
        foreach($opts as $opt){
            $check_option = Options::where('account_id', $account_id)->where('option_name', $opt['option_name'])->where('option_value', $opt['option_value'])->first();

            if($check_option){
                $opt_id = $check_option->option_id;

                $opt_action = Options::find($opt_id)->delete();
                $opt_stat[] = $opt_action;
            }
        }

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];

        if($menu_stat||$opt_stat||$package_deactive||$category_stat){
            return 'true';
        }else{
            return 'false';
        }
    }
}
