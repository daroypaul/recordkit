<?php

namespace App\Packages\consumables\Models;

use Illuminate\Database\Eloquent\Model;

class Consumable extends Model
{
    protected $table = 'sys_consumable';
    protected $primaryKey = 'item_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function category()
    {
        return $this->hasOne('App\packages\assettracking\models\Categories','cat_id','cat_id');
    }

    public function site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','site_id')->where('record_stat', 0);
    }

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }
}
