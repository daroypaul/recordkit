<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetWarranties extends Model
{
    protected $table = 'sys_asset_warranties';
    //protected $primaryKey = 'warranty_id';
    const CREATED_AT = 'date_recorded';
    public $timestamps = false;
    //const UPDATED_AT = 'date_updated';

    public function users()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }
}
