<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetMaintenance extends Model
{
    protected $table = 'sys_asset_maintenances';
    //protected $primaryKey = 'asset_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function users()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function suppliers()
    {
        return $this->hasOne('App\modules\adminprofile\models\Suppliers','supplier_id','supplier_id');
    }
}
