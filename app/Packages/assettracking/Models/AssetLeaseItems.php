<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetLeaseItems extends Model
{
    protected $table = 'sys_asset_lease_items';
    protected $primaryKey = 'item_id';
    public $timestamps = false;

    public function logs()
    {
        return $this->hasOne('App\packages\assettracking\models\AssetLeaseLogs','lease_id','lease_id'); 
    }
}
