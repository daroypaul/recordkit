<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetImages extends Model
{
    protected $table = 'sys_asset_images';
    const CREATED_AT = 'date_recorded';
    public $timestamps = false;

    public function users()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }
    
    public function setUpdatedAt($value)
    {
        // Do nothing.
    }
}
