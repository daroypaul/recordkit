<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetEvents extends Model
{
    protected $table = 'sys_asset_events';
    protected $primaryKey = 'event_id';
    public $timestamps = false;

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function requestor()
    {
        return $this->hasOne('App\modules\adminprofile\models\Employees','employee_id','requestor_id');
    }

    public function prev_site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','prev_site_id')->where('record_stat', 0);
    }

    public function prev_location()
    {
        return $this->hasOne('App\modules\adminprofile\models\Location','location_id','prev_location_id')->where('record_stat', 0);
    }

    public function new_site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','new_site_id')->where('record_stat', 0);
    }

    public function new_location()
    {
        return $this->hasOne('App\modules\adminprofile\models\Location','location_id','new_location_id')->where('record_stat', 0);
    }
}
