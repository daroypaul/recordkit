<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    protected $table = 'sys_categories';
    protected $primaryKey = 'cat_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';
}
