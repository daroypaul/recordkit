<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetImagesLink extends Model
{
    protected $table = 'sys_asset_images';
    protected $primaryKey = 'img_id';
    public $timestamps = false;

    public function users()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function files()
    {
        return $this->hasMany('App\modules\adminprofile\models\UploadedFiles','upload_id','upload_id');
    }
}
