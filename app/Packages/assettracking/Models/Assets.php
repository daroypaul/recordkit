<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    //
    protected $table = 'sys_asset';
    protected $primaryKey = 'asset_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function categories()
    {
        return $this->hasOne('App\packages\assettracking\models\Categories','cat_id','cat_id');
    }

    public function site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','site_id')->where('record_stat', 0);
    }

    public function location()
    {
        return $this->hasOne('App\modules\adminprofile\models\Location','location_id','location_id')->where('record_stat', 0);
    }

    public function custom_values()
    {
        return $this->hasMany('App\packages\assettracking\models\AssetCustomValues','asset_id','asset_id');
    }

    public function image()
    {
        return $this->hasOne('App\Utilities\Uploads','owner_id','asset_id')->where('owner', 'ams_img')->where('upload_use', 1);
    }

    public function documents()
    {
        return $this->hasMany('App\packages\assettracking\models\AssetDocuments','asset_id','asset_id');
    }

    public function creator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function assigned_to()
    {
        return $this->hasOne('App\modules\adminprofile\models\Employees','employee_id','assign_to')->where('record_stat', '<>', 2);
    }

    public function is_leased()
    {
        return $this->hasOne('App\packages\assettracking\models\AssetLeaseItems','asset_id','asset_id')->where('item_stat', 0);
    }
}