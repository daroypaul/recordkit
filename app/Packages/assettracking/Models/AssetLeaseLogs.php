<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetLeaseLogs extends Model
{
    protected $table = 'sys_asset_lease_logs';
    protected $primaryKey = 'lease_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function assets()
    {
        return $this->hasOne('App\packages\assettracking\models\AssetLeaseItems','lease_id','lease_id'); 
    }
}
