<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetInOutItems extends Model
{
    protected $table = 'sys_asset_inout_items';
    protected $primaryKey = 'item_id';
    public $timestamps = false;

    public function logs()
    {
        return $this->hasOne('App\packages\assettracking\models\AssetInOutLogs','inout_id','inout_id')
                        ->orderBy('inout_date','Asc',',','date_updated','Asc'); 
    }
}
