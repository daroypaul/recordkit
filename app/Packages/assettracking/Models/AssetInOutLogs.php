<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetInOutLogs extends Model
{
    protected $table = 'sys_asset_inout_logs';
    protected $primaryKey = 'inout_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function assets()
    {
        return $this->hasOne('App\packages\assettracking\models\AssetInOutItems','inout_id','inout_id'); 
    }
}
