<?php

namespace App\packages\assettracking\models;

use Illuminate\Database\Eloquent\Model;

class AssetCustomValues extends Model
{
    protected $table = 'sys_asset_custom_value';
    public $timestamps = false;

    public function custom_fields()
    {
        return $this->hasOne('App\modules\settings\models\CustomFields','field_id','field_id');
    }
}
