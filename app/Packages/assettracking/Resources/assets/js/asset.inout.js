/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    //Initialize Date Picker

    $(".select2").select2();

    $('table#selected-asset-table').DataTable({
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 5, 6 ] }
         ],
    });

    $('#requestor, #customer').select2({
        ajax: {
            url: $apps_base_url+'/admin/employees/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.emp_fullname,
                            id: item.employee_id
                        }
                    })
                };
            }
          }
    });

    $('#asset_site').select2({
        placeholder : 'Select a Site',
        ajax: {
            url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.site_name,
                            id: item.site_id
                        }
                    })
                };
            }
          }
    }).on('select2:select', function (e) {
        $('#asset_location').val(null).trigger('change');
    });


    $('#asset_location').select2({
        placeholder : 'Select a Location',
        ajax: {
            url: $apps_base_url+'/admin/location/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    site_id: $('#asset_site').val(),
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.location_name,
                            id: item.location_id
                        }
                    })
                };
            }
          }
    });

    $('button#add-employee-1,button#add-employee-2').click(function(){
        $.admin_employee_form('','add','Add New Employee','dropdown');
    });

    $('button#add-site').click(function(){
        $.admin_site_form('','add','Add New Site','dropdown');
    });

    $('button#add-location').click(function(){
        var site_id = $('#asset_site').val();
        $.admin_location_form('',site_id, 'add','Add New Location','dropdown');
    });

    /*$('#asset_site').change(function(){
        var site = $(this).val();
        var form_data = new FormData();
        var target = $('#asset_location');
        form_data.append('site_id', site);
        $('#form-loader').toggle();

        $.ajax({
            type:'POST',
            url : $apps_base_url+'/admin/location/fetchdata',
            dataType: "json",
            data:  form_data,
            processData: false,
            contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                if(data.length){
                    var append = '';
                    append += '<option value="">-- Select Location</option>';
                    $.each(data, function(i, item) {
                        append += '<option value="'+item.location_id+'">'+item.location_name+'</option>';
                    });
                    target.html(append);
                }else{
                    $('#asset_location').html('<option value="">-- Select Location</option>');
                }
                $('#form-loader').toggle();
                
            },
            error :function (data) {
                $('#form-loader').toggle();
            }
        });
    });*/


    $('button#btn-add-asset').click(function(){
        var type = ($('#_type').length&&$('#_type').val().trim()!='') ? $('#_type').val().trim() : 'all';

        $.search_asset_modal('Search Asset',type);
    });

    $('tbody#asset-row-data').on('click', 'button#btn-delete-row', function(e) {
        $('table#selected-asset-table').DataTable().destroy();
        $(this).parents('tr').remove();
        $('table#selected-asset-table').DataTable({
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 6 ] }
             ],
        });
    });

    $('#btn-save-draft').click(function(){
        //form_data.append('id', $('#_id').val()); 
        $('#_stat').val('1');
        $('#form').submit();
    });
    
    $('#btn-done').click(function(){
        //form_data.append('id', $('#_id').val()); 
        $('#_stat').val('0');
        $('#form').submit();
    });

    $('#form').submit(function(e){
        e.preventDefault();
        $('#form-loader .loader__label').text('Processing...');
        $('#form-notification').html('');
        
        var total_asset = $('tr[data-id]').length;

        if(total_asset==0){
            $.notification('Error','Add at least one (1) asset','top-right','error','');
        }else{
            $('#form-loader').toggle();
            var form_data = new FormData($(this)[0]);
            form_data.append('is_draft', $('#_stat').val());
            form_data.append('inout_type', $('#_type').val()); 

            $.ajax({
                type:'POST',
                url : window.location,
                //dataType: "json",
                data:  form_data,
                dataType: "json",
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();

                    $('.form-group').removeClass('has-danger');
                    $('.form-control-feedback').remove();
                    $.each(data.val_errors,function(i,v){
                        $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                        $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                    });

                    if(data.stat){
                        if(data.stat=='success'){
                            if($('#_stat').val()==0){
                                
                            }
                            $('#form')[0].reset();
                                
                            $('table#selected-asset-table').DataTable().destroy();
                            $('tbody#asset-row-data').html('');
                            $('table#selected-asset-table').DataTable({
                                "aoColumnDefs": [
                                    { 'bSortable': false, 'aTargets': [ 6 ] }
                                ],
                            });

                            $('#requestor, #customer, #asset_site, #asset_location').val(null).trigger('change');
                        }

                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        }
    });
});