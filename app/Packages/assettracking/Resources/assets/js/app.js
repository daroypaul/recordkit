/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){ 
    console.log('*********************************************\n** RecordKit Modular Application Software  **\n** Developer: Paul Daroy              **\n** Email: daroypaul@live.com          **\n*********************************************');

    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 0, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 7 ] }
            ],
            "dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<button type="button" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Consumable" id="btn-add-record"><i class="fas fa-plus"></i></button>';
                $('div#action-toolbar').html(add_element);

                

                        
                var bulk_action = '<select class="form-control custom-select" id="select-site">'+
                                        '<option value="">-- All Site --</option>'+
                                    '</select>';
                $('div#bulk-action-toolbar').html(bulk_action);

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/site/fetchdata',
                    dataType: "json",
                    data: {'ref' : 'all',},
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        var site_list =  '<select class="form-control custom-select" id="select-site">'+
                                    '<option value="">-- All Site --</option>';

                        if(data.list&&data.list.length!=0){
                            $.each(data.list,function(i,v){
                                site_list += '<option value="'+v.site_id+'">'+v.site_name+'</option>';
                            });
                        }

                        site_list +=  '</select>';

                        $('div#bulk-action-toolbar').html(site_list);
                        trigger_site_selection();
                    },
                    error :function (data) {
                        $.notification('Error','Unable to load site data list','top-right','error','');
                        $('#form-loader').hide();
                        trigger_site_selection();
                    }
                });
            }
        }).on('click', 'td[data-action]', function(e) {
            var action = $(this).data('action');
            var title= '';

            if(action=='view-address'){
                title= "Address";
                var content = $(this).parents('tr').data('address-content');
            }else if(action=='view-remarks'){
                title= "Remarks";
                var content = $(this).parents('tr').data('remarks-content');
            }
            
            if(content){
                modal_view_content(title,content);
            }else{
                $.notification('','Empty Data','top-right','info','');
            }
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='check-out'&&id){
                $.module_consumable_checkout(id);
            }
            else if(action=='edit'&&id){
                edit_action(id, 'edit','table','single');
            }
            else if(action=='delete'&&id){
                delete_action(id,'single');
            }
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }
        });

        $('input#select-all').click(function(){
            var c = this.checked;
    
            $('input.selected-item').each(function(){
                var id = $(this).parents('tr').data('id');
    
                $(this).prop('checked', c);
    
                if(c){
                    if(id!=0||id){
                        $selected_asset.push(id);
                    }
                }else{
                    $selected_asset = $.grep($selected_asset, function (value) {
                        return value != id;
                    });
                }
            });
        });


        $('#btn-bulk-action').click(function(){
            var opt = $('#bulk-action-option').val();
            $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
            $('#bulk-action-option').removeClass('form-control-danger');
    
            if($selected_asset.length==0){
                $.notification('Error','Select a record','top-right','error','');
            }else if(opt==''){
                $('#bulk-action-option').parents('.form-group').addClass('has-danger');
                $('#bulk-action-option').addClass('form-control-danger');
                $.notification('Error','Select an action option','top-right','error','');
            }else if(opt=='Delete'){
                if($selected_asset.length!=0){
                    delete_action('','multi');
                }
            }else{
                if(opt!='Delete'&&opt!='Email'){
                    $status_updated = 0;
                    $.asset_status_modal($selected_asset, opt + ' Asset', opt);
                }
            }
        });
        

    $('#btn-add-record').click(function(){
        $.module_consumable_form('','add','Add New Consumable','table');
    });

    /*$('#btn-link-asset').click(function(){
        var id = $('#_id').val();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Fetching data...');

        $.ajax({
            type:'POST',
            url :  $apps_base_url+'/insurances/fetchdata',
            dataType: "json",
            data: {
                'id':id,
                'ref':'single'
            },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                if(data.record&&data.record.length!=0){
                    var link_asset = [];
                    var key = [];
                    var ref = 'add';
                    $.each(data.associated_assets, function(i, v){
                        var key = {};
                        key['id'] = v.asset_id;
                        key['tag'] = v.asset_tag;
                        key['name'] = v.asset_model;

                        link_asset.push(key);
                    });
                    
                    if(data.associated_assets){
                        ref = 'edit';
                    }

                    $.link_insurance_modal(ref,'Link an Asset','insurance_view_page', id,data.record.insurance_name, link_asset);
                }else{
                    $.notification('Error','Record not found. Invalid reference ID or insurance record might be deleted','top-right','error','');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });*/


    

    $('button#action-view-form-edit').click(function(){
        var id = $('#_id').val();
        if(id){
            edit_action(id,'edit','form-view','single');//id='',ref='add',target='',fetch_type='single'
        }
    });

    $('button#action-view-form-delete').click(function(){
        var id = $('#_id').val();

        if(id){
            delete_action(id,del_type='single','form-view');
        }
    });
});

function edit_action(id='',ref='add',target='',fetch_type='single'){
    $('#form-loader').toggle();
    $('#form-loader .loader__label').text('Fetching data...');
    $(".tooltip").tooltip("hide");
    $.ajax({
        type:'POST',
        url :  $apps_base_url+'/consumables/fetchdata',
        dataType: "json",
        data: {
            'id':id,
            'ref':fetch_type
        },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function (data) {
            $(".tooltip").tooltip("hide");
            $('#form-loader').toggle();

            if(data.length!=0){
                $.module_consumable_form(id, 'edit','Update Consumable','table',data.item_name, data.item_model, (data.item_brand!=null)?data.item_brand:'', (data.date_purchased!=null)?moment(data.date_purchased).format("MMM DD, YYYY"):'', data.purchased_cost,(data.po_number!=null)?data.po_number:'',data.qty_remain,data.qty_alert, data.cat_id, data.site_id, data.category.cat_name,data.site.site_name);
            }else{
                $.notification('Error','Record not found. Invalid reference ID or insurance record might be deleted','top-right','error','');
            }
        },
        error :function (data) {
            $('#form-loader').toggle();
            $.notification('Error','Internal Server Error','top-right','error','');
            $('#form-loader .loader__label').text('Fetching Data...');
        }
    });
};

function delete_action(id='',del_type='single',origin='table'){
    $(".tooltip").tooltip("hide");

    if(del_type=='single'){
        var msg_text = "Your about to delete consumable record. Are you want to continue?";
        $form_data = new FormData();
        $form_data.append('id', id);
        $form_data.append('del_type', 'single');
    }else{
        var msg_text= "Your about to delete "+$selected_asset.length+" consumable record(s). Are you want to continue?";

        $form_data = new FormData();
        $.each($selected_asset, function(i, v){
            $form_data.append('id[]', v);
        });
        $form_data.append('del_type', 'multi');
    }

    var url = $apps_base_url+'/consumables/delete';
    var settings = {
        title : "Deleting Record",
        text: msg_text,
        confirmButtonText:"Delete",
        showCancelButton:true,
        type : 'warning',
        confirmButtonColor: "#DD6B55",
        //closeOnConfirm: false
    }

    swal(settings).then(result => {
        swal.close();
        
        if(result.value){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Deleting...');

            $.ajax({
                type:'POST',
                url : url,
                dataType: "json",
                data: $form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();
                    $(".tooltip").tooltip("hide");

                    if(del_type=='single'){
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            if(data.stat=='success'){
                                $record_table.row('tr[data-id='+data.id+']').remove().draw();
                            }else{
                                $("tr[data-id='"+id+"']").addClass('table-danger');
                            }
    
                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }
                    }else{
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }else{
                            if(data.sid.length!=0){
                                $.notification('Success','Selected insurance record(s) successfully deleted.','top-right','success','');

                                $.each(data.sid,function(i,v){
                                    $record_table.row('tr[data-id='+v+']').remove().draw();
                                });
                            }

                            if(data.eid.length!=0){
                                $.notification('Error Process','Selected insurance record(s) can not be deleted. Invalid reference ID or record might already deleted. See highlighten row','top-right','error','');
                                
                                $.each(data.eid,function(i,v){
                                    $("tr[data-id='"+v+"']").addClass('table-danger');
                                });
                                
                                $eid = data.eid;
                            }
                        }
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }
    });
}

function trigger_site_selection(){
    $('select#select-site').change(function(){
        var site_id = $(this).val();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Fetching data...');
        $(".tooltip").tooltip("hide");

        $.ajax({
            type:'POST',
            url :  $apps_base_url+'/consumables/fetchdata',
            dataType: "json",
            data: {
                'site':site_id,
                //'ref':''
            },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $(".tooltip").tooltip("hide");
                $('#form-loader').toggle();
    
                if(data.length!=0){
                    $record_table.clear().draw();
                    $.each(data, function(i, item) {
                        var row = $record_table.row.add( {
                            0: item.name,
                            1: item.model,
                            2: item.date_purchased,
                            3: item.cost,
                            4: item.qty,
                            5: item.category,
                            6: item.site,
                            7: '<button type="button" class="btn btn-sm btn-inverse" data-action="check-out" data-toggle="tooltip" title="Check-out"><i class="mdi mdi-logout"></i></button> <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"><i class="ti-pencil"></i></button> <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></button>',
                        } ).draw();

                        $record_table.rows(row).nodes().to$().attr({"data-id":item.id});
                        
                        bootbox.hideAll();

                        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                    });
                }else{
                    $.notification('Error','Unable to display record. Record not found','top-right','error','');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });
}

$.module_consumable_form = function module_consumable_form(id, ref='add',modal_title='New Asset Maintenance',append_type='table',name='', model='', brand='', purchased_date='',cost='',po_number='',qty='',min_qty='', category_id='', site_id='',category_name="",site_name="") {//

    $layout_form = 	'<form class="form-material" id="modal-consumable-form" method="post">'+
                        '<div class="row">'+
                            '<div class="col-lg-12 form-group">'+
                                '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="name" placeholder="Enter Name" value="'+name+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Model <i class="mdi mdi-check-circle form-required-helper text-danger"></i><i class="mdi mdi-help-circle form-required-helper cursor-pointer" data-toggle="tooltip" title="Put Generic if none"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="model" placeholder="Enter Model" value="'+model+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Brand <i class="mdi mdi-help-circle form-required-helper cursor-pointer" data-toggle="tooltip" title="Put Generic/OEM if none"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="brand" placeholder="Enter Brand" value="'+brand+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Purchased Date</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="purchased_date" id="modal-date-start" placeholder="Enter Purchased Date" value="'+purchased_date+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-4 col-lg-3 form-group">'+
                                '<label class="form-control-label text-bold">Cost</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="cost" id="modal-input-cost" placeholder="Enter Cost" value="'+cost+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-3 form-group">'+
                                '<label class="form-control-label text-bold">PO Number</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="order_number" placeholder="Enter PO Number" value="'+po_number+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-3 form-group">'+
                                '<label class="form-control-label text-bold">Stocks (Qty) <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="number" class="form-control" name="qty" placeholder="Total Qty" value="'+qty+'" min="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-3 form-group">'+
                                '<label class="form-control-label text-bold">Stocks Limit (Qty)<i class="mdi mdi-check-circle form-required-helper text-danger"></i><i class="mdi mdi-help-circle form-required-helper cursor-pointer" data-toggle="tooltip" title="It Trigger Email Alert Notification"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="number" class="form-control" name="min_qty" placeholder="Min Qty" value="'+min_qty+'" min="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4 form-group">'+
                                '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-category" data-original-title="Add New Category"><i class="fas fa-plus"></i></button>'+
                               '<label class="form-control-label text-bold">Category <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                               '<div class="form-line">'+
                                   '<select class="form-control custom-select" id="modal-select-category" name="category"><option value="">- Select Category -</option></select>'+ 
                               '</div>'+
                           '</div>'+
                            '<div class="col-lg-4 form-group m-b-0">'+
                                '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-site" data-original-title="Add New Site"><i class="fas fa-plus"></i></button>'+
                               '<label class="form-control-label text-bold">Site <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                               '<div class="form-line">'+
                                   '<select class="form-control custom-select" id="modal-select-site" name="site"><option value="">- Select Site -</option></select>'+ 
                               '</div>'+
                           '</div>'+
                        '</div>'+
                    '</form>';
    
        bootbox.dialog({
            closeButton: false,
            backdrop: true,
            animate: true,
            title: modal_title,
            message: $layout_form,
            size: 'large',
            buttons: {
                done: {
                    label: 'Save',
                    className: "btn-success",
                    callback: function () {
                        $('#form-loader').toggle();
                        $('#form-loader .loader__label').text('Saving...');

                        var form_data = new FormData($('form#modal-consumable-form')[0]);

                        form_data.append('ref',ref);
                        form_data.append('id',id);

                        $.ajax({
                            type:'POST',
                            url : window.location,
                            //dataType: "json",
                            processData: false, // Don't process the files
                            contentType: false,
                            data: form_data,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (data) {
                                $(".tooltip").tooltip("hide");
                                $('#form-loader').toggle();

                                
                                $('.form-group').removeClass('has-danger');
                                $('.form-control-feedback').remove();
                                $.each(data.val_error,function(i,v){
                                   $('form#modal-consumable-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                   $('form#modal-consumable-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                });

                                if(data.stat){
                                    if(data.stat=='success'){
                                        if(ref=='edit'&&append_type=='table'){
                                            var row = $('tr[data-id='+id+']');
                                            $record_table.row(row).remove().draw();
                                        }else{
                                            $('#txt_name').val(data.name);
                                            $('#txt_provider').val(data.provider);
                                            $('#txt_start').val(data.start);
                                            $('#txt_expire').val(data.expire);
                                            $('#txt_cost').val(data.cost);
                                            $('#txt_remarks').val(data.remarks_full);
                                        }

                                        if($('#select-site').val()==$('#modal-select-site').val()||$('#select-site').val()==''){
                                            var row = $record_table.row.add( {
                                                0: data.name,
                                                1: data.model,
                                                2: data.date_purchased,
                                                3: data.cost,
                                                4: data.qty,
                                                5: data.category,
                                                6: data.site,
                                                7: '<button type="button" class="btn btn-sm btn-inverse" data-action="check-out" data-toggle="tooltip" title="Check-out"><i class="mdi mdi-logout"></i></button> <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"><i class="ti-pencil"></i></button> <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></button>',
                                            } ).draw();

                                            if(data.qty<data.qty_limit){
                                                $record_table.rows(row).nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"data-original-title":"Insufficient Stock(s)"}).addClass('text-danger text-bold');
                                            }else{
                                                $record_table.rows(row).nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"data-original-title":""}).addClass('text-default text-bold');
                                            }

                                            $record_table.rows(row).nodes().to$().attr({"data-id":data.id});

                                            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                                        }

                                        
                                            
                                        bootbox.hideAll();
                                    }

                                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                }
                            },error :function (data) {
                                $('#form-loader').toggle();
                                $.notification('Error','Internal Server Error','top-right','error','');
                            }
                        });

                        return false;
                    }
                },
                close: {
                    label: 'Close',
                    className: "btn-inverse",
                }
            }
        }).on("shown.bs.modal", function(e) {
            $('[data-toggle="tooltip"], .tooltip').tooltip('hide');

            $('button#add-category').click(function(){
                $.admin_category_form('','add','Add New Category','dropdown','asset');
            });
        
            $('button#add-site').click(function(){
                $.admin_site_form('','add','Add New Site','dropdown');
            });

            $('#modal-input-cost').autoNumeric('init', {'set': "0.00"});

            $('#modal-date-start').datepicker({
                format: 'M dd, yyyy',
                autoclose: true,
                //title: 'Start Date',
                endDate:'0d'
            }).on('changeDate', function(e) {
                var date2 = $(this).datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                $('#modal-date-expire').datepicker('destroy');
                $('#modal-date-expire').val('');
                $('#modal-date-expire').datepicker({
                    startDate: date2,
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    title: 'Complation Date',
                });
            });
        
            /*$.ajax({
                type:'POST',
                url :  $apps_base_url+'/admin/categories/fetchdata',
                dataType: "json",
                data: {
                    'ref' : 'all',
                    'apps' : 'consumable'
                },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').hide();
                    if(data.list&&data.list.length!=0){
                        var list = '';
                        $.each(data.list,function(i,v){
                            list += '<option value="'+v.cat_id+'">'+v.cat_name+'</option>';
                        });
                        $('select#modal-select-category').append(list);
                        $('select#modal-select-category').val(category);
                    }else{
                        
                    }
                },
                error :function (data) {
                    $.notification('Error','Unable to load category data list','top-right','error','');
                    $('#form-loader').hide();
                }
            });*/

            if(category_id&&category_name){
                $("select#modal-select-category").append('<option value="'+category_id+'" selected>'+category_name+'</option>');
            }

            $('select#modal-select-category').select2({
                dropdownParent: $(".modal"),
                placeholder : 'Select Category',
                ajax: {
                    url: $apps_base_url+'/admin/categories/fetchdata',//$apps_base_url+'/asset/fetch/list'
                    dataType: 'json',
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: function (params) {
                        var query = {
                            ref: '_with_keywords',
                            keywords: params.term,
                            apps : 'consumable'
                        }
    
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.list, function (item) {
                                return {
                                    text: item.cat_name,
                                    id: item.cat_id
                                }
                            })
                        };
                    }
                }
            });

            /*$.ajax({
                type:'POST',
                url :  $apps_base_url+'/admin/site/fetchdata',
                dataType: "json",
                data: {'ref' : 'all',},
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    var site_list = '';
                    if(data.list&&data.list.length!=0){
                        $.each(data.list,function(i,v){
                            site_list += '<option value="'+v.site_id+'">'+v.site_name+'</option>';
                        });
                    }

                    $('#modal-select-site').append(site_list);
                    $('#modal-select-site').val(site);
                },
                error :function (data) {
                    $.notification('Error','Unable to load site data list','top-right','error','');
                    $('#form-loader').hide();
                }
            });*/

            if(site_id&&site_name){
                $("select#modal-select-site").append('<option value="'+site_id+'" selected>'+site_name+'</option>');
            }
            $('#modal-select-site').select2({
                dropdownParent: $(".modal"),
                placeholder : 'Select Site',
                ajax: {
                    url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                    dataType: 'json',
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: function (params) {
                        var query = {
                            ref: '_keywords',
                            keywords: params.term,
                        }
    
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.list, function (item) {
                                return {
                                    text: item.site_name,
                                    id: item.site_id
                                }
                            })
                        };
                    }
                }
            });
        });

}

$.module_consumable_checkout = function module_consumable_checkout(id='', modal_title='Check-Out Consumable') {//
$layout_form = 	'<form class="form-material" id="modal-consumable-checkout-form" method="post">'+
                        '<div class="row">'+
                            '<div class="col-lg-12 form-group">'+
                               '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="modal-add-employee" data-original-title="Add New Employee"><i class="fas fa-plus"></i></button>'+
                               '<label class="form-control-label text-bold">Check-Out To <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                               '<div class="form-line">'+
                                   '<select class="form-control select2" id="modal-select-employee" name="employee">></select>'+ 
                               '</div>'+
                           '</div>'+
                            '<div class="col-lg-7 form-group m-b-0">'+
                                '<label class="form-control-label text-bold">Check-Out Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="checkout_date" id="modal-checkout-date" placeholder="Enter Chechout Date" value="">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-5 form-group">'+
                                '<label class="form-control-label text-bold">Qty<i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="number" class="form-control" name="qty" placeholder="Qty" value="1" min="1" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</form>';

                var consumable = bootbox.dialog({
                        closeButton: false,
                        backdrop: true,
                        animate: true,
                        title: modal_title,
                        message: $layout_form,
                        buttons: {
                            done: {
                                label: 'Save',
                                className: "btn-success",
                                callback: function () 
                                {
                                    $('#form-loader').toggle();
                                    $('#form-loader .loader__label').text('Checking out...');

                                    var form_data = new FormData($('form#modal-consumable-checkout-form')[0]);
                                    form_data.append('id',id);

                                    $.ajax({
                                        type:'POST',
                                        url :  $apps_base_url+'/consumables/checkout',
                                        dataType: "json",
                                        processData: false, // Don't process the files
                                        contentType: false,
                                        data: form_data,
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (data) {
                                            $(".tooltip").tooltip("hide");
                                            $('#form-loader').toggle();

                                            
                                            $('.form-group').removeClass('has-danger');
                                            $('.form-control-feedback').remove();
                                            $.each(data.val_error,function(i,v){
                                            $('form#modal-consumable-checkout-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                            $('form#modal-consumable-checkout-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                            });

                                            if(data.stat){
                                                if(data.stat=='success'){
                                                    bootbox.hideAll();
                                                    
                                                    if(data.stocks<data.stocks_limit){
                                                        $('tr[data-id='+id+'] td:nth-child(5)').attr({"data-toggle":'tooltip',"title":"Insufficient Stock(s)"}).addClass('text-danger text-bold').text(data.stocks);
                                                    }else{
                                                        $('tr[data-id='+id+'] td:nth-child(5)').attr({"data-toggle":'tooltip',"title":""}).addClass('text-default text-bold').text(data.stocks);
                                                    }
                                                }

                                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                            }
                                        },error :function (data) {
                                            $('#form-loader').toggle();
                                            $.notification('Error','Internal Server Error','top-right','error','');
                                        }
                                    });

                                    return false;
                                }
                            },
                            close: {
                                label: 'Close',
                                className: "btn-inverse",
                            }
                        }
                }).on("shown.bs.modal", function(e) {
                    consumable.attr("id", "consumable-checkout-modal");

                    $('button#modal-add-employee').click(function(){
                        $.admin_employee_form('','add','Add New Employee','dropdown');
                    });

                    $('[data-toggle="tooltip"], .tooltip').tooltip('hide');
        
                    $('#modal-checkout-date').datepicker({
                        format: 'M dd, yyyy',
                        autoclose: true,
                        endDate:'0d'
                    });

                    $('#modal-select-employee').select2({
                        dropdownParent: $(".modal"),
                        placeholder : 'Select Employee',
                        ajax: {
                            url : $apps_base_url+'/admin/employees/fetchdata',
                            dataType: "json",
                            type: "POST",
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: function (params) {
                                var query = {
                                    ref: '_keywords',
                                    keywords: params.term,
                                }

                                return query;
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data, function (item) {
                                        return {
                                            text: item.emp_fullname,
                                            id: item.employee_id
                                        }
                                    })
                                };
                            }
                        }
                    });
                });
}