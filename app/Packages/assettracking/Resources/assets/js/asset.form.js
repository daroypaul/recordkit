/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    var origin = $apps_base_url;
    var proto = window.location.hostname;

    $log_table = $('table#record-events-table').DataTable({
        //"order": [[ 1, "desc" ]],
        "scrollX": true,
        "aaSorting": [],
        columnDefs: [
            { type: 'date-dd-mmm-yyyy', targets: 1 }
        ],
        dom: 'lBfrtip',
        buttons: [
            /*'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print'*/
            //'pageLength',
            /*{
                extend: 'excelHtml5',
                title: 'Data export',
                className: 'm-l-10'
            },*/
            {
                extend: 'excelHtml5',
                //title: $('#report-export-title').text(),
                filename: function(){
                    //var d = new Date();
                    //var n = d.getTime();
                    return 'Asset (' + $('#asset-tag').text() + ') Events Log Report';
                },
                title: function(){
                    //var d = new Date();
                    //var n = d.getTime();
                    return 'Asset (' + $('#asset-tag').text() + ') Events Log Report';
                },
                className: 'm-l-10'
            },
            /*{
                extend: 'pdfHtml5',
                title: 'Data export',
            },*/
            'print',
        ],
    });

    $warranty_table = $('#record-warranties-table').DataTable({
        "scrollX": true,
        "order": [[ 1, "asc" ]],
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 5 ] }
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="ti-plus m-r-10"></i>Add Warranty',
                action: function ( e, dt, node, config ) {
                    $.asset_warranty_form();
                }
            },
            /*'pageLength',
            {
                extend: 'excelHtml5',
                title: 'Data export',
            },
            {
                extend: 'pdfHtml5',
                title: 'Data export',
            },*/
            'print',
        ]
    }).on('click', 'button[data-action]', function(e) {
        var id = $(this).parents('tr').data('id');
        var action = $(this).data('action');

        if(action='delete'&&id){
            var url = $apps_base_url+'/asset/warranty/delete';
            var settings = {
                title : "Delete Warranty",
                text:"Are you want to continue?",
                confirmButtonText:"Delete",
                showCancelButton:true,
                type : 'warning',
                confirmButtonColor: "#DD6B55",
                //closeOnConfirm: false
            }
    
        
            swal(settings).then(result => {
                var form_data = new FormData();
                form_data.append('asset_id', $('#_id').val()); 
                form_data.append('id', id);
                swal.close();
                
                if(result.value){
                    $('#form-loader').toggle();
                    $('#form-loader .loader__label').text('Deleting...');
                    $.ajax({
                        type:'POST',
                        url : url,
                        //dataType: "json",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            $('[data-toggle="tooltip"]').tooltip('hide');
                            $('#form-loader').toggle();
    
                            if(data.stat){
                                if(data.stat=='success'){
                                    var row = $('tr[data-id='+id+']');
                                    $warranty_table.row(row).remove().draw();
                                }
    
                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                            }
    
                            //swal(data.stat_title, data.stat_msg, data.stat);
                        },
                        error :function (data) {
                            //swal("Error", "Internal server error", "error");
                            $('#form-loader').toggle();
                            $.notification('Error','Internal Server Error','top-right','error','');
                        }
                    });
                }
            });
        }
    });

    if($('table#record-insurance-table').length){
        $insurance_table = $('table#record-insurance-table').DataTable({
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 7 ] }
            ],
            dom: 'Blfrtip',
            buttons: [
                {
                    text: '<i class="ti-plus m-r-10"></i>Link Insurance',
                    action: function ( e, dt, node, config ) {
                        //$.asset_warranty_form();
                        $.link_insurance_modal('single-asset','Link an Insurance','asset_view_page','','',$('#_id').val());
                    }
                },
                'print',
            ],
            "scrollX": true,
        }).on('click', 'button[data-action]', function(e) {
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');
    
            if(action=='detached'&&id){
                var url = $apps_base_url+'/insurances/detached-asset';
                var settings = {
                    title : "Detached Record",
                    text:"Your about to detached asset to this insurance. Are you want to continue?",
                    confirmButtonText:"Delete",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: false
                }
                
                swal(settings).then(result => {
                    swal.close();
                    
                    if(result.value){
                        $('#form-loader').toggle();
                        $('#form-loader .loader__label').text('Processing...');
    
                        $.ajax({
                            type:'POST',
                            url : url,
                            dataType: "json",
                            data: {
                                'id':id,
                                'by':'insurance'
                            },
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (data) {
                                $(".tooltip").tooltip("hide");
                                $('#form-loader').toggle();
                                
                                $('.table-danger').removeClass('table-danger');
                                if(data.stat){
                                    
                                    if(data.stat=='success'){
                                        $insurance_table.row('tr[data-id='+id+']').remove().draw();
                                    }else{
                                        $("tr[data-id='"+id+"']").addClass('table-danger');
                                    }
    
                                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                }
                            },
                            error :function (data) {
                                $('#form-loader').toggle();
                                $.notification('Error','Internal Server Error','top-right','error','');
                            }
                        });
                    }
                });            
            }
        });        
    }

    $(".nav-item").on('click', '[data-toggle=tab]', function() {
        //
        var target = $(this).attr('href');
        var a = $('.tab-content').find('.tab-pane').length;
        $('.tab-content').find('.tab-pane').removeClass('active');
        $('.tab-content').find(target).addClass('active');
        /*alert('yes, the click actually happened');
        $('.nav-tabs a[href="#samosas"]').tab('show');*/
    });

    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    } );


    if($('input[name=asset_image]').length!=0){
        $('.dropify').dropify();
    }

    /*if($('table#record-table').length!=0){
        var list = $('table#record-table');
        list.footable().on('click', 'a[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');
            var table = list.data('footable');


            
        });
    }*/

    $('#btn-generate-tag').click(function(){
        $('#form-loader').toggle();
        $.ajax({
            type:'POST',
            url :  $apps_base_url+'/asset/fetch/tag',
            //dataType: "json",
            data: {'ref':'asset_tag'},
            processData: false,
            contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                console.log(data);
                $('#form-loader').toggle();

                if(data.new_id){
                    $('#asset_tag').val(data.new_id);
                    $('#_tag_num').val(data.new_counter);
                }
                
            },
            error :function (data) {
                $('#form-loader').toggle();
                console.log('error');
            }
        });
    });

    if($('#asset_category').length&&$('#asset_site').length&&$('#asset_location').length&&$('#asset_status').length){
        $('#asset_category').select2({
            placeholder : 'Select a Site',
            ajax: {
                url: $apps_base_url+'/admin/categories/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        ref: '_with_keywords',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.list, function (item) {
                            return {
                                text: item.cat_name,
                                id: item.cat_id
                            }
                        })
                    };
                }
            }
        });

        $('#asset_site').select2({
            placeholder : 'Select a Site',
            ajax: {
                url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        ref: '_keywords',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.list, function (item) {
                            return {
                                text: item.site_name,
                                id: item.site_id
                            }
                        })
                    };
                }
            }
        }).on('select2:select', function (e) {
            $('#asset_location').val(null).trigger('change');
        });


        $('#asset_location').select2({
            placeholder : 'Select a Site',
            ajax: {
                url: $apps_base_url+'/admin/location/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        site_id: $('#asset_site').val(),
                        ref: '_keywords',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.location_name,
                                id: item.location_id
                            }
                        })
                    };
                }
            }
        });

        $('#asset_category').change(function(){
            var cat = $(this).val();
            var id = $('#_id').val();
            var form_data = new FormData();
            form_data.append('ref', 'asset');
            form_data.append('link_id', id);
            form_data.append('id', cat);
            $('#form-loader').toggle();

            $.ajax({
                type:'POST',
                url : $apps_base_url+'/asset/fetch/custom-field',
                //dataType: "json",
                data:  form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    if(data){
                        $('#custom-fields-container').html(data);
                    }else{
                        $('#custom-fields-container').html('');   
                    }
                    $('#form-loader').toggle();
                    
                },
                error :function (data) {
                    $('#form-loader').toggle();
                }
            });
        });

        $('#asset_status').select2();
    }

    $('button#add-category').click(function(){
        $.admin_category_form('','add','Add New Asset Category','dropdown','asset');//id='', ref='', title='Add New Asset Category', append_type='table', name='', remarks=''
    });

    $('button#add-site').click(function(){
        $.admin_site_form('','add','Add New Site','dropdown');
    });

    $('button#add-location').click(function(){
        var site_id = $('#asset_site').val();
        $.admin_location_form('',site_id, 'add','Add New Location','dropdown');
    });

    /*$('#asset_site').change(function(){
        var site = $(this).val();
        var form_data = new FormData();
        var target = $('#asset_location');
        form_data.append('site_id', site);
        $('#form-loader').toggle();

        $.ajax({
            type:'POST',
            url : $apps_base_url+'/admin/location/fetchdata',
            dataType: "json",
            data:  form_data,
            processData: false,
            contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                if(data.length){
                    var append = '';
                    append += '<option value="">-- Select Location</option>';
                    $.each(data, function(i, item) {
                        append += '<option value="'+item.location_id+'">'+item.location_name+'</option>';
                    });
                    target.html(append);
                }else{
                    $('#asset_location').html('<option value="">-- Select Location</option>');
                }
                $('#form-loader').toggle();
                
            },
            error :function (data) {
                $('#form-loader').toggle();
            }
        });
    });*/

    $('#form').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Processing...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);
        if($('input[type=file]').length){
            form_data.append('asset_image', $('input[type=file]')[0].files[0]); 
        }

        if($('#_id').val()!=0||$('#_id').val()){
            form_data.append('id', $('#_id').val()); 
        }

        form_data.append('action', $('#_type').val()); 

        $.ajax({
            type:'POST',
            url : window.location,
            //dataType: "json",
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching Data...');

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.errors,function(i,v){
                   $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                });
                
                $.each(data.custom_errors,function(i,v){
                    $('input[name="custom_field_value[]"]:eq('+v.key+')').parents('.form-group').addClass('has-danger');
                    $('input[name="custom_field_value[]"]:eq('+v.key+')').parents('.form-group').append('<small class="form-control-feedback">'+v.msg+'</small>');
                });

                /*if(data.error){
                    //$('#form-notification').html(data.error);
                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }else*/
                if(data.stat){
                    if(data.stat=='success'){
                        if($('input[name=is_leasable]:checked').length!=0){
                            $('input[name=is_leasable]').attr('checked', true);
                        }else{
                            $('input[name=is_leasable]').attr('checked', false);
                        }

                        if($('#_type').val()=='add'){
                            $('#form')[0].reset();  

                            $('#form-notification').html(data.success);

                            //if(!$('#_id').val()||$('#_id').val()==0){
                                $('#custom-fields-container').html(''); 
                                //$('#asset_location').html('<option value="">-- Select Location --</option>');
                            //}

                            $('button[class=dropify-clear]').click();

                            //$('#asset_category').
                            $('#asset_category, #asset_site, #asset_location, #asset_status').val(null).trigger('change');
                        }
                    }

                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    /*
    |------------------|
    | Maintenance Form |
    |------------------|
    */

    $('button#btn-new-maintenance').click(function(e){
        $.asset_maintenance_form('','add');
    });

    

    $maintenance_table = $('#record-maintenance-table').DataTable({
        "scrollX": true,
        "order": [[ 3, "desc" ]],
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 10 ] }
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="ti-plus m-r-10"></i>Add Maintenance',
                action: function ( e, dt, node, config ) {
                    $.asset_maintenance_form('','add');
                }
            },
            'pageLength',
            {
                extend: 'excelHtml5',
                title: 'Data export',
            },
            {
                extend: 'pdfHtml5',
                title: 'Data export',
            },
            'print',
        ],
        
    }).on('click', 'button[data-action]', function(e) {
        var id = $(this).parents('tr').data('id');
        var action = $(this).data('action');

        if(action=='delete'&&id){
            var url = $apps_base_url + '/asset/maintenance/delete';
            var settings = {
                title : "Delete Maintenance Record",
                text:"Are you want to continue?",
                confirmButtonText:"Delete",
                showCancelButton:true,
                type : 'warning',
                confirmButtonColor: "#DD6B55",
                //closeOnConfirm: false
            }
    
        
            swal(settings).then(result => {
                var form_data = new FormData();
                form_data.append('asset_id', $('#_id').val()); 
                form_data.append('id', id);
                swal.close();
                if(result.value){
                    $('#form-loader').toggle();
                    $('#form-loader .loader__label').text('Deleting...');

                    $.ajax({
                        type:'POST',
                        url : url,
                        dataType: "json",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            $('#form-loader').toggle();
    
                            if(data.stat){
                                if(data.stat=='success'){
                                    var row = $('tr[data-id='+id+']');
                                    $maintenance_table.row(row).remove().draw();
                                }
    
                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                            }
                        },
                        error :function (data) {
                            //swal("Error", "Internal server error", "error");
                            $('#form-loader').toggle();
                            $.notification('Error','Internal Server Error','top-right','error','');
                        }
                    });
                }
            });
        }else if(action=='edit'&&id){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Fetching...');

            var form_data = new FormData();
            form_data.append('id', id);
            $.ajax({
                type:'POST',
                url : $apps_base_url + '/asset/maintenance/fetch',
                dataType: "json",
                data: form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();

                    if(data){
                        $.asset_maintenance_form(data.id,'edit','Update Asset Maintenance',data.maintain_title,data.supplier_id,data.maintain_type,data.maintain_stat,data.date_start,(data.date_completion)?data.date_completion:'',(data.date_completed)?data.date_completed:'',data.maintain_cost,data.maintain_remarks,data.is_warranty, (data.suppliers) ? data.suppliers.supplier_name : '');
                    }else{
                        $.notification('Error','No data found','top-right','error','');
                    }
                },
                error :function (data) {
                    //swal("Error", "Internal server error", "error");
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }
    });
    

    ////////////////////
    // Image Upload  //
    //////////////////

    $('#btn-image-upload').click(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Uploading image...');
        //$('#form-notification').html('');s
        var form_data = new FormData();
        form_data.append('id', $('#_id').val()); 
        form_data.append('asset_image', $('#upload-image-form input[type=file]')[0].files[0]); 
        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').remove();

        $.ajax({
            type:'POST',
            url : $apps_base_url + '/asset/upload/image',
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching Data...');

                if(data.val_error){
                    $.each(data.val_error,function(i,v){
                        $.notification('Error',v[0],'top-right','error','');
                     });
                }else if(data.stat){
                    if(data.stat=='success'){
                        var append =    ''+
                                            '<div class="card">'+
                                                        '<a class="image-popup-vertical-fit" href="'+$apps_base_url+'/uploaded/file/'+data.name+'"> '+
                                                            '<img class="card-img-top img-responsive" src="'+$apps_base_url+'/uploaded/file/'+data.name+'" alt="Asset Image" />'+
                                                        '</a>'+
                                                    '<div class="card-body" data-image="'+data.id+'">'+
                                                        '<button type="button" href="#" data-action="set-cover" class="btn btn-xs btn-inverse">Set as Cover</button> '+
                                                        '<button type="button" href="#" data-action="delete" class="btn btn-xs btn-danger">Delete</button>'+
                                                    '</div>'+
                                            '</div>';
                        $('#image-gallery').append(append);
                        $('#remove-image-upload').click();
                    }

                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    $('div#image-gallery').on('click', 'button[data-action]', function(e) {
        var action = $(this).data('action');
        var id = $(this).parents('.card-body').data('image');
        var img = $(this).parent('.card-body').parent('.card');
        var self = $(this);

        var form_data = new FormData();
        form_data.append('asset_id', $('#_id').val());
        form_data.append('id', id);

        if(action=='delete'){
            var url = $apps_base_url+'/asset/img/delete';
            var settings = {
                title : "Delete Image",
                text:"Are you want to continue?",
                confirmButtonText:"Delete",
                showCancelButton:true,
                type : 'warning',
                confirmButtonColor: "#DD6B55",
                //closeOnConfirm: false
            }

            swal(settings).then(result => {
                swal.close();
                
                if(result.value){
                    $('#form-loader').toggle();
                    $('#form-loader .loader__label').text('Deleting image...');

                    $.ajax({
                        type:'POST',
                        url : url,
                        //dataType: "json",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            $('#form-loader').toggle();

                            if(data.stat=='success'){
                                var animate = 'zoomOut';
                                img.removeClass().addClass(animate + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                                    $(this).remove();
                                });
                            }

                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        },
                        error :function (data) {
                            $.notification('Error','Internal Server Error','top-right','error','');
                            $('#form-loader').toggle();
                        }
                    });
                }
            });
        }else if(action=='set-cover'){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Setting up image as cover...');
            var url = $apps_base_url+'/asset/img/set-cover';
            $.ajax({
                type:'POST',
                url : url,
                //dataType: "json",
                data: form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();

                    if(data.stat=='success'){
                        var src = img.find('img').attr('src');
                        //src = origin+'/uploaded/file/'+src;

                        $('#cover-image').html('<img src="'+src+'" class="img-responsive img-rounded" />')
                        //$('#cover-image').attr('src', src);

                        $('#image-gallery .card-body').each(function(){//Set new button to each image card
                            var check_btn = $(this).find('button[data-action=set-cover]').length;

                            if(check_btn==0){
                                $(this).prepend('<button type="button" href="#" data-action="set-cover" class="btn btn-xs btn-inverse">Set as Cover</button> ');
                            }
                        });

                        self.remove();//Remove the Set as Cover Button
                    }

                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }
    });

    ///////////////////////
    // Document Upload  //
    /////////////////////

    $document_table = $('table#record-document-table').DataTable({
        "scrollX": true,
        "order": [[ 0, "asc" ]],
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 4 ] }
        ],
    }).on('click', 'button[data-action=delete]', function(e) {
        e.preventDefault();
        var id = $(this).parents('tr').data('docs');
        var url = $apps_base_url+'/asset/docs/delete';
        var settings = {
            title : "Delete Document",
            text:"Are you want to continue?",
            confirmButtonText:"Delete",
            showCancelButton:true,
            type : 'warning',
            confirmButtonColor: "#DD6B55",
            //closeOnConfirm: false
        }

    
        swal(settings).then(result => {
            var form_data = new FormData();
            form_data.append('asset_id', $('#_id').val()); 
            form_data.append('id', id);
            swal.close();
            
            if(result.value){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Deleting document...');
                $.ajax({
                    type:'POST',
                    url : url,
                    //dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('[data-toggle="tooltip"]').tooltip('hide');
                        $('#form-loader').toggle();

                        if(data.stat){
                            if(data.stat=='success'){
                                var row = $('tr[data-docs='+id+']');
                                $document_table.row(row).remove().draw();
                            }

                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }

                        //swal(data.stat_title, data.stat_msg, data.stat);
                    },
                    error :function (data) {
                        //swal("Error", "Internal server error", "error");
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                    }
                });
            }
        });

    });

    $('#btn-docs-upload').click(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Uploading document...');
        //$('#form-notification').html('');s
        var form_data = new FormData();
        form_data.append('id', $('#_id').val()); 
        form_data.append('asset_file', $('#upload-docs-form input[type=file]')[0].files[0]); 
        form_data.append('file_desc', $('input[name=file_desc]').val()); 

        $('.form-group').removeClass('has-danger');
        $('.form-control-feedback').remove();


        $.ajax({
            type:'POST',
            url : $apps_base_url + '/asset/upload/docs',
            data:  form_data,
            //dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                console.log(data);
                $('[data-toggle="tooltip"]').tooltip('hide');

                $('#form-loader').toggle();

                $.each(data.val_error,function(i,v){
                    $('#upload-docs-form #'+i+' input[type=file], #upload-docs-form input[name='+i+']').parents('.form-group').addClass('has-danger');
                    $('#upload-docs-form #'+i+' input[type=file], #upload-docs-form input[name='+i+']').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                    if(data.stat=='success'){
                        var row =    '<tr data-docs="'+data.id+'">'+
                                    '<td>'+data.desc+'</td>'+
                                    '<td>'+data.ext+'</td>'+
                                    '<td>'+data.uploaded_date+'</td>'+
                                    '<td>'+data.uploaded_by+'</td>'+
                                    '<td>'+
                                        //'<a href="'+$apps_base_url+'/uploaded/file/'+data.name+'" class="btn btn-xs btn-info">Download</a> '+
                                        //'<button class="btn btn-xs btn-danger" data-action="delete">Delete</button>'+
                                        '<a href="'+$apps_base_url+'/uploaded/file/'+data.name+'" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Download"><i class="ti-download"></i></a> <button class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>'+
                                    '</td>'+
                                '</tr>';

                            /* $('table#record-document-table').DataTable().destroy();
                                $('table#record-document-table tbody').append(row);
                                $('table#record-document-table').DataTable({
                                    "order": [[ 0, "asc" ]],
                                });*/

                                var row = $document_table.row.add( {
                                    0: data.desc,
                                    1: data.ext,
                                    2: data.uploaded_date,
                                    3: data.uploaded_by,
                                    4: '<a href="'+$apps_base_url+'/uploaded/file/'+data.name+'" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Download"><i class="ti-download"></i></a> <button class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>',

                                    //'<a href="'+$apps_base_url+'/uploaded/file/'+data.name+'" class="btn btn-xs btn-info">Download</a> '+'<button class="btn btn-xs btn-danger" data-action="delete">Delete</button>'
                                } ).draw();

                                $document_table.rows(row).nodes().to$().attr("data-docs", data.id);

                                $('#remove-docs-upload').click();
                                $('input[name=file_desc]').val('');
                    }

                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    
});


$.asset_maintenance_form = function asset_maintenance_form(id, ref='add',modal_title='New Asset Maintenance',title='',supplier='',type='',status='',start_date='',completion_date='',completed_date='',cost='',remarks='',is_warranty='', supplier_name='') {//
    $layout_form = 	'<form class="form-material" id="modal-maintenance-form" method="post">'+
                        '<div class="row">'+
                            '<div class="col-sm-12 form-group">'+
                                '<label class="form-control-label text-bold">Title <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="maintenance_title" placeholder="Title" value="'+title+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-12 form-group">'+
                                '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-supplier" data-original-title="Add New Supplier"><i class="fas fa-plus"></i></button>'+
                                '<label class="form-control-label text-bold">Supplier <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<select class="form-control" id="model-select-supplier" name="service_provider" placeholder="Select Supplier/Provider"></select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-4 form-group">'+
                                '<label class="form-control-label text-bold">Type <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<select class="form-control custom-select" name="maintenance_type" id="modal-input-type">'+
                                        '<option value="">[ Select Type ]</option>'+
                                        '<option value="maintenance">Maintenance</option>'+
                                        '<option value="repair">Repair</option>'+
                                        '<option value="upgrade">Upgrade</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-4 form-group">'+
                                '<label class="form-control-label text-bold">Status <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<select class="form-control custom-select" name="stat" id="modal-input-status">'+
                                        '<option value="active">Active</option>'+
                                        '<option value="completed">Completed</option>'+
                                    '</select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-6 col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Start Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="start_date" id="modal-date-start" placeholder="Start Date" value="'+start_date+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-6 col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Completion Date</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="completion_date" id="modal-date-completion" placeholder="Complation Date" value="'+completion_date+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-12 col-sm-6 col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Completed Date</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="completed_date" id="modal-date-complated" placeholder="Complated Date" value="'+completed_date+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-4 col-lg-4 form-group">'+
                                '<label class="form-control-label text-bold">Cost</label>'+
                                '<div class="form-line">'+
                                    '<input type="text" class="form-control" name="cost" id="modal-input-cost" placeholder="Cost" value="'+cost+'">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-12 form-group m-b-0">'+
                                '<label class="form-control-label text-bold">Remarks</label>'+
                                '<div class="form-line">'+
                                    '<textarea class="form-control" placeholder="Remarks" name="remarks">'+remarks+'</textarea>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-12 form-group">'+
                                '<div class="form-line">'+
                                    '<input type="checkbox" id="modal-is-warranty" class="filled-in" name="is_warranty" value="1" />'+
                                    '<label for="modal-is-warranty" class="m-b-0">Warranty support</label>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</form>';
    
        bootbox.dialog({
            closeButton: false,
            backdrop: true,
            animate: true,
            title: modal_title,
            message: $layout_form,
            size: 'large',
            buttons: {
                done: {
                    label: 'Save',
                    className: "btn-success",
                    callback: function () {
                        $('#form-loader').toggle();
                        $('#form-loader .loader__label').text('Saving...');

                        var form_data = new FormData($('form#modal-maintenance-form')[0]);
                        form_data.append('asset_id', $('#_id').val());
                        form_data.append('ref',ref);
                        form_data.append('id',id);
                        $.ajax({
                            type:'POST',
                            url : $apps_base_url + '/asset/maintenance/add',
                            //dataType: "json",
                            processData: false, // Don't process the files
                            contentType: false,
                            data: form_data,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (data) {
                                $('[data-toggle="tooltip"]').tooltip('hide');
                                $('#form-loader').toggle();
                                $('.form-group').removeClass('has-danger');
                                $('.form-control-feedback').remove();
                                $.each(data.val_error,function(i,v){
                                   $('form#modal-maintenance-form input[name="'+i+'"], form#modal-maintenance-form select[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                   $('form#modal-maintenance-form input[name="'+i+'"], form#modal-maintenance-form select[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                });

                                if(data.stat){
                                    if(data.stat=='success'){
                                        if(ref=='edit'){
                                            var row = $('tr[data-id='+id+']');
                                            $maintenance_table.row(row).remove().draw();
                                        }
                                        
                                                        var row = $maintenance_table.row.add( {
                                                            0: data.type,
                                                            1: data.title,
                                                            2: data.remarks,
                                                            3: data.start_date,
                                                            4: data.completion_date,
                                                            5: data.completed_date,
                                                            6: data.warranty,
                                                            7: data.supplier,
                                                            8: data.recorded_by,
                                                            9: data.status,
                                                            10: '<button class="btn btn-xs btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button> <button class="btn btn-xs btn-danger" data-action="delete" data-action="edit" data-toggle="tooltip" title="Delete Record"><i class="ti-trash"></i></button>',
                                                        } ).draw();

                                                        $maintenance_table.rows(row).nodes().to$().attr("data-id", data.id);
                                        bootbox.hideAll();
                                    }

                                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                }
                            },error :function (data) {
                                $('#form-loader').toggle();
                                $.notification('Error','Internal Server Error','top-right','error','');
                            }
                        });

                        return false;
                    }
                },
                close: {
                    label: 'Close',
                    className: "btn-inverse",
                }
            }
        }).on("shown.bs.modal", function(e) {
            $('#modal-input-type').val(type);
            $('#modal-input-status').val(status);
            $('#modal-is-warranty').attr('checked', (is_warranty)?true:false);
            $('#modal-input-cost').autoNumeric('init', {'set': "0.00"});
            $('#modal-date-start').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                title: 'Start Date',
                endDate:'0d'
            }).on('changeDate', function(e) {
                var date2 = $(this).datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                $('#modal-date-completion,#modal-date-complated').datepicker('destroy');
                $('#modal-date-completion').val('');
                $('#modal-date-completion,#modal-date-complated').datepicker({
                    startDate: date2,
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    title: 'Complation Date',
                });
            });

            $('#modal-date-completion,#modal-date-complated').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                title: 'Complation Date',
                startDate:'0d'
            });

            /*var form_data = new FormData();
            //form_data.append('provider', 1); 
            form_data.append('ref', 'all'); 

            $.ajax({
                type:'POST',
                url : $apps_base_url+'/admin/suppliers/fetchdata',
                dataType: "json",
                processData: false, // Don't process the files
                contentType: false,
                data: form_data,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {

                    var append = '<option value="">::: Select Supplier :::</option>';
                    $.each(data, function(i, item) {
                        append += '<option value="'+item.supplier_id+'">'+item.supplier_name+'</option>';
                    });
                    $('select#model-select-supplier').html(append).val(supplier);
                    //$('select#model-select-supplier#model-select-supplier').val(supplier);
                },error :function (data) {
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });*/

            if(supplier&&supplier_name){
                $("select#model-select-supplier").append('<option value="'+supplier+'" selected>'+supplier_name+'</option>');
            }
            $('select#model-select-supplier').select2({
                dropdownParent: $(".modal"),
                placeholder : 'Select Supplier',
                ajax: {
                    url: $apps_base_url+'/admin/suppliers/fetchdata',//$apps_base_url+'/asset/fetch/list'
                    dataType: 'json',
                    type: "POST",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: function (params) {
                        var query = {
                            ref: '_keywords',
                            keywords: params.term,
                        }
    
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.supplier_name,
                                    id: item.supplier_id
                                }
                            })
                        };
                    }
                }
            });

            $('button#add-supplier').click(function(){
                $.admin_supplier_form('','add','Add New Supplier','dropdown');
            });
        });

}