/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    var origin = $apps_base_url;
    var proto = window.location.hostname;
    
    //Store all selected asset
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';
    //Parameter to check previous process result result: [0] No process done/process reset [1] process done, updated in Status modal form at modal.js
    $status_updated = 0;


    if($('table#record-table').length!=0){
        $record_table = $('#record-table').DataTable({
            "order": [[ 2, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,1,12 ] }
            ],
            //'searching': false,
            "dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-xs-12 col-sm-2  col-md-2  col-lg-2" B>'+
                    '<"col-sm-6 col-md-5 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-2">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: $('#report-export-title').text(),
                },
                'print',
            ],
            fnInitComplete: function(){
                var add_element = '<a href="'+$apps_base_url+'/asset/add" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Asset"><i class="fas fa-plus"></i></a>';
                var bulk_action =  
                                '<div class="form-group m-b-0"><div class="input-group">'+
                                    '<select class="form-control" id="bulk-action-option">'+
                                        '<option value="">- Bulk Action -</option>'+
                                        '<option value="Damage">Damage</option>'+
                                        '<option value="Dispose">Dispose</option>'+
                                        '<option value="Donate">Donate</option>'+
                                        '<option value="Lost">Lost</option>'+
                                        '<option value="Sale">Sale</option>'+
                                        '<option value="Delete">Delete</option>'+
                                    '</select>'+            
                                    '<span class="input-group-btn">'+
                                    '<button id="btn-bulk-action" class="btn btn-default" type="button" tabindex="-1">Apply</button>'+
                                    '</span>'+
                                '</div></div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);
            },
            "scrollX": true,
        }).on('click', 'a[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');


            if(action=='delete'){
                var url = $apps_base_url+'/asset/delete';
                var settings = {
                    title : "Delete Record",
                    text:"Are you want to continue?",
                    confirmButtonText:"Delete",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: false
                }
            }else if(action=='archive'){
                var url = $apps_base_url+'/asset/archive';
                var settings = {
                    title : "Archive Record",
                    text:"Are you want to continue?",
                    confirmButtonText:"Archive",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: true
                }
            }

            
            swal(settings).then(result => {
                var form_data = new FormData();
                form_data.append('id', id);
                swal.close();
                
                if(result.value){
                    $.ajax({
                        type:'POST',
                        url : url,
                        //dataType: "json",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            if(data.stat=='success'){
                                var row = $('tr[data-id='+id+']');
                                //table.removeRow(row);

                                $record_table.row( $('tr[data-id='+id+']') ).remove().draw();
                            }

                            //$.notification(data.stat_title,data.stat_msg,'top-right',data.stat,15000);
                            swal(data.stat_title, data.stat_msg, data.stat); 
                        },
                        error :function (data) {
                            //$.notification('Error','Internal server error','top-right','error',15000);
                            swal("Error", "Internal server error", "error");
                        }
                    });
                }
            });
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;
            $('.table-danger').removeClass('table-danger');

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }

            if($sid.length!=0){
                $.each($sid,function(i,v){
                    $("tr[data-id='"+v+"']").find("td[data-col='status']").text($stat_selected);
                });
            }
        });
    }

    $('#btn-bulk-action').click(function(){
        var opt = $('#bulk-action-option').val();
        $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
        $('#bulk-action-option').removeClass('form-control-danger');

        if($selected_asset.length==0){
            $.notification('Error','Select an asset','top-right','error','');
        }else if(opt==''){
            $('#bulk-action-option').parents('.form-group').addClass('has-danger');
            $('#bulk-action-option').addClass('form-control-danger');
            $.notification('Error','Select an action option','top-right','error','');
        }else if(opt=='Delete'){
            if($selected_asset.length!=0){
                var url = $apps_base_url+'/asset/delete';
                var settings = {
                    title : "Delete Record(s)",
                    text:"Your about to delete "+$selected_asset.length+" asset(s). Are you want to continue?",
                    confirmButtonText:"Delete",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: false
                }
                
                swal(settings).then(result => {
                    var form_data = new FormData();
                    $.each($selected_asset, function(i, v){
                        form_data.append('id[]', v);
                    });
                    swal.close();
                    
                    if(result.value){
                        $.ajax({
                            type:'POST',
                            url : url,
                            dataType: "json",
                            data: form_data,
                            processData: false,
                            contentType: false,
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            success: function (data) {
                                $eid = [];
                                $sid = [];
                                $('.table-danger').removeClass('table-danger');
                                $('input#select-all').prop('checked', false);
                                $('input.selected-item:checked').prop('checked', false);
                                $('#bulk-action-option').val('');
                                $selected_asset = [];
                                $status_updated = 1;

                                if(data.sid.length!=0){
                                    $.notification('Success','Selected asset(s) successfully deleted.','top-right','success','');

                                    $.each(data.sid,function(i,v){
                                        $record_table.row('tr[data-id='+v+']').remove().draw();
                                    });
                                }

                                if(data.eid.length!=0){
                                    $.notification('Error Process','Selected asset(s) can not be deleted. See highlighten row','top-right','error','');
                                    
                                    $.each(data.eid,function(i,v){
                                        $("tr[data-id='"+v+"']").addClass('table-danger');
                                    });
                                    
                                    $eid = data.eid;
                                }

                                if(data.stat){
                                    if(data.stat=='success'){
                                        if($('#_type').val()=='add'){
                                            $('#form')[0].reset();  
                                        }
                                    }
            
                                    $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                }
                            },
                            error :function (data) {
                                $.notification('Error','Internal Server Error','top-right','error','');
                            }
                        });
                    }
                });
            }
        }else{
            if(opt!='Delete'&&opt!='Email'){
                $status_updated = 0;
                $.asset_status_modal($selected_asset, opt + ' Asset', opt);
            }
        }
    });

    $('input#select-all').click(function(){
        var c = this.checked;

        $('input.selected-item').each(function(){
            var id = $(this).parents('tr').data('id');

            $(this).prop('checked', c);

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }
        });
    });

    if($('table#leasable-record-table').length!=0){
        $leasable_table = $('#leasable-record-table').DataTable({
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,10 ] }
            ],
            //'searching': false,
        });
    }

    $('#report-export-title').keyup(function(i, v){
        var title = $(this).text();

        $('title').text('RecordKits Application Suite - Beta  |' + title);
    });
});