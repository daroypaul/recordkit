/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    //Initialize Date Picker

    $(".select2").select2();

    $('table#selected-asset-table').DataTable({
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 6 ] }
         ],
    });

    $('#lease-start').datepicker({
        format: 'M dd, yyyy',
        autoclose: true,
        title: 'Lease Date Start',
        endDate:'0d'
    }).on('changeDate', function(e) {
        var date2 = $(this).datepicker('getDate');
        date2.setDate(date2.getDate());
        $('#lease-expire').datepicker('destroy');
        $('#lease-expire').val('');
        $('#lease-expire').datepicker({
            startDate: date2,
            //endDate:'0d',
            format: 'M dd, yyyy',
            autoclose: true,
            title: 'Complation Date',
        });
    });

    $('#lease-expire').datepicker({
        format: 'M dd, yyyy',
        autoclose: true,
        title: 'Lease Date Expire',
        startDate:'0d'
    });

    $('#requestor, #customer').select2({
        ajax: {
            url: $apps_base_url+'/admin/employees/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.emp_fullname,
                            id: item.employee_id
                        }
                    })
                };
            }
          }
    });

    $('#asset_site').select2({
        placeholder : 'Select a Site',
        ajax: {
            url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.site_name,
                            id: item.site_id
                        }
                    })
                };
            }
          }
    }).on('select2:select', function (e) {
        $('#asset_location').val(null).trigger('change');
    });


    $('#asset_location').select2({
        placeholder : 'Select a Site',
        ajax: {
            url: $apps_base_url+'/admin/location/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    site_id: $('#asset_site').val(),
                    ref: '_keywords',
                    keywords: params.term,
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.location_name,
                            id: item.location_id
                        }
                    })
                };
            }
          }
    });

    $('button#add-employee-1,button#add-employee-2').click(function(){
        $.admin_employee_form('','add','Add New Employee','dropdown');
    });

    $('button#add-site').click(function(){
        $.admin_site_form('','add','Add New Site','dropdown');
    });

    $('button#add-location').click(function(){
        var site_id = $('#asset_site').val();
        $.admin_location_form('',site_id, 'add','Add New Location','dropdown');
    });

    /*$('#asset_site').change(function(){
        var site = $(this).val();
        var form_data = new FormData();
        var target = $('#asset_location');
        form_data.append('site_id', site);
        $('#form-loader').toggle();

        $.ajax({
            type:'POST',
            url : $apps_base_url+'/admin/location/fetchdata',
            dataType: "json",
            data:  form_data,
            processData: false,
            contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                if(data.length){
                    var append = '';
                    append += '<option value="">-- Select Location</option>';
                    $.each(data, function(i, item) {
                        append += '<option value="'+item.location_id+'">'+item.location_name+'</option>';
                    });
                    target.html(append);
                }else{
                    $('#asset_location').html('<option value="">-- Select Location</option>');
                }
                $('#form-loader').toggle();
                
            },
            error :function (data) {
                $('#form-loader').toggle();
            }
        });
    });*/

    $('button#btn-current-list').click(function(){
        if(!$('select#requestor').val()||$('select#requestor').val()==0){
            $.notification('Error','Select an Requestor','top-right','error','');
        }else{
            var requestor = $('select#requestor option:selected').text();
            current_leased_modal($('select#requestor').val(), requestor);
        }
    });


    $('button#btn-add-asset').click(function(){
        var lease_start = $('input#lease-start').val();
        var lease_expire = $('input#lease-expire').val();

        if(!lease_start){
            $.notification('Error','Please enter lease start date','top-right','error','');
        }else if(!lease_expire){
            $.notification('Error','Please enter lease expire date','top-right','error','');
        }else if(!$('select#requestor').val()){
            $.notification('Error','Select an Requestor','top-right','error','');
        }else{
            $.search_asset_modal('Available Leasable Asset', 'lease', lease_start, lease_expire);
        }
    });

    $('tbody#asset-row-data').on('click', 'button#btn-delete-row', function(e) {
        $('table#selected-asset-table').DataTable().destroy();
        $(this).parents('tr').remove();
        $('table#selected-asset-table').DataTable({
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 6 ] }
             ],
        });
    });

    $('#btn-save-draft').click(function(){
        //form_data.append('id', $('#_id').val()); 
        $('#_stat').val('1');
        $('#form').submit();
    });
    
    $('#btn-done').click(function(){
        //form_data.append('id', $('#_id').val()); 
        $('#_stat').val('0');
        $('#form').submit();
    });

    $('#form').submit(function(e){
        e.preventDefault();
        $('#form-loader .loader__label').text('Processing...');
        $('#form-notification').html('');
        
        var total_asset = $('tr[data-id]').length;

        if(total_asset==0){
            $.notification('Error','Add at least one (1) asset','top-right','error','');
        }else{
            $('#form-loader').toggle();
            var form_data = new FormData($(this)[0]);

            $.ajax({
                type:'POST',
                url : window.location,
                data:  form_data,
                dataType: "json",
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {

                    $('#form-loader').toggle();

                    $('.form-group').removeClass('has-danger');
                    $('.form-control-feedback').remove();
                    $.each(data.val_errors,function(i,v){
                        $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                        $('input[name="'+i+'"], select[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                    });

                    if(data.stat){
                        if(data.stat=='success'){
                            $('#form')[0].reset();
                            $('table#selected-asset-table').DataTable().destroy();
                            $('tbody#asset-row-data').html('');
                            $('table#selected-asset-table').DataTable({
                                "aoColumnDefs": [
                                    { 'bSortable': false, 'aTargets': [ 6 ] }
                                ],
                            });

                            //$('#asset_location').html('<option value="">-- Select Location --</option>');
                            //$(".select2").select2();
                            $('#requestor, #customer, #asset_site, #asset_location').val(null).trigger('change');
                        }

                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error',15000);
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        }
    });


    //Modal

    function current_leased_modal(id, requestor='') {//
        $layout_form = 	'<div class="table-responsive" id="search-result"></div>';
            var title = (requestor) ? 'Current Leased Asset of '+requestor:'Current Leased Asset by Requestor';

            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                size: 'large',
                className: "modal-custom-lg",
                buttons: {
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('#search-result').html('<p class="text-center"><i class="fa fa-spin fa-gear" style="font-size:2em;"></i><br/>Fetching records...</p>');
                
                var table = '';
                $.ajax({
                    type:'POST',
                    url : $apps_base_url+'/asset/fetch/leased-by-requestor',
                    dataType: "json",
                    data : {
                        id : id
                    },
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        if(data.list.length!=0){
                            table += '<table id="leased-asset-table" class="table table-condensed">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th></th>'+
                                                '<th>Tag</th>'+
                                                '<th>Description</th>'+
                                                '<th>Model</th>'+
                                                '<th>Serial</th>'+
                                                '<th>Category</th>'+
                                                '<th>Leased&nbsp;Remarks</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>';
                            $.each(data.list, function(i, v) {
                                if(v.image_name){
                                    var img = '<img src="'+$apps_base_url+'/uploaded/file/'+v.image_name+'" class="img-responsives img-rounded" id="cover-image" style="max-width:100px" />';
                                }else{
                                    var img ='<center><i class="ti-image" style="font-size:4em; color:#aaa;"></i></center>';
                                }
                                
                                var serial = (v.asset_serial) ? v.asset_serial : '<center>---</center>';
                                var category = (v.category!=null) ? v.category : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                var site = (v.site!=null) ? v.site : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                var location = (v.location!=null) ? v.location : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                var remarks = (v.remarks) ? v.remarks : '---';
                                table +=    '<tr>'+
                                                '<td>'+img+'</td>'+
                                                '<td>'+v.asset_tag+'</td>'+
                                                '<td>'+v.asset_desc+'</td>'+
                                                '<td>'+v.asset_model+'</td>'+
                                                '<td>'+serial+'</td>'+
                                                '<td>'+category+'</td>'+
                                                '<td>'+remarks+'</td>'+
                                            '</tr>';
                            });

                            table += '</tbody></table>';

                            
                            $('#search-result').html(table);
                            //$('#myTable').DataTable().destroy();
                            $('#leased-asset-table').DataTable({
                                "order": [[ 1, "asc" ]],
                                "aoColumnDefs": [
                                    { 'bSortable': false, 'aTargets': [ 0] }
                                ],
                                'searching': false,
                            });
                        }else{
                            $('#search-result').html(data.stat_msg);
                        }

                    },
                    error :function (data) {
                        $.notification('Error','Internal Server Error','top-right','error','');
                    }
                });
            });
    }
});