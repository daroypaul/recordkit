/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    var origin = $apps_base_url;
    var proto = window.location.hostname;
    
    //Store all selected asset
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';
    //Parameter to check previous process result result: [0] No process done/process reset [1] process done, updated in Status modal form at modal.js
    $status_updated = 0;


    if($('table#record-table').length!=0){
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,12 ] }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    //title: $('#report-export-title').text(),
                    filename: function(){
                        var d = new Date();
                        var n = d.getTime();
                        return $('#report-export-title').text();
                    },
                    className: 'm-l-10'
                },
                'print',
            ],
            /*"dom": '<"row"'+
                    '<"col-xs-12 col-sm-6 col-md-4 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"col-xs-12 col-sm-4 col-md-3 col-lg-3"l>'+
                    '<"hidden-sm-down hidden-md-down col-lg-2">'+
                    '<"col-xs-12 col-sm-12 col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<a href="/asset/add" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Asset"><i class="fas fa-plus"></i></a>';
                var bulk_action =  
                                '<div class="form-group m-b-0">'+
                                    '<select class="form-control custom-select" id="select-site">'+
                                        '<option value="">- Select Site -</option>'+
                                    '</select>'+
                                '</div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);

                var site_list = '<option value="">- Select Site -</option>';
                var form_data = new FormData();
                form_data.append('ref', 'all'); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/site/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        if(data.list&&data.list.length!=0){
                            $.each(data.list,function(i,v){
                                site_list += '<option value="'+v.site_id+'">'+v.site_name+'</option>';
                            });
                        }

                        $('#select-site').html(site_list);
                    },
                    error :function (data) {
                        $.notification('Error','Internal Server Error. Unable to load site options','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }*/
        }).on('click', 'a[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');


            if(action=='delete'){
                var url = $apps_base_url+'/asset/delete';
                var settings = {
                    title : "Delete Record",
                    text:"Are you want to continue?",
                    confirmButtonText:"Delete",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: false
                }
            }else if(action=='archive'){
                var url = $apps_base_url+'/asset/archive';
                var settings = {
                    title : "Archive Record",
                    text:"Are you want to continue?",
                    confirmButtonText:"Archive",
                    showCancelButton:true,
                    type : 'warning',
                    confirmButtonColor: "#DD6B55",
                    //closeOnConfirm: true
                }
            }

            
            swal(settings).then(result => {
                var form_data = new FormData();
                form_data.append('id', id);
                swal.close();
                
                if(result.value){
                    $.ajax({
                        type:'POST',
                        url : url,
                        //dataType: "json",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            if(data.stat=='success'){
                                var row = $('tr[data-id='+id+']');
                                //table.removeRow(row);

                                $record_table.row( $('tr[data-id='+id+']') ).remove().draw();
                            }

                            //$.notification(data.stat_title,data.stat_msg,'top-right',data.stat,15000);
                            swal(data.stat_title, data.stat_msg, data.stat); 
                        },
                        error :function (data) {
                            //$.notification('Error','Internal server error','top-right','error',15000);
                            swal("Error", "Internal server error", "error");
                        }
                    });
                }
            });
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;
            $('.table-danger').removeClass('table-danger');

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }

            if($sid.length!=0){
                $.each($sid,function(i,v){
                    $("tr[data-id='"+v+"']").find("td[data-col='status']").text($stat_selected);
                });
            }
        });
    }

    $('#select-site').select2({
        placeholder : 'Select Site',
        ajax: {
            url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    with: '_all',
                    keywords: params.term,
                }

                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.site_name,
                            id: item.site_id
                        }
                    })
                };
            }
        }
    }).on('select2:select', function (e) {
        $('#select-location').val(null).trigger('change');
    });

    $('#select-site').change(function(){
        var site = $(this).val();
        var form_data = new FormData();
        form_data.append('site_id', site);
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Fetching Data...');
        $record_table.clear().draw();

        $.ajax({
            type:'POST',
            url : $apps_base_url+'/asset/fetch/all-leasable',
            dataType: "json",
            data:  form_data,
            processData: false,
            contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();
                if(data.asset_list.length!=0){
                    $.each(data.asset_list, function(i, item) {
                        var asset_id = item.asset_id;
                        var leased_date = '';
                        var leased_expire = '';
                        var leased_by = '';
                        var is_return = 1;//1=Return
                        var overdue = 0;
                        var overdue_label

                        $.each(data.lease_record, function(i, lease_item) {
                            if(lease_item.asset_id==asset_id){
                                leased_date = moment(lease_item.lease_start).format("DD-MMM-YYYY");
                                leased_expire = moment(lease_item.lease_expire).format("DD-MMM-YYYY");
                                leased_by = lease_item.emp_fullname;
                                is_return = 0;//0=Not Return
                                
                                var today = moment(); //YYYY-MM-DD
                                var expire = moment(lease_item.lease_expire);
                                overdue = today.diff(expire, 'days');
                            }
                        });

                        if(overdue<=1){
                            overdue_label = '1 Day';
                        }else{
                            overdue_label = overdue + ' Days';
                        }

                        var btn = '<div class="btn-group" role="group" aria-label="Basic example" style="min-width:90px !important;">'
                                    +'<a class="btn btn-sm btn-default" href="'+$apps_base_url+'/asset/'+item.asset_id+'/details" data-toggle="tooltip" data-original-title="View Asset Details"><i class="ti-search"></i></a>';
                        if(is_return==1){
                            btn += '<a href="'+$apps_base_url+'/asset/lease/'+item.asset_id+'/new" class="btn btn-info btn-sm"><i class="mdi mdi-logout-variant"></i> Lease</a>';
                            var stat_label = '<span class="badge badge-success">Available</span>';
                        }else if(is_return==0&&overdue==0){
                            btn += '<a href="'+$apps_base_url+'/asset/return-lease/'+item.asset_id+'/new" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a>';
                            var stat_label = '<span class="badge badge-inverse">Leased</span>';
                        }else if(is_return==0&&overdue!=0){
                            btn += '<a href="'+$apps_base_url+'/asset/return-lease/'+item.asset_id+'/new" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a>';
                            var stat_label = '<span class="badge badge-danger">'+overdue_label+' Overdue</span>';
                        }
                        btn += '</div>';

                        var row = $record_table.row.add( {
                            0: (item.image) ? '<img src="'+$apps_base_url+'/uploaded/file/'+item.image.upload_filename+'" style="min-width: 100px; max-width:100px" class="img-responsive img-rounded"/>' : '<i class="ti-image" style="font-size:4em; color:#999;"></i>',
                            1: (leased_date) ? leased_date : '---',
                            2: (leased_expire) ? leased_expire : '---',
                            3: (leased_by) ? leased_by : '---',
                            4: item.asset_tag,
                            5: item.asset_desc,
                            6: item.asset_model,
                            7: (item.asset_serial) ? item.asset_serial : '---',
                            8: (item.categories!=null) ? item.categories.cat_name : '---',
                            9: (item.site!=null) ? item.site.site_name : '---',
                            10: (item.site!=null) ? item.site.site_name : '---',
                            11: stat_label,
                            12: btn,
                        } ).draw();
                        
                        //$record_table.nodes().to$().find('td:nth-child(4)').attr({"data-toggle":'tooltip',"title":item.updated_on});

                        $record_table.rows(row).nodes().to$().attr("data-id", item.asset_id);
                        bootbox.hideAll();
                    });
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    $('input#select-all').click(function(){
        var c = this.checked;

        $('input.selected-item').each(function(){
            var id = $(this).parents('tr').data('id');

            $(this).prop('checked', c);

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }
        });
    });

    if($('table#leasable-record-table').length!=0){
        $leasable_table = $('#leasable-record-table').DataTable({
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,10 ] }
            ],
            //'searching': false,
        });
    }

    $('#report-export-title').keyup(function(i, v){
        var title = $(this).text();

        $('title').text('RecordKits Application Suite - Beta  |' + title);
    });
});