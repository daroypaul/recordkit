/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $('#input_digits').on('keyup keydown', function(e){
        if (($(this).val() > 12 || $(this).val() < 1)
            && e.keyCode != 46
            && e.keyCode != 8
            ) {
            e.preventDefault();     
            $(this).val(12);
        }
    });
    
    $('#input_counter').on('keyup keydown', function(e){
        if ($(this).val() < 1 && e.keyCode != 46 && e.keyCode != 8 ) {
            e.preventDefault();     
            $(this).val(1);
        }
    });
        
    $('#input_prefix,#input_suffix').keyup(function(e){
        display_tag();
    });


    $('input[name=padding],#with_year,#dashed,#input_insert,#input_string,#input_digits,#input_counter').change(function() {
        display_tag();
    });


    $('#form').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Saving...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);

        $.ajax({
            type:'POST',
            url : window.location,
            //dataType: "json",
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                   $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });
});


function display_tag(){
    var prefix = $('#input_prefix').val();
    var suffix = $('#input_suffix').val();
    var counter = $('#input_counter').val();
    var with_padding = $('input[name=padding]:checked').val();
    var digit = $('#input_digits').val();
    var dashed = $('input[name=dashed]:checked').val();
    var with_year = $('#with_year:checked').val();
    var year_string = $('#input_string').val();
    var year_insert = $('#input_insert').val();
    var padding = '', tag = '', year = '', dashed_before = '', dashed_after = '';

    year = (year_string=='2') ? new Date().getFullYear().toString().substr(-2) : new Date().getFullYear();
    padding = (with_padding=='enable') ? str_pad(counter, digit, 0, 'STR_PAD_LEFT') : counter;
    dashed_before = (dashed && prefix) ? '-' : '';
    dashed_after = (dashed && suffix) ? '-' : '';

    if(with_year){
        if(year_insert=='BEF_PREF'){
            tag = year + prefix + dashed_before+padding+dashed_after + suffix;
        }else if(year_insert=='AFT_PREF'){
            tag = prefix + year + dashed_before+padding+dashed_after + suffix;
        }else if(year_insert=='BEF_SUF'){
            tag = prefix + dashed_before+padding+dashed_after + year + suffix;
        }else if(year_insert=='AFT_SUF'){
            tag = prefix + dashed_before+padding+dashed_after + suffix + year;
        }
    }else{
        tag = prefix + dashed_before+padding+dashed_after + suffix;
    }

    $('#display-tag').text(tag);
}

function str_pad(input, pad_length, pad_string, pad_type){
	var output = input.toString();
	if (pad_string === undefined) { pad_string = ' '; }
	if (pad_type === undefined) { pad_type = 'STR_PAD_RIGHT'; }
	if (pad_type == 'STR_PAD_RIGHT') {
		while (output.length < pad_length) {
			output = output + pad_string;
		}
	} else if (pad_type == 'STR_PAD_LEFT') {
		while (output.length < pad_length) {
			output = pad_string + output;
		}
	} else if (pad_type == 'STR_PAD_BOTH') {
		var j = 0;
		while (output.length < pad_length) {
			if (j % 2) {
				output = output + pad_string;
			} else {
				output = pad_string + output;
			}
			j++;
		}
	}
	return output;
};