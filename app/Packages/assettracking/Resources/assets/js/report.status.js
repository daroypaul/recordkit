/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    //$("#record-table tbody tr").popover({placement:"top"});
    $.filter_field_get_data();

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 0, "asc" ]],
            "bFilter": false,
            "aoColumnDefs": [
                //{ 'bSortable': false, 'aTargets': [ 0,10 ] }
            ],
            columnDefs: [
                { type: 'date-dd-mmm-yyyy', targets: 6 }
            ],
            //dom: 'frtip',
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="mdi mdi-settings"></i> Columns',
                    action: function ( e, dt, node, config ) {
                        $('#form-loader').toggle();
                        $('#form-loader .loader__label').text('Loading...');
                        var type = $('#_type').val();
                        var col_displayed = [];

                        $('#record-table th[data-col]').each(function(){
                            var col = $(this).data('col');
                            var is_hidden = $(this).is(':hidden');

                            if(!is_hidden){
                                col_displayed.push(col);
                            }
                        });

                        $.custom_asset_display_column(type, col_displayed);
                    },
                },
                {
                    extend: 'excelHtml5',
                    filename: function(){
                        var d = new Date();
                        var n = d.getTime();
                        return $.trim($('#report-export-title').text());
                    },
                },
                /*{
                    extend: 'pdfHtml5',
                    title: 'Data export',
                },*/
                'print',
            ],
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');
                var form_data = new FormData();
                form_data.append('id', id); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/employees/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            $.admin_employee_form(id, 'edit', 'Update Employee', 'table', data.list.emp_fullname, data.list.emp_gender, data.list.emp_code, data.list.emp_email, data.list.emp_contact, data.list.emp_address, data.list.emp_remarks, data.list.site_id, data.list.department_id);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or employee might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }
        });

        /*$('#select-model').change(function(){
            var model = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + model);            

            display_report('bymodel', model, col_displayed);
        });

        $('#select-category').change(function(){
            var category = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('bycategory', category, col_displayed);
        });

        $('#select-location').change(function(){
            var location = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('bysitelocation', location, col_displayed);
        });

        $('#select-employee').change(function(){
            var id = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('byassignee', id, col_displayed);
        });*/

        $('#btn-add-record').click(function(){
            $.admin_employee_form('','add','Add New Employee','table');
        });

        $('#btn-custom-column').click(function(){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Loading...');
            var type = $('#_type').val();
            var col_displayed = [];

            $('#record-table th[data-col]').each(function(){
                var col = $(this).data('col');
                var is_hidden = $(this).is(':hidden');

                if(!is_hidden){
                    col_displayed.push(col);
                }
            });

            $.custom_asset_display_column(type, col_displayed);
        });

        $('div#pre-filter-container').on('click', 'button#unselect', function(e) {
            var target = $(this).data('target');
            $(target).val(null).trigger('change');
        });
    
        $('#act-show-filters').click(function (param) {
            $('#filter-container').slideToggle();
    
            if ( $('#btn-filter-icon').hasClass('mdi-filter-outline') ) {
                //$('#btn-filter-icon').attr('class', 'mdi mdi-filter-remove-outline m-r-10');
                $(this).html('<i class="mdi mdi-filter-remove-outline m-r-5" id="btn-filter-icon"></i> Less Filters');
            } else {
                //$('#btn-filter-icon').attr('class', 'mdi mdi-filter-outline m-r-10');
                $(this).html('<i class="mdi mdi-filter-outline m-r-5" id="btn-filter-icon"></i> More Filters');
            }
        });

        $('#act-add-filter-fields').click(function (params) {
            //custom-fields-container
    
            var field = $('#filter-fields-option').val();
            var field_name = $('#filter-fields-option option:selected').text();
            var filter_field_item = '';
            var add_on_class = '';
    
            if(field=='status'){
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                                    '<div class="form-group">'+
                                                        '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                        '<label>'+field_name+'</label>'+
                                                        '<select class="custom-select m-b-10" id="filter-fields-option" style="width:100% !important;" placeholder="Select Fields" name="'+field+'">'+
                                                            '<option value="">[ Select Status ]</option>'+
                                                            '<option value="available">Available</option>'+
                                                            '<option value="leased">Leased</option>'+
                                                            '<option value="checked out">Checked Out</option>'+
                                                            '<option value="damage">Damage</option>'+
                                                            '<option value="donated">Donated</option>'+
                                                            '<option value="lost">Lost</option>'+
                                                            '<option value="disposed">Disposed</option>'+
                                                            '<option value="sold">Sold</option>'+
                                                        '</select>'+
                                                    '</div>'+
                                                '</div>';
            }else if(field=='is_leasable'){
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                                    '<div class="form-group">'+
                                                        '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                        '<label>'+field_name+'</label>'+
                                                        '<select class="custom-select m-b-10" id="filter-fields-option" style="width:100% !important;" placeholder="Select Fields" name="'+field+'">'+
                                                            '<option value="no">No</option>'+
                                                            '<option value="yes">Yes</option>'+
                                                        '</select>'+
                                                    '</div>'+
                                                '</div>';
            }else{
                if(field=='date_purchased'){
                    var add_on_class = 'date-picker-default';
                }else if(field=='cost'){
                    var add_on_class = 'currency-input';
                }
    
    
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                            '<div class="form-group">'+
                                                '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                '<label>'+field_name+'</label>'+
                                                '<input type="text" class="form-control '+add_on_class+'" placeholder="'+field_name+' Keywords" name="'+field+'">'+
                                            '</div>'+
                                        '</div>';            
            }
            
            
            var find = $('#filter-container').find("div[data-field='"+field+"']").length;
    
            if(field&&!find){
                $( "#insert-filter-fields" ).before(filter_field_item);
            }
        });
    
        $('#filter-container').on('click', 'button#btn-remove-filter-item', function(e) {
            var field_id = $(this).parents('#filter-field-item').data('field');
            $(this).parents('#filter-field-item').remove();
        });

        $('#btn-generate').click(function (params) {
            var col_displayed = [];    
    
            display_report(col_displayed);
        });
    
        $('#report-export-title').keyup(function(i, v){
            var title = $(this).text();
    
            $('title').text('RecordKits Application Suite - Beta  | ' + title);
        });
});



function display_report(col_displayed){

    $('#form-loader').toggle();
    $('#form-loader .loader__label').text('Generating Reports...');

    var type = $('#_type').val();
    var form_data = new FormData($('form#filter-form')[0]);
    form_data.append('type', $('#_type').val());

    $('#record-table th[data-col]').each(function(){
        var col = $(this).data('col');
        var is_hidden = $(this).is(':hidden');

        if(!is_hidden){
            col_displayed.push(col);
        }
    });

    /*form_data.append('type', type);
    form_data.append('id', id);

    if(date_start&&date_end){
        form_data.append('date_start', date_start);
        form_data.append('date_end', date_end);
    }*/
    
    $.ajax({
        type:'POST',
        url : $apps_base_url+'/reports/status',
        dataType: "json",
        data:  form_data,
        processData: false,
        contentType: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function (data) {
            $record_table.clear().draw();
            $('#form-loader').toggle();

            if(data.asset_list.length!=0){
                var items = {};

                $.each(data.asset_list, function(i, item) {
                    var c1 = 19;
                    var c2 = 20;
                    var asset_id = item.asset_id;

                    items = 
                    {
                        
                        0: item.asset_tag,
                        1: item.asset_desc,
                        2: item.asset_model,
                        3: (item.asset_serial) ? item.asset_serial : '---',
                        4: (item.asset_brand) ? item.asset_brand : '---',
                        5: (item.cat_name) ? item.cat_name : '---',
                        6: (item.date_purchased!=null) ? moment(item.date_purchased).format("DD-MMM-YYYY") : '---',
                        7: (item.purchased_cost!='0.00') ? $.digits(item.purchased_cost) : '---',
                        8: (item.asset_vendor) ? item.asset_vendor : '---',
                        9: (item.site_name) ? item.site_name : '---',
                        10: (item.location_name) ? item.location_name : '---',
                        11: (item.emp_fullname!=null) ? item.emp_fullname : '---',
                        12: (item.user_fullname&&type!='available') ? item.user_fullname : '---',
                        13: (item.date_updated!=null) ? moment(item.date_updated).format("MMM DD, YYYY | hh:ssA").replace(/\s/g,"&nbsp;") : '---',
                        14: (item.asset_remarks) ? item.asset_remarks.replace(/\s/g,"&nbsp;") : '---',
                        15: (item.date_stat!=null) ? moment(item.date_stat).format("MMM DD, YYYY").replace(/\s/g,"&nbsp;") : '---',
                        16: (item.stat_remarks!=null) ? item.stat_remarks : '---',
                        17: (item.stat_to!=null) ? item.stat_to : '---',
                        18: (item.stat_amount!='0.00'&&item.stat_amount!=null) ? $.digits(item.stat_amount) : '---',
                     };

                     $.each(data.custom_fields, function(i, cf) {
                        var field_id = cf.field_id;

                        var index = data.asset_custom_values.findIndex(function(vv) {
                            return vv.field_id == field_id && vv.asset_id == asset_id
                        });

                        if(index!=-1){
                            items[c1++] = data.asset_custom_values[index].custom_value;
                        }else{
                            items[c1++] = '---';
                        }
                    });//custom_fields

                    var row = $record_table.row.add(items).draw();
                    
                    $record_table.nodes().to$().find('td:nth-child(1)').attr({"data-col":'tag'});
                    $record_table.nodes().to$().find('td:nth-child(2)').attr({"data-col":'desc'});
                    $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-col":'model'});
                    $record_table.nodes().to$().find('td:nth-child(4)').attr({"data-col":'serial'});
                    $record_table.nodes().to$().find('td:nth-child(5)').attr({"data-col":'brand'});
                    $record_table.nodes().to$().find('td:nth-child(6)').attr({"data-col":'category'});
                    $record_table.nodes().to$().find('td:nth-child(7)').attr({"data-col":'date-purchased'});
                    $record_table.nodes().to$().find('td:nth-child(8)').attr({"data-col":'cost'});
                    $record_table.nodes().to$().find('td:nth-child(9)').attr({"data-col":'vendor'});
                    $record_table.nodes().to$().find('td:nth-child(10)').attr({"data-col":'site'});
                    $record_table.nodes().to$().find('td:nth-child(11)').attr({"data-col":'location'});
                    $record_table.nodes().to$().find('td:nth-child(12)').attr({"data-col":'assigned-to'});
                    $record_table.nodes().to$().find('td:nth-child(13)').attr({"data-col":'updated-by'});
                    $record_table.nodes().to$().find('td:nth-child(14)').attr({"data-col":'updated-date'});
                    $record_table.nodes().to$().find('td:nth-child(15)').attr({"data-col":'remarks'});
                    $record_table.nodes().to$().find('td:nth-child(16)').attr({"data-col":'status-date'});
                    $record_table.nodes().to$().find('td:nth-child(17)').attr({"data-col":'status-note'});
                    $record_table.nodes().to$().find('td:nth-child(18)').attr({"data-col":'status-to'});
                    $record_table.nodes().to$().find('td:nth-child(19)').attr({"data-col":'status-amount'});

                    $.each(data.custom_fields, function(i, cf) {
                        var field_id = cf.field_id;

                        $record_table.nodes().to$().find('td:nth-child('+c2+')').attr({"data-col":field_id});
                        c2++;
                    });
                });


                $('#record-table').find("th[data-col]").each(function(){
                    var self = $(this).is(':hidden');
                    var col = $(this).data('col');

                    if(self){
                        $('#record-table').find("td[data-col='"+col+"']").hide();
                    }else{
                        $('#record-table').find("td[data-col='"+col+"']").show();
                    }
                });

                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            }
        },
        error :function (data) {
            $record_table.clear().draw();
            $('#form-loader').toggle();
            $.notification('Error','Internal Server Error','top-right','error','');
            $('#form-loader .loader__label').text('Fetching Data...');
        }
    });
}