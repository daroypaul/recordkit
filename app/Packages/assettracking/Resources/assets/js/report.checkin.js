/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    //$("#record-table tbody tr").popover({placement:"top"});
    $.filter_field_get_data();

    $('#date-start').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        clearBtn : true,
        //title: 'Lease Date Start',
        //endDate:'0d'
    }).on('changeDate', function(e) {
        var date2 = $(this).datepicker('getDate');
        date2.setDate(date2.getDate());
        $('#date-end').datepicker('destroy');
        $('#date-end').val('');
        $('#date-end').datepicker({
            startDate: date2,
            //endDate:'0d',
            format: 'dd-M-yyyy',
            autoclose: true,
            clearBtn : true,
        }).on('changeDate', function(e) {
            var start = $('#date-start').val();
            var end = $(this).val();
            var col_displayed = [];
            //display_report('bydate', '', col_displayed, start, end);
        });
    });

    $('#date-end').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        startDate: new Date(moment($('#date-start').val(), 'DD-MMM-YYYY').format("YYYY-MM-DD")),
        clearBtn : true,
    }).on('changeDate', function(e) {
        var start_date = $('#date-start').val();
        var end_date = $(this).val();
        var col_displayed = [];
        //display_report('bydate', '', col_displayed, start, end);
    });

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 0, "asc" ]],
            "bFilter": false,
            "aoColumnDefs": [
                //{ 'bSortable': false, 'aTargets': [ 0,10 ] }
            ],
            columnDefs: [
                { type: 'date-dd-mmm-yyyy', targets: 0 },
                { type: 'date-dd-mmm-yyyy', targets: 8 }
            ],
            //dom: 'frtip',
            dom: 'Bfrtip',
            buttons: [
                {
                    text: '<i class="mdi mdi-settings"></i> Columns',
                    action: function ( e, dt, node, config ) {
                        $('#form-loader').toggle();
                        $('#form-loader .loader__label').text('Loading...');
                        var col_displayed = [];

                        $('#record-table th[data-col]').each(function(){
                            var col = $(this).data('col');
                            var is_hidden = $(this).is(':hidden');

                            if(!is_hidden){
                                col_displayed.push(col);
                            }
                        });

                        $.custom_asset_display_column('inout-report', col_displayed);
                    },
                },
                {
                    extend: 'excelHtml5',
                    filename: function(){
                        var d = new Date();
                        var n = d.getTime();
                        return $.trim($('#report-export-title').text());
                    },
                },
                /*{
                    extend: 'pdfHtml5',
                    title: 'Data export',
                },*/
                'print',
            ],
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');
                var form_data = new FormData();
                form_data.append('id', id); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/employees/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            $.admin_employee_form(id, 'edit', 'Update Employee', 'table', data.list.emp_fullname, data.list.emp_gender, data.list.emp_code, data.list.emp_email, data.list.emp_contact, data.list.emp_address, data.list.emp_remarks, data.list.site_id, data.list.department_id);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or employee might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }
        });

        /*$('#select-model').change(function(){
            var model = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + model);            

            display_report('bymodel', model, col_displayed);
        });

        $('#select-category').change(function(){
            var category = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('bycategory', category, col_displayed);
        });

        $('#select-location').change(function(){
            var location = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('bysitelocation', location, col_displayed);
        });

        $('#select-employee').change(function(){
            var id = $(this).val();
            var col_displayed = [];
            $('#selected-model-caption').text(': ' + $(this).text());            

            display_report('byrequestor', id, col_displayed);
        });*/

        $('#btn-add-record').click(function(){
            $.admin_employee_form('','add','Add New Employee','table');
        });

        $('#btn-custom-column').click(function(){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Loading...');
            var col_displayed = [];

            $('#record-table th[data-col]').each(function(){
                var col = $(this).data('col');
                var is_hidden = $(this).is(':hidden');

                if(!is_hidden){
                    col_displayed.push(col);
                }
            });

            $.custom_asset_display_column('inout-report', col_displayed);
        });

        $('div#pre-filter-container').on('click', 'button#unselect', function(e) {
            var target = $(this).data('target');
            $(target).val(null).trigger('change');
        });
    
        $('#act-show-filters').click(function (param) {
            $('#filter-container').slideToggle();
    
            if ( $('#btn-filter-icon').hasClass('mdi-filter-outline') ) {
                //$('#btn-filter-icon').attr('class', 'mdi mdi-filter-remove-outline m-r-10');
                $(this).html('<i class="mdi mdi-filter-remove-outline m-r-5" id="btn-filter-icon"></i> Less Filters');
            } else {
                //$('#btn-filter-icon').attr('class', 'mdi mdi-filter-outline m-r-10');
                $(this).html('<i class="mdi mdi-filter-outline m-r-5" id="btn-filter-icon"></i> More Filters');
            }
        });

        $('#act-add-filter-fields').click(function (params) {
            //custom-fields-container
    
            var field = $('#filter-fields-option').val();
            var field_name = $('#filter-fields-option option:selected').text();
            var filter_field_item = '';
            var add_on_class = '';
    
            if(field=='status'){
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                                    '<div class="form-group">'+
                                                        '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                        '<label>'+field_name+'</label>'+
                                                        '<select class="custom-select m-b-10" id="filter-fields-option" style="width:100% !important;" placeholder="Select Fields" name="'+field+'">'+
                                                            '<option value="">[ Select Status ]</option>'+
                                                            '<option value="available">Available</option>'+
                                                            '<option value="leased">Leased</option>'+
                                                            '<option value="checked out">Checked Out</option>'+
                                                            '<option value="damage">Damage</option>'+
                                                            '<option value="donated">Donated</option>'+
                                                            '<option value="lost">Lost</option>'+
                                                            '<option value="disposed">Disposed</option>'+
                                                            '<option value="sold">Sold</option>'+
                                                        '</select>'+
                                                    '</div>'+
                                                '</div>';
            }else if(field=='is_leasable'){
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                                    '<div class="form-group">'+
                                                        '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                        '<label>'+field_name+'</label>'+
                                                        '<select class="custom-select m-b-10" id="filter-fields-option" style="width:100% !important;" placeholder="Select Fields" name="'+field+'">'+
                                                            '<option value="no">No</option>'+
                                                            '<option value="yes">Yes</option>'+
                                                        '</select>'+
                                                    '</div>'+
                                                '</div>';
            }else{
                if(field=='date_purchased'){
                    var add_on_class = 'date-picker-default';
                }else if(field=='cost'){
                    var add_on_class = 'currency-input';
                }
    
    
                filter_field_item = '<div class="col-lg-3" data-field="'+field+'" id="filter-field-item">'+
                                            '<div class="form-group">'+
                                                '<button class="btn btn-danger btn-xs float-right" type="button" id="btn-remove-filter-item"><i class="fa fa-times"></i></button>'+
                                                '<label>'+field_name+'</label>'+
                                                '<input type="text" class="form-control '+add_on_class+'" placeholder="'+field_name+' Keywords" name="'+field+'">'+
                                            '</div>'+
                                        '</div>';            
            }
            
            
            var find = $('#filter-container').find("div[data-field='"+field+"']").length;
    
            if(field&&!find){
                $( "#insert-filter-fields" ).before(filter_field_item);
            }
        });
    
        $('#filter-container').on('click', 'button#btn-remove-filter-item', function(e) {
            var field_id = $(this).parents('#filter-field-item').data('field');
            $(this).parents('#filter-field-item').remove();
        });

        $('#btn-generate').click(function (params) {
            var col_displayed = [];    
    
            display_report(col_displayed);
        });
    
        $('#report-export-title').keyup(function(i, v){
            var title = $(this).text();
    
            $('title').text('RecordKits Application Suite - Beta  | ' + title);
        });
});


function display_report(col_displayed){

    $('#form-loader').toggle();
    $('#form-loader .loader__label').text('Generating Reports...');

    var form_data = new FormData($('form#filter-form')[0]);

    $('#record-table th[data-col]').each(function(){
        var col = $(this).data('col');
        var is_hidden = $(this).is(':hidden');

        if(!is_hidden){
            col_displayed.push(col);
        }
    });

    console.log('a');
    
    $.ajax({
        type:'POST',
        url : $apps_base_url+'/reports/checkin',
        dataType: "json",
        data:  form_data,
        processData: false,
        contentType: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function (data) {

            console.log(data);
            $record_table.clear().draw();
            $('#form-loader').toggle();

            if(data.asset_list.length!=0){
                var items = {};

                $.each(data.asset_list, function(i, item) {
                    var c1 = 15;
                    var c2 = 16;
                    var asset_id = item.asset_id;

                   items = 
                   {
                        0: moment(item.inout_date).format("DD-MMM-YYYY"),
                        1: (item.emp_fullname) ? item.emp_fullname : '---',
                        2: item.asset_tag,
                        3: item.asset_desc,
                        4: item.asset_model,
                        5: (item.asset_serial) ? item.asset_serial : '---',
                        6: (item.asset_brand) ? item.asset_brand : '---',
                        7: (item.cat_name) ? item.cat_name : '---',
                        8: (item.date_purchased!=null) ? moment(item.date_purchased).format("DD-MMM-YYYY") : '---',
                        9: (item.purchased_cost!='0.00') ? $.digits(item.purchased_cost) : '---',
                        10: (item.asset_vendor) ? item.asset_vendor : '---',
                        11: (item.site_name) ? item.site_name : '---',
                        12: (item.location_name) ? item.location_name : '---',
                        13: item.user_fullname,
                        14: (item.item_remarks) ? item.item_remarks.replace(/\s/g,"&nbsp;") : '---',
                    };

                    $.each(data.custom_fields, function(i, cf) {
                        var field_id = cf.field_id;

                        var index = data.asset_custom_values.findIndex(function(vv) {
                            return vv.field_id == field_id && vv.asset_id == asset_id
                        });

                        if(index!=-1){
                            items[c1++] = data.asset_custom_values[index].custom_value;
                        }else{
                            items[c1++] = '---';
                        }
                    });//custom_fields

                    var row = $record_table.row.add(items).draw();
                    
                    $record_table.nodes().to$().find('td:nth-child(1)').attr({"data-col":'date'});
                    $record_table.nodes().to$().find('td:nth-child(2)').attr({"data-col":'requestor'});
                    $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-col":'tag'});
                    $record_table.nodes().to$().find('td:nth-child(4)').attr({"data-col":'desc'});
                    $record_table.nodes().to$().find('td:nth-child(5)').attr({"data-col":'model'});
                    $record_table.nodes().to$().find('td:nth-child(6)').attr({"data-col":'serial'});
                    $record_table.nodes().to$().find('td:nth-child(7)').attr({"data-col":'brand'});
                    $record_table.nodes().to$().find('td:nth-child(8)').attr({"data-col":'category'});
                    $record_table.nodes().to$().find('td:nth-child(9)').attr({"data-col":'date-purchased'});
                    $record_table.nodes().to$().find('td:nth-child(10)').attr({"data-col":'cost'});
                    $record_table.nodes().to$().find('td:nth-child(11)').attr({"data-col":'vendor'});
                    $record_table.nodes().to$().find('td:nth-child(12)').attr({"data-col":'site'});
                    $record_table.nodes().to$().find('td:nth-child(13)').attr({"data-col":'location'});
                    $record_table.nodes().to$().find('td:nth-child(14)').attr({"data-col":'recorded-by'});
                    $record_table.nodes().to$().find('td:nth-child(15)').attr({"data-col":'inout-remarks'});

                    $.each(data.custom_fields, function(i, cf) {
                        var field_id = cf.field_id;

                        $record_table.nodes().to$().find('td:nth-child('+c2+')').attr({"data-col":field_id});
                        c2++;
                    });
                });


                $('#record-table').find("th[data-col]").each(function(){
                    var self = $(this).is(':hidden');
                    var col = $(this).data('col');

                    if(self){
                        $('#record-table').find("td[data-col='"+col+"']").hide();
                    }else{
                        $('#record-table').find("td[data-col='"+col+"']").show();
                    }
                });

                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            }
        },
        error :function (data) {
            $record_table.clear().draw();
            $('#form-loader').toggle();
            $.notification('Error','Internal Server Error','top-right','error','');
            $('#form-loader .loader__label').text('Fetching Data...');
        }
    });
}