@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
    
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>
                            <div class="card-body">
                                <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="firstName" class="form-control date-picker-today" placeholder="YYYY-MM-DD" name="move_date" value="{{ isset($asset_record) ? $asset_record->date_purchased : date('F d, Y')}}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">New Site <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select Site" tabindex="1" name="site" id="asset_site">
                                                        <option value="">-- Select Site --</option>
                                                        <?php $site_id = isset($asset_record) ? $asset_record->site_id : '0';?>
                                                        @foreach($sites as $site)
                                                                @if($site->site_id == $site_id)
                                                                    <option value="{{$site->site_id}}" selected>{{$site->site_name}}</option>
                                                                @else
                                                                    <option value="{{$site->site_id}}">{{$site->site_name}}</option>
                                                                @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">New Location <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select Location" tabindex="1" name="location" id="asset_location">
                                                        <option value="">-- Select Location --</option>
                                                        <?php 
                                                            $locations = isset($locations) ? $locations : $locations=[];
                                                            $location_id = isset($asset_record) ? $asset_record->location_id : '0';
                                                        ?>
                                                        @foreach($locations as $location)
                                                                @if($location->location_id == $location_id)
                                                                    <option value="{{$location->location_id}}" selected>{{$location->loc_name}}</option>
                                                                @else
                                                                    <option value="{{$location->location_id}}">{{$location->loc_name}}</option>
                                                                @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <!--<div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="form-control-label">Remarks</label>
                                                    <textarea type="text" class="form-control" placeholder="" name="remarks"></textarea>
                                                </div>
                                            </div>-->
                                            <div class="col-md-12"><hr class="m-t-0"/></div>

                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-condensed" id="selected-asset-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Asset Tag</th>
                                                            <th>Description</th>
                                                            <th>Model</th>
                                                            <th>Serial</th>
                                                            <th>Category</th>
                                                            <th>Remarks</th>
                                                            <th width="50">
                                                                <button id="btn-add-asset" type="button" class="btn btn-sm btn-info"><i class="ti-search m-r-5"></i> Search Asset</button>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="asset-row-data">
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        

                                            <div class="col-sm-12">
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="button" class="btn btn-success" id="btn-done"> <i class="fa fa-check m-r-5"></i> Done</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ custom_asset('js/plugins/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    
    <!--<script src="{{ custom_asset('js/asset.inout.js') }}"></script>-->
    <script src="{{ url('assets/js/CnXOWSQqocYml8BizI5H.js') }}"></script>
@endsection