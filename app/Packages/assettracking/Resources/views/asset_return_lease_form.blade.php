@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
    
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header p-l-0">
                                <!--<button id="btn-current-list"  class="btn btn-sm btn-inverse">View Leased Asset</button>-->
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>
                            <div class="card-body">
                                {!!$error_msg!!}
                                
                                <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label">Return Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="return-date" class="form-control lease-start" placeholder="YYYY-MM-DD" name="return_date" value="{{ date('M d, Y')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Employee" id="add-employee-1"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label">Request By <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="select2 form-control" style="width:100%;" data-live-search="true" data-placeholder="Select Employee" name="requestor" id="requestor">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Employee" id="add-employee-2"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label">Return By <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="select2 form-control"  style="width:100%;" data-live-search="true" data-placeholder="Select Employee" name="customer" id="customer">
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Site" id="add-site"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label">Site <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select Site" tabindex="1" name="site" id="asset_site" style="width:100%;">
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Location" id="add-location"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label">Location <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select Location" tabindex="1" name="location" id="asset_location" style="width:100%;">
                                                    </select>
                                                </div>
                                            </div>

                                            <!--<div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="form-control-label">Remarks</label>
                                                    <textarea type="text" class="form-control" placeholder="" name="remarks"></textarea>
                                                </div>
                                            </div>-->
                                            <div class="col-md-12"><hr class="m-t-0"/></div>
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-condensed" id="selected-asset-table">
                                                    <thead>
                                                        <tr>
                                                            <th>Asset Tag</th>
                                                            <th>Description</th>
                                                            <th>Model</th>
                                                            <th>Serial</th>
                                                            <th>Category</th>
                                                            <th>Remarks</th>
                                                            <th width="50">
                                                                <button id="btn-add-asset" type="button" class="btn btn-sm btn-inverse"><i class="ti-search m-r-5"></i> Add Asset</button>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="asset-row-data">
                                                        @if(count($preload_asset)!=0)
                                                            @foreach($preload_asset as $asset)
                                                            <tr data-id="{{$asset->asset_id}}">
                                                                <td>{{$asset->asset_tag}}</td>
                                                                <td>{{$asset->asset_desc}}</td>
                                                                <td>{{$asset->asset_model}}</td>
                                                                <td>{{($asset->asset_serial) ? $asset->asset_serial : '---'}}</td>
                                                                <td>{!!($asset->cat_name) ? $asset->cat_name : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                                <td><input name="input_asset_remarks[]" value="" class="form-control" type="text" placeholder="Item Remarks"></td>
                                                                <td>
                                                                    <button type="button" class="btn btn-sm btn-danger" id="btn-delete-row">Remove</button>
                                                                    <input name="input_asset_id[]" value="{{$asset->asset_id}}" type="hidden">
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        

                                            <div class="col-sm-12">
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="button" class="btn btn-success" id="btn-done"> <i class="fa fa-check m-r-5"></i> Done</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ custom_asset('js/plugins/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    
    <!--<script src="{{ custom_asset('js/asset.return.lease.js') }}"></script>-->
    <script src="{{ url('assets/js/seW3Uh1BV6K0wCRH2DQI.js') }}"></script>
@endsection