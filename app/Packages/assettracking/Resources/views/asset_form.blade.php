@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>
                            <div class="card-body">
                                <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12 m-b-30">
                                                <h3 class="card-title m-b-0">Asset Details </h3>
                                                <div id="form-notification"></div>
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Description <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Asset Description" name="asset_desc" value="{{ isset($asset_record->asset_desc) ? $asset_record->asset_desc : '' }}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Tag ID <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <div class="input-group">
                                                        <input class="form-control" placeholder="Asset Tag" name="asset_tag" id="asset_tag" type="text" value="{{ isset($asset_record->asset_tag) ? $asset_record->asset_tag : '' }}">
                                                        <span class="input-group-btn">
                                                            <button id="btn-generate-tag" class="btn-rounded btn btn-secondary" type="button" style="border-radius: 60px !important;" data-toggle="tooltip" title="Generate ID"><i class="mdi mdi-autorenew"></i></button>
                                                        </span>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Model <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Asset Model" name="asset_model" value="{{ isset($asset_record) ? $asset_record->asset_model : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Serial</label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Asset Serial" name="asset_serial" value="{{ isset($asset_record) ? $asset_record->asset_serial : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Brand</label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Asset Brand" name="asset_brand" value="{{ isset($asset_record) ? $asset_record->asset_brand : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Vendor</label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Purchased from" name="asset_vendor" value="{{ isset($asset_record) ? $asset_record->asset_vendor : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Purchase Date</label>
                                                    <input type="text" id="firstName" class="form-control date-picker-default" placeholder="YYYY-MM-DD" name="date_purchased" value="{{ isset($asset_record) ? $asset_record->date_purchased : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">P.O Number</label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Purchase Order Number" name="po_number" value="{{ isset($asset_record) ? $asset_record->po_number : ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Cost ({{$system_options->currency->currency_code}})</label>
                                                    <input type="text" id="firstName" class="form-control currency-input" placeholder="Purchase Cost" name="purchased_cost" value="{{ isset($asset_record) ? number_format($asset_record->purchased_cost, 2) : '0.00'}}">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Category" id="add-category"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label text-bold">Category <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select a Category" tabindex="1" id="asset_category" name="asset_category" style="width:100%;">
                                                        <?php 
                                                            echo isset($asset_category) ? '<option value="'.$asset_category->cat_id.'">'.$asset_category->cat_name.'<option>' : '';
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Site" id="add-site"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label text-bold">Site <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select a Site" tabindex="1" name="asset_site" id="asset_site" style="width:100%;">
                                                        <?php 
                                                            echo isset($asset_site) ? '<option value="'.$asset_site->site_id.'">'.$asset_site->site_name.'<option>' : '';
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="Add New Location" id="add-location"><i class="fas fa-plus"></i></button>
                                                    <label class="form-control-label text-bold">Location <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select a Location" tabindex="1" name="asset_location" id="asset_location" style="width:100%;">
                                                        <?php 
                                                            echo isset($asset_location) ? '<option value="'.$asset_location->location_id.'">'.$asset_location->location_name.'<option>' : '';
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--/span-->

                                            
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Status <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <select class="form-control custom-select" data-placeholder="Select a Status" tabindex="1" name="asset_status" id="asset_status" style="width:100%;">
                                                        <?php 
                                                            $status = ['available','leased','checked out','damage','donated','lost','disposed','sold'];
                                                             $current_stat = isset($asset_record)?$asset_record->asset_status:'';
                                                        ?>
                                                        <option value="">-- Select Status --</option>
                                                        @foreach($status as $stat)
                                                            @if($stat==$current_stat)
                                                                <option value="{{$stat}}" selected>{{ucwords($stat)}}</option>
                                                            @else
                                                                <option value="{{$stat}}">{{ucwords($stat)}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row" id="custom-fields-container">
                                            {!! isset($custom_fields) ? $custom_fields : '' !!}
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Remarks</label>
                                                    <textarea class="form-control" placeholder="Asset Remarks" name="asset_remarks">{{ isset($asset_record) ? $asset_record->asset_remarks : ''}}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Leasable Asset</label>
                                                    <div class="switch">
                                                        @if(isset($asset_record) ? $asset_record->is_leasable : '')
                                                            <label>No <input type="checkbox" name="is_leasable" value="1" checked><span class="lever"></span> Yes</label>
                                                        @else
                                                            <label>No <input type="checkbox" name="is_leasable" value="1"><span class="lever"></span> Yes</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            @if(!isset($is_update))

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label text-bold">Image</label>
                                                        <div><small>Note: It will replace the current image uploaded</small></div>
                                                        <?php
                                                            $a = isset($asset_record->asset_image) ? custom_asset('/storage/'.$asset_record->asset_image) : '';
                                                        ?>
                                                        <input type="file" id="input-file-now" name="asset_image" class="dropify form-control" data-default-file="" />
                                                    </div>
                                                </div>

                                                <!--<div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Remove Current Image</label>
                                                        <div class="switch">
                                                            <label>No <input type="checkbox" name="remove_image" value="1"><span class="lever"></span> Yes</label>
                                                        </div>
                                                    </div>
                                                </div>-->
                                            @endif

                                            <div class="col-sm-12">
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <a href="{{url('/asset/list/all')}}" type="button" class="btn btn-inverse">Cancel</a>
                                    </div>
                                    <input type="hidden" id="_tag_num" name="tag_number" value="" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
                <input type="hidden" id="_type" value="{{isset($action_type) ? $action_type : ''}}" />
                
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ custom_asset('js/plugins/dropify/dist/css/dropify.min.css') }}">
    
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    
    <!--<script src="{{ custom_asset('js/asset.form.js') }}"></script>-->
    <script src="{{ url('assets/js/d8At3LmhCBkIapMTiz7n.js') }}"></script>
@endsection