@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor" id="report-export-title" contenteditable="true"><span data-toggle="tooltip" title="For Export Filename" data-placement="bottom">{{$form_title}} <i class="mdi mdi-pencil-box-outline text-default"></i></span></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 p-l-0 p-r-0">
                                        <form action="#" class="form-material" method="post" id="form">
                                            <select class="form-control font-14" tabindex="1" id="select-site">
                                                <option value="all">All Site</option>
                                                <!--@foreach($site_list as $site)
                                                    @if($user_site_id==$site->site_id)
                                                        <option value="{{$site->site_id}}" selected>{{$site->site_name}}</option>
                                                    @else
                                                    <option value="{{$site->site_id}}">{{$site->site_name}}</option>
                                                    @endif
                                                @endforeach-->
                                            </select> 
                                        </form>
                                    </div>
                                    <hr/>
                                    <div class="table-responsive">
                                        <table id="record-table" class="table table-condensed table-striped  font-14" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Leased&nbsp;Start</th>
                                                    <th>Leased&nbsp;End</th>
                                                    <th>Leased&nbsp;By</th>
                                                    <th>Asset&nbsp;Tag</th>
                                                    <th>Description</th>
                                                    <th>Model</th>
                                                    <th>Serial</th>
                                                    <th>Category</th>
                                                    <th>Site</th>
                                                    <th>Location</th>
                                                    <th>Status</th>
                                                    <th data-sort-ignore="true" class="min-width">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($asset_list as $asset)
                                                    <?php
                                                        $leased_date = '';
                                                        $leased_expire = '';
                                                        $leased_by = '---';
                                                        $is_return = 1;//1 = Yes
                                                    ?>

                                                    @foreach ($lease_record as $lease)
                                                        @if($lease->asset_id==$asset->asset_id)
                                                            <?php
                                                                $leased_date = date('d-M-Y', strtotime($lease->lease_start));
                                                                $leased_expire = date('d-M-Y', strtotime($lease->lease_expire));
                                                                $leased_by = $lease->emp_fullname;
                                                                $is_return = 0;//0 = No
                                                            ?>
                                                        @endif
                                                    @endforeach

                                                   

                                                    <tr>
                                                        <td>
                                                            <?php echo isset($asset->image) ? '<img src="'. url('uploaded/file/'.$asset->image->upload_filename) . '" class="img-responsive img-rounded" id="cover-image" style="min-width:100px; max-width:150px" alt="IMG" />' : '<center><i class="ti-image" style="font-size:4em; color:#999;"></i></center>';?>
                                                        </td>
                                                        <td>{{($leased_date)?$leased_date:'---'}}</td>
                                                        <td>{{($leased_expire)?$leased_expire:'---'}}</td>    
                                                        <td>{{$leased_by}}</td>
                                                        <td>{{$asset->asset_tag}}</td>
                                                        <td>{{$asset->asset_desc}}</td>
                                                        <td>{{$asset->asset_model}}</td>
                                                        <td>{!!($asset->asset_serial) ? $asset->asset_serial : '<center>---</center>'!!}</td>
                                                        <td>{{($asset->categories) ? $asset->categories->cat_name : '---'}}</td>
                                                        <td>{{($asset->site) ? $asset->site->site_name : '---'}}</td>
                                                        <td>{{($asset->location) ? $asset->location->location_name : '---'}}</td>
                                                        <td>
                                                                <?php 
                                                            
                                                                if(strtotime(date('Y-m-d'))<=strtotime($leased_expire)){
                                                                    $overdue = 0;
                                                                }else{
                                                                    $todate = new DateTime(date('Y-m-d'));
                                                                    $expire = ($leased_expire) ? $leased_expire : date('Y-m-d');
                                                                    $expire = new DateTime($expire);
                                                                    $overdue = $expire->diff($todate)->format("%a");
                                                                    $days = ($overdue<2) ? $overdue . ' Day' : $overdue . ' Days';
                                                                }
                                                                
                                                                if($is_return==1){
                                                                    echo '<span class="badge badge-success">Available</span>';
                                                                }else if($is_return==0&&$overdue==0){
                                                                    echo '<span class="badge badge-inverse">Leased</span>';
                                                                }else if($overdue!=0){
                                                                    echo '<span class="badge badge-danger">'. $days . ' Overdue</span>';
                                                                }
                                                                //echo ($diff<2) ? $diff . ' Day' : $diff . ' Days';
                                                                ?>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group" role="group" aria-label="Basic example" style="min-width:90px !important;">
                                                                <a class="btn btn-sm btn-default" href="{{url('/asset/'.$asset->asset_id.'/details')}}" data-toggle="tooltip" data-original-title="View Asset Details"><i class="ti-search"></i></a>
                                                                @if($is_return==1)
                                                                    <a href="{{url('/asset/lease/'.$asset->asset_id.'/new')}}" class="btn btn-info btn-sm"><i class="mdi mdi-logout-variant"></i> Lease</a>
                                                                @else
                                                                    <a href="{{url('/asset/return-lease/'.$asset->asset_id.'/new')}}" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a>
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('style')

@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('js')
    <!--<script src="{{ custom_asset('js/asset.leasable.list.js') }}"></script>-->
    <script src="{{ url('assets/js/sniDaIZyjKg512RuTbPG.js') }}"></script>
@endsection