@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor" id="report-export-title" contenteditable="true"><span data-toggle="tooltip" title="For Export Filename" data-placement="bottom">{{$form_title}} <i class="mdi mdi-pencil-box-outline text-default"></i></span></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsives"  id="slimtest1">
                                    <table id="record-table" class="table table-condensed table-striped  font-14" style="width:100%">
                                        <thead>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                </td>
                                                <td>Image</td>
                                                <td>Asset Tag</td>
                                                <td>Description</td>
                                                <td>Model</td>
                                                <td>Serial</td>
                                                <!--<td>Brand</td>-->
                                                <td>Category</td>
                                                <td>Site</td>
                                                <td>Location</td>
                                                <td>Status</td>
                                                <td>Assign&nbsp;To</td>
                                                <td>Leasable</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($asset_record as $asset)
                                                <tr data-id="{{$asset->asset_id}}">
                                                    <td>
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$asset->asset_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$asset->asset_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>
                                                        <?php echo isset($asset->image_name) ? '<img src="'. url('uploaded/file/'.$asset->image_name) . '" class="img-responsive img-rounded" id="cover-image" style="max-width:100px" alt="IMG" />' : '<center><i class="ti-image" style="font-size:4em; color:#999;"></i></center>';?>
                                                    </td>
                                                    <td>{{$asset->asset_tag}}</td>
                                                    <td>{{$asset->asset_desc}}</td>
                                                    <td>{{$asset->asset_model}}</td>
                                                    <td>{!!($asset->asset_serial) ? $asset->asset_serial : '<center>---</center>'!!}</td>
                                                    <!--<td>{!!($asset->asset_brand) ? $asset->asset_brand : '<center>---</center>'!!}</td>-->
                                                    <td>{!!($asset->cat_name) ? $asset->cat_name : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                    <td>{!!($asset->site_deleted==0)?$asset->site_name:'<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                    <td>{!!($asset->location_deleted==0)?$asset->location_name:'<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                    <td data-col="status"><?php echo ucwords($asset->asset_status);?></td>
                                                    <td>{!!($asset->emp_fullname) ? $asset->emp_fullname: '<center>---</center>';!!}</td>
                                                    <td>{!!($asset->is_leasable) ? '<i class="fa fa-check-circle text-success"></i>': '-None-';!!}</td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example" style="min-width:90px !important;">
                                                        <a class="btn btn-sm btn-default" href="{{url('/asset/'.$asset->asset_id.'/details')}}" data-toggle="tooltip" data-original-title="View Asset Details"><i class="ti-search"></i></a>
                                                        <?php
                                                            foreach($inout_assets as $inout)
                                                            {
                                                                if($inout['asset_id']==$asset->asset_id){
                                                                    if($asset->is_leasable==1&&$inout['inout_type']=='leased checkout'){
                                                                        echo '<a class="btn btn-sm btn-inverse" href="'.url('/asset/return-lease/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Return Leased Asset"><i class="mdi mdi-login"></i></a>';
                                                                    }
                                                                    else if($asset->is_leasable==1&&$inout['inout_type']=='return leased'){
                                                                        echo '<a class="btn btn-sm btn-success" href="'.url('/asset/lease/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Lease Asset"><i class="mdi mdi-logout"></i></a>';
                                                                    }else if($inout['inout_type']=='checkout'){
                                                                        echo '<a class="btn btn-sm btn-inverse" href="'.url('/asset/checkout/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Check-In Asset"><i class="mdi mdi-login"></i></a>';
                                                                    }
                                                                    else if($inout['inout_type']=='checkin'){
                                                                        echo '<a class="btn btn-sm btn-success" href="'.url('/asset/checkin/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Check-Out Asset"><i class="mdi mdi-logout"></i></a>';
                                                                    }else{
                                                                        if($asset->is_leasable==0&&$inout['inout_type']=='no-inout-record'){
                                                                            echo '<a class="btn btn-sm btn-success" href="'.url('/asset/checkout/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Check-Out Asset"><i class="mdi mdi-logout"></i></a>';
                                                                        }else if($asset->is_leasable==1&&$inout['inout_type']=='no-inout-record'){
                                                                            echo '<a class="btn btn-sm btn-success" href="'.url('/asset/lease/'.$asset->asset_id.'/new').'" data-toggle="tooltip" data-original-title="Lease Asset"><i class="mdi mdi-logout"></i></a>';
                                                                        }
                                                                    }
                                                                    break;
                                                                }
                                                
                                                            }
                                                        ?>
                                                        <a class="btn btn-sm btn-warning" href="{{url('/asset/'.$asset->asset_id.'/edit')}}" data-toggle="tooltip" data-original-title="Update Asset"><i class="ti-pencil"></i></a>

                                                        </div>

                                                        <!--<div class="btn-group">
                                                            <button class="btn btn-inverse btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action
                                                            </button>
                                                            <div class="dropdown-menu  dropdown-menu-right datatable-dropdown-menu">
                                                                <a class="dropdown-item" href="{{url('/asset/'.$asset->asset_id.'/details')}}"><i class="ti-search"></i>View</a>
                                                                <a class="dropdown-item" href="{{url('/asset/'.$asset->asset_id.'/edit')}}"><i class="ti-pencil"></i>Edit</a>

                                                                <?php
                                                                /*foreach($inout_assets as $inout)
                                                                {
                                                                    if($inout['asset_id']==$asset->asset_id){
                                                                        if($asset->is_leasable==1&&$inout['inout_type']=='leased checkout'){
                                                                            echo '<a class="dropdown-item" href="'.url('/asset/return-lease/'.$asset->asset_id.'/new').'"><i class="mdi mdi-login m-r-10"></i>Return Leased</a>';
                                                                        }
                                                                        else if($asset->is_leasable==1&&$inout['inout_type']=='return leased'){
                                                                            echo '<a class="dropdown-item" href="'.url('/asset/lease/'.$asset->asset_id.'/new').'"><i class="mdi mdi-login m-r-10"></i>Lease </a>';
                                                                        }else if($inout['inout_type']=='checkout'){
                                                                            echo '<a class="dropdown-item" href="'.url('/asset/checkout/'.$asset->asset_id.'/new').'"><i class="mdi mdi-login m-r-10"></i>Check In</a>';
                                                                        }
                                                                        else if($inout['inout_type']=='checkin'){
                                                                            echo '<a class="dropdown-item" href="'.url('/asset/checkin/'.$asset->asset_id.'/new').'"><i class="mdi mdi-logout m-r-10"></i>Check Out</a>';
                                                                        }else{
                                                                            if($asset->is_leasable==0&&$inout['inout_type']=='no-inout-record'){
                                                                                echo '<a class="dropdown-item" href="'.url('/asset/checkout/'.$asset->asset_id.'/new').'"><i class="mdi mdi-logout m-r-10"></i>Check Out</a>';
                                                                            }else if($asset->is_leasable==1&&$inout['inout_type']=='no-inout-record'){
                                                                                echo '<a class="dropdown-item" href="'.url('/asset/lease/'.$asset->asset_id.'/new').'"><i class="mdi mdi-login m-r-10"></i>Lease </a>';
                                                                            }
                                                                        }
                                                                        break;
                                                                    }
                                                    
                                                                }*/
                                                                
                                                                ?>
                                                                
                                                            </div>
                                                        </div>-->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    <!--<script src="{{ custom_asset('js/asset.list.js') }}"></script>-->
    <script src="{{ url('assets/js/1sIkxjYe6Wr9RNHnzwv7.js') }}"></script>
@endsection