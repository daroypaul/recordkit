@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
    
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Asset Management</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="row">
                                        <div class="col-sm-12">
                                            <h3 class="card-title m-t-5 m-b-0 float-left">{{$asset_record->asset_desc}}</h3>

                                            <div class="btn-group float-right">
                                                <button type="button" class="btn btn-secondary dropdown-toggle  btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                     <a class="dropdown-item" href="{{url('/asset/'.$asset_record->asset_id.'/edit')}}"><i class="ti-pencil  m-r-10"></i>Edit</a>
                                                     <!--<a class="dropdown-item" href="#"><i class="ti-archive m-r-10"></i>Archive</a>
                                                     <a class="dropdown-item" href="#"><i class="ti-pencil  m-r-10"></i>Delete</a>-->
                                                </div>
                                            </div>

                                            

                                            <a class="btn btn-sm btn-inverse float-right m-r-5" href="{{url('/asset/list/all')}}">Back</a>
                                            
                                            <br class="clear"/>
                                             <hr class="" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                        </div>
                                        <div class="col-sm-2 text-center" id="cover-image">
                                            <?php echo isset($asset_record->image->upload_filename) ? '<img src="'. url('uploaded/file/'.$asset_record->image->upload_filename) . '" class="img-responsive img-rounded" alt="Asset Image" />' : '<i class="ti-image" style="font-size:14vw; color:#999;"></i>';?>
                                        </div>
                                        <div class="col-sm-10">
                                            <table class="table table-condensed custom-table">
                                                <tr>
                                                    <td class="font-bold" width="15%">Asset Tag</td>
                                                    <td class="b-none" id="asset-tag">{{ ($asset_record->asset_tag) ? $asset_record->asset_tag : '---'}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="font-bold">Model</td>
                                                    <td>{{ ($asset_record->asset_model) ? $asset_record->asset_model : '---'}}</td>
                                                </tr>                                                
                                                <tr>
                                                    <td class="font-bold">Serial</td>
                                                    <td>{{ ($asset_record->asset_serial) ? $asset_record->asset_serial : '---'}}</td>
                                                </tr>                                     
                                                <tr>
                                                    <td class="font-bold">Category</td>
                                                    <td>{!! ($asset_record->categories) ? $asset_record->categories->cat_name : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                </tr>                                   
                                                <tr>
                                                    <td class="font-bold">Site</td>
                                                    <td class="b-none">{!! ($asset_record->site) ? $asset_record->site->site_name : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                </tr>                                   
                                                <tr>
                                                    <td class="font-bold">Location</td>
                                                    <td class="b-none">{!! ($asset_record->location) ? $asset_record->location->location_name : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>'!!}</td>
                                                </tr>                                   
                                                <tr>
                                                    <td class="font-bold">Status</td>
                                                    <td class="b-none">{{ ($asset_record->asset_status) ? ucwords($asset_record->asset_status) : '---'}}</td>
                                                </tr>     
                                            </table>
                                        </div>
                                        <div class="col-sm-5"></div>
                                </div>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#details-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="ti-info-alt"></i></span> <span class="hidden-xs-down">Details</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#events-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="ti-calendar"></i></span> <span class="hidden-xs-down">Events</span></a> </li>
                                <li class="nav-item"> 
                                    <a class="nav-link" data-toggle="tab" href="#maintenance-tab" role="tab">
                                        <span class="hidden-sm-ups m-r-10"><i class="ti-settings"></i></span> 
                                        <span class="hidden-xs-down">Maintenances</span>
                                    </a>
                                </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#images-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="ti-image"></i></span> <span class="hidden-xs-down">Images</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#documents-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="ti-files"></i></span> <span class="hidden-xs-down">Documents</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#warranties-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="ti-tag"></i></span> <span class="hidden-xs-down">Warranties</span></a> </li>
                                

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#insurance-tab" role="tab"><span class="hidden-sm-ups m-r-10"><i class="mdi mdi-shield-outline"></i></span> <span class="hidden-xs-down">Insurances</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane p-20 active" id="details-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="<?php echo (count($custom_values)) ? 'col-sm-6' : 'col-sm-12' ?>">
                                            <table class="table table-condensed custom-table">                                      
                                                <tr>
                                                    <td class="font-bold">Brand</td>
                                                    <td>{{ ($asset_record->asset_brand) ? $asset_record->asset_brand : '---'}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="font-bold" width="25%">Date Purchased</td>
                                                    <td class="b-none">{{ ($asset_record->date_purchased) ? date('F d, Y', strtotime($asset_record->date_purchased)) : '---'}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="font-bold" width="25%">Purchase Order Number</td>
                                                    <td class="b-none">{{ ($asset_record->po_number) ? $asset_record->po_number : '---'}}</td>
                                                </tr>                                   
                                                <tr>
                                                    <td class="font-bold">Cost</td>
                                                    <td class="b-none">{{ ($asset_record->purchased_cost) ? number_format($asset_record->purchased_cost, 2) . ' ' . $system_options->currency->currency_code : '---'}}</td>
                                                </tr>                                   
                                                <tr>
                                                    <td class="font-bold">Vendor</td>
                                                    <td class="b-none">{{ ($asset_record->asset_vendor) ? $asset_record->asset_vendor : '---'}}</td>
                                                </tr>                  
                                                <tr>
                                                    <td class="font-bold">Assigned To</td>
                                                    <td class="b-none">{{ ($asset_record->assigned_to) ? $asset_record->assigned_to->emp_fullname : '---'}}
                                                    </td>
                                                </tr>             
                                                <tr>
                                                    <td class="font-bold">Recorded By</td>
                                                    <td class="b-none">
                                                        {{($asset_record->creator->user_fullname) ? ucwords($asset_record->creator->user_fullname) : '---'}}
                                                        {!! '<br/><code><small>' . date('d-M-Y @ h:iA', strtotime($asset_record->date_recorded. $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes')) . '</small></code>' !!}
                                                    </td>
                                                </tr>
                                                                                   
                                                <tr>
                                                    <td class="font-bold">Updated By</td>
                                                    <td class="b-none">
                                                        {{ ($asset_record->updator->user_fullname) ? ucwords($asset_record->updator->user_fullname) : '---'}}
                                                        {!! '<br/><code><small>' . date('d-M-Y @ h:iA', strtotime($asset_record->date_updated . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes')) . '</small></code>' !!}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="font-bold">Remarks</td>
                                                    <td>{{ ($asset_record->asset_remarks) ? $asset_record->asset_remarks : '---'}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        @if(count($custom_values)!=0)
                                        <div class="col-sm-6">
                                            <table class="table table-condensed custom-table">
                                                @foreach($custom_values as $custom_value)
                                                    <tr>
                                                        <td class="font-bold" width="25%">{{$custom_value->custom_fields->field_label}}</td>
                                                        <td class="b-none">{{ ($custom_value->custom_value) ? $custom_value->custom_value : '---'}}</td>
                                                    </tr> 
                                                @endforeach
                                            </table>
                                        </div>
                                        @endif
                                    </div>

                                </div>
                                <div class="tab-pane p-20 p-t-10" id="events-tab" role="tabpanel">
                                    <div class="table-responsived">
                                        <table class="table table-condensed" id="record-events-table"  style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Events</th>
                                                    <th>Date</th>
                                                    <th>New&nbsp;Site</th>
                                                    <th>New&nbsp;Location</th>
                                                    <th>Previous&nbsp;Site</th>
                                                    <th>Previous&nbsp;Location</th>
                                                    <th>Remarks</th>
                                                    <th>Requestor</th>
                                                    <th>Recorded By</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($events as $event)
                                                <tr>
                                                    <td>
                                                        @if($event->event_type=='checkin')
                                                            Check-In
                                                        @elseif($event->event_type=='checkout')
                                                            Check-Out
                                                        @elseif($event->event_type=='leased')
                                                            Leasing
                                                        @elseif($event->event_type=='return-leased')
                                                            Returning Leased
                                                        @elseif($event->event_type=='movement')
                                                            Transferred
                                                        @endif
                                                    </td>
                                                    <td>{{date('d-M-Y', strtotime($event->event_date))}}</td>
                                                    <td>{{($event->new_site) ? $event->new_site->site_name : '---'}}</td>
                                                    <td>{{($event->new_location) ? $event->new_location->location_name:'---'}}</td>
                                                    <td>{{($event->prev_site) ? $event->prev_site->site_name : '---'}}</td>
                                                    <td>{{($event->prev_location) ? $event->prev_location->location_name : '---'}}</td>
                                                    <td>{{($event->event_remarks) ? $event->event_remarks : '---'}}</td>
                                                    <td>{!!($event->requestor) ? $event->requestor->emp_fullname : '----'!!}</td>
                                                    <td>{{$event->recorder->user_fullname}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                            <!--<thead>
                                                <tr>
                                                    <th>Events</th>
                                                    <th>Date</th>
                                                    <th>Requestor</th>
                                                    <th>Site</th>
                                                    <th>Location</th>
                                                    <th>Remarks</th>
                                                    <th>Recorded By</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($logs as $log)
                                                <tr>
                                                    <td>{{ucwords($log->inout_type)}}</td>
                                                    <td>{{date('d-M-Y', strtotime($log->inout_date))}}</td>
                                                    <td>{{$log->emp_fullname}}</td>
                                                    <td>{!! ($log->site_deleted==0) ? ucfirst($log->site_name) : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>' !!}</td>
                                                    <td>{!! ($log->location_deleted==0) ? ucfirst($log->location_name) : '<code data-toggle="tooltip" title="Data is deleted">Undefined</code>' !!}</td>
                                                    <td>{{ucfirst($log->item_remarks)}}</td>
                                                    <td>{{ucfirst($log->user_fullname)}}</td>
                                                </tr> 
                                            @endforeach
                                            </tbody>-->
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane p-20 p-t-10" id="maintenance-tab" role="tabpanel">
                                    
                                    <div class="table-responsived">
                                        <table class="table table-condensed" id="record-maintenance-table"  style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Title</th>
                                                    <th>Remarks</th>
                                                    <th>Start&nbsp;Date</th>
                                                    <th>Complation</th>
                                                    <th>Complated</th>
                                                    <th>Warranty&nbsp;Support</th>
                                                    <th>Supplier</th>
                                                    <th>Recorded&nbsp;By</th>
                                                    <th>Staus</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($maintenances as $maintenance)
                                                <tr  data-id="{{$maintenance->id}}">
                                                    <td>{{ucfirst($maintenance->maintain_type)}}</td>
                                                    <td>{{$maintenance->maintain_title}}</td>
                                                    <td>{{($maintenance->maintain_remarks) ? $maintenance->maintain_remarks : '---'}}</td>
                                                    <td>{{date('d-M-Y', strtotime($maintenance->date_start . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</td>
                                                    <td>{!! ($maintenance->date_complation) ? date('d-M-Y', strtotime($maintenance->date_complation . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes')) : '<center>---</center>'!!}</td>
                                                    <td>{!! ($maintenance->date_completed) ? date('d-M-Y', strtotime($maintenance->date_completed . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes')) : '<center>---</center>'!!}</td>
                                                    <td class="text-center">{!!($maintenance->is_warranty==1) ? '<i class="fa fa-check-circle text-success"></i>':'<center>---</center>'!!}</td>
                                                    <td>{{($maintenance->suppliers) ? $maintenance->suppliers->supplier_name : '---'}}</td>
                                                    <td>{{($maintenance->users) ? $maintenance->users->user_fullname : '---'}}</td>
                                                    <td>{{ucwords($maintenance->maintain_stat)}}</td>
                                                    <td>
                                                        <button class="btn btn-xs btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                        <button class="btn btn-xs btn-danger" data-action="delete" data-action="edit" data-toggle="tooltip" title="Delete Record"><i class="ti-trash"></i></button>
                                                    </td>
                                                </tr> 
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane p-20 p-b-10" id="images-tab" role="tabpanel">
                                    <form action="/asset/upload/image" class="form-material m-b-20" method="post" id="upload-image-form" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">Image File</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                        <input type="hidden">
                                                        <input type="file" name="asset_image"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" id="remove-image-upload">Remove</a> </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="submit" class="btn btn-sm btn-info btn-block waves" id="btn-image-upload">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-columns p-20" style="background:#e9e9e9;" id="image-gallery">
                                            @foreach($images as $image)
                                                <div class="card">
                                                    <a class="image-popup-vertical-fit" href="{{url('uploaded/file/'.$image->upload_filename)}}"><img class="card-img-top img-responsive" src="{{url('uploaded/file/'.$image->upload_filename)}}" alt="Asset Image"></a>
                                                    <div class="card-body" data-image="{{$image->upload_id}}">
                                                        @if($image->upload_use==0)
                                                            <button type="button" href="#" data-action="set-cover" class="btn btn-xs btn-inverse">Set as Cover</button>
                                                        @endif
                                                        <button type="button" href="#" data-action="delete" class="btn btn-xs btn-danger">Delete</button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        
                                    </div>
                                    
                                </div>
                                <div class="tab-pane p-20" id="documents-tab" role="tabpanel">
                                    <form action="#" class="form-material m-b-0" method="post" id="upload-docs-form" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="form-control-label">File <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                
                                                    <div class="fileinput fileinput-new input-group asset_file" data-provides="fileinput">
                                                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">doc, docx, pdf, txt, zip, and rar</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                        <input type="hidden">
                                                        <input type="file" name="asset_file"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" id="remove-docs-upload">Remove</a> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label class="form-control-label">Description <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="File Description" name="file_desc" value="">
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="submit" class="btn btn-sm btn-info btn-block waves" id="btn-docs-upload">Upload</button>
                                            </div>
                                        </div>
                                    </form>
                                    <hr/>
                                    <div class="table-responsived">
                                        <table class="table table-condensed" id="record-document-table" data-page-navigation=".pagination2" page-size="5"  style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>File&nbsp;Type</th>
                                                    <th>Uploaded&nbsp;Date</th>
                                                    <th>Uploaded&nbsp;By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($documents as $document)
                                                    <tr data-docs="{{$document->id}}">
                                                        <td>{{$document->upload_desc}}</td>
                                                        <td>
                                                            @foreach($filetypes as $filetype) 
                                                                @if($filetype['ext']==$document->upload_extension)
                                                                    <i class="{{$filetype['icon']}} m-r-5" style="font-size:20px !important;"></i> {{$filetype['name']}}
                                                                @elseif($filetype['ext']=='generic')
                                                                    <i class="fa fa-file-o m-r-5" style="font-size:20px !important;"></i> File
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td>{{date('d-M-Y h:iA', strtotime($document->date_recorded . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</td>
                                                        <td>{{$document->users->user_fullname}}</td>
                                                        <td>
                                                            <a href="{{url('files/uploaded/asset/'.$document->upload_filename)}}" class="btn btn-sm btn-info" data-toggle="tooltip" data-original-title="Download"><i class="ti-download"></i></a>
                                                            <button class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane p-20 p-t-10" id="warranties-tab" role="tabpanel">
                                    <div class="table-responsived">
                                        <table class="table table-condensed" id="record-warranties-table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Months</th>
                                                    <th>Expire</th>
                                                    <th>Remaining</th>
                                                    <th>Remarks</th>
                                                    <th>Recorded By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($warranties as $warranty)
                                                <tr data-id="{{$warranty->id}}">
                                                    <td>{{$warranty->months}}</td>
                                                    <td>{{date('d-M-Y', strtotime($warranty->warranty_expire))}}</td>
                                                    <td>
                                                        <?php 
                                                            $todate = new DateTime(date('Y-m-d'));
                                                            $expire = new DateTime($warranty->warranty_expire);
                                                            $diff = $expire->diff($todate)->format("%a");
                                                            echo ($diff<2) ? $diff . ' Day' : $diff . ' Days';
                                                        ?>
                                                    </td>
                                                    <td>{!!($warranty->warranty_remarks) ? $warranty->warranty_remarks:'<center>---</center>'!!}</td>
                                                    <td>{{$warranty->users->user_fullname}}</td>
                                                    <td><button class="btn btn-sm btn-danger" data-action="delete" type="button" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button></td>
                                                </tr> 
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <!---/Insurance-->
                                <div class="tab-pane p-20 p-t-10" id="insurance-tab" role="tabpanel">
                                    <div class="table-responsived">
                                        <table class="table table-condensed" id="record-insurance-table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Provider</th>
                                                    <th>Start</th>
                                                    <th>Expire</th>
                                                    <th>Remaining</th>
                                                    <th>Cost</th>
                                                    <th>Recorded By</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($insurances as $insurance)
                                                <?php
                                                    $todate = new DateTime(date('Y-m-d'));
                                                    $expire = new DateTime(date('Y-m-d', strtotime($insurance->insurance_expire)));

                                                    if(strtotime($insurance->insurance_expire) > strtotime(date('Y-m-d'))){
                                                        $diff =  $todate->diff($expire)->format("%a");
                                                    }else{
                                                        $diff = 0;
                                                    }

                                                    
                                                    $days = ($diff<2) ? '<span class="label label-danger">Expired</span>' : $diff . ' Days';
                                                ?>  
                                                <tr data-id="{{$insurance->id}}">
                                                    <td>{{$insurance->insurance_name}}</td>
                                                    <td>{{$insurance->insurance_provider}}</td>
                                                    <td>{{date('d-M-Y', strtotime($insurance->insurance_start))}}</td>
                                                    <td>{{date('d-M-Y', strtotime($insurance->insurance_expire))}}</td>
                                                    <td>{!!$days!!}</td>
                                                    <td>{{($insurance->insurance_cost) ? number_format($insurance->insurance_cost, 2) . ' ' .$system_options->currency->currency_code : '---'}}</td>
                                                    <td>{{$insurance->user_fullname}}</td>
                                                    <td><button type="button" class="btn btn-sm btn-danger" data-action="detached" data-toggle="tooltip" title="Detached"><i class="fa fa-times"></i></button></td>
                                                </tr> 
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!--/insurance panel-->

                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$asset_record->asset_tag}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ custom_asset('js/plugins/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ custom_asset('css/template/default/css/pages/tab-page.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/file-upload.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('js/plugins/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/user-card.css') }}" rel="stylesheet">
    
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
    
    <!--<script src="{{ custom_asset('js/asset.form.js') }}"></script>-->
    <script src="{{ url('assets/js/d8At3LmhCBkIapMTiz7n.js') }}"></script>
    <script src="{{ custom_asset('js/modal.js') }}"></script>
@endsection