@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Reports</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                            <div class="card">
                                    <div class="card-body">
                                        <form action="#" class="form-material" method="post" id="filter-form">
                                            <div class="row" id="pre-filter-container">
                                                <div class="col-lg-3">
                                                    <div class="form-group m-b-10">
                                                        <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-category">Clear</button>
                                                        <label>Category</label>
                                                        <select class="form-control" tabindex="1" id="select-category" style="width:100% !important;" name="category">
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group m-b-10">
                                                        <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-site">Clear</button>
                                                        <label>Site</label>
                                                        <select class="form-control select2" tabindex="1" id="select-site" style="width:100% !important;" name="site">
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group m-b-10">
                                                        <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-location">Clear</button>
                                                        <label>Location</label>
                                                        <select class="form-control select2" tabindex="1" id="select-location" style="width:100% !important;" name="location">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="filter-container" style="display:none;">
                                                <div class="col-lg-12"> <hr style="margin-top: 0px;" /></div>
                                                
                                                <!--<div class="col-lg-3" data-fields="tag" id="filter-fields">
                                                    <label>Asset Tag</label>
                                                    <input type="text" class="form-control" placeholder="Asset Tag Keywords">
                                                </div>-->
    
                                                <div class="col-lg-3 m-b-10" id="insert-filter-fields">
    
                                                        <div class="input-group mb-3">
                                                                <select class="custom-select" id="filter-fields-option" style="min-height:40px;" placeholder="Select Fields">
                                                                        <option value="">[ Select Fields ]</option>
                                                                        <option value="tag">Tag</option>
                                                                        <option value="desc">Description</option>
                                                                        <option value="model">Model</option>
                                                                        <option value="serial">Serial No</option>
                                                                        <option value="brand">Brand</option>
                                                                        <option value="date_purchased">Date Purchased</option>
                                                                        <option value="cost">Cost</option>
                                                                        <option value="vendor">Vendor</option>
                                                                        <option value="remarks">Asset Remarks</option>
                                                                        <option value="custom_fields">Custom Data Fields</option>
                                                                    </select>
                                                            <span class="input-group-append">
                                                                    <button class="btn btn-success" type="button" id="act-add-filter-fields">Add</button>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                        <button class="btn btn-info float-right" type="button" id="btn-generate">Generate</button>
                                                        <button class="btn btn-default float-right m-r-10" type="button" id="act-show-filters"><i class="mdi mdi-filter-outline m-r-5" id="btn-filter-icon"></i> More Filters</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>

                        <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <!--<div class="row">
                                        <div class="col-xs-12 col-lg-12">
                                            <button class="btn btn-sm btn-inverse" type="button" id="btn-custom-column"><i class="mdi mdi-settings"></i> Columns</button>
                                        </div>
                                    </div>
                                    <hr class="m-b-0"/>-->
                                    <?php

                                        if($type=='donated'||$type=='damage'||$type=='disposed'||$type=='donated'||$type=='sold'||$type=='lost'){
                                            $stat_date = '';
                                            $stat_note = '';
                                            $stat_to = 'display: none;';
                                            $stat_amount = 'display: none;';

                                            if($type!='damage'&&$type!='lost'){
                                                $stat_to = '';
                                            }
                                            if($type=='sold'){
                                                $stat_amount = '';
                                            }
                                        }else{
                                            $stat_date = 'display: none;';
                                            $stat_note = 'display: none;';
                                            $stat_to = 'display: none;';
                                            $stat_amount = 'display: none;';
                                        }
                                    ?>
                                    <div class="table-responsived"  id="slimtest1">
                                        <table id="record-table" class="table table-striped table-condensed" width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-sort-initial="true" data-toggle="true" data-col="tag">Asset&nbsp;Tag</th>
                                                    <th data-col="desc">Description</th>
                                                    <th data-col="model">Model</th>
                                                    <th data-col="serial">Serial</th>
                                                    <th data-col="brand">Brand</th>
                                                    <th data-col="category">Category</th>
                                                    <th data-col="date-purchased">Date&nbsp;Purchased</th>
                                                    <th data-col="cost">Purchased&nbsp;Cost</th>
                                                    <th data-col="vendor">Vendor</th>
                                                    <th data-col="site">Site</th>
                                                    <th data-col="location">Location</th>
                                                    <th data-col="assigned-to">{{($type=='leased') ? 'Leased&nbsp;By' : 'Assigned&nbsp;To'}}</th>
                                                    <th data-col="updated-by">Updated&nbsp;By</th>
                                                    <th data-col="updated-date">Updated&nbsp;On</th>
                                                    <th data-col="remarks">Remarks</th>
                                                    <th data-col="status-date" style="{{$stat_date}}">{{ucfirst($type)}} Date</th>
                                                    <th data-col="status-note" style="{{$stat_note}}">{{ucfirst($type)}} Note</th>
                                                    <th data-col="status-to" style="{{$stat_to}}">{{ucfirst($type)}} To</th>
                                                    <th data-col="status-amount" style="{{$stat_amount}}">{{ucfirst($type)}} Amount</th>

                                                    @foreach($custom_field_list as $custom_field)
                                                        <th data-col="{{$custom_field->field_id}}" data-col-custom="1">{{$custom_field->field_label}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!--/card-->
                        </div>
                    </div><!--/col-12-->
                </div><!--/row-->
                <input type="hidden" id="_report_title" value="{{$form_title}}" />
                <input type="hidden" id="_type" value="{{ucfirst($type)}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    
@endsection

@section('js')
    
    <!--<script src="{{ custom_asset('js/report.status.js') }}"></script>-->
    <script src="{{ url('assets/js/AHBETmQUqkyGhxN8ZIWp.js') }}"></script>
@endsection