@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor" id="report-export-title" contenteditable="true"><span data-toggle="tooltip" title="For Export Filename" data-placement="bottom">{{$form_title}} <i class="mdi mdi-pencil-box-outline text-default"></i></span></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Reports</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="card-body">
                                    <form action="#" class="form-material" method="post" id="filter-form">
                                        <div class="row" id="pre-filter-container">
                                            <div class="col-lg-3">
                                                <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-category">Clear</button>
                                                <label>Category</label>
                                                <select class="form-control" tabindex="1" id="select-category" style="width:100% !important;" name="category">
                                                </select> 
                                            </div>
                                            <div class="col-lg-3">
                                                <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-site">Clear</button>
                                                <label>Site</label>
                                                <select class="form-control select2" tabindex="1" id="select-site" style="width:100% !important;" name="site">
                                                </select> 
                                            </div>
                                            <div class="col-lg-3">
                                                <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-location">Clear</button>
                                                <label>Location</label>
                                                <select class="form-control select2" tabindex="1" id="select-location" style="width:100% !important;" name="location">
                                                </select> 
                                            </div>
                                            <div class="col-lg-3 m-b-20">
                                                <button class="btn btn-default float-right btn-xs" type="button" id="unselect" data-target="#select-employee" name="employee">Clear</button>
                                                <label>Assignee</label>
                                                <select class="form-control select2" tabindex="1" id="select-employee" style="width:100% !important;" name="employee">
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="row" id="filter-container" style="display:none;">
                                            <div class="col-lg-12"> <hr /></div>
                                            
                                            <!--<div class="col-lg-3" data-fields="tag" id="filter-fields">
                                                <label>Asset Tag</label>
                                                <input type="text" class="form-control" placeholder="Asset Tag Keywords">
                                            </div>-->

                                            <div class="col-lg-3 m-b-10" id="insert-filter-fields">

                                                    <div class="input-group mb-3">
                                                            <select class="custom-select" id="filter-fields-option" style="min-height:40px;" placeholder="Select Fields">
                                                                    <option value="">[ Select Fields ]</option>
                                                                    <option value="tag">Tag</option>
                                                                    <option value="desc">Description</option>
                                                                    <option value="model">Model</option>
                                                                    <option value="serial">Serial No</option>
                                                                    <option value="brand">Brand</option>
                                                                    <option value="date_purchased">Date Purchased</option>
                                                                    <option value="cost">Cost</option>
                                                                    <option value="vendor">Vendor</option>
                                                                    <option value="status">Status</option>
                                                                    <option value="is_leasable">Leasable</option>
                                                                    <option value="custom_fields">Custom Data Fields</option>
                                                                </select>
                                                        <span class="input-group-append">
                                                                <button class="btn btn-success" type="button" id="act-add-filter-fields">Add</button>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                    <button class="btn btn-info float-right" type="button" id="btn-generate">Generate</button>
                                                    <button class="btn btn-default float-right m-r-10" type="button" id="act-show-filters"><i class="mdi mdi-filter-outline m-r-5" id="btn-filter-icon"></i> More Filters</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                        <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <!--<div class="row">
                                        
                                        <div class="col-lg-2">
                                        <button class="btn btn-sm btn-inverse float-right" type="button" id="btn-show-filter"><i class="mdi mdi-settings"></i> Filter</button>
                                            <button class="btn btn-sm btn-inverse float-right" type="button" id="btn-custom-column"><i class="mdi mdi-settings"></i> Columns</button>
                                        </div>
                                    </div>
                                    <hr class="m-b-0"/>-->
                                    <div class="table-responsived"  id="slimtest1">
                                        <table id="record-table" class="table table-striped table-condensed">
                                            <thead>
                                                <tr>
                                                    <th data-sort-initial="true" data-toggle="true" data-col="tag">Asset&nbsp;Tag</th>
                                                    <th data-col="desc">Description</th>
                                                    <th data-col="model">Model</th>
                                                    <th data-col="serial">Serial</th>
                                                    <th data-col="brand">Brand</th>
                                                    <th data-col="category">Category</th>
                                                    <th data-col="date-purchased">Date&nbsp;Purchased</th>
                                                    <th data-col="cost">Purchased&nbsp;Cost</th>
                                                    <th data-col="vendor">Vendor</th>
                                                    <th data-col="site">Site</th>
                                                    <th data-col="location">Location</th>
                                                    <th data-col="assigned-to">Assigned&nbsp;To</th>
                                                    <th data-col="updated-by">Updated&nbsp;By</th>
                                                    <th data-col="updated-date" style="width: 120px !important;">Updated&nbsp;On</th>
                                                    <th data-col="remarks">Remarks</th>
                                                    <th data-col="status">Status</th>
                                                    @foreach($custom_field_list as $custom_field)
                                                        <th data-col="{{$custom_field->field_id}}" data-col-custom="1">{{$custom_field->field_label}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!--/card-->
                        </div>
                    </div><!--/col-12-->
                </div><!--/row-->
                <input type="hidden" id="_report_title" value="{{$form_title}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    
@endsection

@section('js')
    
    <!--<script src="{{ custom_asset('js/report.asset.1.js') }}"></script>-->
    <script src="{{ url('assets/js/dMTchms6YfZzWU78QuHr.js') }}"></script>
@endsection