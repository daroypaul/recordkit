@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Settings</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <!--<div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>-->
                            <div class="card-body">
                                <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-sm-12 col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Prefix</label>
                                                            <input type="text" id="input_prefix" class="form-control" placeholder="Prefix" name="prefix" value="{{ isset($ids_option) ? $ids_option->ids_prefix : 'FA' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <div style="border: 1px dashed #06d79c; padding: 8px; text-align: center;">Incremental Number</div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Suffix</label>
                                                            <input type="text" id="input_suffix" class="form-control" placeholder="Suffix" name="suffix" value="{{ isset($ids_option) ? $ids_option->ids_suffix : '' }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Asset Tag Start Number</label>
                                                            <input type="number" id="input_counter" class="form-control" placeholder="Start Number" name="start_number" value="{{ isset($last_number) ? $last_number : '1' }}" min="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group m-b-5">
                                                                <div class="row">
                                                                    <div class="col-lg-5">
                                                                            <label class="form-control-label text-bold">Padding</label><br/>
                                                                            <!--<input name="padding" value="zero" id="radio_1" checked="checked" type="radio" class="with-gap">
                                                                            <label for="radio_1">Zero</label>
                                                                                <br/>
                                                                            <input name="padding" value="none" id="radio_2" type="radio" class="with-gap">
                                                                            <label for="radio_2">None</label>-->
                                                                            <input id="padding" class="filled-in" type="checkbox" name="padding" value="enable" {{ ($ids_option->ids_with_padding) ? 'checked' : '' }} >
                                                                            <label for="padding">Enable</label>
                                                                    </div>
                                                                    <div class="col-lg-7">
                                                                        <span class="form-control-label">No. Of Digits</span>
                                                                        <input type="number" id="input_digits" class="form-control" placeholder="Digits" name="digit" value="{{ isset($ids_option) ? $ids_option->ids_padding_digits : '5' }}" min="1" max="12">               
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                                <input id="dashed" class="filled-in" name="dashed" value="1" type="checkbox" {{ ($ids_option->ids_with_dashed) ? 'checked' : '' }}>
                                                                <label for="dashed">Add Dash (-) on incremental number</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Current Year</label><br/>
                                                            <input id="with_year" name="year" class="filled-in" type="checkbox" value="1" {{ ($ids_option->ids_with_year) ? 'checked' : '' }}>
                                                            <label for="with_year">Enable</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Display String</label>
                                                            <select class="form-control custom-select" id="input_string" name="string">
                                                                <?php
                                                                    $a = ['Last Two (2) Digit', 'Full Year'];
                                                                    $b = ['2','4'];
                                                                    $c = ($ids_option->ids_string) ? $ids_option->ids_string : 2;

                                                                    $i = 0;
                                                                    foreach($a as $strings){
                                                                        if($c==$b[$i]){
                                                                            echo '<option value="'.$b[$i].'" selected>'.$strings.'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$b[$i].'">'.$strings.'</option>';
                                                                        }
                                                                        $i++;
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label class="form-control-label text-bold">Insert Year</label>
                                                            <select class="form-control custom-select" id="input_insert" name="insert_year">
                                                                <!--<option value="BEF_PRE">Before Prefix</option>
                                                                <option value="AFT_PRE" selected>After Prefix</option>
                                                                <option value="BEF_SUF">Before Suffix</option>
                                                                <option value="AFT_SUF">After Suffix</option>-->
                                                                <?php
                                                                    $a = ['Before Prefix', 'After Prefix', 'Before Suffix', 'After Suffix'];
                                                                    $b = ['BEF_PREF','AFT_PREF','BEF_SUF','AFT_SUF'];
                                                                    $c = ($ids_option->ids_string) ? $ids_option->ids_insert_year : 'AFT_PREF';

                                                                    $i = 0;
                                                                    foreach($a as $insert){
                                                                        if($c==$b[$i]){
                                                                            echo '<option value="'.$b[$i].'" selected>'.$insert.'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$b[$i].'">'.$insert.'</option>';
                                                                        }
                                                                        $i++;
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-12 col-lg-4 text-center">
                                                <p class="text-center">Display Asset Tag</p>
                                                <h1 id="display-tag">{{($asset_tag) ? $asset_tag : 'FA-00001'}}</h1>
                                            </div>
                                        
                                            <div class="col-sm-12">
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <a href="/asset/list/all" type="button" class="btn btn-inverse">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
<script src="{{ url('assets/js/6p57rltPGAvjguUhFHWQ.js') }}"></script>
@endsection