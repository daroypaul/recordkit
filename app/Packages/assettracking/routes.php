<?php

Route::group(['middleware' => ['web','auth'], 'namespace' => 'App\Packages\assettracking\Controllers'], function()
{

    #############################
    ##  Main Apps Route Group  ##
    #############################
    Route::group(['prefix' => 'asset'], function () {

        ## Asset Record Manager ##
        Route::get('list/all', 'AssetManagerController@all_list');
        Route::get('list/leasable', 'AssetManagerController@leasable_list');
        Route::get('add', 'AssetManagerController@add');
        Route::post('add', 'AssetManagerController@store');
        Route::get('{id}/edit', 'AssetManagerController@edit');
        Route::post('{id}/edit', 'AssetManagerController@store');
        Route::get('{id}/details', 'AssetManagerController@details');
        Route::post('delete', 'AssetManagerController@delete');
        Route::post('archive', 'AssetManagerController@archive');
        Route::post('bulk_action', 'AssetManagerController@bulk_action');
        Route::post('upload/image', 'AssetManagerController@images');#Upload asset images
        Route::post('upload/docs', 'AssetManagerController@document_upload');#Upload asset images
        Route::post('docs/delete', 'AssetManagerController@document_delete');#Upload asset images
        Route::post('img/delete', 'AssetManagerController@image_delete');#Upload asset images
        Route::post('img/set-cover', 'AssetManagerController@image_cover');#Upload asset images
        Route::post('maintenance/add', 'AssetManagerController@maintenance_store');#Upload asset images
        Route::post('maintenance/delete', 'AssetManagerController@maintenance_delete');
        Route::post('maintenance/fetch', 'AssetManagerController@maintenance_fetch_data');
        Route::post('warranty/add', 'AssetManagerController@warranty_add');
        Route::post('warranty/delete', 'AssetManagerController@warranty_delete');

        #Asset Bulk
        Route::get('move', 'AssetMoveOut@index');
        Route::post('move', 'AssetMoveOut@store');
        Route::get('transfer', 'AssetMoveOut@index');
        Route::post('transfer', 'AssetMoveOut@store');

        #In/Out
        Route::get('checkin', 'AssetInOut@check_in');
        Route::post('checkin', 'AssetInOut@inout_store');
        Route::get('checkout', 'AssetInOut@check_out');
        Route::post('checkout', 'AssetInOut@inout_store');
        Route::get('checkin/{id}/new', 'AssetInOut@check_in');
        Route::post('checkin/{id}/new', 'AssetInOut@inout_store');
        Route::get('checkout/{id}/new', 'AssetInOut@check_out');
        Route::post('checkout/{id}/new', 'AssetInOut@inout_store');

        #Lease
        Route::get('lease', 'AssetLease@lease');
        Route::post('lease', 'AssetLease@lease_store');
        Route::get('lease/{id}/new', 'AssetLease@lease');
        Route::post('lease/{id}/new', 'AssetLease@lease_store');
        
        #Return Lease
        Route::get('return-lease', 'AssetLease@return_leased');
        Route::post('return-lease', 'AssetLease@return_leased_store');
        Route::get('return-lease/{id}/new', 'AssetLease@return_leased');
        Route::post('return-lease/{id}/new', 'AssetLease@return_leased_store');

        #Fitching Records
        Route::POST('fetch/tag', 'AssetTagGenerator@asset_tag');
        //Route::POST('fetch/list', 'AssetManagerController@fetch_all_list');
        Route::POST('fetch/custom-field', 'AssetManagerController@fetch_custom_field');
        Route::POST('fetch/all', 'AssetManagerController@get_all_asset');
        Route::POST('fetch/all-leasable', 'AssetManagerController@fetch_leasable_list');
        Route::POST('fetch/available-leasable', 'AssetManagerController@get_available_leasable_asset');
        Route::POST('fetch/leased-by-requestor', 'AssetManagerController@get_leased_asset_by_requestor');
        Route::POST('fetch/current-leased', 'AssetManagerController@get_current_leased_asset');
        
        //Route::POST('fetch/current-leased', 'AssetLease@fetch_current_leased');
        //Route::GET('fetch/leased-by-requestor', 'AssetManagerController@get_leased_asset_by_requestor');
        //Route::POST('fetch/leased-asset', 'AssetLease@fetch_leased_asset');
        //Route::GET('fetch/available-leasable', 'AssetManagerController@get_available_leasable_asset');
        //Route::GET('fetch/current-leased', 'AssetManagerController@get_current_leased_asset');
        //Route::GET('fetch/all', 'AssetManagerController@get_all_asset');

        ### File Routes ###
        Route::get('asset/{type}/{folder}/{filename}', function ($type, $folder, $filename)
        {
            $path = base_path('app/modules/'.$folder.'/resources/assets/'.$type.'/'.$filename);

            if (!File::exists($path)) {
                abort(404);
            }
        
            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;
        });
    });

    ##########################
    ##  Report Route Group  ##
    ##########################
    Route::group(['prefix' => 'reports'], function () {
        Route::group(['prefix' => 'asset'], function(){
            Route::GET('/', 'ReportAsset@index');
            Route::POST('/', 'ReportAsset@generate_report_new');

            Route::post('/get-custom-field', 'ReportAsset@get_custom_fields');
            Route::get('/{type}', 'ReportAsset@index_report');
            
        });

        Route::group(['prefix' => 'checkout'], function(){
            Route::GET('/', 'ReportCheckout@index');
            Route::POST('/', 'ReportCheckout@generate_report_new');
            Route::get('/{type}', 'ReportCheckout@index_report');
        });

        Route::group(['prefix' => 'checkin'], function(){
            Route::GET('/', 'ReportCheckin@index');
            Route::POST('/', 'ReportCheckin@generate_report');
        });

        Route::group(['prefix' => 'leasable-asset'], function(){
            Route::get('/{type}', 'ReportLease@index');
            Route::POST('/', 'ReportLease@generate_report_new');
        });

        Route::group(['prefix' => 'status'], function(){
            Route::POST('/', 'ReportStatus@generate_report');
            Route::get('/{type}', 'ReportStatus@index_report');
        });
    });

    ############################
    ##  Settings Route Group  ##
    ############################
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/tag', 'SettingAssetTag@index');
        Route::post('/tag', 'SettingAssetTag@store');
    });
});