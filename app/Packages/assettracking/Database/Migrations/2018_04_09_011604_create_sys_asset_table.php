<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset', function (Blueprint $table) {
            $table->increments('asset_id');
            $table->integer('account_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('asset_tag', 255)->nullable();
            $table->string('asset_desc', 500)->nullable();
            $table->string('asset_serial', 255)->nullable();
            $table->string('asset_model', 255)->nullable();
            $table->string('asset_brand', 255)->nullable();
            $table->string('asset_vendor', 255)->nullable();
            $table->string('purchased_cost', 100)->nullable();
            $table->string('po_number', 255)->nullable();
            $table->string('asset_status', 255)->nullable();
            $table->string('asset_remarks', 1000)->nullable();
            $table->integer('assign_to')->nullable();
            $table->date('date_stat')->nullable();
            $table->string('stat_to', 255)->nullable();
            $table->string('stat_amount', 100)->nullable();
            $table->string('stat_remarks', 1000)->nullable();
            $table->integer('is_leasable')->nullable();
            $table->date('date_purchased')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->dateTime('date_deleted')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->string('record_stat', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset');
    }
}
