<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetLeaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_lease_items', function (Blueprint $table) {
            $table->increments('item_id');
            $table->integer('lease_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->string('asset_tag', 255)->nullable();
            $table->string('asset_desc', 500)->nullable();
            $table->string('asset_model', 255)->nullable();
            $table->string('asset_serial', 255)->nullable();
            $table->date('date_returned')->nullable();
            $table->string('release_remarks', 1000)->nullable();
            $table->string('return_remarks', 1000)->nullable();
            $table->integer('item_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_lease_items');
    }
}
