<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetInoutItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_inout_items', function (Blueprint $table) {
            $table->increments('item_id');
            $table->integer('inout_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->integer('cat_id')->nullable();
            $table->string('asset_tag', 255)->nullable();
            $table->string('asset_desc', 500)->nullable();
            $table->string('asset_model', 255)->nullable();
            $table->string('asset_serial', 255)->nullable();
            $table->string('item_remarks', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_inout_items');
    }
}
