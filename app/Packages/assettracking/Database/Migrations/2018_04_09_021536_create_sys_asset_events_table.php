<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_events', function (Blueprint $table) {
            $table->increments('event_id');
            $table->integer('account_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->string('event_type', 50)->nullable();
            $table->date('event_date')->nullable();
            $table->string('event_remarks', '1000')->nullable();
            $table->integer('ref_id')->nullable();
            $table->integer('prev_site_id')->nullable();
            $table->integer('prev_location_id')->nullable();
            $table->integer('new_site_id')->nullable();
            $table->integer('new_location_id')->nullable();
            $table->integer('requestor_id')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('record_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_events');
    }
}
