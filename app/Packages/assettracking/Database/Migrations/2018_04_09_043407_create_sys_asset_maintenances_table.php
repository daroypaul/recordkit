<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('maintain_title', 500)->nullable();
            $table->string('maintain_type', 50)->nullable();
            $table->string('maintain_cost', 100)->nullable();
            $table->string('maintain_remarks', 1000)->nullable();
            $table->string('maintain_stat', 50)->nullable();
            $table->integer('is_warranty')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_completion')->nullable();
            $table->date('date_completed')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_maintenances');
    }
}
