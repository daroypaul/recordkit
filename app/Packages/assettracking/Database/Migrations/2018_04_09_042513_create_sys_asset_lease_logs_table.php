<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetLeaseLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_lease_logs', function (Blueprint $table) {
            $table->increments('lease_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('requestor_id')->nullable();
            $table->date('lease_start')->nullable();
            $table->date('lease_expire')->nullable();
            $table->string('lease_remarks', 1000)->nullable();
            $table->integer('lease_stat')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('record_stat', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_lease_logs');
    }
}
