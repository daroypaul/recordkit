<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetInoutLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_inout_logs', function (Blueprint $table) {
            $table->increments('inout_id');
            $table->integer('account_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('requestor_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('inout_type', 50)->nullable();
            $table->date('inout_date')->nullable();
            //$table->string('inout_requestor_name', 100)->nullable();
            //$table->string('inout_customer_name', 100)->nullable();
            $table->string('inout_remarks', 1000)->nullable();
            $table->string('inout_requestor_name', 100)->nullable();
            $table->integer('inout_with_signature')->nullable();
            $table->longText('inout_sender_signature')->nullable();
            $table->longText('inout_receiver_signature')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('record_stat', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_inout_logs');
    }
}
