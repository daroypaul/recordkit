<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSysAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sys_asset');
        Schema::dropIfExists('sys_asset_custom_value');
        Schema::dropIfExists('sys_asset_events');
        Schema::dropIfExists('sys_asset_inout_logs');
        Schema::dropIfExists('sys_asset_inout_items');
        Schema::dropIfExists('sys_asset_lease_logs');
        Schema::dropIfExists('sys_asset_lease_items');
        Schema::dropIfExists('sys_asset_maintenances');
        Schema::dropIfExists('sys_asset_warranties');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
