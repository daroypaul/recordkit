<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysAssetWarrantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_asset_warranties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->integer('months')->nullable();
            $table->date('warranty_expire')->nullable();
            $table->string('warranty_remarks', 1000)->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->integer('recorded_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_asset_warranties');
    }
}
