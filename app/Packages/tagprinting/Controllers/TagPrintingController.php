<?php

namespace App\Packages\TagPrinting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\packages\assettracking\models\Assets;

use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class TagPrintingController extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_tag_printing')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;


        $data = [
            "form_title"=> "Tag Label Printing",
        ];

        return view('tagprinting::label_printing')->with($data);
    }

    public function generate(Request $request){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_tag_printing')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        if(!count($request->tags)){
            session()->flash('empty_tags', 1);
            return redirect('tools/label-printing');
        }

        $data = [
            "type" => $request->type,
            "size" => $request->size,
            "orientation" => $request->orientation,
            "tags"=>$request->tags,
            "form_title"=> "Tag Label Print Preview",
        ];

        //return $request->tags;
        return view('tagprinting::label_print_preview')->with($data);

    }
}
