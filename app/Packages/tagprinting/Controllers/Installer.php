<?php

namespace App\Packages\TagPrinting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Utilities\Migrations as Migrations;
use Artisan;
use Menus;
use Packages;
use Options;

class Installer
{

    public static function install($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-tag-printing')->count();
        $get_menu = Menus::where('menu_name', 'tools-main')->first();
        $parent_menu = $get_menu->menu_id;
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-tag-printing')->first();
            $menu_id = $get_menu->menu_id;

            ## Activating Menu ##
            $menu = Menus::find($menu_id);
            $menu->is_disable = 0;
            $menu->save();
        }else{
            ### Add Menu Item ###
            $menu = new Menus;
            $menu->account_id = $account_id;
            $menu->parent_menu = $parent_menu;
            $menu->menu_name = 'package-tag-printing';
            $menu->menu_label = 'Tag Label Printing';
            $menu->menu_url = 'tools/label-printing';
            $menu->menu_icon = 'mdi mdi-printer m-r-10';
            $menu->privilege_key = '["exempt"]';
            $menu->is_disable = 0;
            $menu->save();
        }

        ### Adding Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;
        if(!isset($json_data['DNt1xIPoHs4Gbf9qXjwh.js'])){
            $json_data['DNt1xIPoHs4Gbf9qXjwh.js']="app/packages/tagprinting/resources/assets/js/app.js";

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
        $package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

        

        if($menu||$package_activate||$package_installed){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function uninstall($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-tag-printing')->count();
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-tag-printing')->first();
            $menu_id = $get_menu->menu_id;

            ## Activating Menu ##
            $menu = Menus::find($menu_id)->delete();
        }

        ### Delete Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;

        $arr_index = array();
        foreach ($json_data as $key => $value) {
            if($key=='DNt1xIPoHs4Gbf9qXjwh.js'){
                $arr_index[] = $key;
            }
        }

        if($arr_index){
            // delete json data
            foreach ($arr_index as $i)
            {
                unset($json_data[$i]);
            }

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        $package_installed = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];

        if($menu||$package_activate||$package_installed){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function activate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-tag-printing')->count();
        $get_menu = Menus::where('menu_name', 'tools-main')->first();
        $parent_menu = $get_menu->menu_id;
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-tag-printing')->first();
            $menu_id = $get_menu->menu_id;

            ## Activating Menu ##
            $menu = Menus::find($menu_id);
            $menu->is_disable = 0;
            $menu->save();
        }else{
            ### Add Menu Item ###
            $menu = new Menus;
            $menu->account_id = $account_id;
            $menu->parent_menu = $parent_menu;
            $menu->menu_name = 'package-tag-printing';
            $menu->menu_label = 'Tag Label Printing';
            $menu->menu_url = 'tools/label-printing';
            $menu->menu_icon = 'mdi mdi-printer m-r-10';
            $menu->privilege_key = '["exempt"]';
            $menu->is_disable = 0;
            $menu->save();
        }

        ### Adding Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;
        if(!isset($json_data['DNt1xIPoHs4Gbf9qXjwh.js'])){
            $json_data['DNt1xIPoHs4Gbf9qXjwh.js']="app/packages/tagprinting/resources/assets/js/app.js";

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
        //$package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

        

        if($menu||$package_activate){
            return 'true';
        }else{
            return 'false';
        }
    }


    public static function deactivate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-tag-printing')->count();
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-tag-printing')->first();
            $parent_menu = $get_menu->menu_id;

            ## Disable Menu ##
            $menu = Menus::find($parent_menu);
            $menu->is_disable = 1;
            $menu->save();
        }

        ### Delete Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;

        $arr_index = array();
        foreach ($json_data as $key => $value) {
            if($key=='DNt1xIPoHs4Gbf9qXjwh.js'){
                $arr_index[] = $key;
            }
        }

        if($arr_index){
            // delete json data
            foreach ($arr_index as $i)
            {
                unset($json_data[$i]);
            }

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_activate = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        //$package_installed = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];

        if($menu||$package_activate){
            return 'true';
        }else{
            return 'false';
        }
    }
}
