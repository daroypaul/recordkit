<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'tools', 'namespace' => 'App\Packages\TagPrinting\Controllers'], function()
{
    Route::get('/', function() {
        return redirect('dashboard');
    });

    Route::get('/label-printing', 'TagPrintingController@index');
    Route::post('/label-printing', 'TagPrintingController@generate');
});