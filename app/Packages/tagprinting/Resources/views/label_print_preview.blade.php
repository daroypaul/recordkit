<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$form_title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ custom_asset('js/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ custom_asset('css/template/default/css/style.css') }}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ custom_asset('css/template/default/css/colors/'.$system_options->option_skins.'.css') }}" id="theme" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/card-page.css') }}" rel="stylesheet">
    <style>
        @page {
            margin: 0;
        }

        .page{
            /*border:1px solid #cccccc;*/
            /*width: 21cm;*/
            @if($orientation=='landscape')
                @if($size=='letter')
                    width: 11in;
                @elseif($size=='a4')
                    width: 11.69in;
                @elseif($size=='legal')
                    width: 13in;
                @else
                    width: 11in;
                @endif
            @elseif($orientation=='portrait')
                @if($size=='letter')
                    width: 8.5in;
                @elseif($size=='a4')
                    width: 8.27in;
                @elseif($size=='legal')
                    width: 8.5in;
                @else
                    width: 8.5in;
                @endif
            @else
                width: 11in;
            @endif
        }

        .card-columns{
            /*padding: 0.2in !important;*/
            padding: 0.2in !important;
        }

        .card-header{
            padding: 3px 5px;
        }

        .card-body{
            padding: 15px 15px 5px 15px;
        }

        .barcode{
            @if($type=='barcode')
                height: 40px;
                width:100%;
            @elseif($type=='qrcode')
            
            @endif
        }

        .developer{
            font-size:60%;
            margin-bottom:0px;   
        }

        @media print {
            .card-header{
                background-color: #398bf7 !important;
                -webkit-print-color-adjust: exact; 
            }
        }
    </style>
</head>
<body>
    <div class="page">
                        <div class="card-columns" style="column-count: {{($orientation=='landscape') ? 4 : 3}};">

                            @foreach($tags as $tag)
                                
                            <div class="card">
                                <div class="card-header text-center text-white" style="background-color: #398bf7 !important; -webkit-print-color-adjust: exact;">
                                    <p class="m-b-0"><small>{{$company_info->comp_name}}</small></p>
                                </div>
                                
                                <div class="card-body text-center">
                                    <img src="data:image/png;base64,{{($type=='barcode') ? DNS1D::getBarcodePNG('FA-1234567893', 'C39') : DNS2D::getBarcodePNG('$tag', 'QRCODE')}}" class="barcode" alt="barcode" />
                                    <small class="m-b-0s">{{$tag}}</small>
                                    <hr class="m-b-0 m-t-10"/>
                                    <p class="developer">Powered By: RecordKit Modular System</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    
        </div>
    <script src="{{ custom_asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ custom_asset('js/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>