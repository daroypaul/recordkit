@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Tools</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="card">
                            <!--<div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>-->
                            <div class="card-body">
                                <form action="label-printing" class="form-horizontal form-bordered form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="form-group">
                                            <small>Note: The result layout may not print perfectly, you can configure via browser printing settings. Best result in Google Chrome</small>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="control-label text-bold text-right col-md-3">Asset <i class="mdi mdi-check-circle form-required-helper text-danger" title="Required"></i></label>
                                            <div class="col-md-8">
                                                <select class="form-control js-example-theme-multiple" id="asset-tags" multiple="multiple" name="tags[]" style="width: 100%">
                                                </select>
                                                @if(Session::get('empty_tags'))<small class="text-danger">Select an asset</small>@endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                                <label class="control-label text-bold text-right col-md-3">Label Type</label>
                                                <div class="col-md-3">
                                                    <select class="form-control custom-select" name="type">
                                                        <?php
                                                            $skins = ['qrcode','barcode',];

                                                            foreach($skins as $skin){
                                                                    echo '<option value="'.$skin.'">'.strtoupper(str_replace('-', ' ',$skin)).'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-bold text-right col-md-3">Paper Size</label>
                                            <div class="col-md-3">
                                                <select class="form-control custom-select" name="size">
                                                    <?php
                                                        $sizes = ['a4','letter','legal'];

                                                        foreach($sizes as $size){
                                                                echo '<option value="'.$size.'">'.ucwords($size).'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-b-0  last row">
                                            <label class="control-label text-bold text-right col-md-3">Orientation</label>
                                            <div class="col-md-3">
                                                <select class="form-control custom-select" name="orientation">
                                                    <?php
                                                        $sizes = ['landscape','portrait'];

                                                        foreach($sizes as $size){
                                                                echo '<option value="'.$size.'">'.ucwords($size).'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Generate Label</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
    
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    
    <script src="{{ url('assets/js/DNt1xIPoHs4Gbf9qXjwh.js') }}"></script>
@endsection