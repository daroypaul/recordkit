/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    function formatState (data) {
        if (!data.id) { return data.text; }
        //$(data.element).data('img')
        var img = (data.img)? '<img src="'+$apps_base_url+'/uploaded/file/'+data.img+'" width="30" class="img-thumbnail" style="padding:.10rem;" />' : '';
        var $result= $(
          '<span>'+img+' ' + data.text + '</span>'
        );
        return $result;
      };

    $("#asset-tags").select2({
        theme: "classic",
        templateResult: formatState,
        ajax: {
            url: $apps_base_url+'/asset/fetch/all',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    keywords: params.term,
                    type: 'all'
                }

                return query;
              },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        var serial = (item.asset_serial) ? ' - ' + item.asset_serial : '';
                        return {
                            text: item.asset_tag+' - ' + item.asset_model,
                            img: item.image_name,
                            id: item.asset_tag
                        }
                    })
                };
            }
          }
    });

});