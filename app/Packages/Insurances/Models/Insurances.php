<?php

namespace App\Packages\Insurances\Models;

use Illuminate\Database\Eloquent\Model;

class Insurances extends Model
{
    protected $table = 'sys_insurances';
    protected $primaryKey = 'insurance_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';
}
