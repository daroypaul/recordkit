<?php

namespace App\Packages\Insurances\Models;

use Illuminate\Database\Eloquent\Model;

class AssetInsurances extends Model
{
    protected $table = 'sys_asset_insurance_link';
    public $timestamps = false;

    public function insurances()
    {
        return $this->hasMany('App\Packages\Insurances\Models\Insurances','insurance_id','insurance_id');
    }
}
