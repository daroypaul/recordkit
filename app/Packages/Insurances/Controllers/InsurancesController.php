<?php

namespace App\Packages\Insurances\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Packages\Insurances\Models\Insurances;
use App\Packages\Insurances\Models\AssetInsurances;
use App\modules\adminprofile\models\UserGroups;

use Validator;
use Session;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use Mail;
use DateTime;
use Options;
use Menus;

class InsurancesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //return view('insurances::index');
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_insurance')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_group_id = (Auth::check()) ? Auth::user()->user_group_id : 0;

        $insurances = Insurances::where('account_id', $account_id)->where('record_stat', 0)->orderBy('insurance_name', 'asc')->get();

        $counts=0;
        $a = [];
        $b = [];
        foreach($insurances as $insurance){
            $total_asset = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance->insurance_id)->count();

            $a['id'] = $insurance->insurance_id;
            $a['link_assets'] = $total_asset;
            $b[] = $a;
        }
        $data = [
            "record_list"=>$insurances,
            "form_title"=> "Insurances",
            "assets"=>$b,
        ];

        return view('insurances::insurances')->with($data);
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('insurances::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;


        $timezone = Options::apps('timezone');
        $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
        $currency = Options::apps('currency')->currency_code;

        $rules = [
            'name' => 'required|min:2|max:100',
            'provider' => 'required|min:2|max:100',
            'start_date' => 'required|date_format:"Y-m-d"',
            'expiry_date' => 'required|date_format:"Y-m-d"',
            'remarks' => 'max:1000',
            'cost' => "nullable|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/",
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];

        /*if($request->ref=='add'&&!Helpers::get_user_privilege('pkg_insurances_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&!Helpers::get_user_privilege('pkg_insurances_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }*/
        if(!Helpers::get_user_privilege('pkg_insurance')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            
            if($request->ref=='add'){
                $check_record = Insurances::where('account_id', $account_id)->where('record_stat', 0)->where('insurance_name', $request->name)->count();

                if($check_record){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Record';
                    $response['stat_msg'] = 'Insurance already exists.';
                }else{
                    $add_insurance = new Insurances;
                    $add_insurance->account_id = $account_id;
                    $add_insurance->insurance_name = ucfirst($request->name);
                    $add_insurance->insurance_remarks = ucfirst($request->remarks);
                    $add_insurance->insurance_cost = Helpers::converts('currency_to_int', $request->cost);
                    $add_insurance->insurance_provider = ucfirst($request->provider);
                    $add_insurance->insurance_start = $request->start_date;
                    $add_insurance->insurance_expire = $request->expiry_date;
                    $add_insurance->recorded_by = $user_id;
                    $add_insurance->updated_by = $user_id;
                    $add_insurance->record_stat = 0;
                    $add_insurance->save();
                    
                    if($add_insurance){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New insurance successfully added';

                        $todate = new DateTime(date('Y-m-d'));
                        $expire = new DateTime(date('Y-m-d', strtotime($request->expiry_date)));
                        $diff = $expire->diff($todate)->format("%a");

                        $response['remaining'] = ($diff<2) ? $diff . ' Day' : $diff . ' Days';
                        $response['name'] = $add_insurance->insurance_name;
                        $response['provider'] = $add_insurance->insurance_provider;
                        $response['start'] = date('d-M-Y', strtotime($add_insurance->insurance_start));
                        $response['expire'] = date('d-M-Y', strtotime($add_insurance->insurance_expire));
                        $response['cost'] = ($add_insurance->insurance_cost) ? number_format($add_insurance->insurance_cost, 2) . ' ' . $currency : '---';
                        $response['remarks'] = ($add_insurance->insurance_remarks||strlen($add_insurance->insurance_remarks) > 30) ? substr($add_insurance->insurance_remarks,0,30)."..." : '---';
                        $response['remarks_full'] = $add_insurance->insurance_remarks;
                        $response['asset'] = 0;
                        $response['id'] = $add_insurance->insurance_id;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add insurance. Error occured during process.';
                    }
                }
            }else if($request->ref=='edit'){
                $insurance_id= $request->id;
                $check_record = Insurances::where('account_id', $account_id)->where('record_stat', 0)->where('insurance_id', $insurance_id)->count();
                
                if($check_record){
                    //If remarks is updated and name is constant
                    $check1 = Insurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->where('insurance_name', $request->name)->where('record_stat', 0)->count();
                    //If name is updated
                    $check2 = Insurances::where('account_id', $account_id)->where('insurance_name', $request->name)->where('record_stat', 0)->count();

                    if($check1!=0){
                        $update = static::update($request, $insurance_id);
                        $update_done = true;
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'Insurance already exists.';

                        $update_done = false;
                    }else{
                        $update = static::update($request, $insurance_id);
                        $update_done = true;
                    }

                    if($update_done){
                        if($update){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Insurance has been successfully updated';

                            $total_asset = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->count();

                            //Compute Number of Days
                            $todate = new DateTime(date('Y-m-d'));
                            $expire = new DateTime(date('Y-m-d', strtotime($request->expiry_date)));

                            if(strtotime($request->expiry_date) > strtotime(date('Y-m-d'))){
                                $diff = $expire->diff($todate)->format("%a");
                            }else{
                                $diff = 0;
                            }

                            $response['remaining'] = ($diff<2) ? $diff . ' Day' : $diff . ' Days';
                            $response['name'] = $update->insurance_name;
                            $response['provider'] = $update->insurance_provider;
                            $response['start'] = date('d-M-Y', strtotime($update->insurance_start));
                            $response['expire'] = date('d-M-Y', strtotime($update->insurance_expire));
                            $response['cost'] = ($update->insurance_cost) ? number_format($update->insurance_cost, 2) . ' ' . $currency : '---';
                            $response['remarks'] = ($update->insurance_remarks||strlen($update->insurance_remarks) > 30) ? substr($update->insurance_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $update->insurance_remarks;
                            $response['asset'] = $total_asset;
                            $response['id'] = $update->insurance_id;
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update. Error occured during process.';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or record might be deleted.';
                }
            }
        }

        return $response;

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('pkg_insurance')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $record = Insurances::where('account_id', $account_id)
                            ->where('record_stat', 0)
                            ->where('insurance_id', $id)
                            ->first();

        $asset_associated = DB::table('sys_asset_insurance_link')
                            ->select('sys_asset_insurance_link.*', 'sys_asset.*','sys_site.site_name','sys_site_location.location_name')
                            ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_insurance_link.asset_id')
                            ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                            ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                            ->where('sys_asset_insurance_link.account_id', $account_id)
                            ->where('sys_asset_insurance_link.insurance_id', $id)
                            ->orderBy('sys_asset.asset_tag', 'asc')
                            ->get();

        $data = [
            "id"=>$id,
            "record"=>$record,
            "form_title"=> "Insurance Detail",
            "associated_assets"=>$asset_associated,
        ];
    
        //return view('module.insurances_view')->with($data);       
        return view('insurances::insurances_view')->with($data);     
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('insurances::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public static function update(Request $request, $id)
    {
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $add_insurance = Insurances::find($id);
        $add_insurance->account_id = $account_id;
        $add_insurance->insurance_name = ucfirst($request->name);
        $add_insurance->insurance_remarks = ucfirst($request->remarks);
        $add_insurance->insurance_cost = Helpers::converts('currency_to_int', $request->cost);
        $add_insurance->insurance_provider = ucfirst($request->provider);
        $add_insurance->insurance_start = $request->start_date;
        $add_insurance->insurance_expire = $request->expiry_date;
        $add_insurance->updated_by = $user_id;
        $add_insurance->save();

        return $add_insurance;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        $del_type = $request->del_type;
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admsplr');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('pkg_insurance')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            if($del_type=='single'){
                $insurance_id = $request->id;
                
                $check_record = Insurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->count();

                if($check_record){
                    $delete = Insurances::find($insurance_id)->delete();

                    if($delete){
                        $delete_insurance_link = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->delete();

                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Insurance has been successfully deleted';
                        $response['id'] = $insurance_id;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to delete. Error occured during process.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to delete. Invalid reference ID or record might be deleted.';
                }
            }else if($del_type=='multi'){
                $x = [];
                $s = [];
                foreach($request->id as $insurance_id){
                    //$asset_record = Assets::find($insurance_id);
                    
                    $check_record = Insurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->count();
                    if($check_record!=0){
                        $delete_record = Insurances::find($insurance_id)->delete();
    
                        if($delete_record){
                            $s[] = $insurance_id;
                            $delete_insurance_link = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->delete();
                            //$_log_desc .= $name . ', ';
                        }else{
                            $x[] = $insurance_id;
                        }
                    }else{
                        $x[] = $insurance_id;
                    }
                }
    
                $response['eid'] = $x;
                $response['sid'] = $s;
            }
            /**/
        }

        return $response;
    }


    public function link_asset(Request $request)
    {   if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'insurance'=>'required|numeric',
        ];

        $messages = [
            'label.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);#Run the validation

        if(!$request->asset||count($request->asset)==0){
            $validator->getMessageBag()->add('asset', 'Select an asset');
        }

        $val_msg = $validator->errors();
        $response = [];

        if(!Helpers::get_user_privilege('pkg_insurance')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()&&count($request->asset)==0) {
            $response['val_error']= $val_msg;
        }else if($validator->fails()){
            $response['val_error']= $val_msg;
        }else if(count($request->asset)==0){
            $response['val_error']= $val_msg;
        }else{
            $insurance_id = $request->insurance;
            
            $check_record = Insurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->count();

            if($check_record){
                $new_record = [];
                $to_be_remove = [];
                $s = 0;

                if($request->ref=='single-asset'){
                    $asset_id = $request->asset;

                    $insurance_link = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->where('asset_id', $asset_id)->count();

                    if($insurance_link){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Insurance already linked.';
                    }
                    else{
                        $add_record = new AssetInsurances;
                        $add_record->account_id = $account_id;
                        $add_record->asset_id = $asset_id;
                        $add_record->insurance_id = $insurance_id;
                        $add_record->date_recorded = Carbon::now('UTC')->toDateTimeString();
                        $add_record->recorded_by = $user_id;
                        $add_record->save();

                        if($add_record){
                            $asset_associated = DB::table('sys_asset_insurance_link')
                                            ->select('sys_asset_insurance_link.id', 'sys_asset_insurance_link.asset_id', 'sys_users.user_fullname', 'sys_insurances.*')
                                            ->join('sys_insurances', 'sys_insurances.insurance_id', '=', 'sys_asset_insurance_link.insurance_id')
                                            ->join('sys_users', 'sys_users.user_id', '=', 'sys_asset_insurance_link.recorded_by')    ->where('sys_asset_insurance_link.account_id', $account_id)
                                            ->where('sys_asset_insurance_link.asset_id', $asset_id)
                                            ->orderBy('sys_insurances.insurance_name', 'asc')
                                            ->get();
                            
                            $currency = Options::apps('currency')->currency_code;

                            $response['stat_msg'] = 'Linked successfully.';
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['associated_assets'] = $asset_associated;
                            $response['currency'] = $currency;
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to link asset to insurance. Error occured during process.';
                        }
                    }
                }else{
                    foreach($request->asset as $asset_id){
                        $insurance_link = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->where('asset_id', $asset_id)->count();
                        
                        if(!$insurance_link){
                            $new_record[] = $asset_id;
                            //foreach($new_record as $asset_id){
                                $add_record = new AssetInsurances;
                                $add_record->account_id = $account_id;
                                $add_record->asset_id = $asset_id;
                                $add_record->insurance_id = $insurance_id;
                                $add_record->date_recorded = Carbon::now('UTC')->toDateTimeString();
                                $add_record->recorded_by = $user_id;
                                $add_record->save();
        
                                if($add_record){
                                    $s++;
                                }
                                //Carbon::now('UTC')->toDateTimeString();
                            //}
                        }
                    }

                    $asset_insurance_link = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->get();
                    foreach($asset_insurance_link as $asset){
                        if(!in_array($asset->asset_id, $request->asset)){
                            //$to_be_remove[] = $asset->asset_id;
                            $delete_insurance_link = AssetInsurances::find($asset->id)->delete();
                            
                            if($delete_insurance_link){
                                $s++; 
                            }
                        }
                    }


                    if($s){
                        $total_asset = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $insurance_id)->count();
    
                            $asset_associated = DB::table('sys_asset_insurance_link')
                                    ->select('sys_asset_insurance_link.*', 'sys_asset.*','sys_site.site_name','sys_site_location.location_name')
                                    ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_insurance_link.asset_id')
                                    ->leftJoin('sys_site', 'sys_site.site_id', '=', 'sys_asset.site_id')
                                    ->leftJoin('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset.location_id')
                                    ->where('sys_asset_insurance_link.account_id', $account_id)
                                    ->where('sys_asset_insurance_link.insurance_id', $insurance_id)
                                    ->orderBy('sys_asset.asset_tag', 'asc')
                                    ->get();
    
                        if($total_asset){
                            $response['stat_msg'] = 'Linked successfully.';
                        }else{
                            $response['stat_msg'] = 'Unlink successfully.';
                        }
                        
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['total_asset'] = $total_asset;
                        $response['insurance_id'] = $insurance_id;
                        $response['associated_assets'] = $asset_associated;
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to link asset to insurance. Error occured during process.';
                    }
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Insurance record not found or insurance record might already deleted.';
            }
        }

        return $response;

    }

    public function detach(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $id = $request->id;
        $response = [];

        if(!Helpers::get_user_privilege('pkg_insurance')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $check_record = AssetInsurances::where('account_id', $account_id)->where('id', $id)->count();

            if($check_record){
                $delete = AssetInsurances::find($id)->delete();

                if($delete){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Detached successfully';
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to detached. Error occured during process.';
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to detached. Record not found or might already detached';
            }
        }
        return $response;
    }

    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $ref = $request->ref;

        if($ref=='single'){
            $id = $request->id;
            $record = Insurances::where('account_id', $account_id)->where('record_stat', 0)->where('insurance_id', $id)->first();

            //$asset_associated = AssetInsurances::where('account_id', $account_id)->where('insurance_id', $id)->get();

            $asset_associated = DB::table('sys_asset_insurance_link')
                            ->select('sys_asset_insurance_link.*', 'sys_asset.*')
                            ->join('sys_asset', 'sys_asset.asset_id', '=', 'sys_asset_insurance_link.asset_id')
                            ->where('sys_asset_insurance_link.account_id', $account_id)
                            ->where('sys_asset_insurance_link.insurance_id', $id)
                            ->orderBy('sys_asset.asset_tag', 'asc')
                            ->get();
            
            $response['associated_assets'] = $asset_associated;
        }else if($ref=='all'){
            $keywords = (isset($request->keywords)) ? $request->keywords : '';

            $record = Insurances::where('account_id', $account_id)
                                    ->where(function ($query) use ($keywords) {
                                        $query->where('insurance_name', 'LIKE', '%'.$keywords.'%')
                                                ->orwhere('insurance_provider', 'LIKE', '%'.$keywords.'%');
                                    })
                                    ->where('record_stat', 0)
                                    ->orderBy('insurance_name', 'asc')->get();
        }

        $response['record'] =$record;
        

        return $response;
    }
}
