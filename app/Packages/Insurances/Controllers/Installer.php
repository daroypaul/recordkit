<?php

namespace App\Packages\Insurances\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Utilities\Migrations as Migrations;
use Artisan;
use Menus;
use Packages;
use Options;

class Installer
{

    public static function install($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        if($output){
            ### Menu ###
            $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-insurances')->count();
            $get_menu = Menus::where('menu_name', 'tools-main')->first();
            $parent_menu = $get_menu->menu_id;

            $menu = [];
            if($has_menu){
                $get_menu = Menus::where('menu_name', 'package-insurances')->first();
                $menu_id = $get_menu->menu_id;
    
                ## Activating Menu ##
                $menu = Menus::find($menu_id);
                $menu->is_disable = 0;
                $menu->save();
            }else{
                ### Add Menu Item ###
                $menu = new Menus;
                $menu->account_id = $account_id;
                $menu->parent_menu = $parent_menu;
                $menu->menu_name = 'package-insurances';
                $menu->menu_label = 'Insurances';
                $menu->menu_url = 'insurances';
                $menu->menu_icon = 'mdi mdi-shield-outline m-r-10';
                $menu->privilege_key = '["all","package_insurances"]';
                $menu->is_disable = 0;
                $menu->save();
            }

            ### Adding Data on Encrypted filename ###
            /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
            $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
            $json_data = json_decode($get_json_data, true);//Decode JSON to Array

            $update_json_file = false;
            if(!isset($json_data['Fk8jZozHYZe5WfR3mnM4.js'])){
                $json_data['Fk8jZozHYZe5WfR3mnM4.js']="app/packages/insurances/resources/assets/js/app.js";

                $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
            }*/

            ### Update JSON File ###
            $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
            $package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

            if($menu||$package_installed||$package_activate){
                return true;
            }else{
                return false;
            }
        }else{
            ### Menu ###
            $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-insurances')->count();
            $get_menu = Menus::where('menu_name', 'tools-main')->first();
            $parent_menu = $get_menu->menu_id;
            $menu = [];

            if($has_menu){
                $get_menu = Menus::where('menu_name', 'package-insurances')->first();
                $menu_id = $get_menu->menu_id;
    
                ## Activating Menu ##
                $menu = Menus::find($menu_id);
                $menu->is_disable = 0;
                $menu->save();
            }else{
                ### Add Menu Item ###
                $menu = new Menus;
                $menu->account_id = $account_id;
                $menu->parent_menu = $parent_menu;
                $menu->menu_name = 'package-insurances';
                $menu->menu_label = 'Insurances';
                $menu->menu_url = 'insurances';
                $menu->menu_icon = 'mdi mdi-shield-outline m-r-10';
                $menu->privilege_key = '["all","package_insurances"]';
                $menu->is_disable = 0;
                $menu->save();
            }

            ### Adding Data on Encrypted filename ###
            /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
            $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
            $json_data = json_decode($get_json_data, true);//Decode JSON to Array

            $update_json_file = false;
            if(!isset($json_data['Fk8jZozHYZe5WfR3mnM4.js'])){
                $json_data['Fk8jZozHYZe5WfR3mnM4.js']="app/packages/insurances/resources/assets/js/app.js";

                $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
            }*/

            ### Update JSON File ###
            $package_activate = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];
            $package_installed = packages::modify('installed', ['app/packages/'.$package],[1])[0]['value'];

            if($menu||$package_installed||$package_activate){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function uninstall($package)
    {
        $account_id = Auth::user()->account_id;

        ### Run Migration ###
        $migration_path = 'app/packages/'.$package.'/database/migrations/drop';
        $run = Artisan::call('migrate', ['--force' => true, '--path'=>$migration_path]);
        $output = ( trim(Artisan::output())=='Nothing to migrate.') ? 0 : 1;

        ### Delete Migration Data ###
        $migrated_names = ['2018_07_21_041613_drop_sys_insurance_table', '2018_06_03_060029_create_sys_asset_insurance_link_table', '2018_06_03_060054_create_sys_insurances_table'];

        $delete_migration = [];
        foreach($migrated_names as $migration){
            $check_migration = Migrations::where('migration', $migration)->first();
            
            if($check_migration){
                $id = $check_migration->id;
                
                //Delete Migration Data
                $delete_migration = Migrations::find($id)->delete();
            }
        }

        ### Delete Menu Data ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-insurances')->count();
        $delete_menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-insurances')->first();
            $menu_id = $get_menu->menu_id;

            ## Activating Menu ##
            $delete_menu = Menus::find($menu_id)->delete();
        }

        ### Delete Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;

        $arr_index = array();
        foreach ($json_data as $key => $value) {
            if($key=='Fk8jZozHYZe5WfR3mnM4.js'){
                $arr_index[] = $key;
            }
        }

        if($arr_index){
            // delete json data
            foreach ($arr_index as $i)
            {
                unset($json_data[$i]);
            }

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $package_deactive = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];
        $package_uninstalled = packages::modify('installed', ['app/packages/'.$package],[0])[0]['value'];
        
        if(count($delete_migration)||$delete_menu||$package_deactive||$package_uninstalled){
            return 'true';
        }else{
            return 'false';
        }
    }

    public static function activate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-insurances')->count();
        $menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-insurances')->first();
            $menu_id = $get_menu->menu_id;

            ## Disable Menu ##
            $menu = Menus::find($menu_id);
            $menu->is_disable = 0;
            $menu->save();
        }else{
            $get_menu = Menus::where('menu_name', 'tools-main')->first();
            $parent_menu = $get_menu->menu_id;

            ### Add Menu Item ###
            $menu = new Menus;
            $menu->account_id = $account_id;
            $menu->parent_menu = $parent_menu;
            $menu->menu_name = 'package-insurances';
            $menu->menu_label = 'Consumables';
            $menu->menu_url = 'consumables';
            $menu->menu_icon = 'mdi mdi-barcode m-r-10';
            $menu->privilege_key = '["all","package_consumable"]';
            $menu->is_disable = 0;
            $menu->save();
        }

        ### Option Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $add_option = [];

        if(!$has_option){
            ### Add new category option ###
            $add_option = new Options;
            $add_option->account_id = $account_id;
            $add_option->option_name = 'apps_cat_type';
            $add_option->option_value = 'consumable';
            $add_option->save();
        }

        ### Adding Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;
        if(!isset($json_data['Fk8jZozHYZe5WfR3mnM4.js'])){
            $json_data['Fk8jZozHYZe5WfR3mnM4.js']="app/packages/insurances/resources/assets/js/app.js";

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[1])[0]['value'];

        if($menu||$activate_package||$add_option){
            return true;
        }else{
            return false;
        }
    }


    public static function deactivate($package)
    {
        $account_id = Auth::user()->account_id;

        ### Menu ###
        $has_menu = Menus::where('account_id', $account_id)->where('menu_name', 'package-insurances')->count();
        $update_menu = [];

        if($has_menu){
            $get_menu = Menus::where('menu_name', 'package-insurances')->first();
            $parent_menu = $get_menu->menu_id;

            ## Disable Menu ##
            $update_menu = Menus::find($parent_menu);
            $update_menu->is_disable = 1;
            $update_menu->save();
        }

        ### Option Category List ###
        $has_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->count();
        $delete_option = false;

        if($has_option){
            $get_option = Options::where('account_id', $account_id)->where('option_name', 'apps_cat_type')->where('option_value', 'consumable')->first();
            $option_id = $get_option->option_id;
            
            ## Delete Option ##
            $delete_option = Options::find($option_id)->delete();
        }

        ### Delete Data on Encrypted filename ###
        /*$json_base_dir = base_path('app/encrypted_src.json');//Base path of specific JSON File
        $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
        $json_data = json_decode($get_json_data, true);//Decode JSON to Array

        $update_json_file = false;

        $arr_index = array();
        foreach ($json_data as $key => $value) {
            if($key=='Fk8jZozHYZe5WfR3mnM4.js'){
                $arr_index[] = $key;
            }
        }

        if($arr_index){
            // delete json data
            foreach ($arr_index as $i)
            {
                unset($json_data[$i]);
            }

            $update_json_file = file_put_contents($json_base_dir, json_encode($json_data, JSON_PRETTY_PRINT));
        }*/

        ### Update JSON File ###
        $activate_package = packages::modify('active', ['app/packages/'.$package],[0])[0]['value'];

        if($update_menu||$activate_package||$delete_option){
            return true;
        }else{
            return false;           
        }
    }
}
