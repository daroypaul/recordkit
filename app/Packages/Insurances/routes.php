<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'insurances', 'namespace' => 'App\Packages\insurances\Controllers'], function()
{
    Route::get('/', 'InsurancesController@index');
    Route::get('/{id}', 'InsurancesController@show');
    Route::post('/', 'InsurancesController@store');
    Route::post('/linkasset', 'InsurancesController@link_asset');
    Route::post('/delete', 'InsurancesController@delete');
    Route::post('/detached-asset', 'InsurancesController@detach');
    Route::post('/fetchdata', 'InsurancesController@fetch_data');
});