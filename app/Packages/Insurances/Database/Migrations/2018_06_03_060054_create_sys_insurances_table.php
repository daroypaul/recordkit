<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_insurances', function (Blueprint $table) {
            $table->increments('insurance_id');
            $table->integer('account_id')->nullable();
            $table->string('insurance_name', 100)->nullable();
            $table->string('insurance_provider', 100)->nullable();
            $table->string('insurance_remarks', 1000)->nullable();
            $table->string('insurance_cost', 50)->nullable();
            $table->date('insurance_start')->nullable();
            $table->date('insurance_expire')->nullable();
            $table->dateTime('date_recorded')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->integer('recorded_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('record_stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_insurances');
    }
}
