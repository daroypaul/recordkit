/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,9 ] }
            ],
            "dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<button type="button" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Insurance" id="btn-add-record"><i class="fas fa-plus"></i></button>';
                var bulk_action =  
                                '<div class="form-group m-b-0"><div class="input-group">'+
                                    '<select class="form-control custom-select" id="bulk-action-option">'+
                                        '<option value="">- Bulk Action -</option>'+
                                        '<option value="Delete">Delete</option>'+
                                    '</select>'+            
                                    '<span class="input-group-btn">'+
                                    '<button id="btn-bulk-action" class="btn btn-default" type="button" tabindex="-1">Apply</button>'+
                                    '</span>'+
                                '</div></div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);
            }
        }).on('click', 'td[data-action]', function(e) {
            var action = $(this).data('action');
            var title= '';

            if(action=='view-address'){
                title= "Address";
                var content = $(this).parents('tr').data('address-content');
            }else if(action=='view-remarks'){
                title= "Remarks";
                var content = $(this).parents('tr').data('remarks-content');
            }
            
            if(content){
                modal_view_content(title,content);
            }else{
                $.notification('','Empty Data','top-right','info','');
            }
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                edit_action(id, 'edit','table','single');//id='',ref='add',target='',fetch_type='single'
            }else if(action=='delete'&&id){
                delete_action(id,'single');
            }else if(action=='link'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/insurances/fetchdata',
                    dataType: "json",
                    data: {
                        'id':id,
                        'ref':'single'
                    },
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();

                        if(data.record&&data.record.length!=0){
                            var link_asset = [];
                            var key = [];
                            var ref = 'add';
                            $.each(data.associated_assets, function(i, v){
                                var key = {};
                                key['id'] = v.asset_id;
                                key['tag'] = v.asset_tag;
                                key['name'] = v.asset_model;

                                link_asset.push(key);
                            });
                            
                            if(data.associated_assets){
                                ref = 'edit';
                            }

                            $.link_insurance_modal(ref,'Link an Asset','default', id,data.record.insurance_name, link_asset);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or insurance record might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });

                
            }
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }
        });

        $('input#select-all').click(function(){
            var c = this.checked;
    
            $('input.selected-item').each(function(){
                var id = $(this).parents('tr').data('id');
    
                $(this).prop('checked', c);
    
                if(c){
                    if(id!=0||id){
                        $selected_asset.push(id);
                    }
                }else{
                    $selected_asset = $.grep($selected_asset, function (value) {
                        return value != id;
                    });
                }
            });
        });


        $('#btn-bulk-action').click(function(){
            var opt = $('#bulk-action-option').val();
            $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
            $('#bulk-action-option').removeClass('form-control-danger');
    
            if($selected_asset.length==0){
                $.notification('Error','Select a record','top-right','error','');
            }else if(opt==''){
                $('#bulk-action-option').parents('.form-group').addClass('has-danger');
                $('#bulk-action-option').addClass('form-control-danger');
                $.notification('Error','Select an action option','top-right','error','');
            }else if(opt=='Delete'){
                if($selected_asset.length!=0){
                    delete_action('','multi');
                }
            }else{
                if(opt!='Delete'&&opt!='Email'){
                    $status_updated = 0;
                    $.asset_status_modal($selected_asset, opt + ' Asset', opt);
                }
            }
        });
        

    $('#btn-add-record').click(function(){
        $.module_insurance_form('','add','Add New Insurance','table');
    });

    $('#btn-link-asset').click(function(){
        var id = $('#_id').val();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Fetching data...');

        $.ajax({
            type:'POST',
            url :  $apps_base_url+'/insurances/fetchdata',
            dataType: "json",
            data: {
                'id':id,
                'ref':'single'
            },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                if(data.record&&data.record.length!=0){
                    var link_asset = [];
                    var key = [];
                    var ref = 'add';
                    $.each(data.associated_assets, function(i, v){
                        var key = {};
                        key['id'] = v.asset_id;
                        key['tag'] = v.asset_tag;
                        key['name'] = v.asset_model;

                        link_asset.push(key);
                    });
                    
                    if(data.associated_assets){
                        ref = 'edit';
                    }

                    $.link_insurance_modal(ref,'Link an Asset','insurance_view_page', id,data.record.insurance_name, link_asset);
                }else{
                    $.notification('Error','Record not found. Invalid reference ID or insurance record might be deleted','top-right','error','');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });


    //View iNSURANCE

    $associated_asset_table = $('table#asset-table').DataTable({
        "scrollX": true,
        "order": [[ 1, "asc" ]],
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ 6 ] }
        ],
        /*"dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-3"l>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
        fnInitComplete: function(){
                var add_element = '<button type="button" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Insurance" id="btn-add-record"><i class="fas fa-plus"></i></button>';

                $('div#action-toolbar').html(add_element);
            }*/
    }).on('click', 'button[data-action]', function(e) {
        e.preventDefault();
        var id = $(this).parents('tr').data('id');
        var action = $(this).data('action');

        if(action=='detached'&&id){
            var url = $apps_base_url+'/insurances/detached-asset';
            var settings = {
                title : "Detached Record",
                text:"Your about to detached asset to this insurance. Are you want to continue?",
                confirmButtonText:"Delete",
                showCancelButton:true,
                type : 'warning',
                confirmButtonColor: "#DD6B55",
                //closeOnConfirm: false
            }
            
            swal(settings).then(result => {
                swal.close();
                
                if(result.value){
                    $('#form-loader').toggle();
                    $('#form-loader .loader__label').text('Processing...');

                    $.ajax({
                        type:'POST',
                        url : url,
                        dataType: "json",
                        data: {
                            'id':id,
                            'by':'insurance'
                        },
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function (data) {
                            $('#form-loader').toggle();
                            
                            $('.table-danger').removeClass('table-danger');
                            if(data.stat){
                                
                                if(data.stat=='success'){
                                    $associated_asset_table.row('tr[data-id='+id+']').remove().draw();
                                }else{
                                    $("tr[data-id='"+id+"']").addClass('table-danger');
                                }

                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                            }
                        },
                        error :function (data) {
                            $('#form-loader').toggle();
                            $.notification('Error','Internal Server Error','top-right','error','');
                        }
                    });
                }
            });            
        }
    });


    $('button#action-view-form-edit').click(function(){
        var id = $('#_id').val();
        if(id){
            edit_action(id,'edit','form-view','single');//id='',ref='add',target='',fetch_type='single'
        }
    });

    $('button#action-view-form-delete').click(function(){
        var id = $('#_id').val();

        if(id){
            delete_action(id,del_type='single','form-view');
        }
    });
});

function edit_action(id='',ref='add',target='',fetch_type='single'){
    $('#form-loader').toggle();
    $('#form-loader .loader__label').text('Fetching data...');
    $(".tooltip").tooltip("hide");

    $.ajax({
        type:'POST',
        url :  $apps_base_url+'/insurances/fetchdata',
        dataType: "json",
        data: {
            'id':id,
            'ref':fetch_type
        },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function (data) {
            $(".tooltip").tooltip("hide");
            $('#form-loader').toggle();

            if(data.record&&data.record.length!=0){
                $.module_insurance_form(data.record.insurance_id, 'edit','Update Insurance',target,data.record.insurance_name,data.record.insurance_provider,data.record.insurance_start, data.record.insurance_expire,data.record.insurance_cost,data.record.insurance_remarks);
            }else{
                $.notification('Error','Record not found. Invalid reference ID or insurance record might be deleted','top-right','error','');
            }
        },
        error :function (data) {
            $('#form-loader').toggle();
            $.notification('Error','Internal Server Error','top-right','error','');
            $('#form-loader .loader__label').text('Fetching Data...');
        }
    });
};

function delete_action(id='',del_type='single',origin='table'){
    $(".tooltip").tooltip("hide");

    if(del_type=='single'){
        var msg_text = "Your about to delete insurance record. Are you want to continue?";
        //$form_data = [{'id':id,'del_type':'single'}];
        $form_data = new FormData();
        $form_data.append('id', id);
        $form_data.append('del_type', 'single');
    }else{
        var msg_text= "Your about to delete "+$selected_asset.length+" insurance record(s). Are you want to continue?";

        $form_data = new FormData();
        $.each($selected_asset, function(i, v){
            $form_data.append('id[]', v);
        });
        $form_data.append('del_type', 'multi');
    }

    var url = $apps_base_url+'/insurances/delete';
    var settings = {
        title : "Deleting Record",
        text: msg_text,
        confirmButtonText:"Delete",
        showCancelButton:true,
        type : 'warning',
        confirmButtonColor: "#DD6B55",
        //closeOnConfirm: false
    }

    swal(settings).then(result => {
        swal.close();
        
        if(result.value){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Deleting...');

            $.ajax({
                type:'POST',
                url : url,
                dataType: "json",
                data: $form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();
                    $(".tooltip").tooltip("hide");

                    if(del_type=='single'){
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            if(data.stat=='success'){
                                $record_table.row('tr[data-id='+data.id+']').remove().draw();

                                if(origin=='form-view'){
                                    $('#form-loader').toggle();
                                    $('#form-loader .loader__label').text('Redirecting...');

                                    var url = $apps_base_url+'/insurances';

                                    window.location.replace(url);
                                }
                            }else{
                                $("tr[data-id='"+id+"']").addClass('table-danger');
                            }
    
                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }
                    }else{
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }else{
                            if(data.sid.length!=0){
                                $.notification('Success','Selected insurance record(s) successfully deleted.','top-right','success','');

                                $.each(data.sid,function(i,v){
                                    $record_table.row('tr[data-id='+v+']').remove().draw();
                                });
                            }

                            if(data.eid.length!=0){
                                $.notification('Error Process','Selected insurance record(s) can not be deleted. Invalid reference ID or record might already deleted. See highlighten row','top-right','error','');
                                
                                $.each(data.eid,function(i,v){
                                    $("tr[data-id='"+v+"']").addClass('table-danger');
                                });
                                
                                $eid = data.eid;
                            }
                        }
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }
    });
    
    /*swal(settings).then(result => {
        swal.close();
        
        if(result.value){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Deleting...');

            $.ajax({
                type:'POST',
                url : url,
                dataType: "json",
                data: {
                    'id':id,
                    'del_type':'single'
                },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();
                    if(data.stat){
                        if(data.stat=='success'){
                            $record_table.row('tr[data-id='+data.id+']').remove().draw();
                        }else{
                            $("tr[data-id='"+id+"']").addClass('table-danger');
                        }

                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }
    });*/
}

function modal_view_content(title='', content='') {//
    $layout_form = 	'<div class="row"><div class="col-12">'+content+'</div></div>';

    bootbox.dialog({
        closeButton: false,
        backdrop: true,
        animate: true,
        title: title,
        message: $layout_form,
        buttons: {
            close: {
                label: 'Close',
                className: "btn-inverse",
            }
        }
    })
}