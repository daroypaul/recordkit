@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">{!! config('consumables.name') !!}</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <!--<th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>-->
                                                <th>Name</th>
                                                <th>Model</th>
                                                <th>Purchased&nbsp;Date</th>
                                                <th>Cost</th>
                                                <th>Stocks</th>
                                                <th>Category</th>
                                                <th>Site</th>
                                                <th width="110">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($consumables as $consumable)
                                                <tr data-id="{{$consumable->item_id}}">
                                                    <!--<td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$consumable->item_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$consumable->item_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>-->
                                                    <td>{{$consumable->item_name}}</td>
                                                    <td>{{$consumable->item_model}}</td>
                                                    <td>{{($consumable->date_purchased) ? date('d-M-Y', strtotime($consumable->date_purchased)) : '---'}}</td>
                                                    <td>{{($consumable->item_cost) ? number_format($consumable->item_cost, 2) : '---'}}</td>
                                                    <td {!!($consumable->qty_remain<$consumable->qty_alert) ? 'class="text-danger text-bold" data-toggle="tooltip" title="Insufficient Stock(s)"':'class="text-default text-bold"'!!}>{{number_format($consumable->qty_remain)}}</td>
                                                    <td>{{$consumable->category->cat_name}}</td>
                                                    <td>{{$consumable->site->site_name}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-inverse" data-action="check-out" data-toggle="tooltip" title="Check-Out"><i class="mdi mdi-logout"></i></button>
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                        <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete Record"><i class="ti-trash"></i></button>
                                                    </td>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{!! config('consumables.name') !!}
@endsection

@section('style')

@endsection

@section('js')
    
    <script src="{{ url('assets/js/prsO3I0FGPepUqoFyTBb.js') }}"></script>
@endsection