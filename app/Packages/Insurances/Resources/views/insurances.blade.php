@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Name</th>
                                                <th>Provider</th>
                                                <th>Start</th>
                                                <th>Expire</th>
                                                <th>Cost</th>
                                                <th>Remaining</th>
                                                <th>Linked&nbsp;Asset</th>
                                                <th>Remarks</th>
                                                <th width="150">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($record_list as $record)
                                                <?php
                                                    $todate = new DateTime(date('Y-m-d'));
                                                    $expire = new DateTime(date('Y-m-d', strtotime($record->insurance_expire)));

                                                    if(strtotime($record->insurance_expire) > strtotime(date('Y-m-d'))){
                                                        $diff =  $todate->diff($expire)->format("%a");
                                                    }else{
                                                        $diff = 0;
                                                    }

                                                    
                                                    $days = ($diff<2) ? $diff . ' Day' : $diff . ' Days';
                                                ?>
                                                <tr data-id="{{$record->insurance_id}}" data-address-content="{{$record->supplier_address}}" data-remarks-content="{{$record->insurance_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$record->insurance_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$record->insurance_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td><a href="{{url('/insurances/'.$record->insurance_id)}}">{{$record->insurance_name}}</a></td>
                                                    <td>{{$record->insurance_provider}}</td>
                                                    <td>{{date('d-M-Y', strtotime($record->insurance_start))}}</td>
                                                    <td>{{date('d-M-Y', strtotime($record->insurance_expire))}}</td>
                                                    <td>{{($record->insurance_cost) ? number_format($record->insurance_cost, 2) . ' ' .$system_options->currency->currency_code : '---'}}</td>
                                                    <td>{{$days}}</td>
                                                    <td>
                                                        @foreach ($assets as $total_link_asset)
                                                            @if($total_link_asset['id']==$record->insurance_id)
                                                                {{$total_link_asset['link_assets']}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($record->insurance_remarks||strlen($record->insurance_remarks) > 30) ? substr($record->insurance_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td>
                                                        <!--<button type="button" class="btn btn-sm btn-default" data-action="link" data-toggle="tooltip" title="Link an Asset"><i class="fa fa-barcode"></i></button>-->
                                                        <a href="{{url('/insurances/'.$record->insurance_id)}}" type="button" class="btn btn-sm btn-default" data-toggle="tooltip" title="View"><i class="fa fa-search"></i></a>
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"><i class="ti-pencil"></i></button>
                                                        <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')

<script src="{{ url('assets/js/Fk8jZozHYZe5WfR3mnM4.js') }}"></script>
@endsection