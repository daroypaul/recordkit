@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/insurances')}}">Insurances</a></li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                        <div class="card">
                            <div class="card-header" style="background-color: transparent;">
                                <div class="float-right">
                                    <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"  id="action-view-form-edit"><i class="ti-pencil"></i> Edit</button>
                                    <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete"  id="action-view-form-delete"><i class="ti-trash"></i> Delete</button>
                                </div>
                            </div>
                            <div class="card-body">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Name</label>
                                                    <input type="text" class="form-control" placeholder="Insurance Name" value="{{ isset($record->insurance_name) ? $record->insurance_name : '' }}" id="txt_name" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Provider</label>
                                                    <input type="text" class="form-control" placeholder="Insurance Provider" value="{{ isset($record->insurance_provider) ? $record->insurance_provider : '' }}" id="txt_provider" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Start</label>
                                                    <input type="text" class="form-control" placeholder="Date Start" name="name" value="{{date('d-M-Y', strtotime($record->insurance_start))}}" id="txt_start" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Expire</label>
                                                    <input type="text" class="form-control" placeholder="Date Expire" name="name" value="{{date('d-M-Y', strtotime($record->insurance_expire))}}" id="txt_expire" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Cost</label>
                                                    <input type="text" class="form-control" placeholder="Insurance Cost" name="name" value="{{($record->insurance_cost) ? number_format($record->insurance_cost, 2) . ' ' .$system_options->currency->currency_code : '---'}}" id="txt_cost" disabled>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group m-b-0">
                                                    <label class="form-control-label text-bold">Remarks</label>
                                                    <textarea class="form-control" style="max-height:300px !important; resize:vertical;" disabled placeholder="Insurance Remarks" id="txt_remarks">{{$record->insurance_remarks}}</textarea>
                                                </div>
                                            </div>
                                       
                                        </div>
                                    </div>
                                
                            </div>
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#assets" role="tab"><span class="hidden-sm-up"><i class="ti-info"></i></span> <span class="hidden-xs-down">Associated Asset</span></a> </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane p-20 active" id="assets" role="tabpanel">
                                    <button type="button" class="btn btn-success m-r-10 m-b-10" id="btn-link-asset"><i class="fas fa-plus"></i> Link Asset</button>
                                    <div class="table-responsived">
                                        <table class="table table-striped" id="asset-table" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Asset Tag</th>
                                                    <th>Model</th>
                                                    <th>Serial</th>
                                                    <th>Description</th>
                                                    <th>Site</th>
                                                    <th>Location</th>
                                                    <th width="40">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ( $associated_assets as $asset )
                                                    <tr data-id="{{$asset->id}}">
                                                        <td>{{$asset->asset_tag}}</td>
                                                        <td>{{$asset->asset_model}}</td>
                                                        <td>{{($asset->asset_serial) ? $asset->asset_serial : '---'}}</td>
                                                        <td>{{$asset->asset_desc}}</td>
                                                        <td>{{$asset->site_name}}</td>
                                                        <td>{{$asset->location_name}}</td>
                                                        <td width="40">
                                                            <button type="button" class="btn btn-sm btn-danger" data-action="detached" data-toggle="tooltip" title="Detached"><i class="fa fa-times"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-body">
                                    <div class="form-actions float-right">
                                        <a href="{{url('/insurances')}}" type="button" class="btn btn-inverse">Back</a>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($id) ? $id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')

<script src="{{ url('assets/js/Fk8jZozHYZe5WfR3mnM4.js') }}"></script>
@endsection