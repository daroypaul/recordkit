<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class EmailSender extends Controller
{
    //
    public function send(){
        $data = [
            "email" => 'paul_daroy@wvi.org',
            "cc"=> ['daroypaul1@gmail.com','daroypaul@live.com','daroypaul@yahoo.com'],
            "subject" => 'Overdue Leased Asset',
            "bodymessage" => 'Hello World',
            "requestor"=>'test',
            "type"=>'reset_pass',
            "link"=>' http://127.0.0.1:8000/auth/reset?token=AOXsGD1WPHbT9oJ2tdPsGnkCtG3msPjsA6D8ueMSuaMwRBuND44VkLXsFyfgUlGKAzehhVLaZ7oAy2gU'
        ];

        /*Mail::send('mail.email', $data, function($message) use ($data){
            $message->from('notification@recuda-apps.com', 'ICT Asset Tracking System');
            $message->to($data['email']);
            $message->cc($data['cc']);
            $message->subject($data['subject']);
        });*/

        return view('mail.email')->with($data);
    }
}
