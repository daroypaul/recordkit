<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
   
    protected $table = 'sys_users';
    protected $primaryKey = 'user_id';
    const CREATED_AT = 'date_registered';
    const UPDATED_AT = 'date_updated';

    public function username()
    {
        return 'user_login';
    }

    public function getAuthPassword(){
        return $this->user_pass;
    }
}
