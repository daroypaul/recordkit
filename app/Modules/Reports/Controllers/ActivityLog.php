<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\reports\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use ActivityLogs;
use App\modules\adminprofile\models\Users;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class ActivityLog extends Controller
{
    public function generate_report(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $response = [];
        $account_id = Auth::user()->account_id;
        $a = [];
        $b = [];
        $start = date('Y-m-d', strtotime($request->date_start)) . ' 00:00:00';
        $end  = date('Y-m-d', strtotime($request->date_end)) . ' 23:59:59';


        $timezone = Options::apps('timezone');
        $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
        /*if($start==$end){
            $logs = ActivityLogs::with(['user'])->where('account_id', $account_id)->whereBetween('activity_date', [$start, $end])->get();
        }else{
            $logs = ActivityLogs::with(['user'])->where('account_id', $account_id)->whereBetween('activity_date', [$start, $end])->get();
        }*/

        $logs = ActivityLogs::with(['user'])->where('account_id', $account_id)->whereBetween('activity_date', [$start, $end]);

        if($request->action_by){
            $logs = $logs->where('action_by', $request->action_by);
        }

        $logs = $logs->get();

        foreach($logs as $log){
            $a['date'] = date('d-M-Y h:iA', strtotime($log->activity_date . $interval));
            $a['user'] = $log->user->user_fullname;
            $a['desc'] = $log->activity_desc;

            $b[] = $a;
        }

        $response['logs'] = $b;

        return $response;
    }

    public function index_report(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('report_activitylog')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
                        
        $start = date('Y-m-01') . ' 00:00:00';
        $end  = date('Y-m-t') . ' 23:59:59';
        //$start = '2018-03-10';
        //$end = '2018-03-11';

        $user_list = Users::where('account_id', $account_id)
                                        ->where('is_deleted', 0)
                                        ->orderBy('user_fullname', 'asc')->get();
    
        $logs = ActivityLogs::with(['user'])->where('account_id', $account_id)->whereBetween('activity_date', [$start, $end])->get();
        
        //return $logs;
        $data = [
            "form_title"=> 'Activity Log Reports',
            "logs"=> $logs,
            "user_list"=>$user_list,
        ];
        //return $user_list;
        return view('reports::logs')->with($data);
    }
}
