<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'reports', 'namespace' => 'App\modules\reports\controllers'], function()
{
    Route::get('/', function() {
        return redirect('dashboard');
    });
    Route::get('activity-logs', 'ActivityLog@index_report');
    Route::post('activity-logs', 'ActivityLog@generate_report');
});