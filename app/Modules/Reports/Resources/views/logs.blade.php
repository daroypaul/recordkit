@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Reports</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
                                            <label class="text-bold">Start Date</label>
                                            <input class="form-control" type="text" id="date-start" value="<?php echo date('01-M-Y')?>" />
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
                                            <label class="text-bold">End Date</label>
                                            <input class="form-control" type="text" id="date-end" value="<?php echo date('t-M-Y')?>" />
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                                            <label class="text-bold">User</label>
                                            <select class="form-control select2" tabindex="1" id="select-user" placeholder="Select" width="100%">
                                                <option value="">-- User List ---</option>
                                                @if(isset($user_list)&&count($user_list)!=0)
                                                    @foreach ($user_list as $user)
                                                        <option value="{{$user->user_id}}">{{$user->user_fullname}}</option>
                                                    @endforeach
                                                @else
                                                    <!--<option value="0">[ User List ]</option>-->
                                                @endif
                                            </select> 
                                        </div>
                                        <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5">
                                            <button class="btn btn-sm btn-info float-right m-t-10" type="button" id="btn-generate"><i class="mdi mdi-settings"></i> Generate</button>
                                        </div>
                                    </div>
                                    <hr class="m-b-0"/>
                                    <div class="table-responsived"  id="slimtest1">
                                        <table id="record-table" class="table table-striped table-condensed" width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-col="date">Date</th>
                                                    <th data-col="user">Done&nbsp;By</th>
                                                    <th data-col="action">Action&nbsp;Taken</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($logs as $log)
                                                    <tr>
                                                        <td data-col="date" width="15%">{{date('d-M-Y h:iA', strtotime($log->activity_date . $system_options->timezones->timezone_hours.' hours +'.$system_options->timezones->timezone_minutes.' minutes'))}}</td>
                                                        <td data-col="user">{!!($log->user) ? $log->user->user_fullname : '<code>Undefind</code>'!!}</td>
                                                        <td data-col="action">{{$log->activity_desc}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!--/card-->
                        </div>
                    </div><!--/col-12-->
                </div><!--/row-->
                <input type="hidden" id="_report_title" value="{{$form_title}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    
@endsection

@section('js')
    
    <script src="{{ custom_asset('js/log.report.js') }}"></script>
@endsection