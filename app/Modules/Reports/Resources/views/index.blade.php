@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <!--<th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>-->
                                                <th>Package</th>
                                                <th>Description</th>
                                                <th>Developer</th>
                                                <th width="110">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ( $packages as $package )
                                                <tr data-package="{{$package['name']}}">
                                                    <td style="font-size:14px;">{{str_replace(" ","&nbsp;",$package['title'])}}</td>
                                                    <td  style="font-size:13px;">{{ ($package["description"]) ? $package["description"] : '---'}}
                                                    <br/><br/><b class="m-r-10 " style="text-color:#000;">Version {{$package['version_name']}}</b> <small class="label label-info">{{ucwords($package['type'])}}</small>
                                                    </td>
                                                    <td>{{$package['author']}}</td>
                                                    <td>
                                                        <!--<div class="btn-group" role="group" aria-label="Basic example" style="min-width:90px !important;">-->
                                                            @if($package['active'])
                                                                <button class="btn btn-xs btn-inverse" data-action="deactivate">Deactivate</button>

                                                                @if($package['installed'])
                                                                    <button class="btn btn-xs btn-inverse" data-action="uninstall">Uninstall</button>
                                                                @endif
                                                            @else
                                                                @if(!$package['installed'])
                                                                    <button class="btn btn-xs btn-inverse" data-action="install">Install</button>
                                                                @else
                                                                    <button class="btn btn-xs btn-inverse" data-action="activate">Activate</button>
                                                                @endif
                                                            @endif

                                                            <button class="btn btn-xs btn-danger" data-action="delete">Delete</button>
                                                        <!--</div>-->
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    
    <script src="{{ url('assets/js/ZH1reMfRblbqwX6qE5DT.js') }}"></script>
@endsection