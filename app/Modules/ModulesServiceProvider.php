<?php

namespace App\Modules;

use Illuminate\Support\ServiceProvider;
use Packages;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $scan_folder = array_map('strtolower', array_filter(glob('app/modules/*'), 'is_dir'));
        $folders = [];
        
        foreach($scan_folder as $dir){
            $explode_dir = explode('/', $dir);
            $folder = end($explode_dir);

            $folders[] = $folder;
        }

        foreach($folders as $module){
            // Load the routes for each of the modules
            if(file_exists(__DIR__.'/'.$module.'/routes.php')) {
                include __DIR__.'/'.$module.'/routes.php';
            }

            // Load the views
            if(is_dir(__DIR__.'/'.$module.'/resources/views')) {
                $this->loadViewsFrom(__DIR__.'/'.$module.'/resources/views', $module);
            }

            // Load the migrations
            if(is_dir(__DIR__.'/'.$module.'/database/migrations')) {
                $this->loadMigrationsFrom(__DIR__.'/'.$module.'/database/migrations');
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
