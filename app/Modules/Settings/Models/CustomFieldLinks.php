<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class CustomFieldLinks extends Model
{
    protected $table = 'sys_custom_field_links';
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\modules\adminprofile\models\Categories');
    }
}
