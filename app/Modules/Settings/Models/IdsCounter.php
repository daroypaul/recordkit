<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class IdsCounter extends Model
{
    protected $table = 'sys_ids_counter';
    protected $primaryKey = 'counter_id';
    public $timestamps = false;
}
