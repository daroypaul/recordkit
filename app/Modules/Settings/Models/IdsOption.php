<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class IdsOption extends Model
{
    protected $table = 'sys_ids_option';
    protected $primaryKey = 'option_id';
    public $timestamps = false;
    //const CREATED_AT = 'date_recorded';
    //const UPDATED_AT = 'date_updated';
}
