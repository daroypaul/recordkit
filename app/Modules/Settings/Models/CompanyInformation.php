<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class CompanyInformation extends Model
{
    protected $table = 'sys_company_info';
    protected $primaryKey = 'info_id';
    public $timestamps = false;

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }
}
