<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class SystemOptions extends Model
{
    protected $table = 'sys_options';
    protected $primaryKey = 'option_id';
    public $timestamps = false;

    public function currency()
    {
        return $this->hasOne('App\utilities\Currencies','currency_id','option_currency');
    }

    public function timezones()
    {
        return $this->hasOne('App\utilities\TimeZones','timezone_id','option_timezone');
    }
}
