<?php

namespace App\modules\settings\models;

use Illuminate\Database\Eloquent\Model;

class CustomFields extends Model
{
    protected $table = 'sys_custom_fields';
    protected $primaryKey = 'field_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function custom_field_links()
    {
        return $this->hasMany('App\modules\settings\models\CustomFieldLinks','field_id','field_id');
    }

    public function custom_values()
    {
        return $this->hasMany('App\packages\assettracking\models\AssetCustomValues','field_id','field_id');
    }
}
