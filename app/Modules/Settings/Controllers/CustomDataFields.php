<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\settings\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\settings\models\CustomFields;
use App\modules\settings\models\CustomFieldLinks;
use App\packages\assettracking\models\AssetCustomValues;
use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class CustomDataFields extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('setting_datafields')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $custom_fields = CustomFields::where('account_id', $account_id)->where('field_type', 'asset')->orderBy('field_label', 'asc')->get();

        $categories = DB::table('sys_categories')
                            ->select('sys_categories.cat_id','sys_categories.cat_name','sys_custom_fields.field_id')
                            ->join('sys_custom_field_links', 'sys_custom_field_links.ref_id', '=', 'sys_categories.cat_id')
                            ->join('sys_custom_fields', 'sys_custom_fields.field_id', '=', 'sys_custom_field_links.field_id')
                            ->where('sys_custom_fields.account_id', $account_id)
                            ->where('sys_custom_fields.field_type', 'asset')
                            ->orderBy('sys_categories.cat_name', 'asc')
                            ->get();
        
        $data = [
            //"record_list"=>$groups,
            "custom_fields"=>$custom_fields,
            "categories"=>$categories,
            "form_title"=> "Custom Data Fields",
        ];

        //return $asset_tag;
        return view('settings::customfields')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'label'=>'required|max:20|regex:/^[a-zA-Z0-9\s-]+$/u',
            'data_type'=>'required',
        ];

        /*if($request->start_number){
            $rules['start_number'] = 'required|min:1|numeric';
        }*/

        $messages = [
            'label.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);#Run the validation
        $val_msg = $validator->errors();
        $response = [];

        if ($validator->fails()&&count($request->asset_category)==0) {
            $response['val_error']= $validator->errors();
            $response['val_cat_error'] = 1;
        }else if($validator->fails()){
            $response['val_error']= $validator->errors();
        }else if(count($request->asset_category)==0){
            $response['val_error']= $validator->errors();
            $response['val_cat_error'] = 1;
        }else{
            if($request->ref=='add'){
                $count = CustomFields::where('account_id', $account_id)->where('field_type', 'asset')->where('field_label', $request->label)->count();
                
                if($count!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Record';
                    $response['stat_msg'] = 'Custom field already exists';
                }else{
                    $add_record = new CustomFields;
                    $add_record->account_id = $account_id;
                    $add_record->field_label = ucfirst($request->label);
                    $add_record->field_type = 'asset';
                    $add_record->field_data_type = $request->data_type;
                    $add_record->is_required = ($request->is_required) ? $request->is_required : 0;
                    $add_record->recorded_by = $user_id;
                    $add_record->updated_by = $user_id;
                    $add_record->save();

                    if($add_record){
                        $new_id = $add_record->field_id;
                        foreach($request->asset_category as $cat){
                            
                            $add_links = new CustomFieldLinks;
                            $add_links->field_id = $new_id;
                            $add_links->ref_id = $cat;
                            $add_links->save();
                        }

                        $response['a'] = $add_record;
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New custom field has been added';

                        $response['id'] = $new_id;
                        $response['label'] = $add_record->field_label;
                        $response['data_type'] = ucfirst($request->data_type);
                        $response['is_required'] = ($add_record->is_required) ? '<i class="fa fa-check-circle text-success"></i>' : '-None-';

                        //Add Logs
                        $desc = 'added an custom data fields ['.$add_record->field_label.']';
                        Helpers::add_activity_logs(['custon-data-fields-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add custom field. Error occured during process.';
                    }
                }
            }else if($request->ref=='edit'){
                $id = $request->id;
                $check_record = CustomFields::where('account_id', $account_id)->where('field_type', 'asset')->where('field_id', $id)->count();
                
                if($check_record!=0){
                    //If remarks is updated and name is constant
                    $check1 = CustomFields::where('account_id', $account_id)->where('field_type', 'asset')->where('field_id', $id)->where('field_label', $request->label)->count();
                    //If name is updated
                    $check2 = CustomFields::where('account_id', $account_id)->where('field_type', 'asset')->where('field_label', $request->label)->count();

                    if($check1!=0){
                        $update = $this->query_update($id, $request);
                        $response['update_stat'] = $update;
                        $response['a'] = 'proceed 1';
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'Custom field already exists';
                    }else{
                        $update = $this->query_update($id, $request);
                        $response['update_stat'] = $update;
                        $response['a'] = 'proceed 2';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or custom field might be deleted.';
                }
            }

            
            //is_required
        }

        return $response;
        
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //if(!Helpers::get_user_privilege('admcatdel')){
        //    $response['stat'] = 'error';
        //    $response['stat_title'] = 'Action Denied';
        //    $response['stat_msg'] = config('constant.access_denied_msg');
        //}else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                $check_record = CustomFields::where('account_id',$account_id)->where('field_type', 'asset')->where('field_id',$id)->count();
                
                if($check_record!=0){
                    $label = CustomFields::find($id)->field_label;
                    $delete_record = CustomFields::find($id)->delete();
                    
                    //To check if record success fully deleted
                    $check_record = CustomFields::where('account_id',$account_id)->where('field_type', 'asset')->where('field_id',$id)->count();

                    if($check_record==0){
                        $s[] = $id;

                        //Delete links
                        $delete_links = CustomFieldLinks::where('field_id',$id)->delete();
                        //Delete Custom Values
                        $delete_asset_custom_values = AssetCustomValues::where('field_id',$id)->delete();

                        $_log_desc .= $label . ', ';
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted an custom data fields ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['custon-data-fields',0,$desc]);
            }
            
            $response['eid'] = $x;
            $response['sid'] = $s;
        //}

        return $response;
    }

    function query_update($id, Request $request){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'field_label' => ucfirst($request->label),
            'field_type' => 'asset',
            'field_data_type' => $request->data_type,
            'is_required' => ($request->is_required) ? $request->is_required : 0,
            'updated_by' => $user_id,
        ];

        $update = CustomFields::where('account_id', $account_id)->where('field_id', $id)->update($data);

        $field_links = CustomFieldLinks::where('field_id',$id)->get();
        $added_ids = [];

        //Delete existed link
        foreach($field_links as $field_link){
            $cat_id = $field_link->ref_id;
            $link_id = $field_link->id;

            if (in_array($cat_id, $request->asset_category)){
                $added_ids[] = $cat_id;
            }else{
                $delete = CustomFieldLinks::find($link_id)->delete();
            }
        }

        //Add new links
        foreach($request->asset_category as $cat_id){
            if (!in_array($cat_id, $added_ids)){
                $add_links = new CustomFieldLinks;
                $add_links->field_id = $id;
                $add_links->ref_id = $cat_id;
                $add_links->save();
            }
        }

        if($update){
            //$record = Departments::where('account_id', $account_id)->where('department_id', $id)->first();

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'Custom field has been successfully updated';
            $response['id'] = $id;
            $response['label'] = ucfirst($request->label);
            $response['datatype'] = ucfirst($request->data_type);
            $response['is_required'] = ($request->is_required) ? $request->is_required : 0;

            //Add Logs
            $desc = 'updated an custom data fields ['.$request->label.']';
            Helpers::add_activity_logs(['custon-data-fields-edit',$id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update. Error occured during process.';
        }

        return $response;
    }

    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $ref = $request->ref;

        if(!$request->ref&&$request->id){
            $id = $request->id;
            $record = CustomFields::where('account_id', $account_id)->where('field_id',$id)->first();
            $categories = CustomFieldLinks::where('field_id',$id)->get();
        }

        $response['field'] =$record;
        $response['categories'] = $categories;
        
        return $response;
    }
}
