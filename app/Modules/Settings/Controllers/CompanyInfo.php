<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\settings\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\modules\settings\models\CompanyInformation;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Hashids\Hashids;

class CompanyInfo extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('setting_system')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $info = CompanyInformation::where('account_id', $account_id)->first();


        $data = [
            //"record_list"=>$groups,
           // "ids_option"=>$id_options,
            //"last_number"=>$last_number,
            //"asset_tag"=>$asset_tag,
            "info"=>$info,
            "form_title"=> "Company Information",
        ];

        //return $asset_tag;
        return view('settings::company_info')->with($data);
        //$hashids = new Hashids();

        //$a = $hashids->encode($account_id);

        //$serial =  shell_exec('wmic DISKDRIVE GET SerialNumber 2>&1');
        
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        /*$rules = [
            'timezone'=>'required',
            'currency'=>'required',
            'skins'=>'required',
        ];*/

        if($request->type=='info'){
            $rules = [
                'name'=>"required|min:1|max:100|regex:/^[a-zA-Z0-9\s-,.&']+$/u",
                'email'=>"required|email|min:5|max:100",
                'apps_name'=>"required|min:3|max:50",
            ];

            if($request->address){
                $rules['address'] = "max:450";
            }
        }else if($request->type=='logo'){
            if(isset($request->desktop_logo)){
                //$rules['desktop_logo'] = "max:300kb|Mimes:jpeg,jpg,png|dimensions:max-height=50";
                $rules['desktop_logo'] = "max:300kb|Mimes:jpeg,jpg,png";
            }

            if(isset($request->mobile_logo)){
                //$rules['mobile_logo'] = "max:300kb|Mimes:jpeg,jpg,png|dimensions:max-width=50,max-height=50";
                $rules['mobile_logo'] = "max:300kb|Mimes:jpeg,jpg,png";
            }
        }

       $messages = [
            'name.regex' => 'The :attribute format is invalid',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $company_info = CompanyInformation::where('account_id', $account_id)->first();
            $info_id = $company_info->info_id;
            $response['id'] = $company_info;

            if(!$company_info){
                if($request->type=='info'){
                    $add_info = new CompanyInformation;
                    $add_info->comp_name = $request->name;
                    $add_info->comp_address = $request->address;
                    $add_info->comp_email = $request->email;
                    $add_info->apps_name = $request->apps_name;
                    $add_info->date_updated = Carbon::now('UTC')->toDateTimeString();
                    $add_info->updated_by = $user_id;
                    $add_info->save();
                    
                    $info_id = $add_info->info_id;

                    if($add_info){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Company info has been successfully updated';
        
                        //Add Logs
                        $desc = 'updated the company information';
                        Helpers::add_activity_logs(['update-company-info',$info_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update. Error occured during process. 1';
                    }
                }
            }
            else{
                if($request->type=='info'){
                    $update_option = CompanyInformation::find($info_id);
                    $update_option->comp_name = $request->name;
                    $update_option->comp_address = $request->address;
                    $update_option->comp_email = $request->email;
                    $update_option->apps_name = $request->apps_name;
                    $update_option->date_updated = Carbon::now('UTC')->toDateTimeString();
                    $update_option->updated_by = $user_id;
                    $update_option->save();

                    if($update_option){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Company info has been successfully updated';
        
                        //Add Logs
                        $desc = 'updated the company information';
                        Helpers::add_activity_logs(['update-company-info',$info_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update. Error occured during process. 1';
                    }
                }
                else if($request->type=='logo')
                {
                    $generated_key = Helpers::generate_key($info_id);
                    //$hashids = new Hashids("67821ce8362ac6dec24a469bf0a349d", 8, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                    //$encode = $hashids->encode($info_id);
                    
                    //Get File Info
                    $desktop_logo = $this->get_image_info($info_id, 'desktop_logo', $request);
                    $mobile_logo = $this->get_image_info($info_id, 'mobile_logo', $request);

                    $desktop_file = $request->file('desktop_logo');
                    $mobile_file = $request->file('mobile_logo');

                    //New Filename
                    $desktop_filename = $generated_key . '-desktop-logo.' . $desktop_logo['ext'];
                    $mobile_filename = $generated_key . '-mobile-logo.' . $mobile_logo['ext'];

                    //Attemp to upload files
                    $upload_desktop = $desktop_file->storeAs('public/',$desktop_filename);
                    $upload_mobile = $mobile_file->storeAs('public/',$mobile_filename);

                    //Get the whole file path
                    $uploaded_desktop_logo_src = storage_path('app/public/' . $desktop_filename);
                    $uploaded_mobile_logo_src = storage_path('app/public/' . $mobile_filename);

                    $current_dektop_logo = ($company_info->comp_logo_desktop) ? $company_info->comp_logo_desktop : '';
                    $current_mobile_logo = ($company_info->comp_logo_mobile) ? $company_info->comp_logo_mobile : '';

                    if(File::exists($uploaded_desktop_logo_src)&&File::exists($uploaded_mobile_logo_src)){
                        $update_option = CompanyInformation::find($info_id);
                        $update_option->comp_logo_desktop = $desktop_filename;
                        $update_option->comp_logo_mobile = $mobile_filename;
                        $update_option->date_updated = Carbon::now('UTC')->toDateTimeString();
                        $update_option->updated_by = $user_id;
                        $update_option->save();

                        if($update_option){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Application branding logo has been successfully updated';

                            $response['desktop'] = url('/branding').'/'.$desktop_filename;
                            $response['mobile'] = url('/branding').'/'.$mobile_filename;
            
                            //Add Logs
                            $desc = 'updated the application branding logo';
                            Helpers::add_activity_logs(['update-apps-branding',$info_id,$desc]);

                            //Delete Existing Files
                            if($current_mobile_logo&&$current_dektop_logo!=$desktop_filename){
                                $old_desktop_logo_file = storage_path('app/public/' . $current_dektop_logo);

                                $delete_file = File::delete($old_desktop_logo_file);//Attemp to delete
                            }

                            //Delete Existing Files
                            if($current_mobile_logo&&$current_mobile_logo!=$mobile_filename){
                                $old_mobile_logo_file = storage_path('app/public/' . $current_mobile_logo);

                                $delete_file = File::delete($old_mobile_logo_file);//Attemp to delete
                            }
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to upload. Error occured during process.';
                        }
                    }else{
                        $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to upload. Error occured during process.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Error occured during process. 2';
                    $response['a'] = $request->type;
                }
            }
        }

        return $response;
        
    }

    function get_image_info($id, $name='', Request $request){
        $file = $request->file($name);

        $image_filename_with_extension = $file->getClientOriginalName();
        $image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
        $image_fileExt = $file->getClientOriginalExtension();
        $image_size = $file->getSize();
        //$file_name_to_store = $generated_key . '-' . time() . '.' . $image_fileExt;
        //$upload = $file->storeAs('public/ams_files/',$file_name_to_store);//Attempt to upload
        //$uploaded_file = storage_path('app/public/ams_files/' . $file_name_to_store);


        $b['name_with_ext'] = $image_filename_with_extension;
        $b['name'] = $image_fileName;
        $b['ext'] = $image_fileExt;
        $b['size'] = $image_size;
        $a = $b;

        return $a;
    }
}
