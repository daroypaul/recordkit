<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\settings\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\utilities\Currencies;//Get the last number used
use App\utilities\TimeZones;
use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class Systems extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('setting_system')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;

        $currencies = Currencies::orderBy('currency_name', 'asc')->get();
        $timezones = TimeZones::all();

        $a = [];
        $a['timezone'] = Options::where('option_name', 'apps_timezone')->first()->option_value;
        $a['currency'] = Options::where('option_name', 'apps_currency')->first()->option_value;
        $a['skin'] = Options::where('option_name', 'apps_skins')->first()->option_value;
        $options = $a;

        $data = [
            //"record_list"=>$groups,
           // "ids_option"=>$id_options,
            //"last_number"=>$last_number,
            //"asset_tag"=>$asset_tag,
            "currencies"=>$currencies,
            "timezones"=>$timezones,
            "default_options"=>$options,
            "form_title"=> "Application Settings",
        ];

        //return $options;
        //return $asset_tag;
        return view('settings::system')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'timezone'=>'required',
            'currency'=>'required',
            'skins'=>'required',
        ];

        /*if($request->start_number){
            $rules['start_number'] = 'required|min:1|numeric';
        }*/

       /* $messages = [
            'category_name.regex' => 'The :attribute format is invalid (alphanumeric, dash and space only).',
            'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];*/

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $timezone = ($request->timezone) ? $request->timezone : 1;
            $currency = ($request->currency) ? $request->currency : 1;
            $skin = ($request->skins) ? $request->skins : 'default';

            //Get Individual ID
            $option_timezone = Options::where('account_id', $account_id)->where('option_name', 'apps_timezone')->first()->option_id;
            $option_currency = Options::where('account_id', $account_id)->where('option_name', 'apps_currency')->first()->option_id;
            $option_skin = Options::where('account_id', $account_id)->where('option_name', 'apps_skins')->first()->option_id;

            //Update Individual
            $update_timezone = Options::find($option_timezone);
            $update_timezone->option_value = $timezone;
            $update_timezone->save();

            $update_currency = Options::find($option_currency);
            $update_currency->option_value = $currency;
            $update_currency->save();

            $update_skin = Options::find($option_skin);
            $update_skin->option_value = $skin;
            $update_skin->save();

            if($update_timezone&&$update_currency&&$update_skin){
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = 'Application settings has been successfully updated';

                //Add Logs
                $desc = 'updated the application settings';
                Helpers::add_activity_logs(['update-application-settings',0,$desc]);
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to update. Error occured during process.';
            }


            /*$get_id = SystemOptions::where('account_id', $account_id)->where('option_type', 'ams')->first()->option_id;
            $update_option = SystemOptions::find($get_id);
            $update_option->option_timezone = ($request->timezone) ? $request->timezone : 1;
            $update_option->option_currency = ($request->currency) ? $request->currency : 1;
            $update_option->option_skins = ($request->skins) ? $request->skins : 'default';
            $update_option->save();

            if($update_option){
                $response['stat'] = 'success';
                $response['stat_title'] = 'Success';
                $response['stat_msg'] = 'Application settings has been successfully updated';

                //Add Logs
                $desc = 'updated the application settings';
                Helpers::add_activity_logs(['update-application-settings',$get_id,$desc]);
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to update. Error occured during process.';
            }

            $response['a'] = $update_option;*/
        }

        return $response;
        
    }
}
