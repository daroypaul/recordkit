@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Settings</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <!--<div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>-->
                            <div class="card-body">
                                <!---->

                                <div class="vtabs customvtab">
                                    <ul class="nav nav-tabs tabs-vertical customvtab" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab" aria-expanded="true"><span class="hidden-sm-up"><i class="ti-info-alt"></i></span> <span class="hidden-xs-down">Information</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tab2" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">Branding&nbsp;Logo</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="width:100% !important;">
                                        <div class="tab-pane active" id="tab1" role="tabpanel" aria-expanded="true">
                                            <form action="#" class="form-horizontal form-bordered form-material" method="post" id="form-basic-info" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                                <div class="form-body">
                                                    <div class="form-group row">
                                                        <label class="control-label text-bold text-right col-md-3">Company Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                        <div class="col-md-5">
                                                            <input type="text" class="form-control" value="{{$info->comp_name}}" name="name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label text-bold text-right col-md-3">Email <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                        <div class="col-md-5">
                                                            <input type="email" class="form-control" name="email" value="{{$info->comp_email}}" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label text-bold text-right col-md-3">Application Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                        <div class="col-md-5">
                                                            <input type="text" class="form-control" name="apps_name" value="{{$info->apps_name}}" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="control-label text-bold text-right col-md-3">Address</label>
                                                        <div class="col-md-5">
                                                            <textarea class="form-control" name="address">{{$info->comp_address}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type" value="info" />
                                                <div class="form-actions m-t-20">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane p-20" id="tab2" role="tabpanel" aria-expanded="false">
                                            <form action="#" class="form-horizontal form-bordered form-material" method="post" id="form-branding-logo" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <div class="row m-b-40">
                                                            <label class="control-label text-bold text-right col-md-3"></label>
                                                            <div class="col-md-5">
                                                                <img src="{{ ($info->comp_logo_desktop) ? url('/branding').'/'.$info->comp_logo_desktop : url('img/apps_logo.png') }}" alt="Apps Logo" class="img-responsive" id="branding-desktop"  style="width: auto; max-height: 50px;" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="control-label text-bold text-right col-md-3">Desktop Logo <br/><small>(Max Height 40px)</small><i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                            <div class="col-md-5">
                                                                <div class="fileinput fileinput-new input-group asset_file" data-provides="fileinput">
                                                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">Image File</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                                    <input type="hidden">
                                                                    <input type="file" name="desktop_logo"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" id="remove-docs-upload">Remove</a> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row m-b-40">
                                                            <label class="control-label text-bold text-right col-md-3"></label>
                                                            <div class="col-md-5">
                                                                <img src="{{ ($info->comp_logo_mobile) ? url('/branding').'/'.$info->comp_logo_mobile : url('img/icon.png') }}" alt="Mobile Layout Logo" class="img-responsive" id="branding-mobile" style="max-width: 50px;" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="control-label text-bold text-right col-md-3">Mobile Logo <br/><small>(40px x 40px)</small><i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                            <div class="col-md-5">
                                                                <div class="fileinput fileinput-new input-group asset_file" data-provides="fileinput">
                                                                    <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename">Image File</span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                                    <input type="hidden">
                                                                    <input type="file" name="mobile_logo"> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" id="remove-docs-upload">Remove</a> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="type" value="logo" />
                                                <div class="form-actions m-t-20">
                                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/tab-page.css')}}" rel="stylesheet">
<link href="{{ custom_asset('css/template/default/css/pages/file-upload.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
<script src="{{ url('assets/js/Fx6iElwpc9APaSn0NLrB.js') }}"></script>
@endsection