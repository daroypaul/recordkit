@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Settings</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="card">
                            <!--<div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>-->
                            <div class="card-body">
                                <form action="#" class="form-horizontal form-bordered form-material" method="post" id="form" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="form-group row">
                                            <label class="control-label text-bold text-right col-md-4">Timezone</label>
                                            <div class="col-md-5">
                                                <select class="form-control custom-select" name="timezone" id="timezone">
                                                    <?php
                                                        $i=0;
                                                        foreach($timezones as $timezone){
                                                            if($default_options['timezone']==$timezone->timezone_id){
                                                                echo '<option value="'.$timezone->timezone_id.'" selected>'.'('.$timezone->timezone_offset.') '.$timezone->timezone_name.'</option>';
                                                            }else{
                                                                echo '<option value="'.$timezone->timezone_id.'">'.'('.$timezone->timezone_offset.') '.$timezone->timezone_name.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label text-bold text-right col-md-4">Currency</label>
                                            <div class="col-md-4">
                                                <select class="form-control custom-select" name="currency" id="currency">
                                                        <?php 
                                                       
                                                        foreach($currencies as $currency){
                                                            if($default_options['currency']==$currency->currency_id){
                                                                echo '<option value="'.$currency->currency_id.'" selected>'.$currency->currency_name.' ('.$currency->currency_code.')'.'</option>';
                                                            }
                                                            else{
                                                                echo '<option value="'.$currency->currency_id.'">'.$currency->currency_name.' ('.$currency->currency_code.')'.'</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-b-0 last row">
                                                <label class="control-label text-bold text-right col-md-4">Skins</label>
                                                <div class="col-md-2">
                                                    <select class="form-control custom-select" name="skins" id="select-skins">
                                                        <?php
                                                            $skins = ['default','green','red','blue','purple','accent','orange','megna','default-dark','green-dark','red-dark','blue-dark','purple-dark','megna-dark'];

                                                            foreach($skins as $skin){
                                                                if($default_options['skin']==$skin){
                                                                    echo '<option value="'.$skin.'" selected>'.ucwords(str_replace('-', ' ',$skin)).'</option>';
                                                                }else{
                                                                    echo '<option value="'.$skin.'">'.ucwords(str_replace('-', ' ',$skin)).'</option>';
                                                                }
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right m-t-20">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/floating-label.css')}}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
<script src="{{ url('assets/js/wFevKzTOsJ81daGkjoLS.js') }}"></script>
@endsection