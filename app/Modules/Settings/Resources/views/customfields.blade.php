@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Settings</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Label</th>
                                                <th>Data Type</th>
                                                <th>Required</th>
                                                <th>Target Category</th>
                                                <th width="60">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($custom_fields as $custom_field)
                                                <tr data-id="{{$custom_field->field_id}}">
                                                   <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$custom_field->field_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$custom_field->field_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$custom_field->field_label}}</td>
                                                    <td>{{ucfirst($custom_field->field_data_type)}}</td>
                                                    <td>{!!($custom_field->is_required)?'<i class="fa fa-check-circle text-success"></i>':'-None-'!!}</td>
                                                    <td>
                                                        <?php
                                                            $category_list = '';
                                                            foreach($categories as $category){
                                                                if($category->field_id==$custom_field->field_id){
                                                                    $category_list .= $category->cat_name . ', ';
                                                                }
                                                            }

                                                            echo '<small>'.rtrim($category_list,", ").'</small>';
                                                        ?>
                                                    </td>
                                                    <td width="60">
                                                        <button type="button" class="btn btn-sm btn-inverse" data-action="edit">Edit</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
<script src="{{ url('assets/js/JpONvoAg4FSDGhCBVLcf.js') }}"></script>
@endsection