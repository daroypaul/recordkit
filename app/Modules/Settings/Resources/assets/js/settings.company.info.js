/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){  


    $('#select-skins').change(function() {
        var skins = $(this).val();

        $('link#theme').attr('href', $apps_base_url+'/css/template/default/css/colors/'+skins+'.css');
        //display_tag();
    });


    $('#form-basic-info').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Saving...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);

        $.ajax({
            type:'POST',
            url : window.location,
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('#form-basic-info [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('#form-basic-info [name="'+i+'"]').parent().append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                   $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    $('#form-branding-logo').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Saving...');
        $('#form-notification').html('');

        var form_data = new FormData();
        form_data.append('desktop_logo', $('#form-branding-logo input[name=desktop_logo]')[0].files[0]); 
        form_data.append('mobile_logo', $('#form-branding-logo input[name=mobile_logo]')[0].files[0]); 
        form_data.append('type', $('#form-branding-logo input[name=type]').val()); 

        $.ajax({
            type:'POST',
            url : window.location,
            data:  form_data,
            //dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('#form-branding-logo [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('#form-branding-logo [name="'+i+'"]').parents('.col-md-5').append('<div class="form-control-feedback">'+v[0]+'</div>');
                });

                if(data.stat){
                    if(data.stat=='success'){
                        $('#branding-desktop,#apps-branding-logo').attr('src', data.desktop); 
                        $('#branding-mobile,#apps-branding-icon').attr('src', data.mobile);   
                    }

                   $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });
});