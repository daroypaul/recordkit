<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'settings', 'namespace' => 'App\Modules\settings\Controllers'], function()
{
    Route::get('/', function () {
        return redirect('dashboard');
    });
    
    Route::get('/company-info', 'CompanyInfo@index');
    Route::post('/company-info', 'CompanyInfo@store');
    //Route::get('/tag', 'AssetTag@index');
    //Route::post('/tag', 'AssetTag@store');
    Route::get('/application', 'Systems@index');
    Route::post('/application', 'Systems@store');
    Route::get('/data-fields', 'CustomDataFields@index');
    Route::post('/data-fields', 'CustomDataFields@store');
    Route::post('/data-fields/delete', 'CustomDataFields@delete');
    Route::post('/fetch/data-fields', 'CustomDataFields@fetch_data');
});