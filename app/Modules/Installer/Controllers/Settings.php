<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\installer\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\utilities\Currencies;
use App\utilities\TimeZones;
use App\modules\settings\models\CompanyInformation;
use App\modules\adminprofile\models\Users;
use App\modules\settings\models\SystemOptions;

use Installer;
use DB;
use Artisan;
use Validator;
use Session;
use Options;

class Settings extends Controller
{
    public function show(){
        if(Installer::checkServerRequirements()) {
            return redirect('install');
        }
        
        if(!DB::connection()->getDatabaseName())
        {
            return redirect('install/database');
        }

        $check_user = Users::where('account_id', 1)->count();
        $check_company = CompanyInformation::where('account_id', 1)->count();

        //If Account and User is already setup
        if(!$check_user&&!$check_company){
            return redirect('install/accounts');
        }

        $currencies = Currencies::orderBy('currency_name', 'asc')->get();
        $timezones = TimeZones::all();

        $data = [
            "currencies"=>$currencies,
            "timezones"=>$timezones,
        ];

        return view('installer::settings')->with($data);
    }

    public function store(Request $request)
    {
        $rules = [
            'currency'=>'required|int',
            'timezone'=>'required|int',
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }else{
            $timezone = Options::where('account_id', 1)->where('option_name', 'apps_timezone')->count();
            $currency = Options::where('account_id', 1)->where('option_name', 'apps_currency')->count();
            $skin = Options::where('account_id', 1)->where('option_name', 'apps_skins')->count();
            //$check_options = SystemOptions::where('account_id', 1)->count();

            if(!$timezone&&!$currency&&!$skin){
                $add_timezone = new Options;
                $add_timezone->account_id = 1;
                $add_timezone->option_name = 'apps_timezone';
                $add_timezone->option_value = $request->timezone;
                $add_timezone->save();

                $add_currency = new Options;
                $add_currency->account_id = 1;
                $add_currency->option_name = 'apps_currency';
                $add_currency->option_value = $request->currency;
                $add_currency->save();

                $add_skin = new Options;
                $add_skin->account_id = 1;
                $add_skin->option_name = 'apps_skins';
                $add_skin->option_value = 'default';
                $add_skin->save();
                /*$add = new SystemOptions;
                $add->account_id = 1;
                $add->option_type = 'ams';
                $add->option_timezone = $request->timezone;
                $add->option_currency = $request->currency;
                $add->option_skins = 'default';
                $add->save();*/

                if($add_timezone&&$add_currency&&$add_skin){
                    Installer::updateEnv([
                        'APP_INSTALLED'   =>  'true',
                    ]);
                    return redirect('install/completed');
                }else{
                    Session::flash('set_error_msg', 'Unable to update the settings. Error occured during process.');

                    return redirect('install/settings');
                }
            }else{
                return redirect('auth/login');
            }
        }
    }
}
