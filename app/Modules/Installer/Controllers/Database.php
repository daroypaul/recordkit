<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\installer\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Installer;
use File;
use Session;
use Validator;
use DB;
use Artisan;
use Schema;

class Database extends Controller
{
    public function show(){
        
        if(DB::connection()->getDatabaseName())
        {
            return redirect('install/accounts');
        }

        if(Installer::checkServerRequirements()) {
            return redirect('install');
        }

        return view('installer::database');
    }

    public function store(Request $request)
    {
        $host = $request['hostname'];
        $port     = env('DB_PORT', '3306');
        $database = $request['database'];
        $username = $request['username'];
        $password = $request['password'];

        $rules = [
            'hostname'=>'required',
            'database'=>'required',
            'username'=>'required',
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
            //return Redirect::back()->withErrors($validator);
            return back()->withInput()->withErrors($validator);
        }
        else
        {
            // Check database connection
            if (!Installer::createDbTables($host, $port, $database, $username, $password)) {
                //$message = trans('install.error.connection');

                //flash($message)->error()->important();
                Session::flash('db_error_msg', 'Error: Could not connect to the database! Please, make sure the details are correct.');
                return redirect('install/database')->withInput();
            }

            return redirect('install/accounts');
        }
    }
}
