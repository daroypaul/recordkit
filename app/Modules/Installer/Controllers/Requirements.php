<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\installer\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Installer;
use File;
use Session;

class Requirements extends Controller
{
    public function show(){
        $requirements = Installer::checkServerRequirements();


        if (empty($requirements)) {
            if($_ENV['APP_ONSETUP']){
                Installer::createDefaultEnvFile();
            }
        } else {
            $data = [
                "requirements"=>$requirements,
            ];


            return view('installer::requirements')->with($data);
        }
        
        return redirect('install/database');
    }
}
