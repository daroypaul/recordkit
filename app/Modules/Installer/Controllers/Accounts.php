<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\installer\controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\modules\settings\models\CompanyInformation;
use App\modules\adminprofile\models\Users;
use App\utilities\Currencies;
use App\modules\adminprofile\models\UserGroups;
use DB;
use Artisan;
use Session;
use Validator;
use Carbon\Carbon;
use Hash;
use Installer;

class Accounts extends Controller
{
    public function show(){
		if(!DB::connection()->getDatabaseName())
        {
            return redirect('install/database');
        }

        if(Installer::checkServerRequirements()) {
            return redirect('install');
        }

        //Run seeding
        if(!Currencies::count()){
            ini_set('max_execution_time', '-1');
            ini_set('memory_limit', '-1');
            Artisan::call('db:seed', ['--force' => true]);
        }

        $check_user = Users::where('account_id', 1)->count();
        $check_company = CompanyInformation::where('account_id', 1)->count();
        //If Account and User is already setup
        if($check_user&&$check_company){
            return redirect('install/settings');
        }

        return view('installer::accounts');
    }

    public function store(Request $request)
    {


        $rules = [
            'company_name'=>'required|max:100',
            'company_email'=>'required|email|max:50',
            'admin_email'=>'required|email|max:50',
            'admin_username'=>'required|min:5|max:20|alpha_num',
            'apps_name'=>'required|min:5|max:30|regex:/^[a-zA-Z0-9\s]+$/u',
            'admin_password'=>'required|min:8|max:18|regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/'
        ];

        $messages = [
            'apps_name.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(),$rules, $messages);


        if ($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }else{
            $new_user_group = new UserGroups;
            $new_user_group->account_id = 1;
            $new_user_group->group_name = 'Administrator';
            $new_user_group->group_privileges = '["all"]';
            $new_user_group->is_system_built = 1;
            $new_user_group->save();
        
            $user_group_id = $new_user_group->user_group_id;
            $hash_password = Hash::make ($request->admin_password);

            $add_user = new Users;
            $add_user->account_id = 1;
            $add_user->user_group_id = $user_group_id;
            $add_user->site_id = 0;
            $add_user->user_login = strtolower($request->admin_username);
            $add_user->user_pass = $hash_password;
            $add_user->user_fullname = 'Administrator';
            $add_user->user_email = $request->admin_email;
            $add_user->user_remarks = '';
            $add_user->recorded_by = 0;
            $add_user->updated_by = 0;
            $add_user->record_stat = 0;
            $add_user->is_system_built = 1;
            $add_user->is_deleted = 0;
            $add_user->save();
            $user_id = $add_user->user_id;

            $add_info = new CompanyInformation;
            $add_info->account_id = 1;
            $add_info->comp_name = $request->company_name;
            $add_info->comp_address = '';
            $add_info->comp_email = $request->company_email;
            $add_info->apps_name = $request->apps_name;
            $add_info->date_updated = Carbon::now('UTC')->toDateTimeString();
            $add_info->updated_by = $user_id;
            $add_info->save();

            if($add_user&&$add_info){
                return redirect('install/settings');
            }else{
                Session::flash('acct_error_msg', 'Error: Unable to save account details. Please try again.');
                return redirect('install/database')->withInput();
            }
        }
    }
}
