<?php


//Route::group(['middleware' => 'guest'], function () {
Route::group(['middleware' => ['web','guest'], 'prefix' => 'install', 'namespace' => 'App\Modules\installer\Controllers'], function(){

    Route::group(['middleware' => 'install.none'], function () {
        //Route::group(['prefix' => 'install'], function () {
            #Requirements
            Route::get('/', 'Requirements@show'); 
            #Step #1
            Route::get('database', 'Database@show');
            Route::post('database', 'Database@store');
            #Step #2
            Route::get('accounts', 'Accounts@show');
            Route::post('accounts', 'Accounts@store');
            #Step #3
            Route::get('settings', 'Settings@show');
            Route::post('settings', 'Settings@store');
        //});
    });

    #Completed Installation Routes
    Route::view('completed', 'installer::completed');
});