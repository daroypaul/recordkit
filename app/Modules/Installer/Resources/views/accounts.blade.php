@extends('installer::layout')

@section('content')
<div class="login-box card card-outline-inverse" width="500px">
    <div class="card-header">
            <h4 class="m-b-0 text-white">Step 2/3: Account Setup</h4>
    </div>
    <div class="card-body">
        <form method="post" class="form-horizontal" id="loginform" action="{{URL::to('/install/accounts')}}">
            {{csrf_field()}}
            @if(Session::has('acct_error_msg'))
            <div class="alert alert-danger p-10 p-l-15 p-r-15"><small>{{Session::get('acct_error_msg')}}</small></div>
            @endif

            <div class="form-group has-error m-b-10">
                <label>Company Name <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                    <input class="form-control" placeholder="Enter Company Name" type="text" value="{{old('company_name')}}" name="company_name" required>
                </div>
                @if ($errors->has('company_name'))
                    <small class="text-danger">{{ $errors->first('company_name') }}</small>
                @endif
            </div>
            <div class="form-group m-b-10">
                <label>Company Email <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-email"></i></span>
                    <input type="email" class="form-control" placeholder="Enter Company Email" type="text" name="company_email" value="{{old('company_email')}}" required>
                </div>
                @if ($errors->has('company_email'))
                    <small class="text-danger">{{ $errors->first('company_email') }}</small>
                @endif
            </div>
            <div class="form-group m-b-10">
                <label>Application Custom Name <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-apps"></i></span>
                    <input type="text" class="form-control" placeholder="Enter Application" type="text" name="apps_name" value="{{(old('apps_name')) ? old('apps_name') : 'RecordKit'}}" required>
                </div>
                @if ($errors->has('apps_name'))
                    <small class="text-danger">{{ $errors->first('apps_name') }}</small>
                @endif
            </div>
            <hr/>
            <div class="form-group m-b-10">
                <label>Admin Email <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-email"></i></span>
                    <input  type="email" class="form-control" placeholder="Enter Admin Email" type="text" name="admin_email" value="{{old('admin_email')}}" required>
                </div>
                @if ($errors->has('admin_email'))
                    <small class="text-danger">{{ $errors->first('admin_email') }}</small>
                @endif
            </div>
            <div class="form-group m-b-10">
                <label>Admin Username <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                    <input class="form-control" placeholder="Enter Admin Username" type="text" name="admin_username" value="{{old('admin_username')}}" required>
                </div>
                @if ($errors->has('admin_username'))
                    <small class="text-danger">{{ $errors->first('admin_username') }}</small>
                @endif
            </div>
            <div class="form-group m-b-0">
                <label>Admin Password <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-key"></i></span>
                    <input type="password" class="form-control" placeholder="Enter Admin Password" type="text" name="admin_password" required>
                </div>
                @if ($errors->has('admin_password'))
                    <small class="text-danger">{{ $errors->first('admin_password') }}</small>
                @endif
            </div>

            <div class="form-group m-b-10 p-b-0" style="font-size:0.9em;">
                <label class="control-label"><small>Password Requirements:</small></label>
                <ul class="list-icons">
                    <li style="margin:0px !important; line-height:20px;"><i class="fa fa-check text-danger"></i> <small>Must be a minimum of 8 characters</small></li>
                    <li style="margin:0px !important; line-height:20px;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one number</small></li>
                    <li style="margin:0px !important; line-height:20px;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one uppercase character</small></li>
                    <li style="margin:0px !important; line-height:20px;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one lowercase character</small></li>
                </ul>
            </div>

            <div>
                <button type="submit" class="btn btn-inverse waves-effect waves-light float-right">Next</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_name')
Account Setup
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endsection

@section('js')

@endsection