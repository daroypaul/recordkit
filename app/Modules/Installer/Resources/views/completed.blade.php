@extends('installer::layout')

@section('content')
<div class="login-box card card-outline-inverse" width="700px">
    <!--<div class="card-header">
            <h4 class="m-b-0 text-white">Completed</h4>
    </div>-->
    <div class="card-body">
            <!--<h3 class="m-b-20 text-success"></h3>-->
        <div class="alert alert-success m-b-20" style="font-size:1.1em;">Installation Completed Successfully</div>
        

            <a class="btn btn-inverse waves-effect waves-light" href="{{url('/auth/login')}}">Proceed to Login</a>
    </div>
</div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_name')
Installation Completed
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endsection

@section('js')

@endsection