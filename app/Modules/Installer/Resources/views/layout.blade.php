<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    @yield('meta')
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ custom_asset('template2/assets/images/favicon.png') }}">
    <title>@yield('module_name') | {{Config::get('constant.apps_name')}}</title>
    <!--@yield('module_title')-->
    <!-- Bootstrap Core CSS -->
    <link href="{{ custom_asset('js/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- page css -->
    <link href="{{ custom_asset('css/template/default/css/pages/login-register-lock.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/card-page.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ custom_asset('css/template/default/css/style.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Loading...</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <section id="wrapper" class="login-register" style="background-image:url({{ custom_asset('img/login_bg.jpeg') }}); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; padding:3% 0; overflow: auto;">

            @if(request()->getHttpHost()!='uatinout.worldvision.org.ph'&&request()->getHttpHost()!='inout.worldvision.org.ph')
                <a href="/" class="text-center db m-b-30">
                    <img src="{{ custom_asset('img/apps_white_logo.png') }}" alt="Apps Logo" width="200" />
                </a>
            @endif

            @yield('content')

            <div class="text-center text-white m-b-20">
                <small>{!!Config::get('constant.copyright')!!}</small>
            </div>
        </section>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ custom_asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ custom_asset('js/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ custom_asset('js/template/default/perfect-scrollbar.jquery.min.js') }}"></script>

    <script>
        $(function () {
            "use strict";
            $(function () {
                $(".preloader").fadeOut();
            });
            
        });
    </script>
    @yield('js')<!--Custom js loaded per page-->
</body>

</html>