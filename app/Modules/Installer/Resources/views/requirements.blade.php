@extends('installer::layout')

@section('content')
<div class="login-box card card-outline-inverse" width="1000px">
    <div class="card-header">
            <h4 class="m-b-0 text-white">Requirements:</h4>
    </div>
    <div class="card-body">
        <ul class="list-icons">
            @foreach ($requirements as $req)
                <li><i class="fa fa-times text-danger"></i>{!! $req['msg']!!}</li>
            @endforeach
        </ul>

        <a class="btn btn-inverse waves-effect waves-light m-t-20" href="{{url('/install/database')}}">Proceed</a>
    </div>
</div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_name')
Requirements
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endsection

@section('js')

@endsection