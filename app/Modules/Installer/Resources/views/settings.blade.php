@extends('installer::layout')

@section('content')
<div class="login-box card card-outline-inverse" width="500px">
    <div class="card-header">
            <h4 class="m-b-0 text-white">Step 3/3: Apps Setting</h4>
    </div>
    <div class="card-body">
        <form method="post" class="form-horizontal" id="loginform" action="{{URL::to('/install/settings')}}">
            {{csrf_field()}}
            @if(Session::has('set_error_msg'))
            <div class="alert alert-danger p-10 p-l-15 p-r-15"><small>{{Session::get('set_error_msg')}}</small></div>
            @endif

            <div class="form-group has-error m-b-10">
                <label>Timezone <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-av-timer"></i></span>
                    <select class="form-control custom-select" name="timezone" id="timezone" required>
                        <option></option>
                        <?php
                            $i=0;
                            foreach($timezones as $timezone){
                                
                                    echo '<option value="'.$timezone->timezone_id.'">'.'('.$timezone->timezone_offset.') '.$timezone->timezone_name.'</option>';
                            }
                        ?>
                    </select>
                </div>
                @if ($errors->has('timezone'))
                    <small class="text-danger">{{ $errors->first('timezone') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label>Currency <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-coin"></i></span>
                    <select class="form-control custom-select" name="currency" id="currency" placeholder="Select Currency" required>
                        <option></option>
                        <?php 
                       
                        foreach($currencies as $currency){
                            echo '<option value="'.$currency->currency_id.'">'.$currency->currency_name.' ('.$currency->currency_code.')'.'</option>';
                        }
                        ?>
                    </select>
                </div>
                @if ($errors->has('currency'))
                    <small class="text-danger">{{ $errors->first('currency') }}</small>
                @endif
            </div>

            <div>
                <button type="submit" class="btn btn-inverse waves-effect waves-light float-right">Finish</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_name')
Apps Settings
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endsection

@section('js')

@endsection