@extends('installer::layout')

@section('content')
<div class="login-box card card-outline-inverse" width="500px">
    <div class="card-header">
            <h4 class="m-b-0 text-white">Step 1/3: Database Setup</h4>
    </div>
    <div class="card-body">
        <form method="post" class="form-horizontal" id="loginform" action="{{URL::to('/install/database')}}">
            {{csrf_field()}}
            @if(Session::has('db_error_msg'))
            <div class="alert alert-danger p-10 p-l-15 p-r-15"><small>{{Session::get('db_error_msg')}}</small></div>
            @endif
            <div class="form-group has-error m-b-10">
                <label>Host <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-server-network"></i></span>
                    <input class="form-control" placeholder="Enter Localhost" type="text" value="{{(old('hostname')) ? old('hostname') : '' }}" name="hostname">
                </div>
                @if ($errors->has('hostname'))
                    <small class="text-danger">{{ $errors->first('hostname') }}</small>
                @endif
            </div>
            <div class="form-group m-b-10">
                <label>Database <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-database"></i></span>
                    <input class="form-control" placeholder="Enter Database" type="text" name="database" value="{{old('database')}}">
                </div>
                @if ($errors->has('database'))
                    <small class="text-danger">{{ $errors->first('database') }}</small>
                @endif
            </div>
            <div class="form-group m-b-10">
                <label>Username <sus><span class="text-danger">*</span></sus></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-account"></i></span>
                    <input class="form-control" placeholder="Enter Username" type="text" name="username" value="{{old('username')}}">
                </div>
                @if ($errors->has('username'))
                    <small class="text-danger">{{ $errors->first('username') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="mdi mdi-key"></i></span>
                    <input type="password" class="form-control" placeholder="Enter Password" type="text" name="password">
                </div>
            </div>

            <div>
                <button type="submit" class="btn btn-inverse waves-effect waves-light float-right">Next</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_name')
Database Setup
@endsection

@section('style')
@endsection

@section('js')

@endsection