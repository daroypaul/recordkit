<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ custom_asset('template2/assets/images/favicon.png') }}">
    <title>RecordKits Application Suite - Beta | Reset Password</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ custom_asset('js/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- page css -->
    <link href="{{ custom_asset('css/template/default/css/pages/login-register-lock.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ custom_asset('css/template/default/css/style.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{ custom_asset('css/template/default/css/colors/blue.css') }}" id="theme" rel="stylesheet">
    <link href="{{ custom_asset('css/custom.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Loading...</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register" style="background-image:url({{ custom_asset('img/login_bg.jpeg') }}); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; padding:2% 0;">

        <a href="{{url('/auth/login')}}" class="text-center db m-t-20 m-b-30">
            @if(request()->getHttpHost()=='uatinout.worldvision.org.ph'||request()->getHttpHost()=='inout.worldvision.org.ph')
                <img src="{{ custom_asset('img/wvdf_logo.jpg') }}" alt="Apps Logo" width="250" />
            @else
                <img src="{{ custom_asset('img/apps_white_logo.png') }}" alt="Apps Logo" width="240" />
            @endif
        </a>

        <div class="login-box card">
            <div class="card-body">
                <form method="post" class="form-horizontal form-material" id="loginform" action="{{($page=='reset')?url('/auth/reset'):url('auth/reset_password')}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_reset_token" value="{{$_GET['token']}}" />
                    

                    @if($page=='reset')
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3 class="text-center">Reset Password</h3>
                        </div>
                    </div>

                    @if (Session::has('login-error'))
                        <div class="alert alert-danger">{{ Session::get('login-error') }}</div>
                    @elseif (Session::has('reset-success'))
                        <div class="alert alert-success">{{ Session::get('reset-success') }}</div>
                    @endif

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required name="username" placeholder="Username">
                            <small class="text-danger">{{$errors->first('username')}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required name="password" placeholder="Passsword">
                            <small class="text-danger">{{$errors->first('password')}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required name="password_confirmation" placeholder="Confirm Password">
                            <small class="text-danger">{{$errors->first('password')}}</small>
                        </div>
                    </div>
                    
                    <div class="form-group p-b-0">
                        <label class="control-label"><small>Password Requirements:</small></label>
                        <ul class="list-icons">
                            <li style="margin:0px !important;"><i class="fa fa-check text-danger"></i> <small>Must be a minimum of 8 characters</small></li>
                            <li style="margin:0px !important;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one number</small></li>
                            <li style="margin:0px !important;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one uppercase character</small></li>
                            <li style="margin:0px !important;"><i class="fa fa-check text-danger"></i> <small>Must contain at least one lowercase character</small></li>
                        </ul>
                    </div>

                    @else
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username" name="user_name">
                        </div>
                    </div>
                    @endif

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-blocks text-uppercase btn-lg btn-roundeds" type="submit">Reset</button>
                        </div>
                    </div>

                    <!--<div class="form-group text-center m-b-0 p-t-20">
                            <div class="text-center"><a href="http://recuda.com"><img src="{{ custom_asset('img/recuda.png') }}" alt="Apps Logo" width="100" /></a></div>

                            <small>2018 © RecordKits Asset Tracking System | <b>Beta</b></small>
                        </div>-->
                </form>
            </div>
        </div>
    
        <div class="text-center text-white" style="font-size:0.8em;">
            <div>{!!Config::get('constant.copyright')!!}</div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ custom_asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ custom_asset('js/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ custom_asset('js/auth.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    
</body>

</html>