<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ custom_asset('template2/assets/images/favicon.png') }}">
    <title>{{Config::get('constant.apps_name')}} | Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ custom_asset('js/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- page css -->
    <link href="{{ custom_asset('css/template/default/css/pages/login-register-lock.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ custom_asset('css/template/default/css/style.css') }}" rel="stylesheet">
    
    <!-- You can change the theme colors from here -->
    <link href="{{ custom_asset('css/template/default/css/colors/blue.css') }}" id="theme" rel="stylesheet">
    <link href="{{ custom_asset('css/custom.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Loading...</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register" style="background-image:url({{ custom_asset('img/login_bg.jpeg') }}); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; padding:6% 0;">
        
            @if(request()->getHttpHost()!='uatinout.worldvision.org.ph'&&request()->getHttpHost()!='inout.worldvision.org.ph')
                <a href="{{url('/auth/login')}}" class="text-center db m-b-30">
                    <img src="{{ custom_asset('img/apps_white_logo.png') }}" alt="Apps Logo" width="250" />
                </a>
            @endif

        <div class="login-box card">
            <div class="card-body">
                <form method="post" class="form-horizontal form-material" id="loginform" action="{{URL::to('/auth/login')}}">
                    {{ csrf_field() }}
                        
                    @if(request()->getHttpHost()=='uatinout.worldvision.org.ph'||request()->getHttpHost()=='inout.worldvision.org.ph')
                    <a href="/" class="text-center db m-t-10 m-b-30">
                        <img src="{{ custom_asset('img/wvdf_logo.jpg') }}" alt="Apps Logo" width="200" />
                    </a>
                    @else
                        <h5 class="text-center m-t-10 m-b-40">Login to start your session</h5>
                    @endif

                    @if (Session::has('login-error'))
                        <div class="alert alert-danger m-t-30">{{ Session::get('login-error') }}</div>
                    @elseif (Session::has('reset-success'))
                        <div class="alert alert-success m-t-30">{{ Session::get('reset-success') }}</div>
                    @endif

                    <div class="form-group m-t-20">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required name="user_login" placeholder="Username or Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required name="user_pass" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary float-left p-t-0">
                                <input id="checkbox-signup" type="checkbox" class="filled-in chk-col-light-blue" name="remember" value="true">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="text-dark float-right"><i class="fa fa-lock m-r-5"></i> Forgot Password</a>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20 m-b-10">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-blocks text-uppercase btn-roundeds" type="submit">Log In</button>
                        </div>
                    </div>

                    <!--<div class="form-group text-center m-b-0 p-t-20">
                            <div class="text-center"><a href="http://recuda.com"><img src="{{ custom_asset('img/recuda.png') }}" alt="Apps Logo" width="100" /></a></div>

                            <small>2018 © RecordKits Asset Tracking System | <b>Beta</b></small>
                        </div>-->
                </form>

                

                    <!--<div class="footer p-10">
                        
                        <div class="text-center"><img src="{{ custom_asset('img/recuda.png') }}" alt="Apps Logo" width="100" /></div>

                        <div class="form-group text-center m-b-0"><small>2018 © RecordKits Asset Tracking System | <b>Beta</b></small></div>
                    </div>-->
                <form class="form-horizontal" id="recoverform" method="post" action="{{URL::to('/auth/reset_password')}}">
                    {{ csrf_field() }}
                        @if(request()->getHttpHost()=='uatinout.worldvision.org.ph'||request()->getHttpHost()=='inout.worldvision.org.ph')
                        <a href="/" class="text-center db m-t-20 m-b-40">
                            <img src="{{ custom_asset('img/wvdf_logo.jpg') }}" alt="Apps Logo" width="200" />
                        </a>
                        @else
                            <!--<img src="{{ custom_asset('img/weventory_beta.png') }}" alt="Apps Logo" width="200" />-->
                        @endif

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3 class="text-center">Recover Password</h3>
                            <!--<p class="text-muted">Enter your Email and instructions will be sent to you! </p>-->
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username or Email" name="user_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <a href="javascript:void(0)" id="to-login" class="text-dark"><i class="fa fa-lock m-r-5"></i> Back to Log In</a>
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20 m-b-10">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-blocks text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center text-white" style="font-size:0.8em;">
            <div>{!!Config::get('constant.copyright')!!}</div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ custom_asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ custom_asset('js/plugins/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ custom_asset('js/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {

            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $('#to-login').on("click", function() {
            $("#loginform").slideDown();
            $("#recoverform").slideUp();
        });
    </script>
    
</body>

</html>