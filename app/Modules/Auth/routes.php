<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\modules\auth\controllers'], function()
{
    Route::group(['middleware' => 'install.done'], function () {
        Route::group(['middleware' => 'guest', 'prefix' => 'auth'], function () {
            Route::get('login', 'AuthController@login');
            Route::post('login', [ 'as' => 'login', 'uses' => 'AuthController@check']);
            Route::get('reset', 'AuthController@reset_form');
            Route::post('reset', 'AuthController@reset_password');
            Route::post('reset_password', 'AuthController@send_reset_link');
            Route::get('reset_password', function(){
                return redirect('dashboard');
            });
        });

        Route::group(['middleware' => 'auth'], function () {
            Route::get('logout', 'AuthController@logout');
        });
    });
});