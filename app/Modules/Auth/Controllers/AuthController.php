<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\auth\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\modules\adminprofile\models\Users;

use App\modules\auth\controllers\User;
use Hash;
use Auth;
use Redirect;
use Session;
use Validator;
use Carbon\Carbon;
use ResetPassword;
use ActivityLogs;
use Mail;

class AuthController extends Controller
{
    //
    public function login(){
		/*if ($_ENV['APP_INSTALLED']==false) {
			return redirect('install');
		}*/
		
		if (Auth::check())
        {
			return redirect('dashboard');
		}
		
		return view('auth::login');
	}
	
	public function check(Request $request){
		$user_login = $request->post('user_login');
		$user_pass = $request->post('user_pass');
		$remember_me = ($request->remember) ? 'true' : '';
		
		if(filter_var($user_login, FILTER_VALIDATE_EMAIL)) {
			$credentials = [
				'user_email' => $request['user_login'],
				'password' => $request['user_pass'],
				'record_stat' => 0,
				'is_deleted' => 0,
			];
		} else {
			$credentials = [
				'user_login' => $request['user_login'],
				'password' => $request['user_pass'],
				'record_stat' => 0,
				'is_deleted' => 0,
			];
		}

		$rules = array (
				
				'user_login' => 'required',
				'user_pass' => 'required' 
		);
		$validator = Validator::make ( Input::all (), $rules );
		if ($validator->fails ()) {
			//return Redirect::back ()->withErrors ( $validator, 'login' )->withInput ();
			Session::flash('login-error', 'All fields are required');
			return Redirect::back ();
		} else {
			if(Auth::attempt($credentials, $remember_me)){
				//ActivityLogs
				$user_id = Auth::id();
				$account_id = Auth::user()->account_id;

				$new_logs = new ActivityLogs;
				$new_logs->account_id = $account_id;
				$new_logs->action_type = "signin";
				$new_logs->activity_desc = "user has signed-in";
				$new_logs->activity_date = Carbon::now('UTC')->toDateTimeString();
				$new_logs->action_by = $user_id;
				$new_logs->save();

				return redirect('dashboard');
			}else{
				Session::flash('login-error', 'Incorrect username or password');
				return Redirect::back ();
			}
		}	
	}

	public function send_reset_link(Request $request){
		$user_name = $request->post('user_name');

		if(!$user_name){
			Session::flash('login-error', 'Username is required');
			return Redirect::back ();
		}

		//$check_user = Users::where('user_login',$user_name)->where('is_deleted',0)->first();

		$check_user = Users::where(function ($query) use ($user_name)  {
			$query->where('user_login', $user_name)
			->orWhere('user_email', $user_name);
		})->where('is_deleted',0)->first();

		if(count($check_user)){
			$token = str_random(150);
			
			if($check_user->user_email){
				$user_id = $check_user->user_id;
				$exist_request = ResetPassword::where('user_id',$user_id)->get();

				foreach($exist_request as $request){
					$request_id = $request->request_id;

					$delete_request = ResetPassword::find($request_id)->delete();
				}

				$new_request = new ResetPassword;
				$new_request->user_id = $user_id;
				$new_request->username = $check_user->user_login;
				$new_request->token = $token;
				$new_request->date_requested = Carbon::now('UTC')->toDateTimeString();
				$new_request->save();
				
				if($new_request){
					$data = [
						"email" => $check_user->user_email,
						"subject" => 'Reset Password',
						"type"=>'reset_pass',
						"fullname"=>$check_user->user_fullname,
						"link"=>url('/auth/reset').'?token='.$token
					];

					try {
						Mail::send('mail.email', $data, function($message) use ($data){
							$message->from('notification@recuda-apps.com', 'ICT Asset Tracking System');
							$message->to($data['email']);
							$message->subject($data['subject']);
						});
					  }
					  catch (\Exception $e) {
						  
					  }
					
					Session::flash('reset-success', 'A reset password instruction has been sent to your e-mail');
					return Redirect::back ();
				}else{
					Session::flash('login-error', 'Unable to reset password. Error occured during process');
					return Redirect::back ();
				}
			}else{
				Session::flash('login-error', 'User account has no email');
				return Redirect::back ();
			}
		}else{
			Session::flash('login-error', 'User not found');
			return Redirect::back ();
		}

	}
	
	public function reset_form(){
		if (Auth::check())
        {
			return redirect('dashboard');
		}else{
			if(isset($_GET['token'])&&$_GET['token']!=''){
				$token = $_GET['token'];

				//Check token
				$check_request = ResetPassword::where('token',$token)->count();

				if($check_request){
					$data = [
						"page"=>"reset",
					];
					
					return view('auth::reset')->with($data);
				}else{
					$data = [
						"page"=>"request_reset",
					];
					
					//Session::flash('login-error', 'Expired password reset request');
					return redirect('auth/login');
				}
			}else{
				return redirect('dashboard');
			}
		}
	}

	public function reset_password(Request $request){
		$token = $request->_reset_token;

		$check_user = Users::where('user_login',$request->username)->where('is_deleted',0)->first();
		
		$user_id = ($check_user)?$check_user->user_id:'';
		$check_request = ResetPassword::where('token',$token)->where('user_id', $user_id)->first();

		if(count($check_request)){
			$rules = [
				'username'=>'required|min:5|max:30|regex:/(^[A-Za-z@._]+$)+/',
				'password'=>'required|min:8|max:18|confirmed|regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
				'password_confirmation'=>'sometimes|required_with:password',
			];
	
			$messages = [
				'fullname.regex' => 'The :attribute format is invalid.',
				'username.regex' => 'The :attribute format is invalid.',
				'remarks.regex' => 'The :attribute format is invalid.',
				'password.regex' => 'The :attribute format is invalid.',
				'password_confirmation.required_with' => 'The :attribute  field is required.',
			];
	
			$validator = Validator::make($request->all(), $rules, $messages);#Run the validation
			$val_msg = $validator->errors();

			if ($validator->fails()) {
				$data = [
					"page"=>"reset",
					"errors"=>$validator->messages (),
				];
				
				return Redirect::back ()->with($data);
			} else {

				$new_hash_password = Hash::make ($request->password);

				$update = Users::find($user_id);
				$update->user_pass = $new_hash_password;
				$update->updated_by = $user_id;
				$update->save();

				if($update){
					$delete_request = ResetPassword::find($check_request->request_id)->delete();

					Session::flash('reset-success', 'Your password has been reset successfully');
					return redirect('auth/login');
				}else{
					$data = [
						"page"=>"reset",
					];
					
					Session::flash('login-error', 'Unable to reset password');
					return Redirect::back ()->with($data);
				}
			}
		}else{
			Session::flash('login-error', 'Invalid username');
			return Redirect::back ();
		}
	}


    //
    public function register(){
    	return view('auth::register');
	}

    public function store(Request $request){
    	$rules = array (
			'user_fullname' => 'required',
			//'email' => 'required|unique:sys_users|email',
			'user_login' => 'required|unique:sys_users|alpha_num|min:4',
			'user_pass' => 'required|min:6' 
		);
		$validator = Validator::make ( Input::all (), $rules );

		//return Input::all ();
		if ($validator->fails ()) {

			//return $validator;
			//return Redirect::back ()->withErrors ( $validator, 'register' )->withInput ();
			return "error";
		} else {
			$user = new User ();
			$user->user_fullname = $request->get ( 'user_fullname' );
			$user->user_email = $request->get ( 'user_email' );
			$user->user_login = $request->get ( 'user_login' );
			$user->user_pass = Hash::make ( $request->get ( 'user_pass' ) );
			//$user->remember_token = $request->get ( '_token' );
			
			$user->save ();
			
			return Redirect::back ();
		}
	}
	
	public function logout(){
		Auth::logout();
		Session::flush();
		return redirect('auth/login');
	}
}
