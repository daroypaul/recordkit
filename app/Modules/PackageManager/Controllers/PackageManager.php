<?php

namespace App\modules\packagemanager\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Packages;
use Helpers;
use Schema;
use App\Utilities\Migrations as Migrations;

class PackageManager extends Controller
{
    public function installed_index(){
        //return packages::modify('version_code', ['modules/Kms','modules/Insurances','modules/Consumables'],[0,1,2]);

        $packages = packages::data();

        $response = [
            "packages"=>$packages,
            "form_title"=>"Packages"
        ];

        return view('packagemanager::index')->with($response);

        $package = 'consumables';
        $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';
        //App\Packages\Knowledgebase\Providers\KnowledgeBaseServiceProvider

        $installer =  new $className;

        $uninstall = $installer->install($package);

        return $uninstall;

        //$packages = packages::scan_packages();
        $file_routes = [];

        foreach(packages::scan_packages() as $package_dir){
            $package_detail_basepath = base_path($package_dir . '/package.json');

            if(file_exists($package_detail_basepath)) {
                $get_detail_data = file_get_contents($package_detail_basepath);
                $detail_data = json_decode($get_detail_data, true);

                if($detail_data['active']!=0&&$detail_data['installed']!=0){
                    $file_basepath = base_path($package_dir.'/file_routes.json');
                    if (file_exists($file_basepath)) {
                        $get_data = file_get_contents($file_basepath);
                        $json_data = json_decode($get_data, true);

                        foreach($json_data as $key => $value){
                            $file_routes[$key] = $value;
                        }
                    }
                }
            }
        }

        foreach(packages::scan_modules() as $module){
            $file_basepath = base_path($module.'/file_routes.json');
            
            if (File::exists($file_basepath)) {
                $get_data = file_get_contents($file_basepath);
                $json_data = json_decode($get_data, true);

                foreach($json_data as $key => $value){
                    $file_routes[$key] = $value;
                }
            }
        }

        return $file_routes;
        //$a = Helpers::generate_random_string(20);

        //return $a;
    }

    public function install(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if(!$package_data['installed']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            $install = $installer->install($package);
            $response['stat'] = $install;
        }else{
            $response[] = 'Package installed already';
        }

        $response['package'] = packages::data($package);

        return $response;
    }
    
    public function uninstall(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if($package_data['installed']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            $install = $installer->uninstall($package);
            $response['stat'] = $install;
        }else{
            $response[] = 'Package installed already';
        }

        $response['package'] = packages::data($package);
        
        return $response;
    }

    public function activate(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if(!$package_data['active']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            if($package_data['installed']){
                $install = $installer->activate($package);
                $response['stat'] = $install;
            }else{
                $install = $installer->install($package);
                $response['stat'] = $install;
            }
        }else{
            $response[] = 'Package activated already';
        }
        
        $response['package'] = packages::data($package);

        return $response;
    }

    public function deactivate(Request $request){
        $package = $request->package;

        $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

        $installer =  new $className;

        $install = $installer->deactivate($package);

        $response['stat'] = $install;
        $response['package'] = packages::data($package);

        return $response;
    }

    public function delete(){
        //return packages::modify('version_code', ['modules/Kms','modules/Insurances','modules/Consumables'],[0,1,2]);

        
    }
}
