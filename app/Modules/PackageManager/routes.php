<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'packages', 'namespace' => 'App\Modules\packagemanager\Controllers'], function()
{
    Route::get('/', function () {
        return redirect('packages/installed');
    });

    Route::get('/add', function () {
        return 'Under Development';
    });
    
    Route::get('/installed', 'PackageManager@installed_index');    
    Route::post('/install', 'PackageManager@install');    
    Route::post('/uninstall', 'PackageManager@uninstall');    
    Route::post('/activate', 'PackageManager@activate');    
    Route::post('/deactivate', 'PackageManager@deactivate');    
    Route::post('/delete', 'PackageManager@delete');
});