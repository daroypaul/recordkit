@extends('layout.miniside.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-9 align-self-center">
                        <h3 class="text-themecolor">Dashboard</h3>
                    </div>
                    <div class="col-md-3 text-right">
                        <p>Hello, {{ Auth::user()->user_fullname}}</p>
                    </div>

                    <!--<div class="col-md-12">
                        <a href="#" class="float-right"><small>Edit Snippet Widget</small></a>
                    </div>-->
                </div>

                <div class="row">
                    @if(Session::get('denied_access_notify'))
                        <div class="col-12">
                            <div class="alert alert-danger">Access Denied: You do not have the permission to view the page. Please contact your system administration.</div>
                        </div>
                    @endif
                </div>

                    <div class="row">
                        
                        @if(isset($widg_sm_count_asset))
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <!--div class="align-self-center box bg-info text-white"><i class="mdi mdi-barcode" style="font-size:1.5em;"></i></div>-->
                                            <div class="round align-self-center round-info"><i class="mdi mdi-barcode" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h3 class="m-b-0">{{isset($widg_sm_count_asset) ? $widg_sm_count_asset : 0}}</h3>
                                                <h5 class="text-muted m-b-0">Fix Assets</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isset($widg_sm_count_leasable))
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-success"><i class="mdi mdi-calendar-clock" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h3 class="m-b-0">{{isset($widg_sm_count_leasable) ? $widg_sm_count_leasable : 0}}</h3>
                                                <h5 class="text-muted m-b-0">Leasable Assets</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                        @if(isset($widg_sm_count_leased))
                            <div class="col-lg-3 col-md-6  col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-danger"><i class="mdi mdi-bookmark-check" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0">{{isset($widg_sm_count_leased) ? $widg_sm_count_leased : 0}}</h3>
                                                <h5 class="text-muted m-b-0">Leased Assets</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isset($widg_sm_count_asset_value))
                            <div class="col-lg-3 col-md-6  col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-primary"><i class="mdi mdi-coin" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h3 class="m-b-0">{{number_format($widg_sm_count_asset_value, 2)}} {{$system_options->currency->currency_code}}</h3>
                                                <h5 class="text-muted m-b-0">Total Asset Value</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <!-- Column -->
                        <!-- Column -->
                        @if(isset($widg_sm_count_article))
                            <div class="col-lg-3 col-md-6  col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-warning"><i class="mdi mdi-library-books" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                    <h3 class="m-b-0">{{isset($widg_sm_count_article) ? $widg_sm_count_article : 0}}</h3>
                                                <h5 class="text-muted m-b-0">Articles</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isset($widg_sm_count_consumables))
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-body" style="padding:.9em;">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-info"><i class="mdi mdi-recycle" style="font-size:1.5em;"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h3 class="m-b-0">{{$widg_sm_count_consumables}}</h3>
                                                <h5 class="text-muted m-b-0">Consumable Assets</h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <!--@if(request()->getHttpHost()=='uatinout.worldvision.org.ph'||request()->getHttpHost()=='inout.worldvision.org.ph')-->

                        <!--@else-->

                        <!--@endif-->
                        <!-- Column -->
                    </div>

                    <hr style="margin-top:0px;"/>


                    <!---Widget Table-->
                    <div class="row">
                        
                        <!--<div class="col-md-12 m-b-10">
                            <a href="#" class="float-right"><small>Edit Widget</small></a>
                        </div>-->

                        @if(isset($widg_bg_overdue_leased))
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <div class="row">
                                                <div class="col-sm-8 col-md- 8 col-lg-9"><span class="lstick bg-danger"></span>Overdue Leased Asset</div>
                                                <div class="col-sm-4 col-md-4 col-lg-3" style="font-size:14px;">
                                                    <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                                        <select class="b-0 select-site" style="width:100% !important;" data-target="overdue-leased">
                                                            <option value="all">All Sites</option>
                                                        </select>
                                                    </form>
                                                </div>
                                            </div>
                                        </h4>


                                        <div class="table-responsive">
                                            <table class="table m-b-0 font-14" id="overdue-table" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Asset&nbsp;Tag</th>
                                                    <th>Lease&nbsp;Start</th>
                                                    <th>Lease&nbsp;End</th>
                                                    <th>Leased&nbsp;By</th>
                                                    <th>Site</th>
                                                    <th>Location</th>
                                                    <th>Overdue</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($widg_bg_overdue_leased as $leased_item)
                                                <tr>
                                                    <td>{{$leased_item->asset_tag}}</td>
                                                    <td>{{date('d-M-Y', strtotime($leased_item->lease_start))}}</td>
                                                    <td>{{date('d-M-Y', strtotime($leased_item->lease_expire))}}</td>
                                                    <td>{{$leased_item->emp_fullname}}</td>
                                                    <td>{{$leased_item->site_name}}</td>
                                                    <td>{{$leased_item->location_name}}</td>
                                                    <td>
                                                    <?php 
                                                                    
                                                        $todate = new DateTime(date('Y-m-d'));
                                                        $expire = new DateTime($leased_item->lease_expire);
                                                        $overdue = $expire->diff($todate)->format("%a");
                                                        $days = ($overdue<2) ? $overdue . ' Day' : $overdue . ' Days';
                                                            
                                                        echo '<label class="badge badge-danger">'.$days.'</label>';
                                                    ?>
                                                    </td>
                                                    <td><a href="{{url('/asset/return-lease/'.$leased_item->asset_id.'/new')}}" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isset($widg_bg_consumables))
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <div class="row">
                                                <div class="col-sm-8 col-md- 8 col-lg-9"><span class="lstick bg-danger"></span>Out-Of-Stock Consumables</div>
                                                <div class="col-sm-4 col-md-4 col-lg-3" style="font-size:14px;">
                                                    <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                                                        <select class="b-0 select-site" style="width:100% !important;" data-target="consumables">
                                                            <option value="all">All Sites</option>
                                                        </select>
                                                    </form>
                                                </div>
                                            </div>
                                        </h4>


                                        <div class="table-responsive">
                                            <table class="table m-b-0 font-14" id="consumable-table" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Model</th>
                                                    <th>Purchased&nbsp;Date</th>
                                                    <th>Cost</th>
                                                    <th>Stocks</th>
                                                    <th>Category</th>
                                                    <th>Site</th>
                                                    <th width="110">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($widg_bg_consumables as $consumable)
                                                <tr data-id="{{$consumable->item_id}}">
                                                    <td>{{$consumable->item_name}}</td>
                                                    <td>{{$consumable->item_model}}</td>
                                                    <td>{{($consumable->date_purchased) ? date('d-M-Y', strtotime($consumable->date_purchased)) : '---'}}</td>
                                                    <td>{{($consumable->purchased_cost) ? number_format($consumable->purchased_cost, 2) : '---'}}</td>
                                                    <td {!!($consumable->qty_remain<$consumable->qty_alert) ? 'class="text-danger text-bold" data-toggle="tooltip" title="Insufficient Stock(s)"':'class="text-default text-bold"'!!}>{{number_format($consumable->qty_remain)}}</td>
                                                    <td>{{$consumable->category->cat_name}}</td>
                                                    <td>{{$consumable->site->site_name}}</td>
                                                    <td width="100">
                                                        <!--<button type="button" class="btn btn-sm btn-inverse" data-action="check-out" data-toggle="tooltip" title="Check-Out"><i class="mdi mdi-logout"></i></button>-->
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                        <!--<button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete Record"><i class="ti-trash"></i></button>-->
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(isset($widg_bg_featured_articles))
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <div class="row">
                                                <div class="col-lg-12"><span class="lstick bg-success"></span>Featured Articles</div>
                                            </div>
                                        </h4>

                                        <div class="table-responsive">
                                            <table class="table m-b-0 font-14" id="featured-articles-table" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Author</th>
                                                    <th>Categories</th>
                                                    <th>Tags</th>
                                                    <th title="Comments"><i class="font-bold ti-comments"></i></th>
                                                    <th title="Likes"><i class="font-bold ti-thumb-up"></i></th>
                                                    <th title="Views"><i class="font-bold ti-eye"></i></th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($widg_bg_featured_articles as $article)
                                                <tr>
                                                    <td>
                                                        <a href="{{url('/kb/article/'.$article->post_id)}}" style="color: #67757c;">{{$article->post_title}}</a> 
                                                        {!!($article->post_stat=='draft')?'	
                                                        <b> —</b> <span class="badge badge-info">Draft</span>':''!!}
                                                    </td>
                                                    <td>{!!($article->recorder) ? $article->recorder->user_fullname:'---';!!}</td>
                                                    <td>{!!($article->category) ? $article->category->cat_name : '<code>Undefined</code>'!!}</td>
                                                    <td><small>
                                                        <?php
                                                            $a = '';
                                                            foreach($widg_bg_tags as $tag){
                                                                if($article->post_id==$tag->post_id){
                                                                    $a .= $tag->tag_name . ', '; 
                                                                }
                                                            }

                                                            echo ($a) ? rtrim($a,", ") : '---';
                                                        ?></small>
                                                    </td>
                                                    <td>
                                                        @foreach($widg_bg_comments_likes as $comments)
                                                            @if($comments['post_id']==$article->post_id)
                                                                {{$comments['comments']}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                            @foreach($widg_bg_comments_likes as $likes)
                                                            @if($likes['post_id']==$article->post_id)
                                                                {{$likes['likes']}}
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                    <td>{{$article->total_view}}</td>
                                                    <td>
                                                        <a type="button" class="btn btn-xs btn-inverse" data-action="edit" href="{{url('/kb/article/'.$article->post_id.'/edit')}}">Read More</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <!--<div class="col-lg-5">
                            <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"><span class="lstick"></span>Asset by Category</h4>

                                        @if(isset($widgets_asset_by_category))
                                        <div id="chart-category" style="height:200px; width:100%;"></div>
                                        <table class="table vm font-14 m-b-0">
                                            
                                            @foreach($widgets_asset_by_category as $category)
                                                <tr>
                                                    <td>{{$category->cat_name}}</td>
                                                    <td class="text-right font-medium">{{$category->total}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                        @else
                                        <div class="alert alert-info">No Asset Record</div>
                                        @endif
                                    </div>
                                </div>
                        </div>-->
                    </div>

@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/widget-page.css')}}" rel="stylesheet">
    
@endsection

@section('js')
    
    <script src="{{ url('assets/js/prsO3I0FGPepUqoFyTBb.js') }}"></script>
    <script src="{{ url('assets/js/DHZYmfRoxMFUKpySWln6.js') }}"></script>
@endsection

@section('module_title')
Dashboard
@endsection