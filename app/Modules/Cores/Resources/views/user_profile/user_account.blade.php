@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">User</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                
                
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body p-b-0">
                                <center class="m-t-30"> 
                                    <img src="
                                    {{url('img/user-icon.png')}}" class="img-thumbnail" width="100" id="current-avatar" />
                                    <!--<img src="{{($profile->user_avatar) ? $profile->user_avatar : custom_asset('img/avatars/avatar-1.png')}}" class="img-circle img-thumbnail" width="150" id="current-avatar" />
                                    <br/><button class="btn btn-xs btn-warning m-t-10 m-b-20" id="btn-change-avatar">Change Avatar</button>-->
                                    <h3 class="card-title m-t-10 m-b-0">{{ Auth::user()->user_fullname}}</h3>
                                </center>
                            </div>
                            <div>
                                <hr> 
                            </div>
                            <div class="card-body"> 
                                <small class="text-muted">Email Address </small>
                                <h6 id="disp-email">{{($profile->user_email)?$profile->user_email:'---'}}</h6>
                                <small class="text-muted p-t-10 db">Site</small>
                                <h6>{{($profile->site)?$profile->site->site_name:'---'}}</h6>
                                <small class="text-muted p-t-10 db">User Group</small>
                                <h6>{{($profile->group)?$profile->group->group_name:'---'}}</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user" title="Profile"></i></span> <span class="hidden-xs-down">Profile</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#security" role="tab"><span class="hidden-sm-up"><i class="ti-lock" title="Security"></i></span> <span class="hidden-xs-down">Security</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" id="form-profile">
                                            <!--<div class="form-group">
                                                <label class="col-md-12">Dispaly Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="e.g Juan Dela Cruz" class="form-control form-control-line">
                                                </div>
                                            </div>-->
                                            <div class="form-group">
                                                <label for="example-email" class="control-label text-bold col-md-12">Email <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="e.g juandelacruz@example.com" class="form-control form-control-line" name="email" id="example-email" value="{{($profile->user_email)?$profile->user_email:''}}">
                                                </div>
                                            </div>
                                            <div class="form-group m-b-10">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="security" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" id="form-security">
                                            <div class="form-group m-t-10">
                                                <label class="control-label text-bold col-md-12">Current Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control form-control-line" name="current_password">
                                                </div>
                                            </div>                        
                                            <div class="form-group">
                                                <label class="control-label text-bold col-md-12">New Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control form-control-line" name="password">
                                                </div>
                                            </div>                        
                                            <div class="form-group">
                                                <label class="control-label text-bold col-md-12">Confirm New Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                <div class="col-md-12">
                                                    <input type="password" class="form-control form-control-line" name="password_confirmation">
                                                </div>
                                            </div>
                                                
                                            <div class="form-group col-md-12">
                                                <label class="control-label">Password Requirements:</label>
                                                <ul class="list-icons">
                                                    <li><i class="fa fa-check text-danger"></i> Must be a minimum of 8 characters</li>
                                                    <li><i class="fa fa-check text-danger"></i> Must contain at least one number</li>
                                                    <li><i class="fa fa-check text-danger"></i> Must contain at least one uppercase character</li>
                                                    <li><i class="fa fa-check text-danger"></i> Must contain at least one lowercase character</li>
                                                </ul>
                                            </div>
                                            
                                            <div class="form-group m-b-10">
                                                    <div class="control-label text-bold col-sm-12">
                                                        <button class="btn btn-success">Change Password</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <input type="hidden" id="_id" value="{{isset($asset_id) ? $asset_id : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
    <link href="{{ custom_asset('css/template/default/css/pages/tab-page.css')}}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/file-upload.css') }}" rel="stylesheet">
    <link href="{{ custom_asset('css/template/default/css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ custom_asset('js/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ custom_asset('js/template/default/jasny-bootstrap.js') }}"></script>
    <script src="{{ custom_asset('js/user.account.js') }}"></script>
@endsection