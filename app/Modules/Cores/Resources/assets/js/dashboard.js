/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){
    var origin = $apps_base_url;
    var proto = window.location.hostname;
    
    //Store all selected asset
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';
    //Parameter to check previous process result result: [0] No process done/process reset [1] process done, updated in Status modal form at modal.js
    $status_updated = 0;


    if($('table#overdue-table').length!=0){
        $overdue_table = $('#overdue-table').DataTable({
            "scrollX": true,
            "order": [[ 2, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 6,7 ] }
            ],
        });
    }

    if($('table#consumable-table').length!=0){
        $consumable_table = $('#consumable-table').DataTable({
            "scrollX": true,
            "aoColumnDefs": [
                { 'bSortable': false, aTargets: [ -1 ] }
            ],
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='check-out'&&id){
                $.module_consumable_checkout(id);
            }
            else if(action=='edit'&&id){
                $.edit_action(id, 'edit','table','single', $consumable_table, 'from-dashboard');
            }
            else if(action=='delete'&&id){
                //delete_action(id,'single');
            }
        })
    }


    if($('table#featured-articles-table').length!=0){
        $article_table = $('#featured-articles-table').DataTable({
            "scrollX": true,
            //"order": [[ 2, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, aTargets: [ -1 ] }
            ],
        });
    }

    $('.select-site').select2({
        placeholder : 'Select Site',
        ajax: {
            url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    with: '_all',
                    keywords: params.term,
                }

                return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.site_name,
                            id: item.site_id
                        }
                    })
                };
            }
        }
    }).on('select2:select', function (e) {
        var site_id = $('select#select-site').val();
        var target = $(this).data('target');
        
        if(target=='overdue-leased'){
            $('#form-loader .loader__label').text('Fetching...');
            $('#form-loader').toggle();
            
            $.ajax({
                type:'POST',
                url : $apps_base_url+'/dashboard/widget/fetch/data',
                dataType: "json",
                data:  {
                    type : 'overdue-leased-asset',
                    site_id : site_id,
                },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();
                    
                    $overdue_table.clear().draw();

                    if(data.list.length){
                            $.each(data.list, function(i, v){
                                var today = moment(); //YYYY-MM-DD
                                var expire = moment(v.lease_expire);
                                var overdue = today.diff(expire, 'days');
                                
                                if(overdue<=1){
                                    var overdue_label = '<label class="badge badge-danger">'+overdue+' Day</label>';
                                }else{
                                    var overdue_label = '<label class="badge badge-danger">'+overdue+' Days</label>';
                                }


                                var row = $overdue_table.row.add( {
                                    0: v.asset_tag,
                                    1: moment(v.lease_start).format("DD-MMM-YYYY"),
                                    2: moment(v.lease_expire).format("DD-MMM-YYYY"),
                                    3: v.emp_fullname,
                                    4: (v.site_name) ? v.site_name : '---',
                                    5: (v.location_name) ? v.location_name : '---',
                                    6: overdue_label,
                                    7: '<a href="'+$apps_base_url+'/asset/return-lease/'+v.asset_id+'/new" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a>',
                                } ).draw();

                                $overdue_table.rows(row).nodes().to$().attr('');//"data-id", v.id
                            });
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                }
            });
        }else if(target=='consumables'){
            var site_id = $(this).val();
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Fetching data...');
            $(".tooltip").tooltip("hide");
    
            $.ajax({
                type:'POST',
                url :  $apps_base_url+'/consumables/fetchdata',
                dataType: "json",
                data: {
                    'site':site_id,
                    //'ref':''
                },
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $(".tooltip").tooltip("hide");
                    $('#form-loader').toggle();
                    
                    $consumable_table.clear().draw();
                    if(data.length!=0){
                        $.each(data, function(i, item) {
                            var row = $consumable_table.row.add( {
                                0: item.name,
                                1: item.model,
                                2: item.date_purchased,
                                3: item.cost,
                                4: item.qty,
                                5: item.category,
                                6: item.site,
                                7: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"><i class="ti-pencil"></i></button>',
                            } ).draw();

                            if(item.qty<item.qty_limit){
                                $consumable_table.rows(row).nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"data-original-title":"Insufficient Stock(s)"}).addClass('text-danger text-bold');
                            }else{
                                $consumable_table.rows(row).nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"data-original-title":""}).addClass('text-default text-bold');
                            }
    
                            $consumable_table.rows(row).nodes().to$().attr({"data-id":item.id});
                            
                            bootbox.hideAll();
    
                            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                        });
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        }
    });

    /*$('select#select-site').select2().on('select2:select', function (e) {
        ////$('#asset_location').val(null).trigger('change');
        var site_id = $('select#select-site').val();
        
        $('#form-loader .loader__label').text('Fetching...');
        $('#form-loader').toggle();
        
        $.ajax({
            type:'POST',
            url : $apps_base_url+'/dashboard/widget/fetch/data',
            dataType: "json",
            data:  {
                type : 'overdue-leased-asset',
                site_id : site_id,
            },
            //processData: false,
            //contentType: false,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                if(data.list.length){
                    $overdue_table.clear().draw();
                        $.each(data.list, function(i, v){
                            var today = moment(); //YYYY-MM-DD
                            var expire = moment(v.lease_expire);
                            var overdue = today.diff(expire, 'days');
                            
                            if(overdue<=1){
                                var overdue_label = '<label class="badge badge-danger">'+overdue+' Day</label>';
                            }else{
                                var overdue_label = '<label class="badge badge-danger">'+overdue+' Days</label>';
                            }


                            var row = $overdue_table.row.add( {
                                0: v.asset_tag,
                                1: moment(v.lease_start).format("DD-MMM-YYYY"),
                                2: moment(v.lease_expire).format("DD-MMM-YYYY"),
                                3: v.emp_fullname,
                                4: (v.site_name) ? v.site_name : '---',
                                5: (v.location_name) ? v.location_name : '---',
                                6: overdue_label,
                                7: '<a href="'+$apps_base_url+'/asset/return-lease/'+v.asset_id+'/new" class="btn btn-inverse btn-sm"><i class="mdi mdi-login-variant"></i> Return</a>',
                            } ).draw();

                            $overdue_table.rows(row).nodes().to$().attr('');//"data-id", v.id
                        });
                }else{
                    $overdue_table.clear().draw();
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
            }
        });
    });*/
    /*$('select#select-site').select2({
        placeholder : 'Select a Site',
        ajax: {
            url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
            dataType: 'json',
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: function (params) {
                var query = {
                    ref: '_keywords',
                    keywords: params.term,
                }
                return query;
              },
            processResults: function (data) {
                var i =0;
                return {
                    results: $.map(data.list, function (item) {
                        return {
                            text: item.site_name,
                            id: item.site_id
                        };
                    })
                };
            }
          }
    }).on('select2:select', function (e) {
        //$('#asset_location').val(null).trigger('change');
    });*/
});