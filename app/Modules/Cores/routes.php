<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\modules\cores\controllers'], function()
{
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', [ 'as' => 'dashboard', 'uses' => 'DashboardController@index']);
        /*Route::get('/', function() {
            //return redirect('dashboard');

            dd(Route::getRoutes());
        });*/

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/', [ 'as' => 'dashboard', 'uses' => 'DashboardController@index']);
            Route::post('widget/fetch/data', 'DashboardController@widgets_feth_data');
        });

        Route::group(['prefix' => 'fetch'], function () {
            Route::POST('options', 'SystemOption@fetch_data');
        });

        //User Profile
        Route::group(['prefix' => 'user'], function () {
            Route::get('profile', 'UserAccount@index');
            Route::post('profile', 'UserAccount@store');
        });
    });
});