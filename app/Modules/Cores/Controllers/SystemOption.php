<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Modules\Cores\Controllers;

use Illuminate\Http\Request;
use App\Modules\Cores\Controllers\api\Options as OptionResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use Options;

class SystemOption extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $account_id = Auth::user()->account_id;
        $options = Options::where('account_id', $account_id);

        if(isset($request->name)){
            $options = $options->where('option_name', $request->name);
        }

        if(isset($request->order_by)){
            $order_by = ($request->order_by=='name') ? 'option_name':'option_value';
            $options = $options->orderBy($order_by);
        }

        $options = $options->get();

        return OptionResource::collection($options);
    }

    public function fetch_data(Request $request)
    {
            //
            $account_id = Auth::user()->account_id;
            $options = Options::where('account_id', $account_id);

            if(isset($request->name)){
                $options = $options->where('option_name', $request->name);
            }

            if(isset($request->order_by)){
                $order_by = ($request->order_by=='name') ? 'option_name':'option_value';
                $options = $options->orderBy($order_by);
            }

            $options = $options->get();

            return OptionResource::collection($options);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
