<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Modules\Cores\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\modules\settings\models\CompanyInformation;
use App\modules\adminprofile\models\Users;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class UserAccount extends Controller
{
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::id();
        $profile = Users::with(['site','group'])->find($user_id);

        $data = [
            "profile"=>$profile,
            "form_title"=> "Profile Settings",
        ];

        //return $asset_tag;
        return view('cores::user_profile.user_account')->with($data);
        //$hashids = new Hashids();

        //$a = $hashids->encode($account_id);

        //$serial =  shell_exec('wmic DISKDRIVE GET SerialNumber 2>&1');
        
    }


    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        if($request->type=='profile'){
            $rules = [
                'email'=>"required|email|min:5|max:100",
            ];

            if(isset($request->fullname)){
                $rules['fullname'] = "required|min:1|max:100|regex:/^[a-zA-Z ']+$/u";
            }
        }else if($request->type=='security'){
            $rules = [
                'current_password'=>"required",
                'password'=>"required|min:8|max:18|confirmed|regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/",
                'password_confirmation'=>"sometimes|required_with:password",
            ];
        }else if($request->type=='change_avatar'){
            $rules = [
                'avatar'=>"required",
            ];
        }

       $messages = [
            'fullname.regex' => 'The :attribute format is invalid',
        ];

        $validator = Validator::make($request->all(), $rules);#Run the validation
        $response = [];

        if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->type=='profile'){
                $update = Users::find($user_id);
                $update->user_email = $request->email;
                $update->updated_by = $user_id;

                if(isset($request->fullname)){
                    $update->user_fullname = $request->fullname;
                }

                $update->save();

                if($update){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Your profile info has been successfully updated';
    
                    //Add Logs
                    //$desc = 'updated the profile info';
                    //Helpers::add_activity_logs(['update-profile-info','',$desc]);
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Error occured during process.';
                    $response['a'] = $request->type;
                }
            }else if($request->type=='security'){
                $user_info = Users::find($user_id);

                if($user_info){
                    $hashed_password = $user_info->user_pass;

                    if (Hash::check($request->current_password, $hashed_password)) {
                        $new_hash_password = Hash::make ($request->password);

                        if($request->current_password!=$request->password){
                            $update = Users::find($user_id);
                            $update->user_pass = $new_hash_password;
                            $update->updated_by = $user_id;
                            $update->save();

                            if($update){
                                $response['stat'] = 'success';
                                $response['stat_title'] = 'Success';
                                $response['stat_msg'] = 'Password Reset Complete';
                            }else{
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Unable to reset the password. Error occured during process.';
                            }
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = "You've already used this password. Try a different one";
                        }
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Invalid current password.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to reset the password. Error occured during process.';
                }
            }else if($request->type=='change_avatar'){
                $update = Users::find($user_id);
                $update->user_avatar = $request->avatar;
                $update->updated_by = $user_id;
                $update->save();

                if($update){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'Avatar has been successfully updated';
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to change avatar. Error occured during process.';
                }
            }else{
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to update. Error occured during process.';
            }
        }

        return $response;
        
    }
}
