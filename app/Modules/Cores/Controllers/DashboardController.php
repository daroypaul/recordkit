<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Modules\Cores\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;
use Route;
use Packages;
use Schema;

class DashboardController extends Controller
{
    //
    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        $account_id = Auth::user()->account_id;


        $timezone = Options::apps('timezone');

        $date = date('Y-m-d');
        $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
        $todate = date('Y-m-d', strtotime($date . $interval));

        /*
        | Dashboard Widgets |
        */

        $packages = Packages::data();
        $data = [];

        //$ats = $packages[''];
        if( class_exists('\App\packages\assettracking\models\Assets') && isset($packages['assettracking']) && $packages['assettracking']['active'] && Schema::hasTable('sys_asset') ){
            ## Small Widget ##

            $widg_sm_fix_asset_count = 0;
            $widg_sm_leasable_asset_count = 0;
            $widg_sm_leased_asset_count = 0;
            $widg_sm_total_asset_value = 0;

            $widg_sm_fix_asset_count = \App\packages\assettracking\models\Assets::where('account_id', $account_id)->where('record_stat', 'active')->count();
            $widg_sm_leasable_asset_count = \App\packages\assettracking\models\Assets::where('account_id', $account_id)->where('record_stat', 'active')->where('is_leasable', 1)->count();

            $widg_sm_leased_asset_count = DB::table('sys_asset')
                                        ->leftJoin('sys_asset_lease_items', 'sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id')
                                        ->leftJoin('sys_asset_lease_logs', function($join) use ($account_id){
                                            $join->on('sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                                                ->where('sys_asset_lease_logs.account_id', '=', $account_id)
                                                ->where('sys_asset_lease_items.item_stat', '=', 0);
                                        })
                                        ->select('sys_asset.*')
                                        ->where('sys_asset.account_id', $account_id)
                                        ->where('sys_asset.is_leasable', 1)
                                        ->where('sys_asset_lease_items.item_stat', 0)
                                        ->count();

            $widg_sm_total_asset_value = \App\packages\assettracking\models\Assets::where('account_id', $account_id)->where('record_stat', 'active')->sum('purchased_cost');

            $data['widg_sm_count_asset'] =  $widg_sm_fix_asset_count;
            $data['widg_sm_count_leasable'] =  $widg_sm_leasable_asset_count;
            $data['widg_sm_count_leased'] =  $widg_sm_leased_asset_count;
            $data['widg_sm_count_asset_value'] =  $widg_sm_total_asset_value;

            ## Big Widget ##
            $widg_bg_overdue_leased_asset = DB::table('sys_asset')
                            ->Join('sys_asset_lease_items', 'sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id')
                            ->Join('sys_asset_lease_logs', function($join) use ($account_id){
                                $join->on('sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                                    ->where('sys_asset_lease_logs.account_id', '=', $account_id)
                                    ->where('sys_asset_lease_items.item_stat', '=', 0);
                            })
                            ->Join('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
                            ->Join('sys_site', 'sys_site.site_id', '=', 'sys_asset_lease_logs.site_id')
                            ->Join('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset_lease_logs.location_id')
                            ->select('sys_asset.asset_id','sys_asset.asset_tag','sys_asset.asset_model','sys_asset.asset_desc','sys_asset_lease_logs.lease_start','sys_asset_lease_logs.lease_expire','sys_employees.emp_fullname','sys_site.site_name','sys_site_location.location_name')
                            ->where('sys_asset.account_id', $account_id)
                            ->where('sys_asset.is_leasable', 1)
                            ->where('sys_asset_lease_logs.lease_expire', '<', $todate)
                            ->where('sys_asset_lease_items.item_stat', 0)
                            ->orderBy('sys_asset_lease_logs.lease_expire', 'asc')
                            ->get();



            $widg_sm_asset_by_status = DB::table('sys_asset')
                                            ->select('sys_asset.asset_status', DB::raw('count(*) as total'))
                                            ->where('record_stat', 'active')
                                            ->groupBy('asset_status')
                                            ->get();
            
            $widg_sm_asset_by_category = DB::table('sys_asset')
                                            ->select('sys_categories.cat_name', DB::raw('count(*) as total'))
                                            ->join('sys_categories', 'sys_categories.cat_id', '=', 'sys_asset.cat_id')
                                            ->where('sys_asset.record_stat', 'active')
                                            ->groupBy('sys_asset.cat_id')
                                            ->get();
            
            $data['widg_bg_overdue_leased'] =  $widg_bg_overdue_leased_asset;
        }


        if( class_exists('\App\Packages\Knowledgebase\Models\Posts') && isset($packages['knowledgebase']) && $packages['knowledgebase']['active'] && Schema::hasTable('sys_km_posts') ){

            ## Small Widgets ##
            $widg_sm_km_articles = 0;

            $widg_sm_km_articles = \App\Packages\Knowledgebase\Models\Posts::where('account_id',  $account_id)->where('post_stat', 'published')->count();

            $data['widg_sm_count_article'] =  $widg_sm_km_articles;

            ## Big Widget ##
            $widg_bg_featured_articles = \App\Packages\Knowledgebase\Models\Posts::with(['recorder','category'])->where('account_id',  $account_id)->where('post_stat', 'published')->where('is_featured', 1)->orderBy('date_recorded', 'desc')->limit(5)->get();

            $total_likes = [];
            $total_comments = [];
            $tags = [];
            $comments = [];
            $comment_like = [];
    
           // $post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->where('post_stat', '<>', 'delete')->get();
    
            if($widg_bg_featured_articles){
                $tags = DB::table('sys_km_post_tags')
                            ->join('sys_km_tags', 'sys_km_tags.tag_id', '=', 'sys_km_post_tags.tag_id')
                            ->select('sys_km_post_tags.id', 'sys_km_post_tags.post_id', 'sys_km_tags.tag_id', 'sys_km_tags.tag_name')
                            ->where('sys_km_tags.account_id', $account_id)
                            ->orderBy('sys_km_tags.tag_name', 'ASC')
                            ->get();
                
    
                foreach($widg_bg_featured_articles as $a){
                    $b = [];
                    $post_likes = \App\Packages\Knowledgebase\Models\PostLikes::where('account_id',  $account_id)->where('post_id',  $a->post_id)->count();   
    
                    $post_comments = \App\Packages\Knowledgebase\Models\PostComments::where('account_id',  $account_id)->where('post_id',  $a->post_id)->count();   
    
                    $b['post_id'] = $a->post_id;
                    $b['likes'] = $post_likes;
                    $b['comments'] = $post_comments;
    
                    $comment_like[] = $b;
                }       
            }


            $data['widg_bg_featured_articles'] =  $widg_bg_featured_articles;
            $data['widg_bg_tags'] =  $tags;
            $data['widg_bg_comments_likes'] =  $comment_like;
        }

        if( class_exists('\App\Packages\consumables\Models\Consumable') && isset($packages['consumables']) && $packages['consumables']['active'] && Schema::hasTable('sys_consumable') ){

            ## Small Widgets ##
            $widg_sm_count_consumables = 0;

            $q_sm_count_consumables = \App\Packages\consumables\Models\Consumable::where('account_id', $account_id)->where('record_stat', 0)->count();

            $data['widg_sm_count_consumables'] =  $q_sm_count_consumables;
            
            
            ## Big Widget ##
            $q_consumables = \App\Packages\consumables\Models\Consumable::with(['category','site'])->where('account_id', $account_id)->whereRaw('qty_remain <= qty_alert')->where('record_stat', 0)->get();


            $data['widg_bg_consumables'] =  $q_consumables;
        }

        $a = Schema::hasTable('sys_asset');

        
        $a = Route::getRoutes();

        $code = Helpers::generate_string(20);
        
        return view('cores::dashboard')->with($data);
    }

    public function widgets_feth_data(Request $request){

        $data_type = $request->type;
        $account_id = Auth::user()->account_id;
        $response = [];

        $timezone = Options::apps('timezone');

        if($data_type=='overdue-leased-asset'){
            $site = $request->site_id;

            $date = date('Y-m-d');
            $interval = $timezone->timezone_hours.' hours +'.$timezone->timezone_minutes.' minutes';
            $todate = date('Y-m-d', strtotime($date . $interval));

            $widg_sm_overdue_leased_asset = DB::table('sys_asset')
            ->join('sys_asset_lease_items', 'sys_asset_lease_items.asset_id', '=', 'sys_asset.asset_id')
            ->join('sys_asset_lease_logs', function($join) use ($account_id){
                $join->on('sys_asset_lease_logs.lease_id', '=', 'sys_asset_lease_items.lease_id')
                    ->where('sys_asset_lease_logs.account_id', '=', $account_id)
                    ->where('sys_asset_lease_items.item_stat', '=', 0);
            })
            ->Join('sys_employees', 'sys_employees.employee_id', '=', 'sys_asset_lease_logs.requestor_id')
            ->Join('sys_site', 'sys_site.site_id', '=', 'sys_asset_lease_logs.site_id')
            ->Join('sys_site_location', 'sys_site_location.location_id', '=', 'sys_asset_lease_logs.location_id')
            ->select('sys_asset.asset_id','sys_asset.asset_tag','sys_asset.asset_model','sys_asset.asset_desc','sys_asset_lease_logs.lease_start','sys_asset_lease_logs.lease_expire','sys_employees.emp_fullname','sys_site.site_name','sys_site_location.location_name')
            ->where('sys_asset.account_id', $account_id)
            ->where('sys_asset.is_leasable', 1)
            ->where('sys_asset_lease_logs.lease_expire', '<', $todate)
            ->where('sys_asset_lease_items.item_stat', 0);
            //->get();


            if($site!='all'){
                $widg_sm_overdue_leased_asset = $widg_sm_overdue_leased_asset->where('sys_asset_lease_logs.site_id', $site);
            }

            $widg_sm_overdue_leased_asset = $widg_sm_overdue_leased_asset->orderBy('sys_asset_lease_logs.lease_expire', 'asc')->get();

            $response['list'] = $widg_sm_overdue_leased_asset;
            $response['site'] = $site;
        }

        return $response;
    }
}
