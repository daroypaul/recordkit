<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'admin', 'namespace' => 'App\Modules\adminprofile\Controllers'], function()
{
    Route::get('/', function(){
        return redirect('dashboard');
    });
    ## Category
    Route::get('categories', 'Category@index');
    Route::post('categories', 'Category@store');
    //Route::get('article-categories', 'Category@km_index');
    //Route::post('article-categories', 'Category@store');

    Route::post('categories/fetchdata', 'Category@fetch_data');
    Route::post('categories/delete', 'Category@delete');
    ## Site
    Route::get('site', 'Sites@index');
    Route::post('site', 'Sites@store');
    Route::post('site/fetchdata', 'Sites@fetch_data');
    Route::post('site/delete', 'Sites@delete');
    ## location
    Route::get('location', 'Locations@index');
    Route::post('location', 'Locations@store');
    Route::post('location/fetchdata', 'Locations@fetch_data');
    Route::post('location/delete', 'Locations@delete');
    ## Employee
    Route::get('employees', 'Employee@index');
    Route::post('employees', 'Employee@store');
    Route::post('employees/fetchdata', 'Employee@fetch_data');
    Route::post('employees/delete', 'Employee@delete');
    Route::post('employees/s', 'Employee@change_status');
    ## Departments
    Route::get('departments', 'Department@index');
    Route::post('departments', 'Department@store');
    Route::post('departments/fetchdata', 'Department@fetch_data');
    Route::post('departments/delete', 'Department@delete');
    ## Departments
    Route::get('suppliers', 'Supplier@index');
    Route::post('suppliers', 'Supplier@store');
    Route::post('suppliers/fetchdata', 'Supplier@fetch_data');
    Route::post('suppliers/delete', 'Supplier@delete');
    ## User Group
    Route::get('user-groups', 'UserGroup@index');
    Route::get('user-groups/add', 'UserGroup@add');
    Route::post('user-groups/add', 'UserGroup@store');
    Route::get('user-groups/{id}/edit', 'UserGroup@edit');
    Route::post('user-groups/{id}/edit', 'UserGroup@store');
    Route::post('user-groups/fetchdata', 'UserGroup@fetch_data');
    Route::post('user-groups/delete', 'UserGroup@delete');
    ## Users
    Route::get('users', 'User@index');
    Route::post('users', 'User@store');
    Route::post('users/fetchdata', 'User@fetch_data');
    Route::post('users/delete', 'User@delete');
    Route::post('users/resetpass', 'User@reset_password');
    Route::post('users/change-stat', 'User@change_status');
});