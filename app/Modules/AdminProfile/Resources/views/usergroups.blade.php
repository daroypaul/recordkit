@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::get('error_msg'))
                                    <div class="alert alert-danger">
                                        {{Session::get('error_msg')}} 
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                    </div>
                                @endif

                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Name</th>
                                                <th>Total Users</th>
                                                <th width="40%">Remarks</th>
                                                <th width="60">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($record_list as $record)
                                                <tr data-id="{{$record->user_group_id}}" data-remarks-content="{{$record->group_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$record->user_group_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$record->user_group_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$record->group_name}}</td>
                                                    <td>{{count($record->users)}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($record->group_remarks||strlen($record->group_remarks) > 30) ? substr($record->group_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td width="60">
                                                        <a href="{{url('admin/user-groups/'.$record->user_group_id)}}/edit" type="button" class="btn btn-sm btn-inverse" data-action="edit">Edit</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')
<link href="{{ custom_asset('css/template/default/css/pages/ribbon-page.css')}}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ url('assets/js/h7qRmx5fC60jIN2VHi4z.js') }}"></script>
@endsection