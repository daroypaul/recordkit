@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th width="40%">Remarks</th>
                                                <!--
                                                <th>Updated By</th>-->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($category_list as $category)
                                                <tr data-id="{{$category->cat_id}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$category->cat_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$category->cat_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$category->cat_name}}</td>
                                                    <td>{{ucwords($category->apps_type)}}</td>
                                                    <td width="40%">{!!($category->cat_remarks) ? $category->cat_remarks:'---';!!}</td>
                                                    <!--
                                                    <td data-toggle="tooltip" title="{{ date('d-M-Y@h:iA', strtotime($category->date_updated.'+8 hours'))}}">{{$category->updator->user_fullname}}</td>-->
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->

                <input type="hidden" id="_apps" value="{{isset($apps) ? $apps : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    <script src="{{ url('assets/js/4g1eu7zsdRXbWNV8oiTF.js') }}"></script>
@endsection