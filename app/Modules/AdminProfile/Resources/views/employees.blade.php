@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 p-l-0 p-r-0">
                                        <select class="form-control custom-select" tabindex="1" id="select-stat">
                                            <option value="activated">Active Records</option>
                                            <option value="deactivated">Inactive Records</option>
                                        </select> 
                                    </div>
                                <hr/>
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Employee&nbsp;ID</th>
                                                <th width="80%">Name</th>
                                                <th>Gender</th>
                                                <th>Email</th>
                                                <th>Contact</th>
                                                <th>Address</th>
                                                <th>Site</th>
                                                <th>Department</th>
                                                <th>Remarks</th>
                                                <!--
                                                <th>Updated By</th>-->
                                                <th width="100">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employee_list as $employee)
                                                <tr data-id="{{$employee->employee_id}}" data-address-content="{{$employee->emp_address}}" data-remarks-content="{{$employee->emp_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$employee->employee_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$employee->employee_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{($employee->emp_code) ? $employee->emp_code :'---'}}</td>
                                                    <td>{{$employee->emp_fullname}}</td>
                                                    <td>{{ucwords($employee->emp_gender)}}</td>
                                                    <td>{{($employee->emp_email) ? $employee->emp_email :'---'}}</td>
                                                    <td>{{($employee->emp_contact) ? $employee->emp_contact :'---'}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-address">
                                                        {{ ($employee->emp_address||strlen($employee->emp_address) > 30) ? substr($employee->emp_address,0,30)."..." : '---'}}
                                                    </td>
                                                    <td>{{($employee->site) ? $employee->site->site_name :'---'}}</td>
                                                    <td>{{($employee->department) ? $employee->department->dept_name :'---'}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($employee->emp_remarks||strlen($employee->emp_remarks) > 30) ? substr($employee->emp_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td width="60">
                                                        <!--<button type="button" class="btn btn-sm btn-inverse" data-action="edit">Edit</button>-->

                                                        <button type="button" class="btn btn-sm btn-default" data-action="disable" data-toggle="tooltip" title="Disable Employee"><i class="mdi mdi-account-off"></i></button>
                                                        
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    
    <script src="{{ url('assets/js/U2DkbrpYxBy5ORgVfSsc.js') }}"></script>
@endsection