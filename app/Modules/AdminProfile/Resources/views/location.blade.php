@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 p-l-0 p-r-0">
                                        <select class="form-control custom-select" tabindex="1" id="select_site">
                                            <option value="">[ Select Site ]</option>
                                            @foreach($site_list as $site)
                                                    @if($site->site_id == $default_site)
                                                        <option value="{{$site->site_id}}" selected>{{$site->site_name}}</option>
                                                    @else
                                                        <option value="{{$site->site_id}}">{{$site->site_name}}</option>
                                                    @endif
                                            @endforeach
                                        </select> 
                                    </div>
                                <hr/>
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Name</th>
                                                <th width="40%">Remarks</th>
                                                <th width="60">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($location_list as $location)
                                                <tr data-id="{{$location->location_id}}" data-remarks-content="{{$location->location_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$location->location_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$location->location_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$location->location_name}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($location->location_remarks||strlen($location->location_remarks) > 30) ? substr($location->location_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td width="60">
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    
    <script src="{{ url('assets/js/g5yh2pFMBPHrvZL67Ai8.js') }}"></script>
@endsection