@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 p-l-0 p-r-0">
                                        <select class="form-control custom-select" tabindex="1" id="select-stat">
                                            <option value="activated">Active User</option>
                                            <option value="deactivated">Inactive User</option>
                                        </select> 
                                    </div>
                                <hr/>
                                <div class="record-table">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Full&nbsp;Name</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Remarks</th>
                                                <th>User&nbsp;Group</th>
                                                <th>Site</th>
                                                <th width="120">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($record_list as $record)
                                                <tr data-id="{{$record->user_id}}" data-remarks-content="{{$record->user_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$record->user_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$record->user_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$record->user_fullname}}</td>
                                                    <td>{{$record->user_login}}</td>
                                                    <td>{{($record->user_email) ? $record->user_email :'---'}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($record->user_remarks||strlen($record->user_remarks) > 30) ? substr($record->user_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td>{!!($record->group) ? $record->group->group_name :'<code>Unassigned</code>'!!}</td>
                                                    <td>{!!($record->site) ? $record->site->site_name :'---'!!}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-default" data-action="reset" data-toggle="tooltip" title="Reset Password"><i class="mdi mdi-key"></i></button>
                                                        
                                                        <button type="button" class="btn btn-sm btn-default" data-action="disable" data-toggle="tooltip" title="Disable User"><i class="mdi mdi-account-off"></i></button>

                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>

                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    
    <script src="{{ url('assets/js/EoSf0K2C8MswNJ9Wnmg4.js') }}"></script>
@endsection