@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsived"  id="slimtest1">
                                    <table id="record-table" class="table table-striped table-condensed" width="100%">
                                        <thead>
                                            <tr>
                                                <th  width="50">
                                                    <center>
                                                    <input type="checkbox" id="select-all" class="filled-in" value="1" />
                                                        <label for="select-all" class="m-b-0"></label>
                                                        </center>
                                                </th>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th width="40%">Remarks</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($record_list as $record)
                                                <tr data-id="{{$record->supplier_id}}" data-address-content="{{$record->supplier_address}}" data-remarks-content="{{$record->supplier_remarks}}">
                                                    <td width="50">
                                                        <center>
                                                        <input type="checkbox" id="basic_checkbox_{{$record->supplier_id}}" class="filled-in selected-item" value="1" />
                                                        <label for="basic_checkbox_{{$record->supplier_id}}" class="m-b-0"></label>
                                                        </center>
                                                    </td>
                                                    <td>{{$record->supplier_name}}</td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-address">
                                                        {{ ($record->supplier_address||strlen($record->supplier_address) > 30) ? substr($record->supplier_address,0,30)."..." : '---'}}
                                                    </td>
                                                    <td data-toggle="tooltip" title="View full content" class="pointer" data-action="view-remarks">
                                                        {{ ($record->supplier_remarks||strlen($record->supplier_remarks) > 30) ? substr($record->supplier_remarks,0,30)."..." : '---'}}
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--/card-->
                    </div><!--/col-12-->
                </div><!--/row-->
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    <script src="{{ url('assets/js/LfGMJYdnhNj3r9lm04kE.js') }}"></script>
@endsection