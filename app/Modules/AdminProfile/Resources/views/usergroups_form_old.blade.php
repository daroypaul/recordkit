@extends('layout.'.Config::get('constant.default_layout').'.index')

@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">{{$form_title}}</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Admin</li>
                            <li class="breadcrumb-item active">{{$form_title}}</li>
                        </ol>
                    </div>
                    <!--<div>
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm float-right m-l-10"><i class="ti-settings text-white"></i></button>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <form action="#" class="form-material" method="post" id="form" enctype="multipart/form-data">
                        <div class="card">
                            <div class="card-header">
                                <p class="m-t-0 m-b-0 float-right">Note: <i class="mdi mdi-check-circle text-danger"></i> Required fields</p>
                            </div>
                            <div class="card-body p-b-0">
                                
                                 {{csrf_field()}}
                                    <div class="form-body">
                                        <div class="row">
                                            <!--<div class="col-md-12 m-b-30">
                                                <h3 class="card-title m-b-0">Asset Details </h3>
                                                <div id="form-notification"></div>
                                                <hr class="m-b-0" style="border-top: 2px solid rgba(0,0,0,.1) !important;" />
                                            </div>-->
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Group Name" name="name" value="{{ isset($record->group_name) ? $record->group_name : '' }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group m-b-0">
                                                    <label class="form-control-label text-bold">Remarks</label>
                                                    <textarea class="form-control" placeholder="User Group Remarks" name="remarks">{{ isset($record) ? $record->group_remarks : ''}}</textarea>
                                                </div>
                                            </div>
                                       
                                        </div>
                                    </div>
                                
                            </div><!--/card-body-->
                            <div class="card-body p-b-0">
                            <ul class="nav nav-tabs customtab text-bold" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#asset" role="tab"><span class="hidden-sm-up"><i class="ti-info-alt"></i></span> <span class="hidden-xs-down">Assets</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reports" role="tab"><span class="hidden-sm-up"><i class="mdi mdi-chart-line"></i></span> <span class="hidden-xs-down">Reports</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profiles" role="tab"><span class="hidden-sm-up"><i class="ti-list-ol"></i></span> <span class="hidden-xs-down">System Profiles</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#module" role="tab"><span class="hidden-sm-up"><i class="mdi mdi-view-module"></i></span> <span class="hidden-xs-down">Module</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tools" role="tab"><span class="hidden-sm-up"><i class="mdi mdi-wrench"></i></span> <span class="hidden-xs-down">Tools</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab"><span class="hidden-sm-up"><i class=" ti-settings "></i></span> <span class="hidden-xs-down">Settings</span></a> </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane p-20 active" id="asset" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="normal-custom-table">
                                            <tr>
                                                <td width="40%" class="text-right text-bold">Asset Record</td>
                                                <td>
                                                    <input id="pkg_fixasset_view" class="filled-in" name="permissions[]" value="pkg_fixasset_view" type="checkbox" {{isset($pkg_fixasset_view) ? $pkg_fixasset_view: ''}}><label for="pkg_fixasset_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="pkg_fixasset_add" class="filled-in" name="permissions[]" value="pkg_fixasset_add" type="checkbox" {{isset($pkg_fixasset_add) ? $pkg_fixasset_add: ''}}><label for="pkg_fixasset_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="pkg_fixasset_edit" class="filled-in" name="permissions[]" value="pkg_fixasset_edit" type="checkbox" {{isset($pkg_fixasset_edit) ? $pkg_fixasset_edit: ''}}><label for="pkg_fixasset_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="pkg_fixasset_delete" class="filled-in" name="permissions[]" value="pkg_fixasset_delete" type="checkbox" {{isset($pkg_fixasset_delete) ? $pkg_fixasset_delete: ''}}><label for="pkg_fixasset_delete" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="text-right text-bold">Check IN/OUT</td>
                                                <td colspan="4">
                                                    <input id="pkg_fixasset_inout" class="filled-in" name="permissions[]" value="pkg_fixasset_inout" type="checkbox" {{isset($pkg_fixasset_inout) ? $pkg_fixasset_inout: ''}}><label for="pkg_fixasset_inout" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="text-right text-bold">Lease</td>
                                                <td colspan="4">
                                                    <input id="pkg_fixasset_lease" class="filled-in" name="permissions[]" value="pkg_fixasset_lease" type="checkbox" {{isset($pkg_fixasset_lease) ? $pkg_fixasset_lease: ''}}><label for="pkg_fixasset_lease" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane p-20" id="reports" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="text-right text-bold b-0" width="50%" style="">Asset Reports</td>
                                                <td class=" b-0">
                                                    <input id="pkg_fixasset_report_asset" class="filled-in" name="permissions[]" value="pkg_fixasset_report_asset" type="checkbox" {{isset($pkg_fixasset_report_asset) ? $pkg_fixasset_report_asset: ''}}><label for="pkg_fixasset_report_asset" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold">Check-Out Reports</td>
                                                <td>
                                                    <input id="pkg_fixasset_report_checkout" class="filled-in" name="permissions[]" value="pkg_fixasset_report_checkout" type="checkbox" {{isset($pkg_fixasset_report_checkout) ? $pkg_fixasset_report_checkout: ''}}><label for="pkg_fixasset_report_checkout" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold">Lease Reports</td>
                                                <td>
                                                    <input id="pkg_fixasset_report_lease" class="filled-in" name="permissions[]" value="pkg_fixasset_report_lease" type="checkbox" {{isset($pkg_fixasset_report_lease) ? $pkg_fixasset_report_lease: ''}}><label for="pkg_fixasset_report_lease" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold">Status Reports</td>
                                                <td>
                                                    <input id="pkg_fixasset_report_status" class="filled-in" name="permissions[]" value="pkg_fixasset_report_status" type="checkbox" {{isset($pkg_fixasset_report_status) ? $pkg_fixasset_report_status: ''}}><label for="pkg_fixasset_report_status" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="text-right text-bold">Activity Log Reports</td>
                                                <td>
                                                    <input id="report_activitylog" class="filled-in" name="permissions[]" value="report_activitylog" type="checkbox" {{isset($report_activitylog) ? $report_activitylog: ''}}><label for="report_activitylog" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="tab-pane p-20" id="profiles" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="normal-custom-tables table">
                                            <tr>
                                                <td width="30%" class="text-right text-bold b-0"></td>
                                                <td class="b-0">
                                                    <input id="check-all-admview" class="filled-in"  type="checkbox"><label for="check-all-admview" class="m-b-0">All</label>
                                                </td>
                                                <td class="b-0">
                                                    <input id="check-all-admadd" class="filled-in"  type="checkbox"><label for="check-all-admadd" class="m-b-0">All</label>
                                                </td>
                                                <td class="b-0">
                                                    <input id="check-all-admedit" class="filled-in"  type="checkbox"><label for="check-all-admedit" class="m-b-0">All</label>
                                                </td>
                                                <td class="b-0">
                                                    <input id="check-all-admdel" class="filled-in"  type="checkbox"><label for="check-all-admdel" class="m-b-0">All</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Users</td>
                                                <td>
                                                    <input id="admin_user_view" class="filled-in adm-view" name="permissions[]" value="admin_user_view" type="checkbox" {{isset($admin_user_view) ? $admin_user_view: ''}}><label for="admin_user_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_user_add" class="filled-in adm-add" name="permissions[]" value="admin_user_add" type="checkbox" {{isset($admin_user_add) ? $admin_user_add: ''}}><label for="admin_user_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_user_edit" class="filled-in adm-edit" name="permissions[]" value="admin_user_edit" type="checkbox" {{isset($admin_user_edit) ? $admin_user_edit: ''}}><label for="admin_user_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_user_del" class="filled-in adm-del" name="permissions[]" value="admin_user_del" type="checkbox" {{isset($admin_user_del) ? $admin_user_del: ''}}><label for="admin_user_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">User Groups</td>
                                                <td>
                                                    <input id="admin_usergroup_view" class="filled-in adm-view" name="permissions[]" value="admin_usergroup_view" type="checkbox" {{isset($admin_usergroup_view) ? $admin_usergroup_view: ''}} ><label for="admin_usergroup_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_usergroup_add" class="filled-in adm-add" name="permissions[]" value="admin_usergroup_add" type="checkbox" {{isset($admin_usergroup_add) ? $admin_usergroup_add: ''}}><label for="admin_usergroup_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_usergroup_edit" class="filled-in adm-edit" name="permissions[]" value="admin_usergroup_edit" type="checkbox" {{isset($admin_usergroup_edit) ? $admin_usergroup_edit: ''}}><label for="admin_usergroup_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_usergroup_del" class="filled-in adm-del" name="permissions[]" value="admin_usergroup_del" type="checkbox" {{isset($admin_usergroup_del) ? $admin_usergroup_del: ''}}><label for="admin_usergroup_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Sites</td>
                                                <td>
                                                    <input id="admin_site_view" class="filled-in adm-view" name="permissions[]" value="admin_site_view" type="checkbox" {{isset($admin_site_view) ? $admin_site_view: ''}}><label for="admin_site_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_site_add" class="filled-in adm-add" name="permissions[]" value="admin_site_add" type="checkbox" {{isset($admin_site_add) ? $admin_site_add: ''}}><label for="admin_site_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_site_edit" class="filled-in adm-edit" name="permissions[]" value="admin_site_edit" type="checkbox" {{isset($admin_site_edit) ? $admin_site_edit: ''}}><label for="admin_site_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_site_del" class="filled-in adm-del" name="permissions[]" value="admin_site_del" type="checkbox" {{isset($admin_site_del) ? $admin_site_del: ''}}><label for="admin_site_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Locations</td>
                                                <td>
                                                    <input id="admin_location_view" class="filled-in adm-view" name="permissions[]" value="admin_location_view" type="checkbox" {{isset($admin_location_view) ? $admin_location_view: ''}}><label for="admin_location_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_location_add" class="filled-in adm-add" name="permissions[]" value="admin_location_add" type="checkbox" {{isset($admin_location_add) ? $admin_location_add: ''}}><label for="admin_location_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_location_edit" class="filled-in adm-edit" name="permissions[]" value="admin_location_edit" type="checkbox" {{isset($admin_location_edit) ? $admin_location_edit: ''}}><label for="admin_location_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_location_del" class="filled-in adm-del" name="permissions[]" value="admin_location_del" type="checkbox" {{isset($admin_location_del) ? $admin_location_del: ''}}><label for="admin_location_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Categories</td>
                                                <td>
                                                    <input id="admin_cat_view" class="filled-in adm-view" name="permissions[]" value="admin_cat_view" type="checkbox" {{isset($admin_cat_view) ? $admin_cat_view: ''}}><label for="admin_cat_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_cat_add" class="filled-in adm-add" name="permissions[]" value="admin_cat_add" type="checkbox" {{isset($admin_cat_add) ? $admin_cat_add: ''}}><label for="admin_cat_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_cat_edit" class="filled-in adm-edit" name="permissions[]" value="admin_cat_edit" type="checkbox" {{isset($admin_cat_edit) ? $admin_cat_edit: ''}}><label for="admin_cat_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_cat_del" class="filled-in adm-del" name="permissions[]" value="admin_cat_del" type="checkbox" {{isset($admin_cat_del) ? $admin_cat_del: ''}}><label for="admin_cat_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Employees</td>
                                                <td>
                                                    <input id="admin_employee_view" class="filled-in adm-view" name="permissions[]" value="admin_employee_view" type="checkbox" {{isset($admin_employee_view) ? $admin_employee_view: ''}}><label for="admin_employee_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_employee_add" class="filled-in adm-add" name="permissions[]" value="admin_employee_add" type="checkbox" {{isset($admin_employee_add) ? $admin_employee_add: ''}}><label for="admin_employee_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_employee_edit" class="filled-in adm-edit" name="permissions[]" value="admin_employee_edit" type="checkbox" {{isset($admin_employee_edit) ? $admin_employee_edit: ''}}><label for="admin_employee_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_employee_del" class="filled-in adm-del" name="permissions[]" value="admin_employee_del" type="checkbox" {{isset($admin_employee_del) ? $admin_employee_del: ''}}><label for="admin_employee_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Departments</td>
                                                <td>
                                                    <input id="admin_dept_view" class="filled-in adm-view" name="permissions[]" value="admin_dept_view" type="checkbox" {{isset($admin_dept_view) ? $admin_dept_view: ''}}><label for="admin_dept_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_dept_add" class="filled-in adm-add" name="permissions[]" value="admin_dept_add" type="checkbox" {{isset($admin_dept_add) ? $admin_dept_add: ''}}><label for="admin_dept_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_dept_edit" class="filled-in adm-edit" name="permissions[]" value="admin_dept_edit" type="checkbox" {{isset($admin_dept_edit) ? $admin_dept_edit: ''}}><label for="admin_dept_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_dept_del" class="filled-in adm-del" name="permissions[]" value="admin_dept_del" type="checkbox" {{isset($admin_dept_del) ? $admin_dept_del: ''}}><label for="admin_dept_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Suppliers</td>
                                                <td>
                                                    <input id="admin_supplier_view" class="filled-in adm-view" name="permissions[]" value="admin_supplier_view" type="checkbox" {{isset($admin_supplier_view) ? $admin_supplier_view: ''}}><label for="admin_supplier_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_supplier_add" class="filled-in adm-add" name="permissions[]" value="admin_supplier_add" type="checkbox" {{isset($admin_supplier_add) ? $admin_supplier_add: ''}}><label for="admin_supplier_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_supplier_edit" class="filled-in adm-edit" name="permissions[]" value="admin_supplier_edit" type="checkbox" {{isset($admin_supplier_edit) ? $admin_supplier_edit: ''}}><label for="admin_supplier_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_supplier_del" class="filled-in adm-del" name="permissions[]" value="admin_supplier_del" type="checkbox" {{isset($admin_supplier_del) ? $admin_supplier_del: ''}}><label for="admin_supplier_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td colspan="5">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="30%" class="text-right text-bold">Knowledge-Base Categories</td>
                                                <td>
                                                    <input id="admin_km_article_view" class="filled-in adm-view" name="permissions[]" value="admin_km_article_view" type="checkbox" {{isset($admin_km_article_view) ? $admin_km_article_view: ''}}><label for="admin_km_article_view" class="m-b-0">View</label>
                                                </td>
                                                <td>
                                                    <input id="admin_km_article_add" class="filled-in adm-add" name="permissions[]" value="admin_km_article_add" type="checkbox" {{isset($admin_km_article_add) ? $admin_km_article_add: ''}}><label for="admin_km_article_add" class="m-b-0">Add</label>
                                                </td>
                                                <td>
                                                    <input id="admin_km_article_edit" class="filled-in adm-edit" name="permissions[]" value="admin_km_article_edit" type="checkbox" {{isset($admin_km_article_edit) ? $admin_km_article_edit: ''}}><label for="admin_km_article_edit" class="m-b-0">Edit</label>
                                                </td>
                                                <td>
                                                    <input id="admin_km_article_del" class="filled-in adm-del" name="permissions[]" value="admin_km_article_del" type="checkbox" {{isset($admin_km_article_del) ? $admin_km_article_del: ''}}><label for="admin_km_article_del" class="m-b-0">Delete</label>
                                                </td>
                                            </tr>-->
                                        </table>
                                    </div>
                                </div>
                                <!--/profile-->
                                <!--module-->
                                <div class="tab-pane p-20" id="module" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="text-right text-bold b-0" width="40%">Knowledge-Base</td>
                                                <td class="b-0" colspan="4">
                                                    <input id="pkg_km_view" class="filled-in" name="permissions[]" value="pkg_km_view" type="checkbox" {{isset($pkg_km_view) ? $pkg_km_view: ''}}><label for="pkg_km_view" class="m-b-0 m-r-10">View</label>
                                                    
                                                    <input id="pkg_km_search" class="filled-in" name="permissions[]" value="pkg_km_search" type="checkbox" {{isset($pkg_km_search) ? $pkg_km_search: ''}}><label for="pkg_km_search" class="m-b-0 m-r-10">Search&nbsp;Result</label>

                                                    <input id="pkg_km_list" class="filled-in" name="permissions[]" value="pkg_km_list" type="checkbox" {{isset($pkg_km_list) ? $pkg_km_list: ''}}><label for="pkg_km_list" class="m-b-0 m-r-10">Article List</label>

                                                    <input id="pkg_km_add" class="filled-in" name="permissions[]" value="pkg_km_add" type="checkbox" {{isset($pkg_km_add) ? $pkg_km_add: ''}}><label for="pkg_km_add" class="m-b-0 m-r-10">Article Add</label>
                                                    
                                                    <input id="pkg_km_edit" class="filled-in" name="permissions[]" value="pkg_km_edit" type="checkbox" {{isset($pkg_km_edit) ? $pkg_km_edit: ''}}><label for="pkg_km_edit" class="m-b-0 m-r-10">Article Edit</label>
                                                    
                                                    <input id="pkg_km_del" class="filled-in" name="permissions[]" value="pkg_km_del" type="checkbox" {{isset($pkg_km_del) ? $pkg_km_del: ''}}><label for="pkg_km_del" class="m-b-0 m-r-10">Article Delete</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold b-0" width="40%">Insurances</td>
                                                <!--<td class="b-0" colspan="4">
                                                    <input id="pkg_insurance_view" class="filled-in" name="permissions[]" value="pkg_insurance_view" type="checkbox" {{isset($pkg_insurance_view) ? $pkg_insurance_view: ''}}><label for="pkg_insurance_view" class="m-b-0 m-r-10">View</label>

                                                    <input id="pkg_insurance_add" class="filled-in" name="permissions[]" value="pkg_insurance_add" type="checkbox" {{isset($pkg_insurance_add) ? $pkg_insurance_add: ''}}><label for="pkg_insurance_add" class="m-b-0 m-r-10">Add</label>
                                                    
                                                    <input id="pkg_insurance_edit" class="filled-in" name="permissions[]" value="pkg_insurance_edit" type="checkbox" {{isset($pkg_insurance_edit) ? $pkg_insurance_edit: ''}}><label for="pkg_insurance_edit" class="m-b-0 m-r-10">Edit</label>
                                                    
                                                    <input id="pkg_insurance_del" class="filled-in" name="permissions[]" value="pkg_insurance_del" type="checkbox" {{isset($pkg_insurance_del) ? $pkg_insurance_del: ''}}><label for="pkg_insurance_del" class="m-b-0 m-r-10">Delete</label>
                                                </td>-->
                                                <td class="b-0" colspan="4">
                                                    <input id="pkg_insurance" class="filled-in" name="permissions[]" value="pkg_insurance" type="checkbox" {{isset($pkg_insurance) ? $pkg_insurance: ''}}><label for="pkg_insurance" class="m-b-0 m-r-10">Allow</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div><!--/module-->
                                <!--module-->
                                <div class="tab-pane p-20" id="tools" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="text-right text-bold b-0" width="40%">Tag Label Printing</td>
                                                <td class="b-0" colspan="4">
                                                    <input id="pkg_tag_printing" class="filled-in" name="permissions[]" value="pkg_tag_printing" type="checkbox" {{isset($pkg_tag_printing) ? $pkg_tag_printing: ''}}><label for="pkg_tag_printing" class="m-b-0 m-r-10">Allow</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div><!--/tools-->
                                <div class="tab-pane p-20" id="settings" role="tabpanel">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="text-right text-bold b-0" width="50%">Company Information</td>
                                                <td class="b-0">
                                                    <input id="setting_company_info" class="filled-in" name="permissions[]" value="setting_company_info" type="checkbox" {{isset($setting_company_info) ? $setting_company_info: ''}}><label for="setting_company_info" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold" width="50%">Asset Tag</td>
                                                <td>
                                                    <input id="pkg_fixasset_setting_assettag" class="filled-in" name="permissions[]" value="pkg_fixasset_setting_assettag" type="checkbox" {{isset($pkg_fixasset_setting_assettag) ? $pkg_fixasset_setting_assettag: ''}}><label for="pkg_fixasset_setting_assettag" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold" width="50%">Custom Data Fields</td>
                                                <td>
                                                    <input id="setting_datafields" class="filled-in" name="permissions[]" value="setting_datafields" type="checkbox" {{isset($setting_datafields) ? $setting_datafields: ''}}><label for="setting_datafields" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right text-bold" width="50%">System Settings</td>
                                                <td>
                                                    <input id="setting_system" class="filled-in" name="permissions[]" value="setting_system" type="checkbox" {{isset($setting_system) ? $setting_system: ''}}><label for="setting_system" class="m-b-0">Allow</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-body">
                                    <div class="form-actions float-right">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <a href="{{url('/admin/user-groups')}}" type="button" class="btn btn-inverse">Cancel</a>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <input type="hidden" id="_id" value="{{isset($record->user_group_id) ? $record->user_group_id : ''}}" />
                <input type="hidden" id="_type" value="{{isset($type) ? $type : ''}}" />
@endsection

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('module_title')
{{$form_title}}
@endsection

@section('style')

@endsection

@section('js')
    <script src="{{ url('assets/js/h7qRmx5fC60jIN2VHi4z.js') }}"></script>
@endsection