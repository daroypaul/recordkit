/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,3 ] }
            ],
            "dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<button type="button" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New Site" id="btn-add-site"><i class="fas fa-plus"></i></button>';
                var bulk_action =  
                                '<div class="form-group m-b-0"><div class="input-group">'+
                                    '<select class="form-control custom-select" id="bulk-action-option">'+
                                        '<option value="">- Bulk Action -</option>'+
                                        '<option value="Delete">Delete</option>'+
                                    '</select>'+            
                                    '<span class="input-group-btn">'+
                                    '<button id="btn-bulk-action" class="btn btn-default" type="button" tabindex="-1">Apply</button>'+
                                    '</span>'+
                                '</div></div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);
            }
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');

                var form_data = new FormData();
                form_data.append('id', id); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/site/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            $.admin_site_form(data.list.site_id,'edit','Update Site','main', data.list.site_name, data.list.site_remarks);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or site might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }
        }).on('click', 'td[data-action]', function(e) {
            var action = $(this).data('action');
            var name = $(this).parents('tr').find('td:nth-child(2)').text();
            var title= '';

            if(action=='view-remarks'){
                title= name+ ": Remarks";
                var content = $(this).parents('tr').data('remarks-content');
            }

            if(content){
                modal_view_content(title,content);
            }else{
                $.notification('','Empty Data','top-right','info','');
            }
        });

        $('input#select-all').click(function(){
            var c = this.checked;
    
            $('input.selected-item').each(function(){
                var id = $(this).parents('tr').data('id');
    
                $(this).prop('checked', c);
    
                if(c){
                    if(id!=0||id){
                        $selected_asset.push(id);
                    }
                }else{
                    $selected_asset = $.grep($selected_asset, function (value) {
                        return value != id;
                    });
                }
            });
        });


        $('#btn-bulk-action').click(function(){
            var opt = $('#bulk-action-option').val();
            $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
            $('#bulk-action-option').removeClass('form-control-danger');
    
            if($selected_asset.length==0){
                $.notification('Error','Select a site','top-right','error','');
            }else if(opt==''){
                $('#bulk-action-option').parents('.form-group').addClass('has-danger');
                $('#bulk-action-option').addClass('form-control-danger');
                $.notification('Error','Select an action option','top-right','error','');
            }else if(opt=='Delete'){
                if($selected_asset.length!=0){
                    
                    var url = $apps_base_url+'/admin/site/delete';
                    var settings = {
                        title : "Deleting Record(s)",
                        text:"Your about to delete "+$selected_asset.length+" site record(s). Are you want to continue?",
                        confirmButtonText:"Delete",
                        showCancelButton:true,
                        type : 'warning',
                        confirmButtonColor: "#DD6B55",
                        //closeOnConfirm: false
                    }
                    
                    swal(settings).then(result => {
                        var form_data = new FormData();
                        $.each($selected_asset, function(i, v){
                            form_data.append('id[]', v);
                        });
                        swal.close();
                        
                        if(result.value){
                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                data: form_data,
                                processData: false,
                                contentType: false,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $eid = [];
                                    $sid = [];
                                    $('.table-danger').removeClass('table-danger');
                                    $('input#select-all').prop('checked', false);
                                    $('input.selected-item:checked').prop('checked', false);
                                    $('#bulk-action-option').val('');
                                    $selected_asset = [];
                                    $status_updated = 1;
                                    
                                    if(data.stat){
                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }else{
                                        if(data.sid.length!=0){
                                            $.notification('Success','Selected site(s) has been successfully deleted.','top-right','success','');
        
                                            $.each(data.sid,function(i,v){
                                                $record_table.row('tr[data-id='+v+']').remove().draw();
                                            });
                                        }
        
                                        if(data.eid.length!=0){
                                            $.notification('Error Process','Selected site(s) can not be deleted. Invalid reference ID or site might already deleted. See highlighten row','top-right','error','');
                                            
                                            $.each(data.eid,function(i,v){
                                                $("tr[data-id='"+v+"']").addClass('table-danger');
                                            });
                                            
                                            $eid = data.eid;
                                        }
                                    }
                                },
                                error :function (data) {
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });
                        }
                    });
                }
            }else{
                if(opt!='Delete'&&opt!='Email'){
                    $status_updated = 0;
                    $.asset_status_modal($selected_asset, opt + ' Asset', opt);
                }
            }
        });
        

    $('#btn-add-site').click(function(){
        $.admin_site_form('','add','Add New Site','main');
    });
});


function modal_view_content(title='', content='') {//
    $layout_form = 	'<div class="row"><div class="col-12">'+content+'</div></div>';

    bootbox.dialog({
        closeButton: false,
        backdrop: true,
        animate: true,
        title: title,
        message: $layout_form,
        buttons: {
            close: {
                label: 'Close',
                className: "btn-inverse",
            }
        }
    })
}