/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    //$("#record-table tbody tr").popover({placement:"top"});

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 1, "asc" ]],
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ 0,7 ] }
            ],
            "dom": '<"row"<"col-xs-12 col-sm-2  col-md-2  col-lg-1"<"#action-toolbar">>'+
                    '<"col-sm-6 col-md-5 col-lg-3"<"#bulk-action-toolbar">>'+
                    '<"hidden-sm-down hidden-md-down col-lg-4">'+
                    '<"col-md-5 col-lg-4"f>>tip',
            fnInitComplete: function(){
                var add_element = '<button type="button" class="btn btn-success btn-block m-r-10 m-b-10" data-toggle="tooltip" title="Add New User" id="btn-add-record"><i class="fas fa-plus"></i></button>';
                var bulk_action =  
                                '<div class="form-group m-b-0"><div class="input-group">'+
                                    '<select class="form-control custom-select" id="bulk-action-option">'+
                                        '<option value="">- Bulk Action -</option>'+
                                        '<option value="deactivate">Disable</option>'+
                                        '<option value="delete">Delete</option>'+
                                    '</select>'+            
                                    '<span class="input-group-btn">'+
                                    '<button id="btn-bulk-action" class="btn btn-default" type="button" tabindex="-1">Apply</button>'+
                                    '</span>'+
                                '</div></div>';
                $('div#bulk-action-toolbar').html(bulk_action);
                $('div#action-toolbar').html(add_element);
            }
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');
                var form_data = new FormData();
                form_data.append('id', id);
                form_data.append('type', 'user'); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/users/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.length!=0){
                            $.admin_user_form(id,'edit','Update User','table', data.user_fullname,data.user_group_id,data.user_email,data.user_login,data.user_remarks, data.record_stat,(data.site_id)?data.site_id:'',data.site.site_name, data.group.group_name);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or user might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }else if(action=='reset'&&id){
                var name = $(this).parents('tr').find('td:nth-child(2)').text();
                $.admin_user_reset_password_form(id, 'reset-password', 'Reset Password: '+name);
            }else if((action=='disable'||action=='enable')&&id){
                $('#form-loader').toggle();
                
                var opt = (action=='disable') ? 'deactivate' : 'activate';

                if(opt=='deactivate'){
                    var msg_caption = 'deactivated';
                    $('#form-loader .loader__label').text('Deactivating user...');
                    
                }
                else{
                    var msg_caption = 'activated';
                    $('#form-loader .loader__label').text('Activating user...');
                }

                $.ajax({
                    type:'POST',
                    url : $apps_base_url+'/admin/users/change-stat',
                    dataType: "json",
                    data: {
                        'id':id,
                        'ref':opt,
                        'act_type':'single'
                    },
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $(".tooltip").tooltip("hide");
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $('#form-loader').toggle();
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            if(data.stat=='success'){
                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                            }
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                    }
                });
            }
        }).on('click', 'td[data-action]', function(e) {
            var action = $(this).data('action');
            var name = $(this).parents('tr').find('td:nth-child(2)').text();
            var title= '';

            if(action=='view-address'){
                title= name+ ": Address";
                var content = $(this).parents('tr').data('address-content');
            }else if(action=='view-remarks'){
                title= name+ ": Remarks";
                var content = $(this).parents('tr').data('remarks-content');
            }
            
            if(content){
                modal_view_content(title,content);
            }else{
                $.notification('','Empty Data','top-right','info','');
            }
        }).on('click', 'input.selected-item', function(e) {
            var c = this.checked;
            var id = $(this).parents('tr').data('id');
            var t = $('input.selected-item:checked').length;

            if(c){
                if(id!=0||id){
                    $selected_asset.push(id);
                }
            }else{
                $selected_asset = $.grep($selected_asset, function (value) {
                    return value != id;
                });
            }

            if(t!=0){
                $('input#select-all').prop('checked', true);
            }else{
                $('input#select-all').prop('checked', false);
            }
        }).on( 'draw', function () {
            var t = $('input.selected-item:checked').length;

            if(t==0){
                $('input#select-all').prop('checked', false);
            }else{
                $('input#select-all').prop('checked', true);
            }

            //This is to insure that all previously checked (All Table page) will be uncheck including select all after process trigger
            if($status_updated!=0&&$selected_asset.length==0){
                $('input#select-all').prop('checked', false);
                $('input.selected-item:checked').prop('checked', false);
            }

            if($eid.length!=0){
                if($('.table-danger').length==0){
                    $.each($eid,function(i,v){
                        $("tr[data-id='"+v+"']").addClass('table-danger');
                    });
                }
            }
        });

        $('input#select-all').click(function(){
            var c = this.checked;
    
            $('input.selected-item').each(function(){
                var id = $(this).parents('tr').data('id');
    
                $(this).prop('checked', c);
    
                if(c){
                    if(id!=0||id){
                        $selected_asset.push(id);
                    }
                }else{
                    $selected_asset = $.grep($selected_asset, function (value) {
                        return value != id;
                    });
                }
            });
        });


        $('#btn-bulk-action').click(function(){
            var opt = $('#bulk-action-option').val();
            $('#bulk-action-option').parents('.form-group').removeClass('has-danger');
            $('#bulk-action-option').removeClass('form-control-danger');
            
            if($selected_asset.length==0){
                $.notification('Error','Select an user','top-right','error','');
            }else if(opt==''){
                $('#bulk-action-option').parents('.form-group').addClass('has-danger');
                $('#bulk-action-option').addClass('form-control-danger');
                $.notification('Error','Select an action option','top-right','error','');
            }else if(opt=='deactivate'||opt=='activate'){
                $('#form-loader').toggle();
                var form_data = new FormData();
                $.each($selected_asset, function(i, v){
                    form_data.append('id[]', v);
                });
                form_data.append('ref', opt);
                form_data.append('act_type', 'multi');

                if(opt=='deactivate'){
                    var msg_caption = 'deactivated';
                    $('#form-loader .loader__label').text('Deactivating selected user(s)...');
                    
                }else{
                    var msg_caption = 'activated';
                    $('#form-loader .loader__label').text('Activating selected user(s)...');
                }


                $.ajax({
                    type:'POST',
                    url : $apps_base_url+'/admin/users/change-stat',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $eid = [];
                        $sid = [];
                        $('.table-danger').removeClass('table-danger');
                        $('input#select-all').prop('checked', false);
                        $('input.selected-item:checked').prop('checked', false);
                        $('#bulk-action-option').val('');
                        $('#form-loader').toggle();
                        $selected_asset = [];
                        $status_updated = 1;

                        if(data.stat){
                            $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                        }else{
                            if(data.sid.length!=0){
                                $.notification('Success','Selected user(s) has been '+msg_caption,'top-right','success','');

                                $.each(data.sid,function(i,v){
                                    $record_table.row('tr[data-id='+v+']').remove().draw();
                                });
                            }

                            if(data.eid.length!=0){
                                $.notification('Error Process','Selected user(s) can not be '+msg_caption+'. Possible reason: Invalid reference ID; User currently logged in; Might already deleted. See highlighten row','top-right','error','');
                                
                                $.each(data.eid,function(i,v){
                                    $("tr[data-id='"+v+"']").addClass('table-danger');
                                });
                                
                                $eid = data.eid;
                            }
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                    }
                });
            }
            else if(opt=='Delete'||opt=='delete'){
                if($selected_asset.length!=0){
                    var url = $apps_base_url+'/admin/users/delete';
                    var settings = {
                        title : "Deleting Record(s)",
                        text:"Your about to delete "+$selected_asset.length+" user record(s). Are you want to continue?",
                        confirmButtonText:"Delete",
                        showCancelButton:true,
                        type : 'warning',
                        confirmButtonColor: "#DD6B55",
                        //closeOnConfirm: false
                    }
                    
                    swal(settings).then(result => {
                        var form_data = new FormData();
                        $.each($selected_asset, function(i, v){
                            form_data.append('id[]', v);
                        });
                        swal.close();
                        
                        if(result.value){
                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                data: form_data,
                                processData: false,
                                contentType: false,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $eid = [];
                                    $sid = [];
                                    $('.table-danger').removeClass('table-danger');
                                    $('input#select-all').prop('checked', false);
                                    $('input.selected-item:checked').prop('checked', false);
                                    $('#bulk-action-option').val('');
                                    $selected_asset = [];
                                    $status_updated = 1;

                                    if(data.stat){
                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }else{
                                        if(data.sid.length!=0){
                                            $.notification('Success','Selected  user record(s) has been successfully deleted.','top-right','success','');
        
                                            $.each(data.sid,function(i,v){
                                                $record_table.row('tr[data-id='+v+']').remove().draw();
                                            });
                                        }
        
                                        if(data.eid.length!=0){
                                            $.notification('Error Process','Selected user record(s) can not be deleted. Possible reason: Invalid reference ID or might already deleted. See highlighten row','top-right','error','');
                                            
                                            $.each(data.eid,function(i,v){
                                                $("tr[data-id='"+v+"']").addClass('table-danger');
                                            });
                                            
                                            $eid = data.eid;
                                        }
                                    }
                                },
                                error :function (data) {
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });
                        }
                    });
                }
            }else{
                if(opt!='Delete'&&opt!='Email'){
                    $status_updated = 0;
                    $.asset_status_modal($selected_asset, opt + ' Asset', opt);
                }
            }
        });
        

        $('#select-stat').change(function(){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Fetching Data...');
            $record_table.clear().draw();

            var site = $(this).val();
            var form_data = new FormData();
            form_data.append('ref', 'all');
            form_data.append('type', 'user');

            var bulk_opt_list = '<option value="">- Bulk Action -</option>';

            if($(this).val()=='activated'){
                var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="disable" data-toggle="tooltip" title="Disable User"><i class="mdi mdi-account-off"></i></button>';
                form_data.append('stat', 0);
                bulk_opt_list += '<option value="deactivate">Disable</option>';
            }else if($(this).val()=='deactivated'){
                var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="enable" data-toggle="tooltip" title="Enable User"><i class="mdi mdi-account-check"></i></button>';
                form_data.append('stat', 1);
                bulk_opt_list += '<option value="activate">Enable</option>';
            }

            bulk_opt_list += '<option value="delete">Delete</option>';
            $('#bulk-action-option').html(bulk_opt_list);

            $eid = [];
            $sid = [];
            $('.table-danger').removeClass('table-danger');
            $('input#select-all').prop('checked', false);
            $('input.selected-item:checked').prop('checked', false);
            $('#bulk-action-option').val('');
            $selected_asset = [];

            $.ajax({
                type:'POST',
                url : $apps_base_url+'/admin/users/fetchdata',
                dataType: "json",
                data:  form_data,
                processData: false,
                contentType: false,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    $('#form-loader').toggle();

                    if(data.length!=0){
                        $.each(data, function(i, item) {
                            var address_full = item.address_full;
                            var row = $record_table.row.add( {
                                0: '<center>'+
                                        '<input type="checkbox" id="basic_checkbox_'+item.id+'" class="filled-in selected-item" value="1" />'+
                                        '<label for="basic_checkbox_'+item.id+'" class="m-b-0"></label>'+
                                    '</center>',
                                1: item.fullname,
                                2: item.username,
                                3: item.email,
                                4: item.remarks,
                                5: item.group,
                                6: item.site,
                                7: '<button type="button" class="btn btn-sm btn-default" data-action="reset" data-toggle="tooltip" title="Reset Password"><i class="mdi mdi-key"></i></button> '+add_btn+' <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                            } ).draw();

                            $record_table.nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                            
                            $record_table.rows(row).nodes().to$().attr({"data-id":item.id,"data-remarks-content":item.remarks_full});
                            
                        });
                    }
                },
                error :function (data) {
                    $('#form-loader').toggle();
                    $.notification('Error','Internal Server Error','top-right','error','');
                    $('#form-loader .loader__label').text('Fetching Data...');
                }
            });
        });

    $('#btn-add-record').click(function(){
        $.admin_user_form('','add','Add New User','table');
    });
});


function modal_view_content(title='', content='') {//
    $layout_form = 	'<div class="row"><div class="col-12">'+content+'</div></div>';

    bootbox.dialog({
        closeButton: false,
        backdrop: true,
        animate: true,
        title: title,
        message: $layout_form,
        buttons: {
            close: {
                label: 'Close',
                className: "btn-inverse",
            }
        }
    })
}