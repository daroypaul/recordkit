<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Users;
use App\modules\adminprofile\models\Employees;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\UserGroupPrivileges;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class Employee extends Controller
{

    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        $account_id = Auth::user()->account_id;
        $ref = $request->ref;
        $data = [];

        if($request->id){
            $id = $request->id;
            $record = Employees::with(['site', 'department'])->where('account_id', $account_id)->where('employee_id',$id)->where('record_stat','<>', 2)->first();
        }else if($request->ref=='_keywords'){
            $keywords = $request->keywords;

            
            $record = Employees::where('account_id', $account_id)
                            ->where('record_stat',0)
                            ->where(function ($query) use ($keywords) {
                                $query->where('emp_code','like','%'.$keywords.'%')
                                      ->orWhere('emp_fullname','like','%'.$keywords.'%')
                                      ->orWhere('emp_gender','like','%'.$keywords.'%')
                                      ->orWhere('emp_email','like','%'.$keywords.'%')
                                      ->orWhere('emp_address','like','%'.$keywords.'%')
                                      ->orWhere('emp_remarks','like','%'.$keywords.'%');
                            })
                            ->orderBy('emp_fullname','asc')
                            ->get();
        }else if($request->ref=='all'){
            $stat = $request->stat;
            $records = Employees::where('account_id', $account_id)->where('record_stat',$stat)->get();
            $a = [];
            $data = [];
            $i=0;
            foreach($records as $record){
                $a['id'] = $record->employee_id;
                $a['code'] = ($record->emp_code) ? $record->emp_code : '---';
                $a['fn'] = $record->emp_fullname;
                $a['site'] = ($record->site) ? $record->site->site_name : '---';
                $a['department'] = ($record->department) ? $record->department->dept_name : '---';
                $a['gender'] = ucfirst($record->emp_gender);
                $a['email'] = ($record->emp_email) ? $record->emp_email : '---';
                $a['contact'] = ($record->emp_contact) ? $record->emp_contact : '---';
                $a['address'] = ($record->emp_address||strlen($record->emp_address) > 30) ? substr($record->emp_address,0,30)."..." : '---';
                $a['address_full'] = $record->emp_address;
                $a['remarks'] = ($record->emp_remarks||strlen($record->emp_remarks) > 30) ? substr($record->emp_remarks,0,30)."..." : '---';
                $a['remarks_full'] = $record->emp_remarks;

                $data[$i] = $a;
                $i++;
            }

            return $data;
        }

        return $record;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_employee_');

        $read_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_read : 0;

        if($read_acc==0){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }*/
        if(!Helpers::get_user_privilege('admin_employee_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $employees = Employees::with(['recorder','updator', 'site', 'department'])
                            ->where('account_id', $account_id)
                            ->where('record_stat', 0)
                            ->orderBy('emp_fullname', 'asc')->get();

        $data = [
            "employee_list"=>$employees,
            "form_title"=> "Employees",
        ];
        
        //return $employees;
        //return UserGroupPrivileges::where('user_group_id', Auth::user()->user_group_id);
        return view('adminprofile::employees')->with($data);
    }


    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_employee_');
        $read_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_read : 0;
        $write_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_write : 0;
        $modify_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_modify : 0;*/

        $rules = [
            //'last_name'=>'required|min:2|max:40|regex:/(^[A-Za-z ]+$)+/',
            //'first_name'=>'required|min:2|max:40|regex:/(^[A-Za-z ]+$)+/',
            'name'=>'required|min:2|max:40|regex:/(^[A-Za-z ]+$)+/',
            'gender'=>'required',
        ];

        /*if($request->middle_name){
            $rules['middle_name'] = 'min:2|max:40|regex:/(^[A-Za-z ]+$)+/';
        }*/
        if($request->id){
            $rules['id'] = 'min:2|max:40|regex:/(^[A-Za-z0-9-]+$)+/';
        }

        if($request->site){
            $rules['site'] = 'required';
        }

        if($request->department){
            $rules['department'] = 'required';
        }

        if($request->email){
            $rules['email'] = 'email|max:50';
        }

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        if($request->address){
            $rules['address'] = 'min:2|max:500|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        if($request->contact){
            $rules['contact'] = 'regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/';
        }

        $messages = [
            //'last_name.regex' => 'The :attribute format is invalid. Letters only',
            //'first_name.regex' => 'The :attribute format is invalid. Letters only',
            //'middle_name.regex' => 'The :attribute format is invalid. Letters only',
            'name.regex' => 'The :attribute format is invalid. Letters only',
            'contact.regex' => 'The :attribute format is invalid.',
            'remarks.regex' => 'The :attribute format is invalid.',
            'address.regex' => 'The :attribute format is invalid.',
            'id.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];

        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_employee_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_employee_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->ref=='add'){
                $check_record = Employees::where('account_id', $account_id)
                                            //->where('emp_last_name', $request->last_name)
                                            //->where('emp_first_name', $request->first_name)
                                            //->where('emp_middle_name', $request->middle_name)
                                            ->where('emp_fullname', $request->name)
                                            ->where('record_stat','<>',2)
                                            ->count();

                if($check_record!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Record';
                    $response['stat_msg'] = 'Employee already exists.';
                }else{
                    $new_emp = new Employees;
                    $new_emp->account_id = $account_id;
                    //$new_emp->emp_last_name = ucwords($request->last_name);
                    //$new_emp->emp_first_name = ucwords($request->first_name);
                    //$new_emp->emp_middle_name = ucwords($request->middle_name);
                    $new_emp->site_id = ($request->site) ? $request->site : 0;
                    $new_emp->department_id = ($request->department) ? $request->department : 0;
                    $new_emp->emp_code = ($request->id) ? $request->id : '';
                    $new_emp->emp_fullname = ucwords($request->name);
                    $new_emp->emp_gender = $request->gender;
                    $new_emp->emp_address = ($request->address) ? $request->address : '';
                    $new_emp->emp_contact = ($request->contact) ? $request->contact : '';
                    $new_emp->emp_email = ($request->email) ? $request->email : '';
                    $new_emp->emp_remarks = ($request->remarks) ? $request->remarks : '';
                    $new_emp->record_stat = ($request->active_employee) ? 0 : 1;
                    $new_emp->recorded_by = $user_id;
                    $new_emp->updated_by = $user_id;
                    $new_emp->save();
                    $new_id = $new_emp->employee_id;

                    if($new_emp){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New employee record has been added';
                        
                        $response['id'] = $new_id;
                        $response['code'] = ($new_emp->emp_code) ? $new_emp->emp_code : '---';
                        $response['fn'] = $new_emp->emp_fullname;
                        $response['site'] = ($new_emp->site) ? $new_emp->site->site_name : '---';
                        $response['department'] = ($new_emp->department) ? $new_emp->department->dept_name : '---';
                        //$response['ln'] = $new_emp->emp_last_name;
                        //$response['fn'] = $new_emp->emp_first_name;
                        //$response['mn'] = ($new_emp->emp_middle_name) ? $new_emp->emp_middle_name : '---';
                        $response['gender'] = ucfirst($new_emp->emp_gender);
                        $response['email'] = ($new_emp->emp_email) ? $new_emp->emp_email : '---';
                        $response['contact'] = ($new_emp->emp_contact) ? $new_emp->emp_contact : '---';
                        $response['address'] = ($new_emp->emp_address||strlen($new_emp->emp_address) > 30) ? substr($new_emp->emp_address,0,30)."..." : '---';
                        $response['address_full'] = $new_emp->emp_address;
                        $response['remarks'] = ($new_emp->emp_remarks||strlen($new_emp->emp_remarks) > 30) ? substr($new_emp->emp_remarks,0,30)."..." : '---';
                        $response['remarks_full'] = $new_emp->emp_remarks;

                        //Add Logs
                        $desc = 'added an employee record ['.$new_emp->emp_fullname.']';
                        Helpers::add_activity_logs(['employee-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add employee. Error occured during process. Error Code: ErEMPA001';
                    }
                }
            }else if($request->ref=='edit'){
                $id = $request->eid;
                $check_record = Employees::where('account_id', $account_id)->where('employee_id', $id)->where('record_stat','<>',2)->count();

                if($check_record!=0){
                    $check1 = Employees::where('account_id', $account_id)
                                            ->where('employee_id', $id)
                                            ->where('emp_fullname', $request->name)
                                            ->count();
                    
                    $check2 = Employees::where('account_id', $account_id)
                                            ->where('emp_fullname', $request->name)
                                            ->count();
                    
                    if($check1!=0){
                        $response['a'] = 'update 1';
                        //$update = $this->query_update($id, $request);
                        //$response['update_stat'] = $update;
                        $update = static::update($id, $request);
                        $update_done = true;
                    }else{
                        if($check2!=0){
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Duplicate Record';
                            $response['stat_msg'] = 'Employee already exists.';
                            $update_done = false;
                        }else{
                            $response['a'] = 'update 2';
                            //$update = $this->query_update($id, $request);
                            //$response['update_stat'] = $update;
                            $update = static::update($id, $request);
                            $update_done = true;
                        }
                    }

                    if($update_done){
                        if($update){
                            $record = Employees::with(['recorder','updator', 'site', 'department'])->find($id);

                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Employee record has been successfully updated';
                            
                            $response['id'] = $record->employee_id;
                            $response['code'] = ($record->emp_code) ? $record->emp_code : '---';
                            $response['fn'] = $record->emp_fullname;
                            $response['site'] = ($record->site) ? $record->site->site_name : '---';
                            $response['department'] = ($record->department) ? $record->department->dept_name : '---';
                            $response['gender'] = ucfirst($record->emp_gender);
                            $response['email'] = ($record->emp_email) ? $record->emp_email : '---';
                            $response['contact'] = ($record->emp_contact) ? $record->emp_contact : '---';
                            $response['address'] = ($record->emp_address||strlen($record->emp_address) > 30) ? substr($record->emp_address,0,30)."..." : '---';
                            $response['address_full'] = $record->emp_address;
                            $response['remarks'] = ($record->emp_remarks||strlen($record->emp_remarks) > 30) ? substr($record->emp_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $record->emp_remarks;

                            //Add Logs
                            $desc = 'updated an employee record ['.$record->emp_fullname.']';
                            Helpers::add_activity_logs(['employee-edit',$record->employee_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update category. Error occured during process.';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or employee might be deleted. Error Code: ErEMPU002';
                }
            }
        }

        return $response;
    }


    /*function query_update($id, Request $request){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'site_id' => $request->site,
            'department_id' => $request->department,
            'emp_code' => ($request->id) ? $request->id : '',
            'emp_fullname' => ucwords($request->name),
            'emp_gender' => $request->gender,
            'emp_address' => ($request->address) ? $request->address : '',
            'emp_contact' => ($request->contact) ? $request->contact : '',
            'emp_email' => ($request->email) ? $request->email : '',
            'emp_remarks' => ($request->remarks) ? $request->remarks : '',
            'updated_by' => $user_id,
        ];

        $update = Employees::where('account_id', $account_id)->where('employee_id', $id)->update($data);

        if($update){
            $record = Employees::with(['recorder','updator', 'site', 'department'])->where('account_id', $account_id)->where('employee_id',$id)->first();

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'Employee record has been successfully updated';
            
            $response['id'] = $record->employee_id;
            $response['code'] = ($record->emp_code) ? $record->emp_code : '---';
            $response['fn'] = $record->emp_fullname;
            $response['site'] = ($record->site) ? $record->site->site_name : '---';
            $response['department'] = ($record->department) ? $record->department->dept_name : '---';
            $response['gender'] = ucfirst($record->emp_gender);
            $response['email'] = ($record->emp_email) ? $record->emp_email : '---';
            $response['contact'] = ($record->emp_contact) ? $record->emp_contact : '---';
            $response['address'] = ($record->emp_address||strlen($record->emp_address) > 30) ? substr($record->emp_address,0,30)."..." : '---';
            $response['address_full'] = $record->emp_address;
            $response['remarks'] = ($record->emp_remarks||strlen($record->emp_remarks) > 30) ? substr($record->emp_remarks,0,30)."..." : '---';
            $response['remarks_full'] = $record->emp_remarks;

            //Add Logs
            $desc = 'updated an employee record ['.$record->emp_fullname.']';
            Helpers::add_activity_logs(['employee-edit',$record->employee_id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update category. Error occured during process. Error Code: ErCATU001';
        }
        
        return $response;
    }*/

    public static function update($id, Request $request)
    {

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $update = Employees::find($id);
        $update->site_id = $request->site;
        $update->department_id = $request->department;
        $update->emp_code = ($request->id) ? $request->id : '';
        $update->emp_fullname = ucwords($request->name);
        $update->emp_gender = $request->gender;
        $update->emp_address = ($request->address) ? $request->address : '';
        $update->emp_contact = ($request->contact) ? $request->contact : '';
        $update->emp_email = ($request->email) ? $request->email : '';
        $update->emp_remarks = ($request->remarks) ? $request->remarks : '';
        $update->record_stat = ($request->active_employee) ? 0 : 1;
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }


    public function change_status(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_employee_');
        $modify_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_modify : 0;*/

        if(!Helpers::get_user_privilege('admin_employee_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $stat = ($request->ref=='deactivate') ? 1 : 0;

            if($request->act_type=='single'){
                
                $id = $request->id;

                $check_record = Employees::where('account_id',$account_id)->where('employee_id',$id)->where('record_stat','<>',2)->count();

                if($check_record){
                    $update = Employees::find($id);
                    $update->record_stat = $stat;
                    $update->updated_by = $user_id;
                    $update->save();

                    if($update){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Employee has been ' . $request->ref . 'd';
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to ' . $request->ref . ' employee. Error occured during process.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to ' . $request->ref . ' employee. Invalid reference ID or record might already deleted.';
                }
            }
            else if($request->act_type=='multi'){
                $x = [];
                $s = [];

                /*if($request->ref=='deactivate'){
                    $stat = 1;
                }else{
                    $stat = 0;
                }*/

                foreach($request->id as $id){
                    $check_record = Employees::where('account_id',$account_id)->where('employee_id',$id)->where('record_stat','<>',2)->count();

                    if($check_record!=0){
                        $update_employee = Employees::where('account_id', $account_id)->where('employee_id', $id)->update(['record_stat' => $stat]);

                        if($update_employee){
                            $s[] = $id;
                        }else{
                            $x[] = $id;
                        }
                        
                    }else{
                        $x[] = $id;
                    }
                }

                $response['eid'] = $x;
                $response['sid'] = $s;
            }
        }

        return $response;
    }

    public function delete(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_employee_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('admin_employee_edel')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = Employees::where('account_id',$account_id)->where('employee_id',$id)->count();
                if($check_record!=0){
                    $employee = Employees::find($id);
                    //$employee->date_deleted = Carbon::now('UTC')->toDateTimeString();
                    //$employee->deleted_by = $user_id;
                    $employee->record_stat = 2;
                    $employee->save();

                    if($employee){
                        $s[] = $id;

                        $_log_desc .= $employee->emp_fullname . ', ';
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted an employees record ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['employee-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
