<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\UserGroups;
use App\modules\adminprofile\models\UserGroupPrivileges;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class UserGroup extends Controller
{
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        //$keywords = $request->keywords;
        $account_id = Auth::user()->account_id;

        if($request->ref=='all'){
            $record = UserGroups::where('account_id', $account_id)->orderBy('group_name', 'asc')->get();
        }else if($request->ref=='_keywords'){
            $record = UserGroups::where('account_id', $account_id)->where('group_name','like','%'.$request->keywords.'%')->orderBy('group_name', 'asc')->get();
        }
        
        //return $asset_list;
        return $record;
    }
    
    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_usergroup_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_group_id = Auth::user()->user_group_id;

        $groups = UserGroups::with(['recorder','updator', 'users'])->where('account_id', $account_id)->where('is_system_built', 0)->orderBy('group_name', 'asc')->get();

        $data = [
            "record_list"=>$groups,
            "form_title"=> "User Groups",
        ];
        
        
        return view('adminprofile::usergroups')->with($data);
    }

    public function add(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_usergroup_add')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $data = [
            "form_title"=> "New User Groups",
            "type"=>"add",
        ];

        return view('adminprofile::usergroups_form')->with($data);
    }

    public function edit($id){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_usergroup_edit')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $record = UserGroups::where('account_id', $account_id)->where('user_group_id', $id)->where('is_system_built', 0)->first();

        if(count($record)==0){
            session()->flash('error_msg', 'Record not found. Invalid reference id or user group might already deleted.');
            return redirect('admin/user-groups');
        }
        
        $data = [
            "record"=> $record,
            "form_title"=> "Update User Group",
            "type"=>"edit",
        ];

        if($record->group_privileges){
            $privileges = json_decode($record->group_privileges);

            foreach($privileges as $privilege){
                $data[$privilege] = 'checked';
            }
        }
        
        return view('adminprofile::usergroups_form')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $o_priv = array('pkg_fixasset_view','pkg_fixasset_add','pkg_fixasset_edit','pkg_fixasset_delete','pkg_fixasset_inout','pkg_fixasset_lease','admin_user_view','admin_user_add','admin_user_edit','admin_user_del','admin_usergroup_view','admin_usergroup_add','admin_usergroup_edit','admin_usergroup_del','admin_site_view','admin_site_add','admin_site_edit','admin_site_del','admin_location_view','admin_location_add','admin_location_edit','admin_location_del','admin_cat_view','admin_cat_add','admin_cat_edit','admin_cat_del','admin_employee_view','admin_employee_add','admin_employee_edit','admin_employee_del','admin_dept_view','admin_dept_add','admin_dept_edit','admin_dept_del','admin_supplier_view','admin_supplier_add','admin_supplier_edit','admin_supplier_del','pkg_fixasset_report_asset','pkg_fixasset_report_checkout','pkg_fixasset_report_status','report_activitylog','pkg_fixasset_report_lease','pkg_fixasset_setting_assettag','setting_datafields','setting_system','setting_company_info','pkg_insurance', 'pkg_km_view', 'pkg_km_search', 'pkg_km_list', 'pkg_km_add', 'pkg_km_edit', 'pkg_km_del','pkg_tag_printing');

        $s_priv = [];//selected privilege
        $response = [];
        $i = 0;

        $rules = [
            'name'=>'required|min:2|max:100|regex:/(^[A-Za-z -]+$)+/',
        ];

        if($request->remarks){
            $rules['remarks'] = "min:2|max:750|regex:/(^[A-Za-z0-9 ,-._();']+$)+/";
        }

        $messages = [
            'name.regex' => 'The :attribute format is invalid.',
            'remarks.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $val_msg = $validator->errors();

        if(count($request->permissions)!=0){
            foreach($request->permissions as $privilege){
                //if(in_array($privilege, $o_priv)){
                    $s_priv[] = $privilege;
                    $i++;
                //}
            }
        }

        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_usergroup_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_usergroup_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{
            /*if($i==0){
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Select atlease one permission';
            }*/
            if(!count($request->permissions)){
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Select at lease one permission';
            }
            else{
                if($request->ref=='add'){
                    $check_record = UserGroups::where('account_id', $account_id)->where('group_name', $request->name)->count();

                    if($check_record!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'User group already exists.';
                    }else{
                        $new_record = new UserGroups;
                        $new_record->account_id = $account_id;
                        $new_record->group_name = ucwords($request->name);
                        $new_record->group_privileges = json_encode($s_priv);
                        $new_record->group_remarks = $request->remarks;
                        $new_record->recorded_by = $user_id;
                        $new_record->updated_by = $user_id;
                        $new_record->is_system_built = 0;
                        $new_record->save();

                        if($new_record){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'New user group has been added';

                            //Add Logs
                            //$desc = 'created a user group ['.$new_record->group_name.']';
                            //Helpers::add_activity_logs(['user-group-add',$new_record->user_group_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to add user group. Error occured during process. Error Code: ErGRPA001';
                        }
                        //$response['a'] = json_encode($s_priv);
                    }
                }else if($request->ref=='edit'){
                    $group_id = $request->id;
                    $check_record = UserGroups::where('account_id', $account_id)->where('user_group_id', $group_id)->count();

                    if($check_record==0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update. Invalid reference ID or user group might already deleted. Error Code: ErGRPU001';
                    }else{
                        $check1 = UserGroups::where('account_id', $account_id)->where('user_group_id', $group_id)->where('group_name', $request->name)->count();
                        //If name is updated
                        $check2 = UserGroups::where('account_id', $account_id)->where('group_name', $request->name)->count();
                        $i = 0;

                        if($check1!=0){
                            $update = $this->query_update($group_id, $request, $s_priv);
                            $i++;
                            $response['a'] = 'proceed 1';
                        }else if($check2!=0){
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Duplicate Record';
                            $response['stat_msg'] = 'User group already exists.';
                        }else{
                            $update = $this->query_update($group_id, $request, $s_priv);
                            $i++;
                            $response['a'] = 'proceed 2';
                        }

                        if($i){
                            if($update){
                                $response['stat'] = 'success';
                                $response['stat_title'] = 'Success';
                                $response['stat_msg'] = 'User group has been successfully updated';
                                //Add Logs
                                $group = UserGroups::find($group_id);
                                $desc = 'updated a user group ['.$group->group_name.']';
                                Helpers::add_activity_logs(['user-group-edit',$group_id,$desc]);
                            }else{
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Unable to update. Error occured during process. Error Code: ErGRPU002';
                            }
                        }
                    }
                }
            }
        }


        return $response;
    }


    function query_update($id, Request $request, $privileges){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'group_name' => ucwords($request->name),
            'group_privileges' => json_encode($privileges),
            'group_remarks' => $request->remarks,
            'updated_by' => $user_id,
        ];


        //return $id;
        return UserGroups::where('account_id', $account_id)->where('user_group_id', $id)->update($data);
    }



    public function delete(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';

        if(!Helpers::get_user_privilege('admin_usergroup_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = UserGroups::where('account_id',$account_id)->where('user_group_id',$id)->count();
                if($check_record!=0){
                    $record = UserGroups::find($id);
                    $delete_record = UserGroups::find($id)->delete();
                    
                    //To check if supplier is deleted
                    $check_record = UserGroups::where('account_id',$account_id)->where('user_group_id',$id)->count();

                    if($check_record==0){
                        $s[] = $id;

                        $_log_desc .= $record->group_name . ', ';
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted a user group ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['user-group-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
