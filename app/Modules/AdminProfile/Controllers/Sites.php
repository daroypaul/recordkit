<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;

class Sites extends Controller
{
    /*public function __construct()
    {
        config(['app.timezone' => 'Asia/Taipei']);
    }*/
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->ref;

        if(!$request->ref&&$request->id){
            $id = $request->id;
            $sites = Site::where('account_id', $account_id)->where('site_id',$id)->where('record_stat',0)->first();
        }else if($request->ref=='all'){
            $sites = Site::where('account_id', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();
        }else if($request->ref=='_keywords'){
            $sites = Site::where('account_id', $account_id)->where('record_stat',0)->where('site_name','like','%'.$request->keywords.'%')->orderBy('site_name', 'asc')->get();

            if(isset($request->with)&&$request->with=='_all'){
                $a['site_name'] = 'All Sites';
                $a['site_id'] = 'all';
                $b[] = $a;
        
                foreach($sites as $site){
                    $a['site_id'] = $site['site_id'];
                    $a['site_name'] = $site['site_name'];
                    $a['site_remarks'] = $site['site_remarks'];
        
                    $b[] = $a;
                }
        
                $sites = (object) $b;
            }
        }

        $response['list'] =$sites;
        
        //return $asset_list;
        return $response;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_site_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;
        $read_acc = $privilege->privileges[0]->privilege_read;

        if($read_acc==0){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }*/
        if(!Helpers::get_user_privilege('admin_site_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $sites = Site::with(['recorder','updator'])
                            ->where('account_id', $account_id)
                            ->where('record_stat',0)->orderBy('site_name', 'asc')
                            ->where('record_stat',0)->get();

        $data = [
            "site_list"=>$sites,
            "form_title"=> "Site Profile",
        ];
        //return $sites;
        return view('adminprofile::site')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'site_name'=>'required|min:3|max:100|regex:/(^[A-Za-z0-9 -]+$)+/',
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        $messages = [
            'site_name.regex' => 'The :attribute format is invalid (alphanumeric, dash and space only).',
            'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];

        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_site_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_site_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->ref=='add'){
                $check_record = Site::where('account_id', $account_id)->where('site_name', $request->site_name)->count();

                if($check_record!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Site already exists.';
                }else{
                    $new_site = new Site;
                    $new_site->account_id = $account_id;
                    $new_site->site_name = ucwords($request->site_name);
                    $new_site->site_remarks = ucfirst($request->remarks);
                    $new_site->recorded_by = $user_id;
                    $new_site->updated_by = $user_id;
                    $new_site->save();
                    $new_id = $new_site->site_id;
                    
                    if($new_site){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New site has been added';
                        
                        $response['id'] = $new_id;
                        $response['name'] = $new_site->site_name;
                        $response['remarks'] = ($new_site->site_remarks||strlen($new_site->site_remarks) > 30) ? substr($new_site->site_remarks,0,30)."..." : '---';
                        $response['remarks_full'] = $new_site->site_remarks;

                        //Add Logs
                        $desc = 'added a site ['.$new_site->site_name.']';
                        Helpers::add_activity_logs(['site-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add site. Error occured during process. Error Code: ErSTEA001';
                    }
                }
            }else if($request->ref=='edit'){
                $site_id = $request->id;
                $check_record = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->count();
                
                if($check_record!=0){
                    //If remarks is updated and name is constant
                    $check1 = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->where('site_name', $request->site_name)->count();
                    //If name is updated
                    $check2 = Site::where('account_id', $account_id)->where('record_stat',0)->where('site_name', $request->site_name)->count();

                    if($check1!=0){
                        $update = static::update($site_id, $request);
                        $update_done = true;
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Site already exists.';
                        $update_done = false;
                    }else{
                        $update = static::update($site_id, $request);
                        $update_done = true;
                    }

                    if($update_done){
                        if($update){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Site has been successfully updated';
                            $response['id'] = $update->site_id;
                            $response['name'] = $update->site_name;
                            $response['remarks'] = ($update->site_remarks||strlen($update->site_remarks) > 30) ? substr($update->site_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $update->site_remarks;

                            //Add Logs
                            $desc = 'updated a site ['.$update->site_name.']';
                            Helpers::add_activity_logs(['site-edit',$site_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update. Error occured during process. Error Code: ErSTEU001';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or site might be deleted. Error Code: ErSTEU002';
                }
            }
        }

        return $response;
    }

    public static function update($site_id, Request $request)
    {
        $user_id = Auth::user()->user_id;

        $update = Site::find($site_id);
        $update->site_name = ucwords($request->site_name);
        $update->site_remarks = ucfirst($request->remarks);
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_site_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('admin_site_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = Site::where('account_id',$account_id)->where('site_id',$id)->where('record_stat',0)->count();
                if($check_record!=0){
                    /*$site = Site::find($id);
                    $site->date_deleted = Carbon::now('UTC')->toDateTimeString();
                    $site->deleted_by = $user_id;
                    $site->record_stat = 1;
                    $site->save();*/
                    $site = Site::find($id);
                    $delete_site = Site::find($id)->delete();

                    if($delete_site){
                        /*$update_location = Location::where('account_id', $account_id)
                                                ->where('site_id', $id)
                                                ->where('record_stat',0)
                                                ->update(['record_stat' => 1,'date_deleted'=> Carbon::now('UTC')->toDateTimeString(),'deleted_by' => $user_id]);*/
                        $delete = Location::where('account_id', $account_id)
                                                ->where('site_id', $id)->delete();
                        
                        if($delete){
                            $s[] = $id;

                            $_log_desc .= $site->site_name . ', ';
                        }else{
                            $x[] = $id; 
                        }
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted a site ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['site-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
