<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Site;
use App\modules\adminprofile\models\Location;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class Locations extends Controller
{
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $id = $request->id;
        $data = [];

        if($request->id){
            $id = $request->id;
            $data = Location::with(['site', 'recorder','updator'])->where('account_id', $account_id)->where('location_id', $id)->where('record_stat',0)->first();
        }else if($request->site_id&&$request->ref=='_keywords'){
            $id = $request->id;
            $data = Location::with(['site', 'recorder','updator'])
                            ->where('account_id', $account_id)
                            ->where('site_id', $request->site_id)
                            ->where('record_stat',0)
                            ->where('location_name','like','%'.$request->keywords.'%')->get();
        }else{
            $site_id = $request->site_id;
            $check_site = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->count();

            if($check_site!=0){
                $location_data = Location::with(['site', 'recorder','updator'])->where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->orderBy('location_name', 'asc')->get();

                $a = [];
                $i=0;
                foreach($location_data as $location){
                    $a['location_id'] = $location->location_id;
                    $a['site_id'] = $location->site_id;
                    $a['location_name'] = $location->location_name;
                    $a['remarks'] = ($location->location_remarks||strlen($location->location_remarks) > 30) ? substr($location->location_remarks,0,30)."..." : '---';
                    $a['remarks_full'] = $location->location_remarks;
                    $data[$i] = $a;
                    $i++;
                }
            }
        }

        return $data;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_location_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $records = [];

        $all_site = Site::where('account_id', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->get();
        
        $sites = Site::with(['recorder','updator'])->where('account_id', $account_id)->where('record_stat',0)->orderBy('site_name', 'asc')->limit(1)->first();
        $site_id = ($sites) ? $sites->site_id : 0;

        if($sites){
            $records = Location::with(['site', 'recorder','updator'])->where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->orderBy('location_name', 'asc')->get();
        }

        $data = [
            "location_list"=>$records,
            "site_list"=>$all_site,
            "default_site"=>$site_id,
            "form_title"=> "Location Profile",
        ];

        //return $records;

        return view('adminprofile::location')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'location_name'=>'required|min:3|max:100|regex:/(^[A-Za-z0-9\s-()]+$)+/u',
            'site'=>'required'
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        $messages = [
            'location_name.regex' => 'The :attribute format is invalid (alphanumeric, dash, space and () only).',
            'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];

        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_location_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_location_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            $site_id = $request->site;
            $check_site = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->count();

            if($check_site==0){
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Site not found. Invalid reference ID or site already deleted. Error Code: ErLCSTE001';
            }else{
                if($request->ref=='add'){
                    $check_record = Location::where('account_id', $account_id)
                                    ->where('site_id', $site_id)
                                    ->where('location_name', $request->location_name)
                                    ->where('record_stat',0)->count();
                    
                    if($check_record!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Location already exists.';
                    }else{
                        $new_location = new Location;
                        $new_location->account_id = $account_id;
                        $new_location->site_id = $site_id;
                        $new_location->location_name = ucwords($request->location_name);
                        $new_location->location_remarks = ucfirst($request->remarks);
                        $new_location->recorded_by = $user_id;
                        $new_location->updated_by = $user_id;
                        $new_location->save();
                        
                        if($new_location){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'New location has been added';
                            
                            $response['id'] = $new_location->location_id;
                            $response['name'] = $new_location->location_name;
                            $response['remarks'] = ($new_location->location_remarks||strlen($new_location->location_remarks) > 30) ? substr($new_location->location_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $new_location->location_remarks;
                            //Add Logs
                            $desc = 'added a location ['.$new_location->location_name.']';
                            Helpers::add_activity_logs(['location-add',$new_location->location_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to add location. Error occured during process. Error Code: ErLOCA001';
                        }
                    }
                }else if($request->ref=='edit'){
                    $location_id = $request->id;

                    $check_record = Location::where('account_id', $account_id)
                                    ->where('location_id', $location_id)
                                    ->where('record_stat',0)->count();

                    if($check_record==0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update. Invalid reference ID or location already deleted. Error Code: ErLOCU002';
                    }else{
                        //Location name and site is constant
                        $check1 = Location::where('account_id', $account_id)
                                                ->where('location_id', $location_id)
                                                ->where('site_id', $site_id)
                                                ->where('location_name', $request->location_name)
                                                ->where('record_stat',0)
                                                ->count();
                        //Site is contant
                        $check2 = Location::where('account_id', $account_id)
                                                ->where('location_id', $location_id)
                                                ->where('site_id', $site_id)
                                                ->where('record_stat',0)
                                                ->count();
                        
                        //Check if Location name is exists
                        $check3 = Location::where('account_id', $account_id)
                                                ->where('site_id', $site_id)
                                                ->where('location_name', $request->location_name)
                                                ->where('record_stat',0)->count();

                        if($check1!=0){
                            //$update = $this->query_update($location_id, $site_id, $request->location_name, $request->remarks);
                            //$response['update_stat'] = $update;
                            $update = static::update($location_id, $site_id, $request);
                            $update_done = true;
                        }else if($check2!=0){
                            if($check3!=0){
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Location already exists.';
                                $update_done = false;
                            }else{
                                //$update = $this->query_update($location_id, $site_id, $request->location_name, $request->remarks);
                                //$response['update_stat'] = $update;
                                $update = static::update($location_id, $site_id, $request);
                                $update_done = true;
                            }
                        }else{//If Site is updated
                            if($check3!=0){
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Location already exists.';
                                $update_done = false;
                            }else{
                                //$update = $this->query_update($location_id, $site_id, $request->location_name, $request->remarks);
                                //$response['update_stat'] = $update;
                                $update = static::update($location_id, $site_id, $request);
                                $update_done = true;
                            }
                        }

                        if($update_done){
                            if($update){
                                $response['stat'] = 'success';
                                $response['stat_title'] = 'Success';
                                $response['stat_msg'] = 'Location has been successfully updated';
                                $response['id'] = $update->location_id;
                                $response['name'] = $update->location_name;
                                $response['remarks'] = ($update->location_remarks||strlen($update->location_remarks) > 30) ? substr($update->location_remarks,0,30)."..." : '---';
                                $response['remarks_full'] = $update->location_remarks;
                                
                                $desc = 'updated a location ['.$update->location_name.']';
                                Helpers::add_activity_logs(['location-edit',$location_id,$desc]);
                            }else{
                                $response['stat'] = 'error';
                                $response['stat_title'] = 'Error';
                                $response['stat_msg'] = 'Unable to update. Error occured during process. Error Code: ErSTEU001';
                            }
                        }
                    }
                }
            }
        }

        return $response;
    }

    function query_update($location_id, $site_id, $name='', $remarks=''){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'site_id' => $site_id,
            'location_name' => ucwords($name),
            'location_remarks' => ucfirst($remarks),
            'updated_by' => $user_id
        ];

        $update = Location::where('account_id', $account_id)
                                ->where('location_id', $location_id)
                                ->where('record_stat',0)
                                ->update($data);

        if($update){
            $record = Location::where('account_id', $account_id)
                                    ->where('location_id', $location_id)
                                    ->where('record_stat',0)->first();

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'Location has been successfully updated';
            $response['id'] = $record->location_id;
            $response['name'] = $record->location_name;
            $response['remarks'] = ($record->location_remarks) ? $record->location_remarks : "---";

            //Add Logs
            $desc = 'updated a location ['.$record->location_name.']';
            Helpers::add_activity_logs(['location-edit',$location_id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update. Error occured during process. Error Code: ErSTEU001';
        }

        return $response;
    }

    public static function update($location_id, $site_id, Request $request)
    {

        $user_id = Auth::user()->user_id;

        $update = Location::find($location_id);
        $update->site_id = $site_id;
        $update->location_name = ucwords($request->location_name);
        $update->location_remarks = ucfirst($request->remarks);
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_location_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('admin_location_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                $check_record = Location::where('account_id',$account_id)->where('location_id',$id)->where('record_stat',0)->first();

                if(count($check_record)!=0){
                    $site_id = $check_record->site_id;
                    $check_site = Site::where('account_id', $account_id)->where('site_id', $site_id)->where('record_stat',0)->count();
                    
                    if($check_site!=0){
                        /*$location = Location::find($id);
                        $location->date_deleted = Carbon::now('UTC')->toDateTimeString();
                        $location->deleted_by = $user_id;
                        $location->record_stat = 1;
                        $location->save();*/
                        $location = Location::find($id)->delete();

                        if($location){
                            $s[] = $id;
                            $_log_desc .= $check_record->location_name . ', ';
                        }else{
                            $x[] = $id;
                        }
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted a location ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['location-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
