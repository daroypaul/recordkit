<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Suppliers;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;

class Supplier extends Controller
{
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->ref;

        if(!$request->ref&&$request->id){
            $id = $request->id;
            $record = Suppliers::where('account_id', $account_id)->where('supplier_id',$id)->first();
        }else if($request->ref=='all'){
            $record = Suppliers::where('account_id', $account_id)->orderBy('supplier_name', 'asc')->get();
        }else if($request->ref=='_keywords'){
            $keywords = $request->keywords;

            $record = Suppliers::where('account_id', $account_id)->where('supplier_name','like','%'.$keywords.'%')->orderBy('supplier_name', 'asc')->get();
        }
        
        //return $asset_list;
        return $record;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admsplr');

        $read_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_read : 0;

        if($read_acc==0){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }*/
        if(!Helpers::get_user_privilege('admin_supplier_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $suppliers = Suppliers::with(['recorder','updator'])
                            ->where('account_id', $account_id)
                            ->orderBy('supplier_name', 'asc')->get();

        $data = [
            "record_list"=>$suppliers,
            "form_title"=> "Suppliers/Service Providers",
        ];
        
        //return UserGroupPrivileges::where('user_group_id', Auth::user()->user_group_id);
        return view('adminprofile::suppliers')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admsplr');
        $delete_acc = $privilege->privileges[0]->privilege_delete;
        $write_acc = $privilege->privileges[0]->privilege_write;
        $modify_acc = $privilege->privileges[0]->privilege_modify;*/

        $rules = [
            'name'=>'required|min:2|max:100|regex:/(^[A-Za-z0-9 .&-]+$)+/',
        ];

        if($request->address){
            $rules['address'] = 'min:2|max:500|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._();]+$)+/';
        }

        $messages = [
            'name.regex' => 'The :attribute format is invalid.',
            'remarks.regex' => 'The :attribute format is invalid.',
            'address.regex' => 'The :attribute format is invalid.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];



        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_supplier_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_supplier_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->ref=='add'){
                $check_record = Suppliers::where('account_id', $account_id)->where('supplier_name', $request->name)->count();
                $response['a'] = $check_record;

                if($check_record!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Record';
                    $response['stat_msg'] = 'Supplier already exists.';
                }else{
                    $new_record = new Suppliers;
                    $new_record->account_id = $account_id;
                    $new_record->supplier_name = ucwords($request->name);
                    $new_record->supplier_address = ($request->address) ? ucfirst($request->address) : '';
                    $new_record->supplier_remarks = ($request->remarks) ? ucfirst($request->remarks) : '';
                    //$new_record->cat_remarks = ucfirst($request->remarks);
                    $new_record->recorded_by = $user_id;
                    $new_record->updated_by = $user_id;
                    $new_record->save();
                    $new_id = $new_record->supplier_id;
                    
                    if($new_record){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New supplier has been added';
                        
                        $response['id'] = $new_id;
                        $response['name'] = $new_record->supplier_name;
                        $response['address'] = ($new_record->supplier_address||strlen($new_record->supplier_address) > 30) ? substr($new_record->supplier_address,0,30)."..." : '---';
                        $response['address_full'] = $new_record->supplier_address;
                        $response['remarks'] = ($new_record->supplier_remarks||strlen($new_record->supplier_remarks) > 30) ? substr($new_record->supplier_remarks,0,30)."..." : '---';
                        $response['remarks_full'] = $new_record->supplier_remarks;

                        //Add Logs
                        $desc = 'added an supplier ['.$new_record->supplier_name.']';
                        Helpers::add_activity_logs(['supplier-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add supplier. Error occured during process. Error Code: ErSPLRA001';
                    }
                }
            }else if($request->ref=='edit'){
                $supplier_id = $request->id;
                $check_record = Suppliers::where('account_id', $account_id)->where('supplier_id', $supplier_id)->count();
                
                if($check_record!=0){
                    //If remarks is updated and name is constant
                    $check1 = Suppliers::where('account_id', $account_id)->where('supplier_id', $supplier_id)->where('supplier_name', $request->name)->count();
                    //If name is updated
                    $check2 = Suppliers::where('account_id', $account_id)->where('supplier_name', $request->name)->count();

                    if($check1!=0){
                        //$update = $this->query_update($supplier_id, $request);
                        //$response['update_stat'] = $update;
                        $update = static::update($supplier_id, $request);
                        $update_done = true;
                        $response['a'] = 'proceed 1';
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'Supplier already exists.';
                        $update_done = false;
                    }else{
                        //$update = $this->query_update($supplier_id, $request);
                        //$response['update_stat'] = $update;
                        $update = static::update($supplier_id, $request);
                        $update_done = true;
                    }

                    if($update_done){
                        if($update){
                            $record = Suppliers::find($supplier_id);

                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Supplier has been successfully updated';
                            $response['id'] = $supplier_id  ;
                            $response['name'] = ucwords($request->name);
                            $response['address'] = ($record->supplier_address||strlen($record->supplier_address) > 30) ? substr($record->supplier_address,0,30)."..." : '---';
                            $response['address_full'] = $record->supplier_address;
                            $response['remarks'] = ($record->supplier_remarks||strlen($record->supplier_remarks) > 30) ? substr($record->supplier_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $record->supplier_remarks;

                            //Add Logs
                            $desc = 'updated an supplier ['.$record->supplier_name.']';
                            Helpers::add_activity_logs(['supplier-edit',$supplier_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update category. Error occured during process.';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or supplier might be deleted. Error Code: ErSPLRU002';
                }
            }
        }

        return $response;
    }

    /*function query_update($id, Request $request){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'supplier_name' => ucwords($request->name),
            'supplier_address' => ($request->address) ? $request->address : '',
            'supplier_remarks' => ($request->remarks) ? $request->remarks : '',
            'updated_by' => $user_id,
        ];

        $update = Suppliers::where('account_id', $account_id)->where('supplier_id', $id)->update($data);

        if($update){
            $record = Suppliers::where('account_id', $account_id)->where('supplier_id', $id)->first();

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'Supplier has been successfully updated';
            $response['id'] = $id;
            $response['name'] = ucwords($request->name);
            $response['address'] = ($record->supplier_address||strlen($record->supplier_address) > 30) ? substr($record->supplier_address,0,30)."..." : '---';
            $response['address_full'] = $record->supplier_address;
            $response['remarks'] = ($record->supplier_remarks||strlen($record->supplier_remarks) > 30) ? substr($record->supplier_remarks,0,30)."..." : '---';
            $response['remarks_full'] = $record->supplier_remarks;


            //Add Logs
            $desc = 'updated an supplier ['.$record->supplier_name.']';
            Helpers::add_activity_logs(['supplier-edit',$id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update supplier. Error occured during process. Error Code: ErSPLRU001';
        }

        return $response;
    }*/

    public static function update($id, Request $request)
    {

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $update = Suppliers::find($id);
        $update->supplier_name = ucwords($request->name);
        $update->supplier_address = ($request->address) ? $request->address : '';
        $update->supplier_remarks = ($request->remarks) ? $request->remarks : '';
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admsplr');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('admin_supplier_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = Suppliers::where('account_id',$account_id)->where('supplier_id',$id)->count();
                if($check_record!=0){
                    $name = Suppliers::find($id)->supplier_name;
                    $delete_record = Suppliers::find($id)->delete();
                    
                    //To check if supplier is deleted
                    $check_record = Suppliers::where('account_id',$account_id)->where('supplier_id',$id)->count();

                    if($check_record==0){
                        $s[] = $id;
                        $_log_desc .= $name . ', ';
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted an supplier ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['supplier-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
