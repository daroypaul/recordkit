<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Users;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Hash;
use Session;

class User extends Controller
{

    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $record = [];

        if($request->id&&$request->type=='user'){
            $user_id = $request->id;
            $record = Users::with(['site','group'])->where('account_id', $account_id)->where('user_id',$user_id)->where('is_deleted',0)->where('is_system_built', 0)->first();
            return $record;
        }else if($request->ref=='all'&&$request->type=='user'){
            $stat = $request->stat;
            $records = Users::with(['site','group'])->where('account_id', $account_id)->where('record_stat',$stat)->where('is_deleted',0)->where('is_system_built', 0)->get();
            $a = [];
            $data = [];
            $i=0;
            foreach($records as $record){
                $a['id'] = $record->user_id;
                $a['fullname'] = $record->user_fullname;
                $a['username'] = $record->user_login;
                $a['email'] = $record->user_email;
                $a['remarks'] = ($record->user_remarks||strlen($record->user_remarks) > 30) ? substr($record->user_remarks,0,30)."..." : '---';
                $a['remarks_full'] = $record->user_remarks;
                $a['group'] = ($record->group) ? $record->group->group_name : '<code>Unassigned</code>';
                $a['site'] = ($record->site) ? $record->site->site_name : '<code>Unassigned</code>';
                

                $data[$i] = $a;
                $i++;
            }

            return $data;
        }
    }

    public function index(){
        if (!Auth::check())
        {
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_user_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $records = Users::with(['group','site'])
                            ->where('account_id', $account_id)
                            ->where('record_stat', 0)
                            ->where('is_deleted', 0)
                            ->where('is_system_built', 0)
                            ->orderBy('user_fullname', 'asc')->get();

        $data = [
            "record_list"=>$records,
            "form_title"=> "System Users",
        ];
        
        //return $records;
        return view('adminprofile::users')->with($data);
    }


    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'fullname'=>'required|min:2|max:40|regex:/(^[A-Za-z ]+$)+/',
            'username'=>'required|min:5|max:30|alpha_num',
            'email'=>'required|max:50|email',
            'user_group'=>'required',
        ];

        if($request->ref=='add'){
            $rules['password'] = 'required|min:8|max:18|confirmed|regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/';
            $rules['password_confirmation'] = 'sometimes|required_with:password';
        }

        if($request->remarks){
            //$rules['remarks'] = "min:2|max:750|regex:/(^[A-Za-z0-9 ,-._();']+$)+/";
            $rules['remarks'] = "min:2|max:750";
        }

        //^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$

        $messages = [
            'fullname.regex' => 'The :attribute format is invalid.',
            'username.regex' => 'The :attribute format is invalid.',
            'remarks.regex' => 'The :attribute format is invalid.',
            'password.regex' => 'The :attribute format is invalid.',
            'password_confirmation.required_with' => 'The :attribute  field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $val_msg = $validator->errors();

        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_user_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_user_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{
            if($request->ref=='add'){
                $check_username = Users::where('user_login', $request->username)->count();
                $check_fullname = Users::where('account_id', $account_id)->where('user_fullname', $request->fullname)->where('is_deleted', 0)->count();

                if($check_username!=0){
                    $val_msg->add('username', 'Already taken');
                    $response['val_error']= $val_msg;
                }
                if($check_fullname!=0){
                    $val_msg->add('fullname', 'Already exists');
                    $response['val_error']= $val_msg;
                }
                
                if($check_username==0&&$check_fullname==0){
                    $hash_password = Hash::make ( $request->password );

                    $new_record = new Users;
                    $new_record->account_id = $account_id;
                    $new_record->user_group_id = $request->user_group;
                    $new_record->site_id = ($request->site) ? $request->site : 0;
                    $new_record->user_login = strtolower($request->username);
                    $new_record->user_pass = $hash_password;
                    $new_record->user_fullname = ucwords($request->fullname);
                    $new_record->user_email = $request->email;
                    $new_record->user_remarks = ($request->remarks) ? $request->remarks : '';
                    $new_record->recorded_by = $user_id;
                    $new_record->updated_by = $user_id;
                    $new_record->record_stat = ($request->active_user) ? 0 : 1;
                    $new_record->is_deleted = 0;
                    $new_record->is_system_built = 0;
                    $new_record->save();

                    if($new_record){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New user has been added';

                        $response['id'] = $new_record->user_id;
                        $response['fullname'] = ucwords($new_record->user_fullname);
                        $response['username'] = $new_record->user_login;
                        $response['email'] = $new_record->user_email;
                        $response['group'] = ($new_record->group) ? $new_record->group->group_name : '<code>Unassigned</code>';
                        //$response['email'] = $new_record->user_email;
                        $response['remarks'] = ($new_record->user_remarks||strlen($new_record->user_remarks) > 30) ? substr($new_record->user_remarks,0,30)."..." : '---';
                        $response['remarks_full'] = $new_record->user_remarks;

                        //Add Logs
                        $desc = 'created a user ['.$new_record->user_fullname.']';
                        Helpers::add_activity_logs(['user-add',$new_record->user_id,$desc]);
                    }else{

                    }
                }
            }else if($request->ref=='edit'){
                $user_id = $request->id;
                $check_record = Users::where('account_id', $account_id)->where('user_id', $user_id)->where('is_deleted', 0)->count();

                if($check_record==0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or user might already deleted. Error Code: ErUSRU001';
                }else{
                    $check1 = Users::where('account_id', $account_id)->where('user_id', $user_id)->where('user_login', $request->username)->where('user_fullname', $request->fullname)->where('is_deleted', 0)->count();//If Fullname and Username is same

                    $check2 = Users::where('account_id', $account_id)->where('user_id', $user_id)->where('user_login', $request->username)->where('is_deleted', 0)->count();//If Username is same 

                    $check3 = Users::where('account_id', $account_id)->where('user_id', $user_id)->where('user_fullname', $request->fullname)->where('is_deleted', 0)->count();//If Fullname is same

                    $check_username = Users::where('user_login', $request->username)->count();

                    $check_fullname = Users::where('account_id', $account_id)->where('user_fullname', $request->fullname)->where('is_deleted', 0)->count();

                    if($check1!=0){
                        //$update = $this->query_update($user_id, $request);
                        //$response['update_stat'] = $update;

                        $update = static::update($user_id, $request);
                        $update_done = true;
                    }else if($check2!=0){
                        if($check_fullname!=0){
                            $val_msg->add('fullname', 'Already exists');
                            $response['val_error']= $val_msg;
                            $response['a'] = 'error 1';
                            $update_done = false;
                        }else{
                            //$update = $this->query_update($user_id, $request);
                            //$response['update_stat'] = $update;
                            $response['a'] = 'proceed 2';

                            $update = static::update($user_id, $request);
                            $update_done = true;
                        }
                    }else if($check3!=0){
                        if($check_username!=0){
                            $val_msg->add('username', 'Already exists');
                            $response['val_error']= $val_msg;
                            $response['a'] = 'error 2';
                            $update_done = false;
                        }else{
                            //$update = $this->query_update($user_id, $request);
                            //$response['update_stat'] = $update;
                            $response['a'] = 'proceed 3';
                            $update = static::update($user_id, $request);
                            $update_done = true;
                        }
                    }else{
                        if($check_fullname!=0){
                            $val_msg->add('fullname', 'Already exists');
                            $response['val_error']= $val_msg;
                            $response['a'] = 'error 3';
                            $update_done = false;
                        }
                        if($check_username!=0){
                            $val_msg->add('username', 'Already exists');
                            $response['val_error']= $val_msg;
                            $response['a'] = 'error 4';
                            $update_done = false;
                        }

                        if($check_username==0&&$check_fullname==0){
                            //$update = $this->query_update($user_id, $request);
                            //$response['update_stat'] = $update;
                            $response['a'] = 'proceed 4';
                            $update_done = false;

                            $update = static::update($user_id, $request);
                            $update_done = true;
                        }
                    }

                    if($update_done){
                        if($update){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'User has been successfully updated';
                            
                            
                            $record = Users::with(['site','group'])->find($user_id);
                            
                            $response['id'] = $record->user_id;
                            $response['fullname'] = ucwords($record->user_fullname);
                            $response['username'] = $record->user_login;
                            $response['email'] = $record->user_email;
                            $response['group'] = ($record->group) ? $record->group->group_name : '<code>Unassigned</code>';
                            $response['remarks'] = ($record->user_remarks||strlen($record->user_remarks) > 30) ? substr($record->user_remarks,0,30)."..." : '---';
                            $response['remarks_full'] = $record->user_remarks;
                            $response['a'] = ($request->active_user) ? 1 : 0;

                            //Add Logs
                            $desc = 'updated a user ['.$record->user_fullname.']';
                            Helpers::add_activity_logs(['user-edit',$user_id,$desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update category. Error occured during process. Error Code: ErCATU001';
                        }
                    }
                }
            }
        }

        return $response;
    }


    /*function query_update($id, Request $request){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'user_group_id' => $request->user_group,
            'site_id' => ($request->site) ? $request->site : 0,
            'user_login' => strtolower($request->username),
            'user_fullname' => ucwords($request->fullname),
            'user_email' => $request->email,
            'user_remarks' => ($request->remarks) ? $request->remarks : '',
            'updated_by' => $user_id,
            'record_stat' => ($request->active_user) ? 0 : 1,
        ];

        $update = Users::where('account_id', $account_id)->where('user_id', $id)->update($data);

        if($update){
            //$record = Users::where('account_id', $account_id)->where('user_id', $id)->first();
            $record = Users::find($id);

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'User has been successfully updated';
            
            $response['id'] = $record->user_id;
            $response['fullname'] = ucwords($record->user_fullname);
            $response['username'] = $record->user_login;
            $response['email'] = $record->user_email;
            $response['group'] = ($record->group) ? $record->group->group_name : '<code>Unassigned</code>';
            //$response['email'] = $new_record->user_email;
            $response['remarks'] = ($record->user_remarks||strlen($record->user_remarks) > 30) ? substr($record->user_remarks,0,30)."..." : '---';
            $response['remarks_full'] = $record->user_remarks;
            $response['a'] = ($request->active_user) ? 1 : 0;

            //Add Logs
            $desc = 'updated a user ['.$record->user_fullname.']';
            Helpers::add_activity_logs(['user-edit',$id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update user. Error occured during process. Error Code: ErUSRU002';
        }

        return $response;
    }*/

    public static function update($id, Request $request)
    {

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $update = Users::find($id);
        $update->site_id = ($request->site) ? $request->site : 0;
        $update->user_login = strtolower($request->username);
        $update->user_fullname = ucwords($request->fullname);
        $update->user_email = $request->email;
        $update->user_remarks = ($request->remarks) ? $request->remarks : '';
        $update->updated_by = $user_id;
        $update->record_stat = ($request->active_user) ? 0 : 1;
        $update->save();

        return $update;
    }

    //reset_password

    public function reset_password(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $response = [];

        $rules = [
            'password'=>'required|min:8|max:18|confirmed|regex:/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
            'password_confirmation'=>'sometimes|required_with:password',
        ];

        $messages = [
            'password.regex' => 'The :attribute format is invalid.',
            'password_confirmation.required_with' => 'The :attribute  field is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $val_msg = $validator->errors();

        if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_user_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $val_msg;
        }else{
            $id = $request->id;
            $check_record = Users::where('account_id', $account_id)->where('user_id', $id)->where('is_deleted', 0)->count();

            if($check_record==0){
                $response['stat'] = 'error';
                $response['stat_title'] = 'Error';
                $response['stat_msg'] = 'Unable to reset password. Invalid reference ID or user might already deleted. Error Code: ErUSRPASRESU001';
            }else{
                $data = [
                    'user_pass' => Hash::make ($request->password),
                    'updated_by' => $user_id
                ];
        
                $update_pass = Users::where('account_id', $account_id)->where('user_id', $id)->update($data);
            
                if($update_pass){
                    $response['stat'] = 'success';
                    $response['stat_title'] = 'Success';
                    $response['stat_msg'] = 'User password successfully reset';

                    //Add Logs
                    $user = Users::find($id);
                    $desc = 'reset a user password of user ['.$user->user_fullname.']';
                    Helpers::add_activity_logs(['user-edit',$id,$desc]);
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update reset password. Error occured during process. Error Code: ErUSRPASRESU002';
                }
            }
        }
        return $response;
    }

    public function change_status(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        if(!Helpers::get_user_privilege('admin_user_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{

            $stat = ($request->ref=='deactivate') ? 1 : 0;

            if($request->act_type=='single'){
                $id = $request->id;

                $check_record = Users::where('account_id',$account_id)->where('user_id', $id)->where('is_deleted', 0)->count();

                if($check_record){
                    $update = Users::find($id);
                    $update->record_stat = $stat;
                    $update->updated_by = $user_id;
                    $update->save();

                    if($update){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'User has been ' . $request->ref . 'd';
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to ' . $request->ref . ' user. Error occured during process.';
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to ' . $request->ref . ' user. Invalid reference ID or record might already deleted.';
                }
            }
            else if($request->act_type=='multi'){
                $x = [];
                $s = [];

                if($request->ref=='deactivate'){
                    $stat = 1;
                }else{
                    $stat = 0;
                }

                foreach($request->id as $id){
                    $check_record = Users::where('account_id',$account_id)->where('user_id',$id)->where('is_deleted', 0)->count();

                    if($check_record!=0&&$id!=$user_id){
                        $update_record = Users::where('account_id', $account_id)->where('user_id', $id)->update(['record_stat' => $stat,'updated_by' => $user_id]);

                        if($update_record){
                            $s[] = $id;
                        }else{
                            $x[] = $id;
                        }
                        
                    }else{
                        $x[] = $id;
                    }
                }

                $response['eid'] = $x;
                $response['sid'] = $s;
            }
        }

        return $response;
    }


    public function delete(Request $request)
    {
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        if(!Helpers::get_user_privilege('admin_user_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                $check_record = Users::where('account_id',$account_id)
                                    ->where('user_id',$id)
                                    ->where('is_deleted', 0)
                                    ->where('is_system_built', 0)
                                    ->count();
                
                if($check_record!=0&&$id!=$user_id){
                    $record = Users::find($id);
                    $record->is_deleted = 1;
                    $record->save();

                    if($record){
                        $s[] = $id;

                        $user = Users::find($id);
                        $desc = 'deleted a user  ['.$record->user_fullname.']';
                        Helpers::add_activity_logs(['user-delete',$id,$desc]);
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
