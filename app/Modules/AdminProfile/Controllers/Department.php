<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Departments;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Session;
use Options;

class Department extends Controller
{
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        //$keywords = $request->keywords;
        $account_id = Auth::user()->account_id;
        $ref = $request->ref;

        if($request->id){
            $id = $request->id;
            $record = Departments::where('account_id', $account_id)->where('department_id',$id)->first();
        }else if($request->ref=='_keywords'){
            $keywords = $request->keywords;

            $record = Departments::where('account_id', $account_id)->where('dept_name','like','%'.$keywords.'%')->orderBy('dept_name', 'asc')->get();
        }else if($request->ref=='all'){
            $record = Departments::where('account_id', $account_id)->orderBy('dept_name', 'asc')->get();
        }

        //$response['list'] =$record;
        //return $asset_list;
        return $record;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_dept_');

        $read_acc = (count($privilege->privileges)!=0) ? $privilege->privileges[0]->privilege_read : 0;

        if($read_acc==0){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }*/
        if(!Helpers::get_user_privilege('admin_dept_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $departments = Departments::with(['recorder','updator'])
                            ->where('account_id', $account_id)
                            ->orderBy('dept_name', 'asc')->get();

        $data = [
            "department_list"=>$departments,
            "form_title"=> "Departments",
        ];
        
        //return UserGroupPrivileges::where('user_group_id', Auth::user()->user_group_id);
        return view('adminprofile::departments')->with($data);
    }

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_dept_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;
        $write_acc = $privilege->privileges[0]->privilege_write;
        $modify_acc = $privilege->privileges[0]->privilege_modify;*/

        $rules = [
            'name'=>'required|min:2|max:50|regex:/(^[A-Za-z &-]+$)+/',
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        $messages = [
            'name.regex' => 'The :attribute format is invalid.',
            //'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];



        if($request->ref=='add'&&!Helpers::get_user_privilege('admin_dept_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&!Helpers::get_user_privilege('admin_dept_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->ref=='add'){
                $check_record = Departments::where('account_id', $account_id)->where('dept_name', $request->name)->count();
                $response['a'] = $check_record;

                if($check_record!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Duplicate Record';
                    $response['stat_msg'] = 'Department already exists.';
                }else{
                    $new_record = new Departments;
                    $new_record->account_id = $account_id;
                    $new_record->dept_name = ucwords($request->name);
                    //$new_record->cat_remarks = ucfirst($request->remarks);
                    $new_record->recorded_by = $user_id;
                    $new_record->updated_by = $user_id;
                    $new_record->save();
                    $new_id = $new_record->department_id;
                    
                    if($new_record){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New department has been added';
                        
                        $response['id'] = $new_id;
                        $response['name'] = $new_record->dept_name;

                        //Add Logs
                        $desc = 'added an department ['.$new_record->dept_name.']';
                        Helpers::add_activity_logs(['department-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add department. Error occured during process.';
                    }
                }
            }else if($request->ref=='edit'){
                $department_id = $request->id;
                $check_record = Departments::where('account_id', $account_id)->where('department_id', $department_id)->count();
                
                if($check_record!=0){
                    //If remarks is updated and name is constant
                    $check1 = Departments::where('account_id', $account_id)->where('department_id', $department_id)->where('dept_name', $request->name)->count();
                    //If name is updated
                    $check2 = Departments::where('account_id', $account_id)->where('dept_name', $request->name)->count();

                    if($check1!=0){
                        //$update = $this->query_update($department_id, $request);
                        //$response['update_stat'] = $update;
                        $update = static::update($department_id, $request);
                        $update_done = true;
                        $response['a'] = 'proceed 1';
                    }else if($check2!=0){
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Duplicate Record';
                        $response['stat_msg'] = 'Department already exists.';
                        $update_done = false;
                    }else{
                        //$update = $this->query_update($department_id, $request);
                        //$response['update_stat'] = $update;
                        $update = static::update($department_id, $request);
                        $update_done = true;
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or department might be deleted';
                    $update_done = false;
                }

                if($update_done){
                    if($update){
                        $record = Departments::find($department_id);

                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'Department has been successfully updated';

                        $response['id'] = $department_id;
                        $response['name'] = ucwords($request->name);
                        //Add Logs
                        $desc = 'updated an department ['.$record->dept_name.']';
                        Helpers::add_activity_logs(['department-edit',$department_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to update category. Error occured during process.';
                    }
                }
            }
        }

        return $response;
    }

    /*function query_update($id, Request $request){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $data = [
            'dept_name' => ucwords($request->name),
            'updated_by' => $user_id,
        ];

        $update = Departments::where('account_id', $account_id)->where('department_id', $id)->update($data);

        if($update){
            $record = Departments::find($id);

            $response['stat'] = 'success';
            $response['stat_title'] = 'Success';
            $response['stat_msg'] = 'Department has been successfully updated';
            $response['id'] = $id;
            $response['name'] = ucwords($request->name);
            //$response['remarks'] = ($record->cat_remarks) ? $record->cat_remarks : "---";
            //$response['updated_by'] = $record->updator->user_fullname;
            //$response['updated_on'] = date('d-M-Y@h:iA', strtotime($record->date_updated.'+8 hours'));

            //Add Logs
            $desc = 'updated an department ['.$record->dept_name.']';
            Helpers::add_activity_logs(['department-edit',$id,$desc]);
        }else{
            $response['stat'] = 'error';
            $response['stat_title'] = 'Error';
            $response['stat_msg'] = 'Unable to update department. Error occured during process. Error Code: ErDEPTU001';
        }

        return $response;
    }*/

    public static function update($id, Request $request)
    {

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $update = Departments::find($id);
        $update->dept_name = ucwords($request->name);
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';
        //Get the privilege of user
        /*$privilege = Helpers::get_user_privilege(Auth::id(), 'admin_dept_');
        $delete_acc = $privilege->privileges[0]->privilege_delete;*/

        if(!Helpers::get_user_privilege('admin_dept_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = Departments::where('account_id',$account_id)->where('department_id',$id)->count();
                if($check_record!=0){
                    $name = Departments::find($id)->dept_name;
                    $delete_record = Departments::find($id)->delete();
                    
                    //To check if department is deleted
                    $check_record = Departments::where('account_id',$account_id)->where('department_id',$id)->count();

                    if($check_record==0){
                        $s[] = $id;

                        $_log_desc .= $name . ', ';
                        //Remove department foreign key to all assets 
                        //$update_asset = Assets::where('account_id', $account_id)->where('department_id', $id)->where('record_stat', 0)->update(['department_id' => 0,'updated_by' => $user_id]);
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }
            
            if($_log_desc){
                $desc = 'deleted an department ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['department-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
