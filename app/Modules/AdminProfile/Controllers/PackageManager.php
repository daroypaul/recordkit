<?php

namespace App\modules\packagemanager\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Packages;
use Helpers;
use Schema;
use App\Utilities\Migrations as Migrations;

class PackageManager extends Controller
{
    public function installed_index(){
        //return packages::modify('version_code', ['modules/Kms','modules/Insurances','modules/Consumables'],[0,1,2]);

        $packages = packages::data();

        $response = [
            "packages"=>$packages,
            "form_title"=>"Packages"
        ];

        return view('packagemanager::index')->with($response);

        $package = 'knowledgebase';
        $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';
        //App\Packages\Knowledgebase\Providers\KnowledgeBaseServiceProvider

        $installer =  new $className;

        

        //$uninstall = $installer->activate($package);

        //return $uninstall;

        $packages = packages::scan_packages();

        //$a = [];
        foreach($packages as $package){
            $provider_file_base = base_path('app/packages/'.$package.'/providers/*.php');
            $files = File::glob('app/packages/'.$package.'/providers/*.php');
            
            /*if($files){
                foreach($files as $file){
                    $filename = pathinfo($file, PATHINFO_FILENAME);

                    $provider_namespace = "App\\Packages\\".$package."\\Providers\\". $filename;

                    //$provider_namespace = str_replace('/', "\\", $provider_namespace);

                }
            }*/
            //$filename = pathinfo($a, PATHINFO_FILENAME);
            //return $filename;
            //File::glob($modules_dir[$modules_dir_key] . '/package.json')[0];

        }

        $a = Helpers::generate_random_string(20);

        return $a;
    }

    public function install(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if(!$package_data['installed']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            $install = $installer->install($package);
            $response['stat'] = $install;
        }else{
            $response[] = 'Package installed already';
        }

        $response['package'] = packages::data($package);

        return $response;
    }
    
    public function uninstall(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if($package_data['installed']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            $install = $installer->uninstall($package);
            $response['stat'] = $install;
        }else{
            $response[] = 'Package installed already';
        }

        $response['package'] = packages::data($package);
        
        return $response;
    }

    public function activate(Request $request){
        $package = $request->package;
        
        $package_data = packages::data($package);

        if(!$package_data['active']){
            $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

            $installer =  new $className;

            if($package_data['installed']){
                $install = $installer->activate($package);
                $response['stat'] = $install;
            }else{
                $install = $installer->install($package);
                $response['stat'] = $install;
            }
        }else{
            $response[] = 'Package activated already';
        }
        
        $response['package'] = packages::data($package);

        return $response;
    }

    public function deactivate(Request $request){
        $package = $request->package;

        $className = 'App\\Packages\\' . $package . '\\Controllers\\Installer';

        $installer =  new $className;

        $install = $installer->deactivate($package);

        $response['stat'] = $install;
        $response['package'] = packages::data($package);

        return $response;
    }

    public function delete(){
        //return packages::modify('version_code', ['modules/Kms','modules/Insurances','modules/Consumables'],[0,1,2]);

        
    }
}
