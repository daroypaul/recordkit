<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\modules\adminprofile\controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\Users;
use App\modules\adminprofile\models\Categories;
use App\modules\adminprofile\models\UserGroupPrivileges;
use App\packages\assettracking\models\Assets;
use App\packages\assettracking\models\AssetInOutItems;
use App\packages\assettracking\models\AssetLeaseItems;
use App\packages\assettracking\models\AssetCustomValues;

use Validator;
use Helpers;#Custom Helper Class
use Carbon\Carbon;
use DateTime;
use Options;

class Category extends Controller
{
    public function fetch_data(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $ref = $request->ref;
        $apps = ($request->apps) ? $request->apps : 'asset';

        if(!$request->ref&&$request->id){
            $id = $request->id;
            $record = Categories::where('account_id', $account_id)->where('cat_id',$id)->where('record_stat', 0)->first();
        }else if($request->ref=='all'){
            $record = Categories::where('account_id', $account_id)->where('apps_type', $apps)->where('record_stat', 0)->orderBy('cat_name', 'asc')->get();
        }else if($request->ref=='_with_keywords'){
            $keywords = $request->keywords;
            $record = Categories::where('account_id', $account_id)
                                    ->where('apps_type', $apps)
                                    ->where('record_stat', 0)
                                    ->where(function ($query) use ($keywords) {
                                        $query->where('cat_name','like','%'.$keywords.'%');
                                    })->orderBy('cat_name', 'asc')->get();
        }

        $response['list'] =$record;
        
        return $response;
    }

    public function index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }

        if(!Helpers::get_user_privilege('admin_cat_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $categories = Categories::with(['recorder','updator'])
                            ->where('account_id', $account_id)
                            ->where('record_stat', 0)
                            //->where('apps_type', 'asset')
                            ->orderBy('cat_name', 'asc')->get();

        $data = [
            "category_list"=>$categories,
            "form_title"=> "Categories",
            "apps"=>"asset",
        ];
        

        return view('adminprofile::categories')->with($data);
    }

    /*public function km_index(){
        if (!Auth::check())
        {
            Session::flash('login-error', 'Your session has been expired');
            return redirect('auth/login');
        }else if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
            Session::flash('login-error', 'Your account has been disabled/deleted');
            return redirect('auth/login');
        }
        //Get the privilege of user

        if(!Helpers::get_user_privilege('admin_km_article_view')){
            session()->flash('denied_access_notify', 1);
            return redirect('dashboard');
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $categories = Categories::with(['recorder','updator'])
                            ->where('account_id', $account_id)
                            ->where('apps_type', 'article')
                            ->orderBy('cat_name', 'asc')->get();

        $data = [
            "category_list"=>$categories,
            "form_title"=> "Article Categories",
            "apps"=>"km",
        ];
        
        //return UserGroupPrivileges::where('user_group_id', Auth::user()->user_group_id);
        return view('adminprofile::categories')->with($data);
    }*/

    public function store(Request $request){
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }

        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;

        $rules = [
            'category_name'=>'required|min:3|max:100|regex:/(^[A-Za-z0-9 -]+$)+/',
            'type'=>'required',
        ];

        if($request->remarks){
            $rules['remarks'] = 'min:2|max:750|regex:/(^[A-Za-z0-9 ,-._()]+$)+/';
        }

        $messages = [
            'category_name.regex' => 'The :attribute format is invalid (alphanumeric, dash and space only).',
            'remarks.regex' => 'The :attribute format is invalid (alphanumic and ,-._() only).',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);#Run the validation
        $response = [];

        $response['apps'] = $request->apps;

        if($request->ref=='add'&&$request->apps=='asset'&&!Helpers::get_user_privilege('admin_cat_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&$request->apps=='asset'&&!Helpers::get_user_privilege('admin_cat_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if($request->ref=='add'&&$request->apps=='article'&&!Helpers::get_user_privilege('admin_km_article_add')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->ref=='edit'&&$request->apps=='article'&&!Helpers::get_user_privilege('admin_km_article_edit')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }
        else if ($validator->fails()) {
            $response['val_error']= $validator->errors();
        }else{
            if($request->ref=='add'){
                $check_record = Categories::where('account_id', $account_id)->where('apps_type', $request->type)->where('cat_name', $request->category_name)->count();

                if($check_record!=0){
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Category already exists.';
                }else{
                    $new_cat = new Categories;
                    $new_cat->account_id = $account_id;
                    $new_cat->apps_type = $request->type;
                    $new_cat->cat_name = ucwords($request->category_name);
                    $new_cat->cat_remarks = ucfirst($request->remarks);
                    $new_cat->recorded_by = $user_id;
                    $new_cat->updated_by = $user_id;
                    $new_cat->record_stat = 0;
                    $new_cat->save();
                    $new_id = $new_cat->cat_id;
                    
                    if($new_cat){
                        $response['stat'] = 'success';
                        $response['stat_title'] = 'Success';
                        $response['stat_msg'] = 'New category has been added';
                        
                        $response['id'] = $new_id;
                        $response['name'] = $new_cat->cat_name;
                        $response['type'] = ucwords($new_cat->apps_type);
                        $response['remarks'] = ($new_cat->cat_remarks) ? $new_cat->cat_remarks : "---";

                        //Add Logs
                        $desc = 'added an category ['.$new_cat->cat_name.']';
                        Helpers::add_activity_logs(['category-add',$new_id,$desc]);
                    }else{
                        $response['stat'] = 'error';
                        $response['stat_title'] = 'Error';
                        $response['stat_msg'] = 'Unable to add category. Error occured during process. Error Code: ErCATA001';
                    }
                }
            }else if($request->ref=='edit'){
                $category_id = $request->id;
                $check_record = Categories::where('account_id', $account_id)->where('cat_id', $category_id)->count();
                
                if($check_record!=0){
                    //Check If remarks is updated and name is constant
                    $check_name_type = Categories::where('account_id', $account_id)->where('cat_id', $category_id)->where('apps_type', $request->type)->where('cat_name', $request->category_name)->count();
                    //Check If type is constant
                    $check_type = Categories::where('account_id', $account_id)->where('cat_id', $category_id)->where('apps_type', $request->type)->count();
                    //Check If type is constant
                    $check_name = Categories::where('account_id', $account_id)->where('apps_type', $request->type)->where('cat_name', $request->category_name)->count();

                    if($check_name_type){//If only remark updated
                        $update = static::update($category_id, $request);
                        $update_done = true;
                    }else if($check_type){//If only name is update
                        if($check_name){
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Category already exists.';
                            $update_done = false;
                        }else{
                            $update = static::update($category_id, $request);
                            $update_done = true;
                        }
                    }else{//If type is updated
                        if($check_name){
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Category already exists.';
                            $update_done = false;
                        }else{
                            $update = static::update($category_id, $request);
                            $update_done = true;
                        }
                    }

                    if($update_done){
                        if($update){
                            $response['stat'] = 'success';
                            $response['stat_title'] = 'Success';
                            $response['stat_msg'] = 'Category has been successfully updated';
                            $response['id'] = $update->cat_id;
                            $response['name'] = ucwords($update->cat_name);
                            $response['type'] = ucwords($update->apps_type);
                            $response['remarks'] = ($update->cat_remarks) ? $update->cat_remarks : "---";
                            
                            //Add Logs
                            $desc = 'updated an category ['.$update->cat_name.']';
                            Helpers::add_activity_logs(['category-edit',$category_id, $desc]);
                        }else{
                            $response['stat'] = 'error';
                            $response['stat_title'] = 'Error';
                            $response['stat_msg'] = 'Unable to update category. Error occured during process. Error Code: ErCATU001';
                        }
                    }
                }else{
                    $response['stat'] = 'error';
                    $response['stat_title'] = 'Error';
                    $response['stat_msg'] = 'Unable to update. Invalid reference ID or site might be deleted. Error Code: ErSTEU002';
                }
            }
        }

        return $response;
    }

    public static function update($id, Request $request)
    {
        $user_id = Auth::user()->user_id;
        $timezone = Options::apps('timezone');
        $response = [];

        $update = Categories::find($id);
        $update->cat_name = ucwords($request->category_name);
        $update->cat_remarks = ucfirst($request->category_remarks);
        $update->apps_type = $request->type;
        $update->updated_by = $user_id;
        $update->save();

        return $update;
    }

    public function delete(Request $request)
    {   
        if(Auth::user()->record_stat==1||Auth::user()->is_deleted==1){
            Auth::logout();
            Session::flush();
        }
        
        //$id = $request->input('id');
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $_log_desc = '';

        if($request->apps=='asset'&&!Helpers::get_user_privilege('admin_cat_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else if($request->apps=='article'&&!Helpers::get_user_privilege('admin_km_article_del')){
            $response['stat'] = 'error';
            $response['stat_title'] = 'Action Denied';
            $response['stat_msg'] = config('constant.access_denied_msg');
        }else{
            $x = [];
            $s = [];
            foreach($request->id as $id){
                //$asset_record = Assets::find($id);
                
                $check_record = Categories::where('account_id',$account_id)->where('cat_id',$id)->count();
                if($check_record!=0){
                    $catogory = Categories::find($id)->cat_name;
                    $delete_record = Categories::find($id)->delete();
                    
                                    //To check if category is deleted
                    $check_record = Categories::where('account_id',$account_id)->where('cat_id',$id)->count();

                    if($check_record==0){
                        $s[] = $id;

                        //Delete Custom Values
                        $delete_asset_custom_values = AssetCustomValues::where('cat_id',$id)->delete();

                        //Remove category foreign key of all assets 
                        $update_asset = Assets::where('account_id', $account_id)->where('cat_id', $id)->where('record_stat', 0)->update(['cat_id' => 0,'updated_by' => $user_id]);
                        //$update_inout = AssetInOutItems::where('account_id', $account_id)->where('cat_id', $id)->update(['cat_id' => '']);
                        //$update_lease = AssetLeaseItems::where('account_id', $account_id)->where('cat_id', $id)->update(['cat_id' => '']);
                        $_log_desc .= $catogory . ', ';
                    }else{
                        $x[] = $id;
                    }
                }else{
                    $x[] = $id;
                }
            }

            if($_log_desc){
                $desc = 'deleted an category ['.substr($_log_desc, 0, -2) .']';
                Helpers::add_activity_logs(['asset-category-delete',0,$desc]);
            }

            $response['eid'] = $x;
            $response['sid'] = $s;
        }

        return $response;
    }
}
