<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'sys_categories';
    protected $primaryKey = 'cat_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function custom_field_links()
    {
        return $this->hasMany('App\modules\settings\models\CustomFieldLinks','ref_id','cat_id');
    }
}
