<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class UserGroupPrivileges extends Model
{
    protected $table = 'sys_user_group_privileges';
    protected $primaryKey = 'privilege_id';
    public $timestamps = false;
}
