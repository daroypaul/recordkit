<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'sys_employees';
    protected $primaryKey = 'employee_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }

    public function site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','site_id')->where('record_stat', 0);
    }

    public function department()
    {
        return $this->hasOne('App\modules\adminprofile\models\Departments','department_id','department_id');
    }
}