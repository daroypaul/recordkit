<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = 'sys_site_location';
    protected $primaryKey = 'location_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','site_id');
    }
    
    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }
}
