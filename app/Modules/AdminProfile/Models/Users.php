<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'sys_users';
    protected $primaryKey = 'user_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function group()
    {
        return $this->hasOne('App\modules\adminprofile\models\UserGroups','user_group_id','user_group_id');
    }

    public function site()
    {
        return $this->hasOne('App\modules\adminprofile\models\Site','site_id','site_id');
    }
}