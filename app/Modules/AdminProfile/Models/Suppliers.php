<?php

namespace App\modules\adminprofile\models;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    protected $table = 'sys_suppliers';
    protected $primaryKey = 'supplier_id';
    const CREATED_AT = 'date_recorded';
    const UPDATED_AT = 'date_updated';

    public function recorder()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','recorded_by');
    }

    public function updator()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','updated_by');
    }
}
