<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\modules\adminprofile\models\UserGroups;
use App\modules\settings\models\CompanyInformation;

//use App\Utilities\GetOptions;
use View;
use Options;
use Menus;
use App;
USE Blade;

use Helpers;#Custom Helper Class

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //
        Schema::defaultStringLength(191);
        Resource::withoutWrapping();

        //$account_id = Auth::user()->account_id;
         
            View::composer('*', function($view) {
                if(DB::connection()->getDatabaseName())
                {
                    $id = (Auth::check()) ? Auth::user()->account_id : 0;
                    $user_id = (Auth::check()) ? Auth::id() : 0;
                    $user_group_id = (Auth::check()) ? Auth::user()->user_group_id : 0;
                    
                    $view->with('system_options', Options::apps());
                    $view->with('all_options', Options::get_all());
                    $view->with('menu_list', Options::get_available_menu());

                    $view->with('nav_menu', Menus::get_nav_menu());
                    $view->with('nav_menu_sidemini', Menus::get_sidemini_nav_menu());

                    $view->with('company_info', CompanyInformation::where('account_id', $id)->first());

                    if(Auth::check()){
                        $record = UserGroups::where('account_id', $id)->where('user_group_id', $user_group_id)->first();
                        $privileges = ($record) ? json_decode($record->group_privileges) : [];

                        $view->with('user_privileges', $privileges);
                    }
                }
                //$post = Posts::with(['recorder','updator','category'])->where('account_id',  $account_id)->orderBy('date_recorded', 'desc')->limit(2)->get();
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      
    }
}
