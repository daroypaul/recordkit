<?php

namespace App\utilities;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
    protected $table = 'sys_data_currency';
    protected $primaryKey = 'currency_id';
    public $timestamps = false;
}
