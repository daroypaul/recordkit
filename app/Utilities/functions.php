<?php
Use Illuminate\Support\Facades\Config;

if (!function_exists('custom_asset')) {
    function custom_asset($path, $secure = null){
        $base = (Config::get('constant.apps_setup')&&Config::get('constant.apps_setup')=='root') ? 'public/' : '/';
        return app('url')->asset($base . $path, $secure);
    } 
}

?>