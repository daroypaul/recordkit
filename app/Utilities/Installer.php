<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Utilities;


use Artisan;
use Config;
use DB;
use File;

class Installer
{
    public static function checkServerRequirements()
    {
        $requirements = array();

        if (ini_get('safe_mode')) {
            //$requirements[] = trans('install.requirements.disabled', ['feature' => 'Safe Mode']);
            $requirements[] = ['msg' => '<b>safe_mode</b> feature is enabled','type' => 'Feature'];
        }

        if (ini_get('register_globals')) {
            //$requirements[] = trans('install.requirements.disabled', ['feature' => 'Register Globals']);
            $requirements[] = ['msg' => '<b>register_globals</b> feature is enabled','type' => 'Feature'];
        }

        if (ini_get('magic_quotes_gpc')) {
            //$requirements[] = trans('install.requirements.disabled', ['feature' => 'Magic Quotes']);
            $requirements[] = ['msg' => '<b>magic_quotes_gpc</b> feature is enabled','type' => 'Feature'];
        }

        if (!ini_get('file_uploads')) {
            //$requirements[] = trans('install.requirements.enabled', ['feature' => 'File Uploads']);
            $requirements[] = ['msg' => '<b>file_uploads</b> feature is disabled','type' => 'Feature'];
        }

        if (!class_exists('PDO')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'MySQL PDO']);
            $requirements[] = ['msg' => '<b>PDO</b> extension is not installed','type' => 'Extension'];
        }

        if (!extension_loaded('openssl')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'OpenSSL']);
            $requirements[] = ['msg' => '<b>openssl</b> extension is not loaded','type' => 'Extension'];
        }

        if (!extension_loaded('tokenizer')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'Tokenizer']);
            $requirements[] = ['msg' => '<b>tokenizer</b> extension is not loaded','type' => 'Extension'];
        }

        if (!extension_loaded('mbstring')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'mbstring']);
            $requirements[] = ['msg' => '<b>mbstring</b> extension is not loaded','type' => 'Extension'];
        }

        if (!extension_loaded('curl')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'cURL']);
            $requirements[] = ['msg' => '<b>curl</b> extension is not loaded','type' => 'Extension'];
        }

        if (!extension_loaded('xml')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'XML']);
            $requirements[] = ['msg' => '<b>xml</b> extension is not loaded','type' => 'Extension'];
        }

        if (!extension_loaded('zip')) {
            //$requirements[] = trans('install.requirements.extension', ['extension' => 'ZIP']);
            $requirements[] = ['msg' => '<b>zip</b> extension is not loaded','type' => 'Extension'];
        }

        if (!is_writable(base_path('storage/app'))) {
            //$requirements[] = trans('install.requirements.directory', ['directory' => 'storage/app']);
            $requirements[] = ['msg' => '<b>storage/app</b> directory is read only mode or not existed','type' => 'Directory'];
        }

        if (!is_writable(base_path('storage/app/public'))) {
            //$requirements[] = trans('install.requirements.directory', ['directory' => 'storage/app/uploads']);
            $requirements[] = ['msg' => '<b>storage/app/public</b> directory is read only mode or not existed','type' => 'Directory'];
        }

        if (!is_writable(base_path('storage/framework'))) {
            //$requirements[] = trans('install.requirements.directory', ['directory' => 'storage/framework']);
            $requirements[] = ['msg' => '<b>storage/framework</b> directory is read only mode or not existed','type' => 'Directory'];
        }

        if (!is_writable(base_path('storage/logs'))) {
            //$requirements[] = trans('install.requirements.directory', ['directory' => 'storage/logs']);
            $requirements[] = ['msg' => '<b>storage/logs</b> directory is read only mode or not existed','type' => 'Directory'];
        }

        return $requirements;
    }


	public static function createDefaultEnvFile()
	{
        // Rename file
        if (is_file(base_path('.env.example'))) {
            File::move(base_path('.env.example'), base_path('.env'));
        }

        // Update .env file
        static::updateEnv([
            'APP_KEY'   =>  'base64:'.base64_encode(random_bytes(32)),
            'APP_URL'   =>  url('/'),
            'APP_ONSETUP' => 'true',
        ]);
    }

    public static function createDbTables($host, $port, $database, $username, $password)
    {
        if (!static::isDbValid($host, $port, $database, $username, $password)) {
            return false;
        }

        // Set database details
        static::saveDbVariables($host, $port, $database, $username, $password);

        // Try to increase the maximum execution time
        //set_time_limit(600); // 5 minutes
        ini_set('max_execution_time', '-1');
        ini_set('memory_limit', '-1');

        // Create tables
        Artisan::call('migrate', ['--force' => true]);

        // Create Roles
        //Artisan::call('db:seed', ['--class' => 'Database\Seeds\Roles', '--force' => true]);

        //Artisan::call('db:seed');

        return true;
    }

    /**
     * Check if the database exists and is accessible.
     *
     * @param $host
     * @param $port
     * @param $database
     * @param $host
     * @param $database
     * @param $username
     * @param $password
     *
     * @return bool
     */
    public static function isDbValid($host, $port, $database, $username, $password)
    {
        Config::set('database.connections.install_test', [
            'host'      => $host,
            'port'      => $port,
            'database'  => $database,
            'username'  => $username,
            'password'  => $password,
            'driver'    => env('DB_CONNECTION', 'mysql'),
            'charset'   => env('DB_CHARSET', 'utf8mb4'),
        ]);

        try {
            DB::connection('install_test')->getPdo();
        } catch (\Exception $e) {;
            return false;
        }

        // Purge test connection
        DB::purge('install_test');

        return true;
    }

    public static function saveDbVariables($host, $port, $database, $username, $password)
    {
        //$prefix = strtolower(str_random(3) . '_');
        $prefix = "sys_";

        // Update .env file
        static::updateEnv([
            'DB_HOST'       =>  $host,
            'DB_PORT'       =>  $port,
            'DB_DATABASE'   =>  $database,
            'DB_USERNAME'   =>  $username,
            'DB_PASSWORD'   =>  $password,
            //'DB_PREFIX'     =>  $prefix,
        ]);

        $con = env('DB_CONNECTION', 'mysql');

        // Change current connection
        $db = Config::get('database.connections.' . $con);

        $db['host'] = $host;
        $db['database'] = $database;
        $db['username'] = $username;
        $db['password'] = $password;
        //$db['prefix'] = $prefix;

        Config::set('database.connections.' . $con, $db);

        DB::purge($con);
        DB::reconnect($con);
    }
    
    public static function updateEnv($data)
    {
        if (empty($data) || !is_array($data) || !is_file(base_path('.env'))) {
            return false;
        }

        $env = file_get_contents(base_path('.env'));

        $env = explode("\n", $env);

        foreach ($data as $data_key => $data_value) {
            foreach ($env as $env_key => $env_value) {
                $entry = explode('=', $env_value, 2);

                // Check if new or old key
                if ($entry[0] == $data_key) {
                    $env[$env_key] = $data_key . '=' . $data_value;
                } else {
                    $env[$env_key] = $env_value;
                }
            }
        }

        $env = implode("\n", $env);

        file_put_contents(base_path('.env'), $env);

        return true;
    }
}
