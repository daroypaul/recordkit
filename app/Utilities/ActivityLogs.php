<?php

namespace App\utilities;

use Illuminate\Database\Eloquent\Model;

class ActivityLogs extends Model
{
    protected $table = 'sys_activity_logs';
    protected $primaryKey = 'activity_id';
    public $timestamps = false;


    public function action_by()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','action_by');
    }

    public function user()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','action_by');
    }
}
