<?php

namespace App\utilities;

use Illuminate\Database\Eloquent\Model;

class TimeZones extends Model
{
    protected $table = 'sys_data_timezone';
    protected $primaryKey = 'timezone_id';
    public $timestamps = false;
}
