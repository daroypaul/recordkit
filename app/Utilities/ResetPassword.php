<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $table = 'sys_reset_password';
    protected $primaryKey = 'request_id';
    public $timestamps = false;
}
