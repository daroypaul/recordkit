<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\utilities\TimeZones;
use App\utilities\Currencies;

use Options;

class GetOptions extends Model
{
    protected $table = 'sys_option';
    protected $primaryKey = 'option_id';
    public $timestamps = false;

    public static function apps($option='')
    {

        $account_id = (Auth::check()) ? Auth::user()->account_id : 0;


        $timezone = DB::table('sys_option')
                        ->join('sys_data_timezone', 'sys_data_timezone.timezone_id', '=', 'sys_option.option_value')
                        ->where('sys_option.account_id', $account_id)
                        ->where('option_name', 'apps_timezone')
                        ->first();
        $currency = DB::table('sys_option')
                        ->join('sys_data_currency', 'sys_data_currency.currency_id', '=', 'sys_option.option_value')
                        ->where('sys_option.account_id', $account_id)
                        ->where('option_name', 'apps_currency')
                        ->first();
        $skins = DB::table('sys_option')
                        ->where('sys_option.account_id', $account_id)
                        ->where('option_name', 'apps_skins')
                        ->first();

        if($option=='timezone'){
            $data = $timezone;
        }else if($option=='currency'){
            $data = $currency;
        }else if($option=='skins'){
            $data = $skins->option_value;
        }else{
            $data = new \StdClass();
            $data->timezones = $timezone;
            $data->currency = $currency;
            $data->option_skins = (Auth::check()) ? $skins->option_value : 'default';
        }

        return $data;
    }

    # $return_type = object or array #
    public static function get_available_menu($categorize=true, $return_type='object')
    {
        $account_id = (Auth::check()) ? Auth::user()->account_id : 0;
        $get_menu = Options::where('account_id', $account_id)->where('option_name', 'menu_option')->get();

        if($return_type == 'object'){
            $menu = new \StdClass();
        }else if($return_type == 'array'){
            $menu = [];
        }

        foreach($get_menu as $list){
            $convert_to_array = unserialize($list->option_value);

            $parent = $convert_to_array['parent_menu'];
            $caption = $convert_to_array['caption'];
            $url = $convert_to_array['url'];
            
            if($return_type === 'object'){
                $menu->$parent[] = (object) $convert_to_array;
            }else if($return_type === 'array'){
                $menu[$parent][] = $convert_to_array;
            }
        }

        if($categorize){
            return $menu;
        }else{
            return $get_menu;
        }
    }

    public static function get_all($target_name=false)
    {
        $account_id = (Auth::check()) ? Auth::user()->account_id : 0;
        $data = Options::where('account_id', $account_id);

        if($target_name){
            $data = $data->where('option_name', $target_name);
        }

        $data = $data->get();

        $menu = new \StdClass();

        foreach($data as $list){
            $name = $list->option_name;

            $item = new \StdClass();
            $item->id = $list->option_id;
            $item->name = $list->option_name;
            $item->value = $list->option_value;

            $menu->$name[] = $item;
        }


        return $menu;
    }
}
