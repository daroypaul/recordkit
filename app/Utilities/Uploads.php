<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Uploads extends Model
{
    protected $table = 'sys_uploads';
    protected $primaryKey = 'upload_id';
    const CREATED_AT = 'date_uploaded';
    public $timestamps = false;

    public function users()
    {
        return $this->hasOne('App\modules\adminprofile\models\Users','user_id','uploaded_by');
    }
}
