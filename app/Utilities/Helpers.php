<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Utilities;

use Illuminate\Support\Facades\Auth;
use App\modules\adminprofile\models\Users;
use App\modules\adminprofile\models\UserGroups;
use App\modules\settings\models\SystemOptions;
use ActivityLogs;
use Carbon\Carbon;

class Helpers
{

    public static function add_activity_logs($option=[]){
        //$option[0];
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_fullname = Auth::user()->user_fullname;

        //Add Logs
        $new_logs = new ActivityLogs;
        $new_logs->account_id = $account_id;
        $new_logs->action_type = $option[0];
        $new_logs->action_ref_id = $option[1];
        $new_logs->activity_desc = $user_fullname . ' '.$option[2];
        $new_logs->activity_date = Carbon::now('UTC')->toDateTimeString();
        $new_logs->action_by = $user_id;
        $new_logs->save();
    }

    public static function get_file_info($name, Request $request){
        $file = $request->file($name);

        $image_filename_with_extension = $file->getClientOriginalName();
        $image_fileName = pathinfo($image_filename_with_extension, PATHINFO_FILENAME);
        $image_fileExt = $file->getClientOriginalExtension();
        $image_size = $file->getSize();


        $b['name_with_ext'] = $image_filename_with_extension;
        $b['name'] = $image_fileName;
        $b['ext'] = $image_fileExt;
        $b['size'] = $image_size;
        $a = $b;

        return $a;
    }

    public static function system_options($option){
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $record = SystemOptions::with('currency')->where('account_id', $account_id)->first();

        return $record->$option;
    }

    public static function get_user_privilege($code)
    {
        $account_id = Auth::user()->account_id;
        $user_id = Auth::user()->user_id;
        $user_group_id = Auth::user()->user_group_id;

        $record = UserGroups::where('account_id', $account_id)->where('user_group_id', $user_group_id)->first();
        

        if(count($record)!=0){
            $privileges = json_decode($record->group_privileges);
            if(in_array($code, $privileges)||in_array('all', $privileges)){
                $x = 1;
            }else{
                $x = 0;
            }
        }else{
            $x = 0;
        }

        return $x;
    }

    public static function converts($key, $val)

    {

        if($key=='currency_to_int'){
            return preg_replace("/([^0-9\\.])/i", "", $val);
        }

    }

    public static function remove_whitespaces($val)
    {
        return preg_replace('/\s+/', ' ', $val);
    }

    public static function generate_key($key='', $type='md5'){
        if($type=="md5"){
            $result = ($key) ? md5(uniqid(rand(), true)) : md5(uniqid($key, true));
            //$result = md5(uniqid($key, true));
        }


        return $result;
    }

    /*public static function random_string($length = 10){
        $characters = '01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for($i=0; $i < $length; $i++){
            $randomString .= $characters[rand(0, $charactersLength -1)];
        }

        return $randomString;
    }*/

    public static function generate_string($length = 10) {
        return substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }


    public static function file_types($type=''){
        $file_types = [
            'pdf'=>[
                'ext'=>'pdf',
                'name'=>'PDF',
                'icon'=>'fa  fa-file-pdf-o',
            ],
            'docx'=>[
                'ext'=>'docx',
                'name'=>'Word Document',
                'icon'=>'fa fa-file-word-o',
            ],
            'doc'=>[
                'ext'=>'doc',
                'name'=>'Word',
                'icon'=>'fa fa-file-word-o',
            ],
            'xlsx'=>[
                'ext'=>'xlsx',
                'name'=>'Spread Sheet',
                'icon'=>'fa fa-file-excel-o',
            ],
            'xls'=>[
                'ext'=>'xls',
                'name'=>'Spread Sheet',
                'icon'=>'fa fa-file-excel-o',
            ],
            'pptx'=>[
                'ext'=>'pptx',
                'name'=>'Powerpoint',
                'icon'=>'fa fa-file-powerpoint-o',
            ],
            'ppt'=>[
                'ext'=>'ppt',
                'name'=>'Powerpoint',
                'icon'=>'fa fa-file-powerpoint-o',
            ],
            'rar'=>[
                'ext'=>'rar',
                'name'=>'Archive',
                'icon'=>'fa fa-file-zip-o',
            ],
            'zip'=>[
                'ext'=>'zip',
                'name'=>'Compress',
                'icon'=>'fa fa-file-zip-o',
            ],
            'txt'=>[
                'ext'=>'txt',
                'name'=>'Text',
                'icon'=>'fa fa-file-text-o',
            ],
            'general'=>[
                'ext'=>'txt',
                'name'=>'Text',
                'icon'=>'fa fa-file-o',
            ],
        ];

        if($type){
            if(!count(array_get($file_types, $type))){
                $response = array_get($file_types, 'general');
            }else{
                
                $response = array_get($file_types, $type);
            }
        }else{
            $response = $file_types;
        }



        return $response;
    }

}