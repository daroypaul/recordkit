<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;

class Migrations extends Model
{
    protected $table = 'migrations';
    //protected $primaryKey = 'menu_id';
    public $timestamps = false;
}
