<?php
/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\modules\adminprofile\models\UserGroups;

use Menus;
use URL;

class GetMenus extends Model
{
    protected $table = 'sys_menus';
    protected $primaryKey = 'menu_id';
    public $timestamps = false;


    public static function get_sidemini_nav_menu()
    {
        ######################
        # Dynamic Menu
        ####################
        $account_id = (Auth::check()) ? Auth::user()->account_id : 0;
        $user_group_id = (Auth::check()) ? Auth::user()->user_group_id : 0;

        $group_privileges = UserGroups::where('account_id', $account_id)->where('user_group_id', $user_group_id)->first();
        $user_privileges = ($group_privileges) ? json_decode($group_privileges->group_privileges) : [];

        $get_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->get();

        $list = '';

        $xx = '';
        $bb = '';
        
        
        foreach($get_menu as $menu)
        {
            $menu_id = $menu->menu_id;
            $top_menu_priv = ($menu->privilege_key) ? json_decode($menu->privilege_key) : [];

            $get_level_one_menu = Menus::where('account_id', $account_id)->where('parent_menu', $menu_id)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->get();
            $get_level_one_menu_count = Menus::where('account_id', $account_id)->where('parent_menu', $menu_id)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->count();
            $bb .= "====<br/>";

            foreach($top_menu_priv as $aa){
                if(in_array($aa, $user_privileges) || $aa=='exempt')
                {
                    if(!$get_level_one_menu_count){
                        $list .= '<li class="main-item"><a class="waves-effect waves-dark" href="' . URL::to('/' . $menu->menu_url) .'"><i class="'.$menu->menu_icon.'"></i>
                        <span class="hide-menu">' . $menu->menu_label . '</span></a></li>';
                    }
                    else {
                        //Level 1 Menu
                        $list .= '<li class="main-item"> 
                                    <a class="has-arrow waves-effect waves-dark" href="' . URL::to('/' . $menu->menu_url) .'" aria-expanded="false">
                                        <i class="'.$menu->menu_icon.'"></i>
                                        <span class="hide-menu">' . $menu->menu_label . '</span>
                                    </a>
                                
                                    <ul aria-expanded="false" class="collapse">';

                                    if($get_level_one_menu)
                                    {
                                        foreach($get_level_one_menu as $level_one)
                                        {
                                            $level_one_menu_id = $level_one->menu_id;
                                            $level_one_priv = ($level_one->privilege_key) ? json_decode($level_one->privilege_key) : [];
                
                                            $get_level_two_menu = Menus::where('account_id', $account_id)
                                                                    ->where('parent_menu', $level_one_menu_id)
                                                                    ->where('is_disable', '<>', 1)
                                                                    ->orderBy('menu_order', 'asc')->get();
                                            $get_level_two_menu_count = Menus::where('account_id', $account_id)
                                                                            ->where('parent_menu', $level_one_menu_id)
                                                                            ->where('is_disable', '<>', 1)
                                                                            ->orderBy('menu_order', 'asc')->count();
                
                                            foreach($level_one_priv as $bb)
                                            {
                                                if(in_array($bb, $user_privileges) || $bb=='exempt')
                                                {   
                                                    if($get_level_two_menu_count){
                                                        $list .= '<li> <a class="has-arrow waves-effect waves-dark" href="' . URL::to('/' . $level_one->menu_url) .'" aria-expanded="false"><i class="'.$level_one->menu_icon.'"></i> ' . $level_one->menu_label . '</a>';
                                                                    
                                                        $list .= '<ul aria-expanded="false" class="collapse">';

                                                        foreach($get_level_two_menu as $level_two){
                                                            $level_one_menu_id = $level_two->menu_id;
                                                            $level_two_priv = ($level_two->privilege_key) ? json_decode($level_two->privilege_key) : [];
                                        
                                                            $get_level_three_menu = Menus::where('account_id', $account_id)
                                                                                        ->where('parent_menu', $level_one_menu_id)
                                                                                        ->where('is_disable', '<>', 1)
                                                                                        ->orderBy('menu_order', 'asc')->get();
                                                            $get_level_three_menu_count = Menus::where('account_id', $account_id)
                                                                                            ->where('is_disable', '<>', 1)
                                                                                            ->where('parent_menu', $level_one_menu_id)
                                                                                            ->orderBy('menu_order', 'asc')->count();
                
                                                            foreach($level_one_priv as $bb)
                                                            {
                                                                if(in_array($bb, $user_privileges) || $bb=='exempt')
                                                                {

                                                                    $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $level_two->menu_url) .'" aria-expanded="false"><i class="'.$level_two->menu_icon.'"></i> ' . $level_two->menu_label . '</a>';
                                                                    //Level 3 Menu
                                                                    
                                                                    break;
                                                                }//End of IF Statement
                                                            }//End of Forloop
                                                        }//End of Forloop of $get_level_two_menu
                                                        
                                                        $list .= "</ul></li>";
                                                    }else{
                                                        $list .= '<li><a href="' . URL::to('/' . $level_one->menu_url) .'"><i class="'.$level_one->menu_icon.'"></i> ' . $level_one->menu_label . '</a></li>';
                                                    }


                                                    break;
                                                }
                                            }
                                        }

                                    }

                        $list .= "  </ul>
                                </li>";
                        //Level 1 Menu
                    }

                    break;
                }//End of If
            }//End of Forloop
        }//End of Forloop of $get_menu

        return $list;
    }

    public static function get_nav_menu()
    {
        ######################
        # Dynamic Menu
        ####################
        $account_id = (Auth::check()) ? Auth::user()->account_id : 0;
        $user_group_id = (Auth::check()) ? Auth::user()->user_group_id : 0;

        $group_privileges = UserGroups::where('account_id', $account_id)->where('user_group_id', $user_group_id)->first();
        $user_privileges = ($group_privileges) ? json_decode($group_privileges->group_privileges) : [];

        $get_menu = Menus::where('account_id', $account_id)->where('parent_menu', 0)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->get();

        $list = '';

        $xx = '';
        $bb = '';
        
        foreach($get_menu as $menu)
        {
            $menu_id = $menu->menu_id;
            $top_menu_priv = ($menu->privilege_key) ? json_decode($menu->privilege_key) : [];

            $get_level_one_menu = Menus::where('account_id', $account_id)->where('parent_menu', $menu_id)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->get();
            $get_level_one_menu_count = Menus::where('account_id', $account_id)->where('parent_menu', $menu_id)->where('is_disable', '<>', 1)->orderBy('menu_order', 'asc')->count();
            $bb .= "====<br/>";

            foreach($top_menu_priv as $aa){
                if(in_array($aa, $user_privileges) || $aa=='exempt')
                {
                    //Level 1 Menu
                    if($get_level_one_menu_count){
                        $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $menu->menu_url) .'" aria-expanded="false">'.
                                '<i class="'.$menu->menu_icon.'"></i><span class="hide-menu">' . $menu->menu_label . '</span></a>';
                    }else{
                        $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $menu->menu_url) .'" aria-expanded="false">'.
                                '<i class="'.$menu->menu_icon.'"></i><span class="hide-menu">' . $menu->menu_label . '</span></a></li>';
                    }//End of Else
                    
                    
                    $list .= '<ul aria-expanded="false" class="collapse">';

                    if($get_level_one_menu)
                    {
                        foreach($get_level_one_menu as $level_one)
                        {
                            $level_one_menu_id = $level_one->menu_id;
                            $level_one_priv = ($level_one->privilege_key) ? json_decode($level_one->privilege_key) : [];

                            $get_level_two_menu = Menus::where('account_id', $account_id)
                                                    ->where('parent_menu', $level_one_menu_id)
                                                    ->where('is_disable', '<>', 1)
                                                    ->orderBy('menu_order', 'asc')->get();
                            $get_level_two_menu_count = Menus::where('account_id', $account_id)
                                                            ->where('parent_menu', $level_one_menu_id)
                                                            ->where('is_disable', '<>', 1)
                                                            ->orderBy('menu_order', 'asc')->count();

                            foreach($level_one_priv as $bb){
                                if(in_array($bb, $user_privileges) || $bb=='exempt')
                                {
                                    //Level 2 Menu
                                    if($get_level_two_menu_count){
                                        $list .= '<li> <a class="has-arrow waves-effect waves-dark" href="' . URL::to('/' . $level_one->menu_url) .'" aria-expanded="false">'.
                                                '<i class="'.$level_one->menu_icon.'"></i><span class="hide-menu">' . $level_one->menu_label . '</span></a>';
                                    }else{
                                        $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $level_one->menu_url) .'" aria-expanded="false">'.
                                                '<i class="'.$level_one->menu_icon.'"></i><span class="hide-menu">' . $level_one->menu_label . '</span></a></li>';
                                    }

                                    
                                    $list .= '<ul aria-expanded="false" class="collapse">';

                                    
                                    if($get_level_two_menu){
                                        foreach($get_level_two_menu as $level_two){
                                            $level_one_menu_id = $level_two->menu_id;
                                            $level_two_priv = ($level_two->privilege_key) ? json_decode($level_two->privilege_key) : [];
                        
                                            $get_level_three_menu = Menus::where('account_id', $account_id)
                                                                        ->where('parent_menu', $level_one_menu_id)
                                                                        ->where('is_disable', '<>', 1)
                                                                        ->orderBy('menu_order', 'asc')->get();
                                            $get_level_three_menu_count = Menus::where('account_id', $account_id)
                                                                            ->where('is_disable', '<>', 1)
                                                                            ->where('parent_menu', $level_one_menu_id)
                                                                            ->orderBy('menu_order', 'asc')->count();

                                            foreach($level_one_priv as $bb)
                                            {
                                                if(in_array($bb, $user_privileges) || $bb=='exempt')
                                                {
                                                    //Level 3 Menu
                                                    if($get_level_three_menu_count){
                                                        $list .= '<li> <a class="has-arrow waves-effect waves-dark" href="' . URL::to('/' . $level_two->menu_url) .'" aria-expanded="false">'.
                                                                '<i class="'.$level_two->menu_icon.'"></i><span class="hide-menu">' . $level_two->menu_label . '</span></a>';
                                                    }else{
                                                        $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $level_two->menu_url) .'" aria-expanded="false">'.
                                                                '<i class="'.$level_two->menu_icon.'"></i><span class="hide-menu">' . $level_two->menu_label . '</span></a></li>';
                                                    }
                                
                                                    
                                                    $list .= '<ul aria-expanded="false" class="collapse">';

                                                    if($get_level_three_menu){
                                                        foreach($get_level_three_menu as $level_three){
                                                            $level_one_menu_id = $level_two->menu_id;
                                        
                                                            $list .= '<li> <a class="waves-effect waves-dark" href="' . URL::to('/' . $level_three->menu_url) .'" aria-expanded="false">'.
                                                                        '<i class="'.$level_three->menu_icon.'"></i><span class="hide-menu">' . $level_three->menu_label . '</span></a></li>';
                                                        }//End of Forloop of $get_level_three_menu
                                                    }//End of If
                                
                                                    if($get_level_two_menu_count){
                                                        $list .= "</ul></li>";
                                                    }else{
                                                        $list .= "</ul>";
                                                    }
                                                    //Level 3 Menu
                                                    
                                                    break;
                                                }//End of IF Statement
                                            }//End of Forloop
                                        }//End of Forloop of $get_level_two_menu
                                    }//End of IF

                                    if($get_level_two_menu_count){
                                        $list .= "</ul></li>";
                                    }else{
                                        $list .= "</ul>";
                                    }
                                    //End of Level 2 Menu

                                    break;
                                }//End of IF Statement
                            }//End of Forloop
                        }//End of Forloop of $get_level_one_menu
                    }//End of If

                    if($get_level_one_menu_count){
                        $list .= "</ul></li>";
                    }else{
                        $list .= "</ul>";
                    }
                    //Level 1 Menu

                    break;
                }//End of If
            }//End of Forloop
        }//End of Forloop of $get_menu

        return $list;
    }
}