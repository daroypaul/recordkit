<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use File;

class Packages
{
    public static function data($package=''){
        $modules_dir = static::scan_packages();
        
        $modules_data = [];
        $json_arr = [];
        $i = 0;

        foreach($modules_dir as $dir){
            $json_file_dir = File::glob($dir . '/package.json')[0];
            
            $json_file_base = base_path($json_file_dir);

            $get_data = file_get_contents($json_file_base);

            //Decode JSON
            $json_data[] = json_decode($get_data, true);
            $name = strtolower($json_data[$i]['name']);

            if($package){
                if($package==$name){
                    $modules_data = $json_data[$i];
                    break;
                }
            }else{
                $modules_data[$name] = $json_data[$i];
            }
            
            $i++; 
        }

        return $modules_data;
    }

    public static function scan_packages(){
        $modules_dir = array_filter(glob('app/packages/*'), 'is_dir');

        return array_map('strtolower', $modules_dir);
    }

    public static function scan_modules(){
        $modules_dir = array_filter(glob('app/modules/*'), 'is_dir');

        return array_map('strtolower', $modules_dir);
    }

    public static function scan_folders(){
        $scan_folder = array_map('strtolower', array_filter(glob('app/packages/*'), 'is_dir'));
        
        $folders = [];
        
        foreach($scan_folder as $dir){
            $explode_dir = explode('/', $dir);
            $folder = end($explode_dir);

            $folders[] = $folder;
        }

        return $folders;
    }

    public static function modify($mod_key, $target_module, $mod_val){
        $modules_dir = static::scan_packages();
        $modules_data = [];
        $json_arr = [];
        $i = 0;
        
        $target_module = (is_array($target_module)) ? array_map('strtolower', array_filter($target_module)) : [];
        $result = [];

        $i = 0;
        foreach($target_module as $module){
            $check_json_dir = in_array($module, $modules_dir);
            $modules_dir_key = array_search($module, $modules_dir);

            if($check_json_dir){
                $get_json_dir = File::glob($modules_dir[$modules_dir_key] . '/package.json')[0];//Get only specific JSON File
                $json_base_dir = base_path($get_json_dir);//Base path of specific JSON File
                $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
                $json_data = json_decode($get_json_data, true);//Decode JSON to Array

                // Updating JSON data
                foreach ($json_data as $key => $value) {
                    if($key==$mod_key){
                        $json_data[$mod_key] = $mod_val[$i];
                    }
                }

                //Update the JSON file
                $update_json_file = file_put_contents($get_json_dir, json_encode($json_data, JSON_PRETTY_PRINT));
                //$update_json_file = true;

                if($update_json_file){
                    $result_item['value'] = true;
                    $result_item['remark'] = 'JSON File Updated';

                    //$result[$target_module[$i]] = $result_item;
                    $result[] = $result_item;
                }
            }else{
                //$result_item['name'] = $target_module[$i];
                $result_item['value'] = false;
                $result_item['remark'] = 'Module not found';

                //$result[$target_module[$i]] = $result_item;
                $result[] = $result_item;
            }

            $i++;
        }

        /*foreach($modules_dir as $dir){
            $dir = strtolower($dir);

            if( ( count($target_module)!=0 && in_array($dir, $target_module) ) || count($target_module)==0 ){
                $get_json_dir = File::glob($dir . '/module.json')[0];//Get only specific JSON File
                $json_base_dir = base_path($get_json_dir);//Base path of specific JSON File
                $get_json_data = file_get_contents($json_base_dir);//Get the JSON Data
                $json_data[] = json_decode($get_json_data, true);//Decode JSON to Array
                $target_module_key = array_search($dir, $target_module);

                // Updating JSON data
                foreach ($json_data as $key => $value) {
                    if($key==$mod_key){
                        $json_data[$i][$mod_key] = $mod_val[$i];
                    }
                }

                //Update the JSON file
                //$update_json_file = file_put_contents($get_json_dir, json_encode($json_data[$i], JSON_PRETTY_PRINT));
                $update_json_file = true;

                $result_item['name'] = $target_module[$target_module_key];

                if($update_json_file){
                    $result_item['value'] = true;
                    $result_item['remark'] = '';

                    $result[] = $result_item;
                }
            }
            
            $i++; 
        }*/

        return $result;
    }
}
