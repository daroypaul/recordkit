/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function () {
    $app_dir = 'root';

    if($app_dir=='root'){
        var getUrl = window.location;
        $apps_base_url = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    }else{
        $apps_base_url = window.location.origin
    }

    $('body .dropdown-toggle').dropdown(); 

    $('body').on('focus',".date-picker-default", function(){
        $(this).datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
    });

    $('body').on('focus',".date-picker-today", function(){
        $(this).datepicker({
            format: 'MM dd, yyyy',
            autoclose: true,
            endDate:'0d'
        });
    });

    $("body").tooltip({
        selector: '[data-toggle="tooltip"]'
    });
    

    $('body').on('focus',".currency-input", function(){
        $(this).autoNumeric('init', {'set': "0.00"});
    });
    

    //Global Function
    $.notification = function notification(heading,text,position,icon,hideAfter,duplicate=2){
            $.toast({
             heading: heading,
             text: text,
             position: position,
             //loaderBg:'#999999',
             icon: icon,
             hideAfter: hideAfter,
             //hideAfter: 3000, 
             stack: duplicate,
           });
    }

    $.digits = function digits(e){
        return e.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

    $.shorten_string = function shorten_string(str, length, start='first', with_asterisk=1){

        var new_str = '';

        if(start=='first'){
            if(str.length > 10) new_str = str.substring(0,length);
        }
        
        if(with_asterisk) new_str = new_str + '...';
        
        return new_str;
    }

    $('button#btn-search-km').click(function(){
        
        //$('form#search-km-form').submit();
    });

    $('form#search-km-form').submit(function(e){
        //e.preventDefault();

        var key = $('input#input-search-km').val().trim();

        if(key){
            /*var new_link = $apps_base_url + '/kb/search?q='+key;

            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Submitting...');
            window.location.replace(new_link);*/
            true;
        }else{
            //$.notification('Error','Please enter search key','top-right','error','');
            e.preventDefault();
            $('.form-group').removeClass('has-danger');
            $('.form-control-feedback').remove();
            $('form#search-km-form [name="q"]').parents('.form-group').addClass('has-danger');
            $('form#search-km-form [name="q"]').parents('.form-group').append('<small class="form-control-feedback">Search key is required</small>');
        }
    });

    $.capitalize_first_letter = function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    $.ucwords = function ucwords(str){
        str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });

        return str;
    }
});


$.fn.digits = function(){ 
    return this.each(function(e){
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}