/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function () {
    var selected = '';//Used in Media files


    $.filter_field_get_data = function filter_field_get_data(title='Asset Warranty') {//
        $('#select-category').select2({
            placeholder : 'Select Asset Cateory',
            ajax: {
                url: $apps_base_url+'/admin/categories/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        ref: '_with_keywords',
                        apps : 'asset',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.list, function (item) {
                            return {
                                text: item.cat_name,
                                id: item.cat_id
                            }
                        })
                    };
                }
            }
        });

        $('#select-site').select2({
            placeholder : 'Select Site',
            ajax: {
                url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        ref: '_keywords',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.list, function (item) {
                            return {
                                text: item.site_name,
                                id: item.site_id
                            }
                        })
                    };
                }
            }
        }).on('select2:select', function (e) {
            $('#select-location').val(null).trigger('change');
        });


        $('#select-location').select2({
            placeholder : 'Select Location',
            ajax: {
                url: $apps_base_url+'/admin/location/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        site_id: $('#select-site').val(),
                        ref: '_keywords',
                        keywords: params.term,
                    }

                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.location_name,
                                id: item.location_id
                            }
                        })
                    };
                }
            }
        });


        $('#select-employee').select2({
            placeholder : 'Select Employee',
            ajax: {
                url: $apps_base_url+'/admin/employees/fetchdata',//$apps_base_url+'/asset/fetch/list'
                dataType: 'json',
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: function (params) {
                    var query = {
                        ref: '_keywords',
                        keywords: params.term,
                    }
    
                    return query;
                  },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.emp_fullname,
                                id: item.employee_id
                            }
                        })
                    };
                }
              }
        });
    }

    $.asset_warranty_form = function asset_warranty_form(title='Asset Warranty') {//
        $layout_form = 	'<form class="form-material" id="modal-warranty-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-xs-12 col-sm-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Months <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="months" placeholder="Warranty Length" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-sm-6 col-lg-8 form-group">'+
                                    '<label class="form-control-label text-bold">Expire <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="warranty_expire" id="modal-date-expire" placeholder="Warranty Expire" value="">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="Remarks" name="remarks"></textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-warranty-form')[0]);
                            form_data.append('asset_id', $('#_id').val());

                            $.ajax({
                                type:'POST',
                                url : $apps_base_url + '/asset/warranty/add',
                                //dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $('[data-toggle="tooltip"]').tooltip('hide');
                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-warranty-form input[name="'+i+'"], form#modal-warranty-form textarea[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-warranty-form input[name="'+i+'"], form#modal-warranty-form textarea[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            
                                                            var row = $warranty_table.row.add( {
                                                                0: data.months,
                                                                1: data.expire,
                                                                2: data.remaining,
                                                                3: data.remarks,
                                                                4: data.recorded_by,
                                                                5: '<button class="btn btn-sm btn-danger" data-action="delete" type="button" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>',
                                                            } ).draw();

                                                            

                                                            $warranty_table.rows(row).nodes().to$().attr("data-id", data.id);
                                            bootbox.hideAll();
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('#modal-input-cost').autoNumeric('init', {'set': "0.00"});
                $('#modal-date-expire').datepicker({
                    format: 'dd-M-yyyy',
                    autoclose: true,
                    title: 'Start Date',
                    startDate:'0d'
                });
            });

    }
    
    // Asset Selection Modal
    $.search_asset_modal = function search_asset_modal(title='Search Asset', ref='all', lease_start='', lease_expire='', requestor='') {//
        $layout_form = 	'<form class="form-material" id="modal-search-asset-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-sm-12 form-group m-b-10">'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="maintenance_title" placeholder="Enter any keyword to search..." id="search-key">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 form-group m-b-0" >'+
                                    '<div class="table-responsived p-r-5" id="search-result"></div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';

                        bootbox.dialog({
                            closeButton: false,
                            backdrop: true,
                            animate: true,
                            title: title,
                            message: $layout_form,
                            //size: 'large',
                            className: "modal-custom-xl",
                            buttons: {
                                done: {
                                    label: 'Add',
                                    className: "btn-success",
                                    callback: function () {
                                        var total_selected = $('input.modal-selected-selected:checked').length;
                                        var new_data_row = '';
                                        var exist_asset = '';

                                        if(total_selected!=0){
                                                $('input.modal-selected-selected:checked').each(function(e){
                                                    var id = $(this).parents('tr').data('id');
                                                    var found_id = $('#asset-row-data').find("tr[data-id='"+id+"']").length;

                                                    if(found_id==0){
                                                        new_data_row +=   '<tr data-id="'+id+'">'+
                                                                            '<td>'+$(this).parents('tr').data('tag')+'</td>'+
                                                                            '<td>'+$(this).parents('tr').data('desc')+'</td>'+
                                                                            '<td>'+$(this).parents('tr').data('model')+'</td>'+
                                                                            '<td>'+$(this).parents('tr').data('serial')+'</td>'+
                                                                            '<td>'+$(this).parents('tr').data('cat-name')+'</td>'+
                                                                            '<td>'+'<input name="input_asset_remarks[]" value="'+$(this).parents('tr').find('input#modal-asset-remarks').val()+'" class="form-control" type="text" placeholder="Item Remarks" />'+'</td>'+
                                                                            '<td><button type="button" class="btn btn-sm btn-danger" id="btn-delete-row"><i class="fa fa-times"></i></button>'+
                                                                                '<input type="hidden" name="input_asset_id[]" value="'+id+'" />'+
                                                                            '</td>'+
                                                                        '</tr>';
                                                    }else{
                                                        exist_asset += $(this).parents('tr').data('tag') + ', ';
                                                    }
                                                });

                                                exist_asset = exist_asset.slice(0,-2);

                                                if(exist_asset){
                                                    $.notification('Error','This asset tag(s): '+exist_asset+' already added on the list','top-center','error','');
                                                    return false;
                                                }else{
                                                    $('table#selected-asset-table').DataTable().destroy();
                                                    $('#asset-row-data').append(new_data_row);
                                                    $('table#selected-asset-table').DataTable({
                                                        "aoColumnDefs": [
                                                            { 'bSortable': false, 'aTargets': [ 6 ] }
                                                         ],
                                                    });
                                                    bootbox.hideAll();
                                                }

                                            
                                        }else{
                                            $.notification('Error','Select an asset.','top-center','error','');
                                        }

                                        return false;
                                    }
                                },
                                close: {
                                    label: 'Close',
                                    className: "btn-inverse",
                                }
                            }
                        }).on("shown.bs.modal", function(e) {
                            $('input#search-key').focus();
                            
                            $('#modal-search-asset-form').submit(false);


                            if(ref!='all'&&ref!='checkin'&&ref!='checkout'){
                                var form_data = new FormData();
                                form_data.append('keywords', '');
                                form_data.append('ref', ref);

                                if(ref=='lease'){
                                    url = $apps_base_url+'/asset/fetch/available-leasable';
                                    form_data.append('lease_start', lease_start);
                                    form_data.append('lease_expire', lease_expire);
                                }else if(ref=='return-lease'){
                                    url = $apps_base_url+'/asset/fetch/leased-by-requestor';
                                    form_data.append('id', requestor);

                                }

                                $('#search-result').html('<p class="text-center"><i class="fa fa-spin fa-gear" style="font-size:2em;"></i><br/>Fetching records...</p>');
                                
                                
                                var table = '';
                                $.ajax({
                                    type:'POST',
                                    url : url,
                                    //dataType: "json",
                                    data:  form_data,
                                    processData: false,
                                    contentType: false,
                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    success: function (data) {
                                        if(data.list.length!=0){
                                            table += '<table id="asset-table" class="table table-condensed" style="width:100%; font-size:0.9em;"> '+
                                                        '<thead>'+
                                                            '<tr>'+
                                                                '<th></th>'+
                                                                '<th>Image</th>'+
                                                                '<th>Tag</th>'+
                                                                '<th>Description</th>'+
                                                                '<th>Model</th>'+
                                                                '<th>Serial</th>'+
                                                                '<th>Category</th>';
                                            if(ref=='return-lease'){
                                                table +=        '<th>Current&nbsp;Leased&nbsp;By</th>';
                                            }

                                            table +=            '<th>Site</th>'+
                                                                '<th>Location</th>'+    
                                                                '<th width="200">Remarks</th>'+
                                                            '</tr>'+
                                                        '</thead>'+
                                                        '<tbody>';
                                            $.each(data.list, function(i, v) {
                                                if(v.image_name){
                                                    var img = '<img src="'+$apps_base_url+'/uploaded/file/'+v.image_name+'" class="img-responsive img-rounded" id="cover-image" alt="Asset Image" style="max-width:100px;" />';
                                                }else{
                                                    var img ='<center><i class="ti-image" style="font-size:4em; color:#aaa;"></i></center>';
                                                }

                                                var serial = (v.asset_serial) ? v.asset_serial : '<center>---</center>';
                                                var category = (v.category) ? v.category : '<code data-toggle="tooltip" title="Data might be deleted">Undefined</code>';
                                                var leased_by = (v.custodian) ? v.custodian : '<code data-toggle="tooltip" title="Data might be deleted">Undefined</code>';
                                                var site = (v.site) ? v.site : '<code data-toggle="tooltip" title="Data might be deleted">Undefined</code>';
                                                var location = (v.location) ? v.location : '<code data-toggle="tooltip" title="Data might be deleted">Undefined</code>';

                                                table +=    '<tr data-id="'+v.asset_id+'" data-tag="'+v.asset_tag+'" data-desc="'+v.asset_desc+'" data-model="'+v.asset_model+'" data-serial="'+v.asset_serial+'"  data-cat="'+v.cat_id+'"  data-cat-name="'+v.category+'"  data-site="'+v.site_id+'">'+
                                                                '<td>'+
                                                                    '<input type="checkbox" id="basic_checkbox_'+i+'" class="filled-in modal-selected-selected" value="1" />'+
                                                                    '<label for="basic_checkbox_'+i+'" class="m-b-0"></label>'+
                                                                '</td>'+
                                                                '<td>'+img+'</td>'+
                                                                '<td>'+v.asset_tag+'</td>'+
                                                                '<td>'+v.asset_desc+'</td>'+
                                                                '<td>'+v.asset_model+'</td>'+
                                                                '<td>'+serial+'</td>'+
                                                                '<td>'+category+'</td>';
                                                if(ref=='return-lease'){
                                                    table +=    '<td>'+leased_by+'</td>';
                                                }

                                                table +=        '<td>'+site+'</td>'+
                                                                '<td>'+location+'</td>'+
                                                                '<td><input type="text" class="form-control" id="modal-asset-remarks" placeholder="Remarks"></td>'+
                                                            '</tr>';
                                            });

                                            table += '</tbody></table>';

                                            
                                            $('#search-result').html('<!--<h4 class="m-b-0 m-t-20">Current leased by the requestor:</h4>-->' + table);
                                            //$('#myTable').DataTable().destroy();
                                            $('#asset-table').DataTable({
                                                "scrollX": true,
                                                "order": [[ 2, "asc" ]],
                                                "aoColumnDefs": [
                                                    { 'bSortable': false, 'aTargets': [ 0,1,7 ] }
                                                ],
                                                'searching': false,
                                            });
                                        }else{
                                            $('#search-result').html(data.stat_msg);
                                        }

                                    },
                                    error :function (data) {
                                        $.notification('Error','Internal Server Error','top-right','error','');
                                    }
                                });
                            }
                            
                            //trigger the search
                            $('input#search-key').keyup(function(e){
                                if(e.which!=37&&e.which!=38&&e.which!=39&&e.which!=40&&e.which!=9&&e.which!=16&&e.which!=17&&e.which!=18){
                                    var keyword = $.trim($(this).val());
                                    $('.modal .form-group').removeClass('has-danger');
                                    $('.modal .form-control-feedback').remove();
                                    $('#search-result').html('<p class="text-center"><i class="fa fa-spin fa-gear" style="font-size:2em;"></i><br/>Searching...</p>');

                                    if(keyword==''){
                                        $('#search-result').html('');
                                        $('input#search-key').parents('.form-group').addClass('has-danger');
                                        $('input#search-key').parents('.form-group').append('<small class="form-control-feedback">Provide a keyword</small>');
                                    }else{
                                        //if(e.which!=37||e.which!=38||e.which!=39||e.which!=40||e.which!=9){
                                            var url = '';
                                            var form_data = new FormData();
                                            form_data.append('keywords', keyword);
                                            form_data.append('ref', ref);

                                            //var param = ['keywords' : keyword,'ref' : ref}];
                                            var param = {
                                                'keywords':keyword, 'ref':ref
                                            };

                                            if(ref=='lease'){
                                                url = $apps_base_url+'/asset/fetch/available-leasable';
                                                form_data.append('lease_start', lease_start);
                                                form_data.append('lease_expire', lease_expire);

                                                param['lease_start'] = lease_start;
                                                param['lease_expire'] = lease_expire;
                                            }else if(ref=='return-lease'){
                                                url = $apps_base_url+'/asset/fetch/current-leased';
                                                form_data.append('id', requestor);

                                                param['id'] = requestor;
                                            }else{
                                                url = $apps_base_url+'/asset/fetch/all';
                                            }

                                            var table = '';
                                            $.ajax({
                                                type:'POST',
                                                url : url,
                                                data:  param,
                                                dataType: "json",
                                                //processData: false,
                                                //contentType: false,
                                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                success: function (data) {

                                                    if(data.list.length!=0){
                                                        table += '<table id="asset-table" class="table table-condensed" width="100%" style="font-size:0.9em;">'+
                                                                    '<thead>'+
                                                                        '<tr>'+
                                                                            '<th></th>'+
                                                                            '<th></th>'+
                                                                            '<th>Tag</th>'+
                                                                            '<th>Category</th>'+
                                                                            '<th>Description</th>'+
                                                                            '<th>Model</th>'+
                                                                            '<th>Serial</th>';

                                                        if(ref=='return-lease'){
                                                            table +=        '<th>Current&nbsp;Leased&nbsp;By</th>';
                                                        }

                                                        table +=            '<th>Site</th>'+
                                                                            '<th>Location</th>'+
                                                                            '<th width="200">Remarks</th>'+
                                                                        '</tr>'+
                                                                    '</thead>'+
                                                                    '<tbody>';
                                                        $.each(data.list, function(i, v) {
                                                            if(v.image_name){
                                                                var img = '<img src="'+$apps_base_url+'/uploaded/file/'+v.image_name+'" class="img-responsives img-rounded" id="cover-image" alt="Asset Image" style="max-width:100px" />';
                                                            }else{
                                                                var img ='<center><i class="ti-image" style="font-size:4em; color:#aaa;"></i></center>';
                                                            }
                                                            
                                                            var serial = (v.asset_serial) ? v.asset_serial : '<center>---</center>';
                                                            var category = (v.category!=null) ? v.category : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                                            var site = (v.site!=null) ? v.site : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                                            var location = (v.location!=null) ? v.location : '<code data-toggle="tooltip" data-original-title="Data is deleted">Undefined</code>';
                                                            var leased_by = (v.custodian) ? v.custodian : '<code data-toggle="tooltip" title="Data might be deleted">Undefined</code>';

                                                            table +=    '<tr data-id="'+v.asset_id+'" data-tag="'+v.asset_tag+'" data-desc="'+v.asset_desc+'" data-model="'+v.asset_model+'" data-serial="'+v.asset_serial+'"  data-cat="'+v.cat_id+'"  data-cat-name="'+v.category+'"  data-site="'+v.site_id+'">'+
                                                                            '<td>'+
                                                                                '<input type="checkbox" id="basic_checkbox_'+i+'" class="filled-in modal-selected-selected" value="1" />'+
                                                                                '<label for="basic_checkbox_'+i+'" class="m-b-0"></label>'+
                                                                            '</td>'+
                                                                            '<td>'+img+'</td>'+
                                                                            '<td>'+v.asset_tag+'</td>'+
                                                                            '<td>'+category+'</td>'+
                                                                            '<td>'+v.asset_desc+'</td>'+
                                                                            '<td>'+v.asset_model+'</td>'+
                                                                            '<td>'+serial+'</td>';
                                                            if(ref=='return-lease'){
                                                                table +=    '<td>'+leased_by+'</td>';
                                                            }

                                                            table +=        '<td>'+site+'</td>'+
                                                                            '<td>'+location+'</td>'+
                                                                            '<td><input type="text" class="form-control" id="modal-asset-remarks" placeholder="Remarks"></td>'+
                                                                        '</tr>';
                                                            
                                                                
                                                        });

                                                        table += '</tbody></table>';

                                                        
                                                        $('#search-result').html(table);
                                                        //$('#myTable').DataTable().destroy();
                                                        $('#asset-table').DataTable({
                                                            "scrollX": true,
                                                            "order": [[ 2, "asc" ]],
                                                            "aoColumnDefs": [
                                                                { 'bSortable': false, 'aTargets': [ 0,1,9 ] }
                                                            ],
                                                            'searching': false,
                                                        });
                                                    }else{
                                                        $('#search-result').html(data.stat_msg);
                                                    }

                                                },
                                                error :function (data) {
                                                    //$.notification('Error','Internal Server Error','top-right','error','');
                                                }
                                            });
                                        //}
                                    }
                                }
                            });
                        });
    }

    $.asset_status_modal = function asset_status_modal(ids=[], title='', opt) {//
        if(opt=='Dispose'){
            var date_label = 'Disposed Date';
            var remarks_label = 'Notes';
            var to = 'Disposed To';
            var stat = 'disposed';
        }else if(opt=='Donate'){
            var date_label = 'Donated';
            var remarks_label = 'Notes';
            var to = 'Donated To';
            var stat = 'donated';
        }else if(opt=='Lost'){
            var date_label = 'Date Lost';
            var remarks_label = 'Notes';
            var stat = 'lost';
        }else if(opt=='Sale'){
            var date_label = 'Date Lost';
            var remarks_label = 'Notes';
            var to = 'Sold To';
            var stat = 'sold';
        }else if(opt=='Damage'){
            var date_label = 'Date Damage';
            //var remarks_label = 'Notes';
            var stat = 'damage';
        }

        $layout_form = 	'<form class="form-material form-horizontal" id="modal-status-form" method="post">'+        
                            '<div class="form-group row">'+
                                '<label for="exampleInputuname3" class="col-sm-4 control-label text-right">'+date_label+' <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                '<div class="col-sm-5 in">'+
                                    '<input class="form-control" placeholder="Date" type="text" id="modal-input-date" name="date">'+
                                '</div>'+
                            '</div>';
        if(opt!='Lost'&&opt!='Damage'){
        $layout_form +=     '<div class="form-group row">'+
                                '<label for="exampleInputuname3" class="col-sm-4 control-label text-right">'+to+'</label>'+
                                '<div class="col-sm-8 in">'+
                                    '<input class="form-control" placeholder="'+to+'" type="text" name="to" id="modal-input-to">'+
                                '</div>'+
                            '</div>';
        }

        if(opt=='Sale'){
        $layout_form +=     '<div class="form-group row">'+
                                '<label for="exampleInputuname3" class="col-sm-4 control-label text-right">Amount</label>'+
                                '<div class="col-sm-5 in">'+
                                    '<input class="form-control" placeholder="Amount" type="text" id="modal-input-amount" name="amount">'+
                                '</div>'+
                            '</div>'; 
        }
        
        $layout_form +=     '<div class="form-group row m-b-0">'+
                                '<label for="exampleInputuname3" class="col-sm-4 control-label text-right">Remark</label>'+
                                '<div class="col-sm-8 in">'+
                                    '<textarea class="form-control" name="notes" placeholder="Remarks" id="modal-input-remarks"></textarea>'+
                                '</div>'+
                            '</div>';
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size : 'large',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success",
                        callback: function () {
                            $('tr').removeClass('table-danger');
                            var form_data = new FormData($('form#modal-status-form')[0]);
                            form_data.append('stat', stat);
                            $.each(ids, function(i, v){
                                form_data.append('id[]', v);
                            });

                            $.ajax({
                                type:'POST',
                                url : $apps_base_url + '/asset/bulk_action',
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $eid = [];
                                    $sid = [];
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();

                                    $.each(data.val_error,function(i,v){
                                        $('form#modal-status-form .form-control[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                        $('form#modal-status-form .form-control[name="'+i+'"]').parents('.form-group').children('.in').append('<small class="col-xs-12 form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.sid.length!=0){
                                        $.notification('Status Updated','Asset(s) successfully updated.','top-right','success','');
                                    }

                                    if(data.eid.length!=0){
                                        $.notification('Error Process','Unable to update some asset(s). See highlight row(s)','top-right','error','');
                                        $.each(data.eid,function(i,v){
                                            $("tr[data-id='"+v+"']").addClass('table-danger');
                                        });
                                        
                                        $eid = data.eid;
                                    }

                                    if(!data.val_error){
                                        $('input#select-all').prop('checked', false);
                                        $('input.selected-item:checked').prop('checked', false);
                                        $('#bulk-action-option').val('');
                                        $selected_asset = [];
                                        $status_updated = 1;
                                        bootbox.hideAll();

                                        
                                        $.each(data.sid,function(i,v){
                                            $("tr[data-id='"+v+"']").find("td[data-col='status']").text(data.stat);
                                        });

                                        $sid = data.sid;
                                        $stat_selected = data.stat;
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                //asset/bulk_action
                $('#modal-input-amount').autoNumeric('init', {'set': ""});
                $('#modal-input-date').datepicker({
                    format: 'M dd, yyyy',
                    autoclose: true,
                    title: date_label,
                    //endDate:'0d'
                });
                
                
                //form_data.append('id', id);
            });
    }


    //////////////////////
    // Admin 
    //////////////////////

    $.admin_user_form = function admin_user_form(id='', ref='', title='Add New User', append_type='table', fullname='',user_group_id='',email='',username='',remarks='', stat=0, site_id='', site_name='', group_name='') {//
        $layout_form = 	'<form class="form-material" id="modal-user-form" method="post">';
        
        $layout_form +=     '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Full Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="fullname" id="modal-input-name" placeholder="" value="'+fullname+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6 form-group">'+
                                   '<label class="form-control-label text-bold">User Group <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<select class="form-control" id="modal-select-group" name="user_group"><option value="">[ Select User Group ]</option></select>'+ 
                                   '</div>'+
                               '</div>'+
                               '<div class="col-xs-12 col-lg-6 form-group">'+
                                    '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-site" data-original-title="Add New Site"><i class="fas fa-plus"></i></button>'+
                                    '<label class="form-control-label text-bold">Site</label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control" id="modal-select-site" name="site"><option value="">- Select Site -</option></select>'+ 
                                    '</div>'+
                                '</div>'+
                               '<div class="col-lg-6 form-group">'+
                                   '<label class="form-control-label text-bold">Email <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="email" class="form-control" name="email" id="modal-input-email" placeholder="" value="'+email+'">'+ 
                                   '</div>'+
                               '</div>'+
                               '<div class="col-lg-6 form-group">'+
                                   '<label class="form-control-label text-bold">Username (Unique) <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="text" class="form-control" name="username" id="modal-input-username" placeholder="" value="'+username+'">'+ 
                                   '</div>'+
                               '</div>';

        if(ref=='add'){
        $layout_form +=         '<div class="col-lg-6 form-group">'+
                                    '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" id="view-requirements" title="" data-content="View Password Requirements">Requirements</button>'+
                                   '<label class="form-control-label text-bold">Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="password" class="form-control" name="password" id="modal-input-password" placeholder="" value="">'+ 
                                   '</div>'+
                               '</div>'+
                               '<div class="col-lg-6 form-group">'+
                                   '<label class="form-control-label text-bold">Confirm Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="password" class="form-control" name="password_confirmation" id="modal-input-conpass" placeholder="" value="">'+ 
                                   '</div>'+
                               '</div>';
        }

        $layout_form +=
                                /*'<div class="col-xs-12 col-lg-6 form-group">'+
                                   '<label class="form-control-label text-bold">Site</label>'+
                                   '<div class="form-line">'+
                                       '<select class="form-control custom-select" id="modal-select-site" name="site"><option value="">[ Select Site ]</option></select>'+ 
                                   '</div>'+
                               '</div>'+*/
                               
                                '<div class="col-lg-12 form-group">'+
                                   '<label class="form-control-label text-bold">Remarks</label>'+
                                   '<div class="form-line">'+
                                       '<textarea class="form-control" placeholder="" name="remarks">'+remarks+'</textarea>'+
                                   '</div>'+
                               '</div>'+
                               '<div class="col-lg-12 form-group m-b-0">'+
                                    '<input type="checkbox" id="mod-active-user" class="filled-in selected-item" value="1" name="active_user" checked />'+
                                    '<label for="mod-active-user" class="m-b-0"> Active User</label>'+
                               '</div>'+
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'large',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-user-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);
                            $.ajax({
                                type:'POST',
                                url : window.location,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });
                                    
                                    if(data.stat){
                                        if(data.stat=='success'){

                                            if(ref=='edit'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }

                                            if($('#select-stat').val()=='activated'){
                                                var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="disable" data-toggle="tooltip" title="Disable User"><i class="mdi mdi-account-off"></i></button>';
                                            }else{
                                                var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="enable" data-toggle="tooltip" title="Enable User"><i class="mdi mdi-account-check"></i></button>';
                                            }
                                            
                                            var row = $record_table.row.add( {
                                                0: '<center>'+
                                                        '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                        '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                    '</center>',
                                                1: data.fullname,
                                                2: data.username,
                                                3: data.email,
                                                4: data.remarks,
                                                5: data.group,
                                                6: ($('#modal-select-site').val()) ? $('#modal-select-site option:selected').text() : '---',
                                                7: '<button type="button" class="btn btn-sm btn-default" data-action="reset" data-toggle="tooltip" title="Reset Password"><i class="mdi mdi-key"></i></button> '+add_btn+' <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                            } ).draw();
                                            
                                            $record_table.nodes().to$().find('td:nth-child(5)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                                            
                                            $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-remarks-content":data.remarks_full}); 


                                            if($('#select-stat').val()=='activated'&&!$('#mod-active-user').is(":checked")){
                                                $record_table.row('tr[data-id='+data.id+']').remove().draw();
                                            }
                                            else if($('#select-stat').val()=='deactivated'&&$('#mod-active-user').is(":checked")){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }

                                            bootbox.hideAll();

                                            //Clear Select/Bulk Action
                                            $eid = [];
                                            $sid = [];
                                            $('.table-danger').removeClass('table-danger');
                                            $('input#select-all').prop('checked', false);
                                            $('input.selected-item:checked').prop('checked', false);
                                            $('#bulk-action-option').val('');
                                            $selected_asset = [];
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();

                $('#view-requirements').click(function(){
                    $.password_requirements('Password Requirements', '<ul><li>Must be a minimum of 8 characters</li><li>Must contain at least one number</li><li>Must contain at least one uppercase character</li><li>Must contain at least one lowercase character </li></ul>');
                });

                $('#modal-user-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });

                if(stat==0){
                    $('#mod-active-user').prop('checked', true);
                }else{
                    $('#mod-active-user').prop('checked', false);
                }


                /*var group_list = '<option value="">[ Select User Group ]</option>';
                var form_data = new FormData();
                form_data.append('ref', 'all'); 
                
                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/user-groups/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        if(data&&data.length!=0){
                            $.each(data,function(i,v){
                                if(user_group_id==+v.user_group_id){
                                    group_list += '<option value="'+v.user_group_id+'" selected>'+v.group_name+'</option>';
                                }else{
                                    group_list += '<option value="'+v.user_group_id+'">'+v.group_name+'</option>';
                                }
                            });
                        }

                        $('#modal-select-group').html(group_list);
                    },
                    error :function (data) {
                        $.notification('Error','Internal Server Error. Unable to load user group options','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });*/

                if(user_group_id&&group_name){
                    $("select#modal-select-group").append('<option value="'+user_group_id+'" selected>'+group_name+'</option>');
                }
                $('#modal-select-group').select2({
                    dropdownParent: $(".modal"),
                    placeholder : 'Select User Group',
                    ajax: {
                        url: $apps_base_url+'/admin/user-groups/fetchdata',//$apps_base_url+'/asset/fetch/list'
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                ref: '_keywords',
                                keywords: params.term,
                            }
        
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.group_name,
                                        id: item.user_group_id
                                    }
                                })
                            };
                        }
                    }
                });

                if(site_id&&site_name){
                    $("select#modal-select-site").append('<option value="'+site_id+'" selected>'+site_name+'</option>');
                }
                $('#modal-select-site').select2({
                    dropdownParent: $(".modal"),
                    placeholder : 'Select Site',
                    ajax: {
                        url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                ref: '_keywords',
                                keywords: params.term,
                            }
        
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.list, function (item) {
                                    return {
                                        text: item.site_name,
                                        id: item.site_id
                                    }
                                })
                            };
                        }
                    }
                });

                $('button#add-site').click(function(){
                    $.admin_site_form('','add','Add New Site','dropdown');
                });
            });

    }


    $.password_requirements = function password_requirements(title='', content='') {//
        $layout_form = 	'<div class="row"><div class="col-12">'+content+'</div></div>';
    
        bootbox.dialog({
            closeButton: false,
            backdrop: true,
            animate: true,
            title: title,
            message: $layout_form,
            buttons: {
                close: {
                    label: 'Close',
                    className: "btn-inverse",
                }
            }
        })
    }

    $.admin_user_reset_password_form = function admin_user_reset_password_form(id='', ref='', title='Reset Password') {//
        $layout_form = 	'<form class="form-material" id="modal-user-reset-password-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                   '<label class="form-control-label text-bold">Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="password" class="form-control" name="password" id="modal-input-password" placeholder="" value="">'+ 
                                   '</div>'+
                               '</div>'+
                               '<div class="col-lg-12 form-group m-b-10">'+
                                   '<label class="form-control-label text-bold">Confirm Password <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                   '<div class="form-line">'+
                                       '<input type="password" class="form-control" name="password_confirmation" id="modal-input-conpass" placeholder="" value="">'+ 
                                   '</div>'+
                               '</div>'
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-user-reset-password-form')[0]);
                            form_data.append('ref', 'reset-password');
                            form_data.append('id', id);
                            $.ajax({
                                type:'POST',
                                url : $apps_base_url+'/admin/users/resetpass',
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $('#form-loader').toggle();

                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            bootbox.hideAll();
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-password').focus();

                $('form#modal-user-reset-password-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });
            });

    }

    $.admin_category_form = function admin_category_form(id='', ref='', title='Add New Category', append_type='table', apps='', name='', remarks='') {//
        $layout_form = 	'<form class="form-material" id="modal-category-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="category_name" id="modal-input-name" placeholder="Category" value="'+name+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-6 form-group">'+
                                    '<label class="form-control-label text-bold">Type <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control custom-select" id="modal-select-type" name="type"><option value="">- Select Type -</option></select>'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="Remarks" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            var category_form = bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-category-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);
                            form_data.append('apps', apps);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/categories';
                            }



                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-category-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-category-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(append_type=='main'||append_type=='table'){
                                                
                                                if(ref=='edit'){
                                                    $record_table.row('tr[data-id='+id+']').remove().draw();
                                                }

                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.name,
                                                    2: data.type,
                                                    3: data.remarks,
                                                    //3: data.updated_by,
                                                    4: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();
                                                
                                                //$record_table.nodes().to$().find('td:nth-child(4)').attr({"data-toggle":'tooltip',"title":data.updated_on});

                                                $record_table.rows(row).nodes().to$().attr("data-id", data.id);
                                            }
                                        
                                            //bootbox.hideAll();
                                            category_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();


                $('#modal-category-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/fetch/options',
                    dataType: "json",
                    data: {
                        'name' : 'apps_cat_type',
                        'order_by' : 'value'
                    },
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').hide();
                        if(data&&data.length!=0){
                            var list = '';
                            $.each(data,function(i,v){
                                list += '<option value="'+v.value+'">'+$.capitalize_first_letter(v.value)+'</option>';
                            });
                            $('select#modal-select-type').append(list);
                            $('select#modal-select-type').val(apps);
                        }else{
                            
                        }
                    },
                    error :function (data) {
                        $.notification('Error','Unable to load category data list','top-right','error','');
                        $('#form-loader').hide();
                    }
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });
    }

    $.admin_employee_form = function admin_employee_form(id='', ref='', title='Add New Asset Category', append_type='table', fn='', gender='', code='', email='', contact='', address='', remarks='', site_id='', department_id='',site_name='', dept_name='', stat = 0) {//
        $layout_form = 	'<form class="form-material" id="modal-employee-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Full Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="name" id="modal-input-name" value="'+fn+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Gender <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control custom-select" id="modal-select-gender" name="gender">'+
                                            '<option value="">[ Select Gender ]</option>'+
                                            '<option value="male">Male</option>'+
                                            '<option value="female">Female</option>'+
                                        '</select>'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Employee ID</label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="id" placeholder="" value="'+code+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Email</label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="email" placeholder="" value="'+email+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Contact</label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="contact" placeholder="" value="'+contact+'">'+ 
                                    '</div>'+
                                '</div>'+
                                /*'<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                   '<label class="form-control-label text-bold">Site</label>'+
                                   '<div class="form-line">'+
                                       '<select class="form-control custom-select" id="modal-select-site" name="site"><option value="">- Select Site -</option></select>'+ 
                                   '</div>'+
                               '</div>'+*/
                               '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-site" data-original-title="Add New Site"><i class="fas fa-plus"></i></button>'+
                                    '<label class="form-control-label text-bold">Site</label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control" id="modal-select-site" name="site"><option value="">- Select Site -</option></select>'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-md-6 col-lg-4 form-group">'+
                                    '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-department" data-original-title="Add New Department"><i class="fas fa-plus"></i></button>'+
                                   '<label class="form-control-label text-bold">Department</label>'+
                                   '<div class="form-line">'+
                                       '<select class="form-control custom-select" id="modal-select-department" name="department"><option value="">[ Select Department ]</option></select>'+ 
                                   '</div>'+
                               '</div>'+
                                '<div class="col-xs-12 col-lg-8 form-group">'+
                                    '<label class="form-control-label text-bold">Address</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="" name="address">'+address+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-10">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-0">'+
                                     '<input type="checkbox" id="mod-active-employee" class="filled-in selected-item" value="1" name="active_employee" checked />'+
                                     '<label for="mod-active-employee" class="m-b-0"> Active Employee</label>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
    var employee_form = bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                size: 'large',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-employee-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('eid', id);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/employees';
                            }
                        
                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-employee-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-employee-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(append_type=='main'||append_type=='table'){
                                                if(ref=='edit'){ 
                                                    $record_table.row('tr[data-id='+id+']').remove().draw();
                                                }

                                                if($('#select-stat').val()=='activated'){
                                                    var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="disable" data-toggle="tooltip" title="Disable Employee"><i class="mdi mdi-account-off"></i></button>';
                                                }else{
                                                    var add_btn = '<button type="button" class="btn btn-sm btn-default" data-action="enable" data-toggle="tooltip" title="Enable Employee"><i class="mdi mdi-account-check"></i></button>';
                                                }

                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.code,
                                                    2: data.fn,
                                                    3: data.gender,
                                                    4: data.email,
                                                    5: data.contact,
                                                    6: data.address,
                                                    7: data.site,
                                                    8: data.department,
                                                    9: data.remarks,
                                                    10: add_btn + ' <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();

                                                
                                                $record_table.nodes().to$().find('td:nth-child(7)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-address"}).addClass('pointer');
                                                
                                                $record_table.nodes().to$().find('td:nth-child(10)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                                                
                                                $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-address-content":data.address_full,"data-remarks-content":data.remarks_full});  
                                                
                                                if($('#select-stat').val()=='activated'&&!$('#mod-active-employee').is(":checked")){
                                                    $record_table.row('tr[data-id='+data.id+']').remove().draw();
                                                }
                                                else if($('#select-stat').val()=='deactivated'&&$('#mod-active-employee').is(":checked")){
                                                    $record_table.row('tr[data-id='+id+']').remove().draw();
                                                }

                                                //Clear Select/Bulk Action
                                                $eid = [];
                                                $sid = [];
                                                $('.table-danger').removeClass('table-danger');
                                                $('input#select-all').prop('checked', false);
                                                $('input.selected-item:checked').prop('checked', false);
                                                $('#bulk-action-option').val('');
                                                $selected_asset = [];
                                            }
                                                

                                            employee_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                employee_form.attr("id", "employee-form-modal");

                $('input#modal-input-name').focus();

                $('#modal-select-gender').val(gender);

                if(stat==0){
                    $('#mod-active-employee').prop('checked', true);
                }else{
                    $('#mod-active-employee').prop('checked', false);
                }

                $('#modal-employee-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });

                if(site_id&&site_name){
                    $("select#modal-select-site").append('<option value="'+site_id+'" selected>'+site_name+'</option>');
                }
                $('#modal-select-site').select2({
                    dropdownParent: $(".modal"),
                    placeholder : 'Select Site',
                    ajax: {
                        url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                ref: '_keywords',
                                keywords: params.term,
                            }
        
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.list, function (item) {
                                    return {
                                        text: item.site_name,
                                        id: item.site_id
                                    }
                                })
                            };
                        }
                    }
                });

                if(department_id&&dept_name){
                    $("select#modal-select-department").append('<option value="'+department_id+'" selected>'+dept_name+'</option>');
                }
                $('#modal-select-department').select2({
                    dropdownParent: $(".modal"),
                    placeholder : 'Select Department',
                    ajax: {
                        url: $apps_base_url+'/admin/departments/fetchdata',//$apps_base_url+'/asset/fetch/list'
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                ref: '_keywords',
                                keywords: params.term,
                            }
        
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.dept_name,
                                        id: item.department_id
                                    }
                                })
                            };
                        }
                    }
                });

                $('button#add-site').click(function(){
                    $.admin_site_form('','add','Add New Site','dropdown');
                });

                $('button#add-department').click(function(){
                    $.admin_department_form('','add','Add New Department','dropdown');
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });

    }

    $.admin_department_form = function admin_department_form(id='', ref='', title='Add New Department', append_type='table', name='') {//
        $layout_form = 	'<form class="form-material" id="modal-department-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group m-b-10">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="name" id="modal-input-name" placeholder="Department" value="'+name+'">'+ 
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
        var department_form = bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-department-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/departments';
                            }

                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-department-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-department-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });


                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(ref=='edit'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }

                                            if(append_type=='main'||append_type=='table'){
                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.name,
                                                    2: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();

                                                $record_table.rows(row).nodes().to$().attr("data-id", data.id);
                                            }

                                            department_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();

                $('#modal-department-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });

    }

    $.admin_supplier_form = function admin_supplier_form(id='', ref='', title='Add New Supplier', append_type='table', name='', address='', remarks='') {//
        $layout_form = 	'<form class="form-material" id="modal-supplier-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="name" id="modal-input-name" placeholder="" value="'+name+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Address</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="" name="address">'+address+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            var supplier_form = bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-supplier-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/suppliers';
                            }

                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-supplier-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-supplier-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });
                                    
                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(ref=='edit'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }
                                            
                                            if(append_type=='main'||append_type=='table'){
                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.name,
                                                    2: data.address,
                                                    3: data.remarks,
                                                    4: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();
                                                
                                                $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-address"}).addClass('pointer');
                                                
                                                $record_table.nodes().to$().find('td:nth-child(4)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                                                
                                                $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-address-content":data.address_full,"data-remarks-content":data.remarks_full});
                                            }

                                            //bootbox.hideAll();
                                            supplier_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();

                $('#modal-department-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });

    }

    $.admin_site_form = function admin_site_form(id='', ref='', title='Add New Site', append_type='table', name='', remarks='') {//
        $layout_form = 	'<form class="form-material" id="modal-site-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="site_name" id="modal-input-name" placeholder="Site" value="'+name+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="Remarks" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            var site_form = bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-site-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/site';
                            }

                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-site-form input[name="'+i+'"], form#modal-site-form textarea[name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-site-form input[name="'+i+'"], form#modal-site-form textarea[name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });
                                    
                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(ref=='edit'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }

                                            if(append_type=='main'||append_type=='table'){
                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.name,
                                                    2: data.remarks,
                                                    3: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();

                                                $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                                            
                                                $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-remarks-content":data.remarks_full});
                                            }

                                            //bootbox.hideAll();
                                            site_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();

                $('#modal-site-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });

    }

    $.admin_location_form = function admin_location_form(id='', site_id='', ref='', title='Add New Location', append_type='table', name='', remarks='', site_name='') {//
        $layout_form = 	'<form class="form-material" id="modal-location-form" method=x"post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="location_name" id="modal-input-name" placeholder=Location Name" value="'+name+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group">'+
                                    '<button class="btn btn-xs btn-default float-right" type="button" data-toggle="tooltip" title="" id="add-site" data-original-title="Add New Site"><i class="fas fa-plus"></i></button>'+
                                    '<label class="form-control-label text-bold">Site</label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control" id="modal-select-site" name="site"><option value="">- Select Site -</option></select>'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="Remarks" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
        var location_form =  bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');
                            var form_data = new FormData($('form#modal-location-form')[0]);
                            form_data.append('ref', ref);
                            form_data.append('id', id);

                            if(append_type=='main'||append_type=='table'){
                                var url = window.location;
                            }else{
                                var url = $apps_base_url+'/admin/location';
                            }

                            $.ajax({
                                type:'POST',
                                url : url,
                                dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $eid = [];
                                    $sid = [];

                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-location-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-location-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            if(ref=='edit'){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                            }

                                            if($('#modal-select-site').val()==$('#select_site').val()){

                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.name,
                                                    2: data.remarks,
                                                    3: '<button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update Record"><i class="ti-pencil"></i></button>',
                                                } ).draw();

                                                $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');
                                            
                                                $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-remarks-content":data.remarks_full});  
                                            }

                                            location_form.modal("hide");
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                    

                                    $('.table-danger').each(function(){
                                        $(this).removeClass('table-danger');
                                    });
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('input#modal-input-name').focus();
                var site_list = '<option value="">- Select Site -</option>';
                var form_data = new FormData();
                form_data.append('ref', 'all'); 

                $('#modal-location-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });

                if(site_id&&site_name){
                    $("select#modal-select-site").append('<option value="'+site_id+'" selected>'+site_name+'</option>');
                }
                $('#modal-select-site').select2({
                    dropdownParent: $(".modal"),
                    placeholder : 'Select Site',
                    ajax: {
                        url: $apps_base_url+'/admin/site/fetchdata',//$apps_base_url+'/asset/fetch/list'
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                ref: '_keywords',
                                keywords: params.term,
                            }
        
                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.list, function (item) {
                                    return {
                                        text: item.site_name,
                                        id: item.site_id
                                    }
                                })
                            };
                        }
                    }
                });

                $('button#add-site').click(function(){
                    $.admin_site_form('','add','Add New Site','dropdown');
                });
            }).on('hidden.bs.modal', function (e) {
                var modal_open = $('.modal').length;

                if(modal_open!=0){
                    $('body').addClass('modal-open');
                }
            });

    }

    ////////////////////////
    // Settings
    ///////////////////////
    $.settings_custom_fields_form = function settings_custom_fields_form(id='', ref='', title='Add Custom Data Field', label='',datatype='',is_required=0,categories=[]) {//
        $layout_form = 	'<form class="form-material" id="modal-custom-field-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-lg-12 form-group">'+
                                    '<label class="form-control-label text-bold">Label <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="label" id="mod-input-name" value="'+label+'">'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-lg-8 form-group">'+
                                    '<label class="form-control-label text-bold">Data Type <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<select class="form-control custom-select" name="data_type" id="mod-select-datatype" name="gender">'+
                                            '<option value="">[ Select Data Type ]</option>'+
                                            '<option value="currency">Currency</option>'+
                                            '<option value="date">Date</option>'+
                                            '<option value="email">Email</option>'+
                                            '<option value="number">Number</option>'+
                                            '<option value="text">Text</option>'+
                                        '</select>'+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 col-lg-4 form-group">'+
                                    '<label class="form-control-label text-bold">Required Field</label>'+
                                    '<div class="form-line">'+
                                        '<input type="checkbox" id="mod-is-required" class="filled-in mod-category" name="is_required" value="1" />'+
                                        '<label for="mod-is-required" class="m-b-0">Yes</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12 m-b-0 form-group">'+
                                    '<label class="form-control-label text-bold">Target Category <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line" id="mod-category-list">'+
                                        /*'<select class="form-control custom-select" id="modal-select-categories" name="categories" multiple>'+
                                            '<option value="">[ Select Categories ]</option>'+
                                        '</select>'+*/
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                //size: 'small',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var cat_selected = "<small>";
                            $('#modal-custom-field-form input[type="checkbox"]:checked').each(function(){
                                var cat_name = $(this).data('name');
                                cat_selected += cat_name + ", ";
                            });
                        
                            cat_selected = cat_selected.slice(0,-2) + "</small>";

                                var form_data = new FormData($('form#modal-custom-field-form')[0]);
                                form_data.append('ref', ref);
                                form_data.append('id', id);

                                $.ajax({
                                    type:'POST',
                                    url : window.location,
                                    dataType: "json",
                                    processData: false, // Don't process the files
                                    contentType: false,
                                    data: form_data,
                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                    success: function (data) {

                                        $eid = [];
                                        $sid = [];

                                        $('#form-loader').toggle();
                                        $('.form-group').removeClass('has-danger');
                                        $('.form-control-feedback').remove();
                                        $('#mod-category-list').removeClass('form-control-feedback');
                                        
                                        $.each(data.val_error,function(i,v){
                                            $('form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                            $('form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                        });

                                        if(data.val_cat_error){
                                            $('#mod-category-list').append('<small class="form-control-feedback text-danger">Select target category<br/></small>');
                                        }

                                        if(ref=='add'){
                                            if(data.stat){
                                                if(data.stat=='success'){
                                                    
                                                    /*var cat_selected = "<small>";
                                                    $('#modal-custom-field-form input[type="checkbox"]:checked').each(function(){
                                                        var cat_name = $(this).data('name');
                                                        cat_selected += cat_name + ", ";
                                                    });
                        
                                                    cat_selected = cat_selected.slice(0,-2) + "</small>";

                                                    $('input[name="mod-category"]:checked').each(function(){
                                                        var cat_name = $(this).data('name');
                                                    });*/

                                                    var row = $record_table.row.add( {
                                                        0: '<center>'+
                                                                '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                                '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                            '</center>',
                                                        1: data.label,
                                                        2: data.data_type,
                                                        3: (data.is_required) ? '<i class="fa fa-check-circle text-success"></i>' : '-None-',
                                                        4: cat_selected,
                                                        5: '<button type="button" class="btn btn-sm btn-inverse" data-action="edit">Edit</button>',
                                                    } ).draw();
                                                    
                                                    //$record_table.nodes().to$().find('td:nth-child(4)').attr({"data-toggle":'tooltip',"title":data.updated_on});

                                                    $record_table.rows(row).nodes().to$().attr("data-id", data.id);
                                                    bootbox.hideAll();
                                                }

                                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                            }
                                        }
                                        else if(ref=='edit'){
                                            if(data.stat){
                                                $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                            }

                                            if(data.update_stat){
                                                $record_table.row('tr[data-id='+id+']').remove().draw();
                                                var row = $record_table.row.add( {
                                                    0: '<center>'+
                                                            '<input type="checkbox" id="basic_checkbox_'+data.update_stat.id+'" class="filled-in selected-item" value="1" />'+
                                                            '<label for="basic_checkbox_'+data.update_stat.id+'" class="m-b-0"></label>'+
                                                        '</center>',
                                                    1: data.update_stat.label,
                                                    2: data.update_stat.datatype,
                                                    3: (data.update_stat.is_required) ? '<i class="fa fa-check-circle text-success"></i>' : '-None-',
                                                    4: cat_selected,
                                                    5: '<button type="button" class="btn btn-sm btn-inverse" data-action="edit">Edit</button>',
                                                } ).draw();

                                                //$record_table.nodes().to$().find('td:nth-child(4)').attr({"data-toggle":'tooltip',"title":data.update_stat.updated_on});
                                                $record_table.rows(row).nodes().to$().attr("data-id", data.update_stat.id);
                                                bootbox.hideAll();

                                                $.notification(data.update_stat.stat_title, data.update_stat.stat_msg,'top-right', data.update_stat.stat,'');
                                            }
                                        }

                                        $('.table-danger').each(function(){
                                            $(this).removeClass('table-danger');
                                        });
                                    },error :function (data) {
                                        $('#form-loader').toggle();
                                        $.notification('Error','Internal Server Error','top-right','error','');
                                    }
                                });
                            //}
                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('#mod-select-datatype').val(datatype);

                if(is_required){
                    $('#mod-is-required').prop('checked', true);
                }

                $('input#modal-input-name').focus();

                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching Data...');
                var form_data = new FormData();
                form_data.append('ref', 'all'); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/categories/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            var list = '';
                            $.each(data.list,function(i,v){
                                var checked = '';
                                if($.inArray(v.cat_id, categories) !== -1){
                                    checked = 'checked';
                                }   

                                list += '<input type="checkbox" id="cat_'+v.cat_id+'" class="filled-in" data-name="'+v.cat_name+'" name="asset_category[]" value="'+v.cat_id+'" '+checked+' />'+
                                        '<label for="cat_'+v.cat_id+'" class="m-b-0">'+v.cat_name+'</label><br/>';
                            });
                            $('#mod-category-list').html(list);
                        }else{
                            //$.notification('Error','Record not found. Invalid reference ID or category might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        //$.notification('Error','Internal Server Error','top-right','error','');
                        //$('#form-loader .loader__label').text('Fetching Data...');
                    }
                });

                

                $('#modal-category-form').submit(function(e){
                    e.preventDefault();
                    $('.btn-modal-save').click();
                });
            });

    }

    //////////////////////
    // Report
    //////////////////////

    $.custom_asset_display_column = function custom_asset_display_column(type="", col_displayed=[]) {
        $layout_form = 	'<div class="row">'+
                            '<div class="col-sm-12 col-lg-6">'+
                                '<table class="table table-bordered custom-table">'+
                                    '<tr>'+
                                        '<th colspan="2"><label for="chck-all-default">Default Column</label></th>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-tag" class="filled-in mod-select-col" value="tag" />'+
                                                '<label for="col-tag" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-tag">Tag</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-desc" class="filled-in mod-select-col" value="desc" />'+
                                                '<label for="col-desc" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-desc">Description</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-model" class="filled-in mod-select-col" value="model" />'+
                                                '<label for="col-model" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-model">Model</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-serial" class="filled-in mod-select-col" value="serial" />'+
                                                '<label for="col-serial" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-serial">Serial</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-brand" class="filled-in mod-select-col" value="brand" />'+
                                                '<label for="col-brand" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-brand">Brand</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-category" class="filled-in mod-select-col" value="category" />'+
                                                '<label for="col-category" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-category">Category</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-date-purchased" class="filled-in mod-select-col" value="date-purchased" />'+
                                                '<label for="col-date-purchased" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-date-purchased">Date Purchased</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-cost" class="filled-in mod-select-col" value="cost" />'+
                                                '<label for="col-cost" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-cost">Purchased Cost</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-vendor" class="filled-in mod-select-col" value="vendor" />'+
                                                '<label for="col-vendor" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-vendor">Vendor</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-site" class="filled-in mod-select-col" value="site" />'+
                                                '<label for="col-site" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-site">Site</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-location" class="filled-in mod-select-col" value="location" />'+
                                                '<label for="col-location" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-location">Location</label></td>'+
                                    '</tr>';

        if(type=='asset-report'||type=='Leased'||type=='Available'){
            var assignto_caption = (type=='Leased') ? 'Leased By' : 'Assigned To';

            $layout_form            += 	'<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-assigned-to" class="filled-in mod-select-col" value="assigned-to" />'+
                                                    '<label for="col-assigned-to" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-assigned-to">'+assignto_caption+'</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-updated-by" class="filled-in mod-select-col" value="updated-by" />'+
                                                    '<label for="col-updated-by" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-updated-by">Updated By</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-updated-date" class="filled-in mod-select-col" value="updated-date" />'+
                                                    '<label for="col-updated-date" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-updated-date">Updated Date</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-remarks" class="filled-in mod-select-col" value="remarks" />'+
                                                '<label for="col-remarks" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-remarks">Asset Remarks</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-status" class="filled-in mod-select-col" value="status" />'+
                                                '<label for="col-status" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-status">Asset Status</label></td>'+
                                    '</tr>';
        }else if(type=='inout-report'){
        $layout_form            += 	'<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-date" class="filled-in mod-select-col" value="date" />'+
                                                '<label for="col-date" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-date">In/Out Date</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-requestor" class="filled-in mod-select-col" value="requestor" />'+
                                                '<label for="col-requestor" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-requestor">Requested By</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-recorded-by" class="filled-in mod-select-col" value="recorded-by" />'+
                                                '<label for="col-recorded-by" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-recorded-by">Recorded By</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-inout-remarks" class="filled-in mod-select-col" value="inout-remarks" />'+
                                                '<label for="col-inout-remarks" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-inout-remarks">Remarks</label></td>'+
                                    '</tr>'; 
        }else if(type=='lease-report'){
        $layout_form            += 	'<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-lease-start" class="filled-in mod-select-col" value="lease-start" />'+
                                                '<label for="col-lease-start" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-lease-start">Leased Start</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-lease-end" class="filled-in mod-select-col" value="lease-end" />'+
                                                '<label for="col-lease-end" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-lease-end">Leased Expire</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-leased-by" class="filled-in mod-select-col" value="leased-by" />'+
                                                '<label for="col-leased-by" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-leased-by">Leased By</label></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                        '<td width="40">'+
                                            '<center>'+
                                                '<input type="checkbox" id="col-lease-remarks" class="filled-in mod-select-col" value="lease-remarks" />'+
                                                '<label for="col-lease-remarks" class="m-b-0"></label>'+
                                            '</center>'+
                                        '</td>'+
                                        '<td ><label for="col-lease-remarks">Lease Remarks</label></td>'+
                                    '</tr>'; 
        }else if(type=='Donated'||type=='Damage'||type=='Disposed'||type=='Lost'||type=='Sold'){
            $layout_form            += 	'<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-assigned-to" class="filled-in mod-select-col" value="assigned-to" />'+
                                                    '<label for="col-assigned-to" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-assigned-to">Assigned To</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-updated-by" class="filled-in mod-select-col" value="updated-by" />'+
                                                    '<label for="col-updated-by" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-updated-by">Updated By</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-updated-date" class="filled-in mod-select-col" value="updated-date" />'+
                                                    '<label for="col-updated-date" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-updated-date">Updated Date</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-status-date" class="filled-in mod-select-col" value="status-date" />'+
                                                    '<label for="col-status-date" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-status-date">'+type+' Date</label></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-status-note" class="filled-in mod-select-col" value="status-note" />'+
                                                    '<label for="col-status-note" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-status-note">'+type+' Note</label></td>'+
                                        '</tr>'; 

            if(type!='Damage'&&type!='Lost'){
                $layout_form            += 	'<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-status-to" class="filled-in mod-select-col" value="status-to" />'+
                                                    '<label for="col-status-to" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-status-to">'+type+' To</label></td>'+
                                        '</tr>';
            }

            if(type=='Sold'){
                $layout_form            += 	'<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-status-amount" class="filled-in mod-select-col" value="status-amount" />'+
                                                    '<label for="col-status-amount" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-status-amount">'+type+' Amount</label></td>'+
                                        '</tr>';
            }
        }

        $layout_form        += 	'</table>'+
                            '</div>'+
                            '<div class="col-sm-12 col-lg-6">'+
                                '<table class="table table-bordered custom-table" id="mod-table-custom-column">'+
                                    '<tr id="row-header">'+
                                        '<th colspan="2"><label for="chck-all-custom">Custom Field</label></th>'+
                                    '</tr>'+
                                    '<tr id="row-no-data">'+
                                        '<td colspan="2">No Data</td>'+
                                    '</tr>'+
                                '</table>'+
                            '</div>'+
                        '</div>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: 'Custom Column Display',
                message: $layout_form,
                size: 'large',
                buttons: {
                    done: {
                        label: 'Apply',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            var selected = $('input.mod-select-col:checked').length;
                            var td_span = $('#record-table').find('td.dataTables_empty').attr('colspan');
                            var empty_row = $('#record-table').find('td.dataTables_empty').length;

                            if(empty_row){
                                $('#record-table').find('td.dataTables_empty').prop('colspan', selected);
                            }
                            
                            $('input.mod-select-col').each(function(){
                                var c = this.checked;
                                var col = $(this).val();
                                var th = $('#record-table').find("th[data-col='"+col+"']").length;
                                var td = $('#record-table').find("td[data-col='"+col+"']").length;
                                var th_visibility = $('#record-table').find("th[data-col='"+col+"']").is(':hidden');
                                var td_visibility = $('#record-table').find("td[data-col='"+col+"']").is(':hidden');
                                

                                if(!c){//Not Check
                                    if(th){
                                        $('table').find("th[data-col='"+col+"']").hide();
                                        $('table').find("td[data-col='"+col+"']").hide();
                                    }
                                }else{
                                    if(th){
                                        $('table').find("th[data-col='"+col+"']").show();
                                        $('table').find("td[data-col='"+col+"']").show();
                                    }
                                }

                                

                                
                            });

                            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                            //return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('.modal').find("input[type=checkbox]").prop('checked', false);
                $.each(col_displayed,function(i,v){
                    var is_checked = $('.modal').find("input[id='col-"+v+"']:checked").length;
                    
                    $('.modal').find("input[id='col-"+v+"']").prop('checked', true);
                });

                var form_data = new FormData();
                form_data.append('ref', 'all'); 
                
                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/reports/asset/get-custom-field',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();

                        var row = '';
                        if(data&&data.length!=0){


                            $.each(data,function(i,v){
                                row += '<tr>'+
                                            '<td width="40">'+
                                                '<center>'+
                                                    '<input type="checkbox" id="col-'+v.field_id+'" class="filled-in mod-select-col" value="'+v.field_id+'" />'+
                                                    '<label for="col-'+v.field_id+'" class="m-b-0"></label>'+
                                                '</center>'+
                                            '</td>'+
                                            '<td ><label for="col-'+v.field_id+'">'+v.field_label+'</label></td>'+
                                        '</tr>';
                            });

                            if(row!=''){
                                $('#mod-table-custom-column').append(row);
                                $('#mod-table-custom-column #row-no-data').remove();
                            }

                            $.each(col_displayed,function(i,v){
                                var is_checked = $('.modal').find("input[id='col-"+v+"']:checked").length;
                                
                                $('.modal').find("input[id='col-"+v+"']").prop('checked', true);
                            });
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error.','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            });

    }

    //////////////////////
    // Media File
    //////////////////////

    $.madia_files = function madia_files(ref='', title='Media Files') {//
        $layout_form = 	'<form class="form-material" id="modal-custom-field-form" method="post" style="margin-top:-10px;">'+
                            '<ul class="nav nav-tabs profile-tab" role="tablist">'+
                                '<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#avatar" role="tab"  style="padding: 5px 10px !important;"><span class="hidden-sm-up"><i class="ti-user" title="Avatars Image"></i></span> <span class="hidden-xs-down">Avatar Stock Image</span></a> </li>'+
                                //'<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#upload" role="tab"><span class="hidden-sm-up"><i class="ti-user" title="Upload File"></i></span> <span class="hidden-xs-down">Upload</span></a> </li>'+
                            '</ul>'+
                            '<div class="tab-content">'+
                                '<div class="tab-pane active p-t-20" id="avatar" role="tabpanel">'+
                                    '<div class="row" id="avatar-list">'+
                                    '</div>'+
                                '</div>'+
                                /*'<div class="tab-pane" id="upload" role="tabpanel">'+
                                    'Upload here'+
                                '</div>'+*/
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: title,
                message: $layout_form,
                size: 'large',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success btn-modal-save",
                        callback: function () {
                            if(ref='change-avatar'){
                                if(selected==''){
                                    $.notification('','Select an avatar','top-center','error','',1);
                                }else{
                                    var form_data = new FormData();
                                    form_data.append('type', 'change_avatar');
                                    form_data.append('avatar', selected); 

                                    $('#form-loader').toggle();
                                    $('#form-loader .loader__label').text('Changing profile...');

                                    $.ajax({
                                        type:'POST',
                                        url : window.location,
                                        data:  form_data,
                                        dataType: "json",
                                        processData: false, // Don't process the files
                                        contentType: false, // Set content type to false as jQuery will tell the server its a query string
                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                        success: function (data) {
                                            $('#form-loader').toggle();
                                            if(data.stat){
                                                if(data.stat=='success'){
                                                    $('img#current-avatar,img#profile-img-box,img#profile-img').attr('src', $apps_base_url + '/'+selected);
                                                    bootbox.hideAll();
                                                }
                            
                                               $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                            }
                                        },
                                        error :function (data) {
                                            $('#form-loader').toggle();
                                            $.notification('Error','Internal Server Error','top-right','error','');
                                            $('#form-loader .loader__label').text('Fetching Data...');
                                        }
                                    });
                                }
                            }
                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                var selected_avatar = '';
                var avatars = '';
                for(i=1;i<=50;i++){
                    avatars += '<div class="col-6 col-sm-3 col-md-3 col-lg-2">'+
                                    '<div class="card b-all">'+
                                        '<div class="card-body p-10">'+
                                            '<a href="#" id="avatar-item" data-val="avatars/avatar-'+i+'.png">'+
                                            '<img src="'+$apps_base_url+'/img/avatars/avatar-'+i+'.png" class="img-responsive" />'+
                                            '</a>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
                }

                $('#avatar-list').html(avatars);

                $('#avatar-list').on("click", "#avatar-item", function(e) {
                    e.preventDefault();
                    var avatar = $(this).data('val');
                    $('#avatar-list').find('#selected').remove();
                    $(this).parents('.card').append('<i id="selected" class="mdi mdi-check-circle text-success m-l-5" style="font-size: 1.5em; position: absolute;"></i>');

                    selected = avatar;
                });
            });

    }

    ///////////////
    // Add-On Module
    //////////////////
    $.module_insurance_form = function module_insurance_form(id, ref='add',modal_title='New Asset Maintenance',append_type='table',name='',provider='',start='',expire='',cost='',remarks='') {//
        $layout_form = 	'<form class="form-material" id="modal-insurance-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-sm-12 form-group">'+
                                    '<label class="form-control-label text-bold">Name <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="name" placeholder="Insurance Name" value="'+name+'">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 form-group">'+
                                    '<label class="form-control-label text-bold">Provider <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="provider" placeholder="Insurance Provider" value="'+provider+'">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-sm-6 col-lg-6 form-group">'+
                                    '<label class="form-control-label text-bold">Start Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="start_date" id="modal-date-start" placeholder="Insurance Expiry Date" value="'+start+'">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-sm-6 col-lg-6 form-group">'+
                                    '<label class="form-control-label text-bold">Expiry Date <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="expiry_date" id="modal-date-expire" placeholder="Insurance Expiry Date" value="'+expire+'">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-4 col-lg-6 form-group">'+
                                    '<label class="form-control-label text-bold">Cost</label>'+
                                    '<div class="form-line">'+
                                        '<input type="text" class="form-control" name="cost" id="modal-input-cost" placeholder="Insurance Cost" value="'+cost+'">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 form-group m-b-0">'+
                                    '<label class="form-control-label text-bold">Remarks</label>'+
                                    '<div class="form-line">'+
                                        '<textarea class="form-control" placeholder="Insurance Remarks" name="remarks">'+remarks+'</textarea>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: modal_title,
                message: $layout_form,
                //size: 'large',
                buttons: {
                    done: {
                        label: 'Save',
                        className: "btn-success",
                        callback: function () {
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-insurance-form')[0]);

                            form_data.append('ref',ref);
                            form_data.append('id',id);

                            $.ajax({
                                type:'POST',
                                url : $apps_base_url + '/insurances',
                                //dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $(".tooltip").tooltip("hide");
                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-insurance-form [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-insurance-form [name="'+i+'"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.stat){
                                        if(data.stat=='success'){
                                            
                                            if(ref=='edit'&&append_type=='table'){
                                                var row = $('tr[data-id='+id+']');
                                                $record_table.row(row).remove().draw();
                                            }else{
                                                $('#txt_name').val(data.name);
                                                $('#txt_provider').val(data.provider);
                                                $('#txt_start').val(data.start);
                                                $('#txt_expire').val(data.expire);
                                                $('#txt_cost').val(data.cost);
                                                $('#txt_remarks').val(data.remarks_full);
                                            }
                                            
                                            var row = $record_table.row.add( {
                                                0: '<center>'+
                                                        '<input type="checkbox" id="basic_checkbox_'+data.id+'" class="filled-in selected-item" value="1" />'+
                                                        '<label for="basic_checkbox_'+data.id+'" class="m-b-0"></label>'+
                                                    '</center>',
                                                1: '<a href="'+$apps_base_url + '/insurances/'+data.id+'">'+data.name+'</a>',
                                                2: data.provider,
                                                3: data.start,
                                                4: data.expire,
                                                5: data.cost,
                                                6: data.remaining,
                                                7: data.asset,
                                                8: data.remarks,
                                                9: '<a href="'+$apps_base_url + '/insurances/'+data.id+'" type="button" class="btn btn-sm btn-default" data-toggle="tooltip" title="View"><i class="fa fa-search"></i></a> <button type="button" class="btn btn-sm btn-warning" data-action="edit" data-toggle="tooltip" title="Update"><i class="ti-pencil"></i></button> <button type="button" class="btn btn-sm btn-danger" data-action="delete" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></button>',
                                            } ).draw();

                                            $record_table.nodes().to$().find('td:nth-child(9)').attr({"data-toggle":'tooltip',"title":"View full content", "data-action":"view-remarks"}).addClass('pointer');

                                            $record_table.rows(row).nodes().to$().attr({"data-id":data.id,"data-remarks-content":data.remarks_full});
                                            
                                            bootbox.hideAll();

                                            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('[data-toggle="tooltip"], .tooltip').tooltip('hide');

                $('#modal-input-cost').autoNumeric('init', {'set': "0.00"});

                $('#modal-date-start').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    title: 'Start Date',
                    //endDate:'0d'
                }).on('changeDate', function(e) {
                    var date2 = $(this).datepicker('getDate');
                    date2.setDate(date2.getDate() + 1);
                    $('#modal-date-expire').datepicker('destroy');
                    $('#modal-date-expire').val('');
                    $('#modal-date-expire').datepicker({
                        startDate: date2,
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        title: 'Complation Date',
                    });
                });
            
                $('#modal-date-expire').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    title: 'Complation Date',
                    startDate:'0d'
                });
            });

    }

    $.link_insurance_modal = function link_insurance_modal(ref='add',modal_title='Link an Asset',append_type='table',insurance_id='', insurance_name='',asset_id='',asset_name='') {//
        $layout_form = 	'<form class="form-material" id="modal-insurance-link-form" method="post">'+
                            '<div class="row">'+
                                '<div class="col-sm-12 form-group">'+
                                    '<label class="form-control-label text-bold">Insurance <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                    '<select class="form-control select2" id="modal-input-insurance" name="insurance" placeholder="Select an Insurance" style="width:100%"></select>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-sm-12 form-group m-b-0" id="modal-asset-container">'+
                                    '<label class="form-control-label text-bold">Asset <i class="mdi mdi-check-circle form-required-helper text-danger"></i></label>'+
                                    '<div class="form-line">'+
                                        ''+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
        
            bootbox.dialog({
                closeButton: false,
                backdrop: true,
                animate: true,
                title: modal_title,
                message: $layout_form,
                size: 'large',
                buttons: {
                    done: {
                        label: 'Link',
                        className: "btn-success",
                        callback: function () {
                            $('[data-toggle="tooltip"], .tooltip').tooltip('hide');
                            $('#form-loader').toggle();
                            $('#form-loader .loader__label').text('Saving...');

                            var form_data = new FormData($('form#modal-insurance-link-form')[0]);

                            form_data.append('ref',ref);
                            //form_data.append('id',id);

                            $.ajax({
                                type:'POST',
                                url : $apps_base_url + '/insurances/linkasset',
                                //dataType: "json",
                                processData: false, // Don't process the files
                                contentType: false,
                                data: form_data,
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                success: function (data) {
                                    $('#form-loader').toggle();
                                    $('.form-group').removeClass('has-danger');
                                    $('.form-control-feedback').remove();
                                    $.each(data.val_error,function(i,v){
                                       $('form#modal-insurance-link-form [name="'+i+'"],form#modal-insurance-link-form [name="'+i+'[]"]').parents('.form-group').addClass('has-danger');
                                       $('form#modal-insurance-link-form [name="'+i+'"],form#modal-insurance-link-form [name="'+i+'[]"]').parents('.form-group').append('<small class="form-control-feedback">'+v[0]+'</small>');
                                    });

                                    if(data.val_cat_error){
                                        $('#modal-input-asset').append('<small class="form-control-feedback text-danger">Select target category<br/></small>');
                                    }

                                    if(data.stat){
                                        if(data.stat=='success'){


                                            if(append_type=='insurance_view_page'){
                                                $associated_asset_table.clear().draw();
                                                $.each(data.associated_assets, function(i, v){
                                                    var row = $associated_asset_table.row.add( {
                                                        0: v.asset_tag,
                                                        1: v.asset_model,
                                                        2: (v.asset_serial) ? v.asset_serial : '---',
                                                        3: v.asset_desc,
                                                        4: (v.site_name) ? v.site_name : '---',
                                                        5: (v.location_name) ? v.location_name : '---',
                                                        6: '<button type="button" class="btn btn-sm btn-danger" data-action="detached" data-toggle="tooltip" title="Detached"><i class="fa fa-times"></i></button>',
                                                    } ).draw();

                                                    $associated_asset_table.rows(row).nodes().to$().attr("data-id", v.id);
                                                });

                                            }else if(append_type=='asset_view_page'){
                                                //var currency_code = data.options.currency.currency_code;
                                                var currency_code = data.currency;

                                                $insurance_table.clear().draw();
                                                $.each(data.associated_assets, function(i, v){
                                                    
                                                    var today = moment(); //YYYY-MM-DD
                                                    var expire = moment(v.insurance_expire);
                                                    var overdue = expire.diff(today, 'days');
                                                    
                                                    if(overdue<=1){
                                                        var overdue_label = '<span class="label label-danger">Expired</span>';
                                                    }else{
                                                        var overdue_label = overdue + ' Days';
                                                    }

                                                    var row = $insurance_table.row.add( {
                                                        0: v.insurance_name,
                                                        1: v.insurance_provider,
                                                        2: moment(v.insurance_start).format("DD-MMM-YYYY"),
                                                        3: moment(v.insurance_expire).format("DD-MMM-YYYY"),
                                                        4: overdue_label,
                                                        5: (v.insurance_cost) ? number_format(v.insurance_cost) + ' ' + currency_code  : '---',
                                                        6: v.user_fullname,
                                                        7: '<button type="button" class="btn btn-sm btn-danger" data-action="detached" data-toggle="tooltip" title="Detached"><i class="fa fa-times"></i></button>',
                                                    } ).draw();

                                                    $insurance_table.rows(row).nodes().to$().attr("data-id", v.id);
                                                });   
                                            }else{
                                                var row = $('tr[data-id='+data.insurance_id+']');
                                                $record_table.row(row).nodes().to$().find('td:nth-child(8)').text(data.total_asset);
                                            }

                                            bootbox.hideAll();

                                            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
                                        }

                                        $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                                    }
                                },error :function (data) {
                                    $('#form-loader').toggle();
                                    $.notification('Error','Internal Server Error','top-right','error','');
                                }
                            });

                            return false;
                        }
                    },
                    close: {
                        label: 'Close',
                        className: "btn-inverse",
                    }
                }
            }).on("shown.bs.modal", function(e) {
                $('[data-toggle="tooltip"], .tooltip').tooltip('hide');
                if(insurance_id&&insurance_name){
                    $("#modal-input-insurance").append('<option value="'+insurance_id+'" selected>'+insurance_name+'</option>');
                }

                $("#modal-input-insurance").select2({
                    //theme: "classic",
                    dropdownParent: $(".bootbox"),
                    placeholder: 'Select an Insurance',
                    ajax: {
                        url: $apps_base_url+'/insurances/fetchdata',
                        dataType: 'json',
                        type: "POST",
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: function (params) {
                            var query = {
                                keywords: params.term,
                                ref: 'all'
                            }
                            return query;
                          },
                        processResults: function (data) {
                            return {
                                results: $.map(data.record, function (item) {
                                    return {
                                        text: item.insurance_name,
                                        id: item.insurance_id
                                    }
                                })
                            };
                        }
                      },
                });

                function formatState (data) {
                    if (!data.id) { return data.text; }
                    var img = (data.img)? '<img src="'+$apps_base_url+'/uploaded/file/'+data.img+'" width="30" class="img-thumbnail" style="padding:.10rem;" />' : '';
                    var $result= $(
                      '<span>'+img+' ' + data.text + '</span>'
                    );
                    return $result;
                  };
                


                  $('#modal-asset-container .form-line').html('');
                if(ref=='single-asset'){
                    $('#modal-asset-container').hide();
                    $('#modal-asset-container .form-line').append('<input type="hidden" name="asset" value="'+asset_id+'" />');
                }
                else{
                    $('#modal-asset-container').show();
                    $('#modal-asset-container .form-line').append('<select class="form-control select2" id="modal-input-asset" name="asset[]" placeholder="Select an Asset" style="width:100%" multiple></select>');

                    if(asset_id.length){
                        var pre_option = '';
                        $.each(asset_id, function(i, v){
                            pre_option+= '<option value="'+v.id+'" selected>'+v.tag+ ' - ' +v.name+'</option>';
                            
                            
                        });
                        
                        $("#modal-input-asset").html(pre_option);
                    }

                    $("#modal-input-asset").select2({
                        theme: "classic",
                        multiple : true,
                        templateResult: formatState,
                        dropdownParent: $(".bootbox"),
                        ajax: {
                            url: $apps_base_url+'/asset/fetch/all',
                            dataType: 'json',
                            type: "POST",
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: function (params) {
                                var query = {
                                    keywords: params.term,
                                    type: 'public'
                                }
                
                                return query;
                            },
                            processResults: function (data) {
                                return {
                                    results: $.map(data.list, function (item) {
                                        var serial = (item.asset_serial) ? ' - ' + item.asset_serial : '';
                                        return {
                                            text: item.asset_tag+' - ' + item.asset_model,
                                            img: item.image_name,
                                            id: item.asset_id
                                        }
                                    })
                                };
                            }
                        }
                    });
                }
            });

    }
});


function number_format(number, decimals, dec_point, thousands_point) {

    if (number == null || !isFinite(number)) {
        throw new TypeError("number is not valid");
    }

    if (!decimals) {
        var len = number.toString().split('.').length;
        decimals = len > 1 ? len : 0;
    }

    if (!dec_point) {
        dec_point = '.';
    }

    if (!thousands_point) {
        thousands_point = ',';
    }

    number = parseFloat(number).toFixed(decimals);

    number = number.replace(".", dec_point);

    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);

    return number;
}