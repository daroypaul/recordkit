/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){    
    $selected_asset = [];
    $eid = [];
    $sid = [];
    $stat_selected = '';

    //$("#record-table tbody tr").popover({placement:"top"});
    $(".select2").select2();

    $('#date-start').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        //title: 'Lease Date Start',
        //endDate:'0d'
    }).on('changeDate', function(e) {
        var date2 = $(this).datepicker('getDate');
        date2.setDate(date2.getDate());
        $('#date-end').datepicker('destroy');
        $('#date-end').val('');
        $('#date-end').datepicker({
            startDate: date2,
            //endDate:'0d',
            format: 'dd-M-yyyy',
            autoclose: true,
            //title: 'Complation Date',
        }).on('changeDate', function(e) {
            var start = $('#date-start').val();
            var end = $(this).val();
            var col_displayed = [];
            //display_report(start, end);
        });
    });

    $('#date-end').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        //title: 'Lease Date Expire',
        //endDate:'0d'
    }).on('changeDate', function(e) {
        var start = $('#date-start').val();
        var end = $(this).val();
        var col_displayed = [];
    });

    $status_updated = 0;
        $record_table = $('#record-table').DataTable({
            "scrollX": true,
            "order": [[ 0, "asc" ]],
            "bFilter": false,
            "aoColumnDefs": [
                //{ 'bSortable': false, 'aTargets': [ 0,10 ] }
            ],
            columnDefs: [
                { type: 'date-dd-mmm-yyyy', targets: 0 }
            ],
            //dom: 'frtip',
            dom: 'Bfrtip',
            buttons: [
                /*'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'*/
                'pageLength',
                {
                    extend: 'excelHtml5',
                    title: 'Data export',
                },
                /*{
                    extend: 'pdfHtml5',
                    title: 'Data export',
                },*/
                'print',
            ],
        }).on('click', 'button[data-action]', function(e) {
            e.preventDefault();
            var id = $(this).parents('tr').data('id');
            var action = $(this).data('action');

            if(action=='edit'&&id){
                $('#form-loader').toggle();
                $('#form-loader .loader__label').text('Fetching data...');
                var form_data = new FormData();
                form_data.append('id', id); 

                $.ajax({
                    type:'POST',
                    url :  $apps_base_url+'/admin/employees/fetchdata',
                    dataType: "json",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success: function (data) {
                        $('#form-loader').toggle();
                        if(data.list&&data.list.length!=0){
                            $.admin_employee_form(id, 'edit', 'Update Employee', 'table', data.list.emp_fullname, data.list.emp_gender, data.list.emp_code, data.list.emp_email, data.list.emp_contact, data.list.emp_address, data.list.emp_remarks, data.list.site_id, data.list.department_id);
                        }else{
                            $.notification('Error','Record not found. Invalid reference ID or employee might be deleted','top-right','error','');
                        }
                    },
                    error :function (data) {
                        $('#form-loader').toggle();
                        $.notification('Error','Internal Server Error','top-right','error','');
                        $('#form-loader .loader__label').text('Fetching Data...');
                    }
                });
            }
        });

        $('#btn-generate').click(function(){
            $('#form-loader').toggle();
            $('#form-loader .loader__label').text('Generating reports...');
            var start = $('#date-start').val();
            var end = $('#date-end').val();
            var col_displayed = [];

            display_report(start, end);

        });
});


function display_report(date_start='', date_end=''){

    var form_data = new FormData();

    if(date_start&&date_end){
        form_data.append('date_start', date_start);
        form_data.append('date_end', date_end);
    }
    
    form_data.append('action_by', $('#select-user').val());
    
    $.ajax({
        type:'POST',
        url : $apps_base_url+'/reports/activity-logs',
        dataType: "json",
        data:  form_data,
        processData: false,
        contentType: false,
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function (data) {
            $record_table.clear().draw();
            $('#form-loader').toggle();
            if(data.logs.length!=0){
                var items = {};

                $.each(data.logs, function(i, item) {
                    var c1 = 15;
                    var c2 = 16;

                   items = 
                   {
                        0: item.date,
                        1: item.user,
                        2: item.desc,
                    };

                    var row = $record_table.row.add(items).draw();
                    
                    $record_table.nodes().to$().find('td:nth-child(1)').attr({"data-col":'date'});
                    $record_table.nodes().to$().find('td:nth-child(2)').attr({"data-col":'user'});
                    $record_table.nodes().to$().find('td:nth-child(3)').attr({"data-col":'action'});
                });

                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            }
        },
        error :function (data) {
            $record_table.clear().draw();
            $('#form-loader').toggle();
            $.notification('Error','Internal Server Error','top-right','error','');
            $('#form-loader .loader__label').text('Fetching Data...');
        }
    });
}