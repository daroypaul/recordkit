/**
 * Weventory - Asset Management System
 *
 * @package  RecordKits Softwares <www.recuda.com>
 * @author   Paul Daroy <daroypaul@live.com>
 */
$(function(){ 

    $('#btn-change-avatar').click(function() {
        $.madia_files('change-avatar','Select an Avatar');
    });


    $('#form-profile').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Updating profile...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);
        form_data.append('type', 'profile'); 

        $.ajax({
            type:'POST',
            url : window.location,
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('#form-profile [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('#form-profile [name="'+i+'"]').parent().append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                    if(data.stat=='success'){
                        $('#disp-email').text($('#form-profile [name="email"]').val());
                    }

                   $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });

    $('#form-security').submit(function(e){
        e.preventDefault();
        $('#form-loader').toggle();
        $('#form-loader .loader__label').text('Resetting password...');
        $('#form-notification').html('');

        var form_data = new FormData($(this)[0]);
        form_data.append('type', 'security'); 

        $.ajax({
            type:'POST',
            url : window.location,
            data:  form_data,
            dataType: "json",
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
                $('#form-loader').toggle();

                $('.form-group').removeClass('has-danger');
                $('.form-control-feedback').remove();
                $.each(data.val_error,function(i,v){
                   $('#form-security [name="'+i+'"]').parents('.form-group').addClass('has-danger');
                   $('#form-security [name="'+i+'"]').parent().append('<small class="form-control-feedback">'+v[0]+'</small>');
                });

                if(data.stat){
                    if(data.stat=='success'){
                        $('#form-security')[0].reset();
                    }

                   $.notification(data.stat_title,data.stat_msg,'top-right',data.stat,'');
                }
            },
            error :function (data) {
                $('#form-loader').toggle();
                $.notification('Error','Internal Server Error','top-right','error','');
                $('#form-loader .loader__label').text('Fetching Data...');
            }
        });
    });
});